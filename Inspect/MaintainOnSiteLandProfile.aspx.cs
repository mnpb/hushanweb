﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inspect_MaintainOnSiteLandProfile : SuperPage
{
    #region Properties

    //string FilterStationName
    //{
    //    get
    //    {
    //        return SearchStationUserControl.FilterStationName;
    //    }
    //}

    List<OnSiteLandProfile> StationBaseInfos
    {
        get
        {
            return InspectController.Instance.GetAllOnSiteLandProfiles();
            //var allInfos = WaterController.Instance.GetAllStations();
            //if (string.IsNullOrEmpty(FilterStationName))
            //{
            //    return allInfos;
            //}
            //else
            //{
            //    return (from info in allInfos
            //            where info.StationName.Contains(FilterStationName)
            //            select info).ToList();
            //}
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowStationBaseInfos(0);
        }
    }
    private void ShowStationBaseInfos(int pageIndex)
    {
        try
        {
            StationBaseGridView.DataSource = StationBaseInfos;
            StationBaseGridView.PageIndex = pageIndex;
            StationBaseGridView.DataBind();
        }
        catch
        {
        }
    }

    ///// <summary>
    ///// 顯示「新增基本資料介面」
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void AddButton_Click(object sender, EventArgs e)
    //{
    //    MaintainOnSiteLandProfileUserControl.ProfileID = 0;
    //    Popup.Title = "水位站基本資料";
    //    Popup.Show();
    //}

    #region GridView Events
    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowStationBaseInfos(e.NewPageIndex);
    }

    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int profileID = Convert.ToInt32(StationBaseGridView.DataKeys[e.NewEditIndex].Values["ProfileID"].ToString());
        MaintainOnSiteLandProfileUserControl.ProfileID = profileID;
        Popup.Title = "地籍現勘資料";
        Popup.Show();
    }

    //protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    try
    //    {
    //        string stationID = StationBaseGridView.DataKeys[e.RowIndex].Values["StationID"].ToString();
    //        if (!WaterController.Instance.DeleteStation(stationID))
    //        {
    //            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
    //        }
    //    }
    //    finally
    //    {
    //        StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
    //        ShowStationBaseInfos(0);
    //    }
    //}

    #endregion

    //protected void SearchStationUserControl_FilterStation(object sender, EventArgs e)
    //{
    //    ShowStationBaseInfos(0);
    //}
    //protected void SearchStationUserControl_ClearFilter(object sender, EventArgs e)
    //{
    //    ShowStationBaseInfos(0);
    //}
    protected void MaintainOnSiteLandProfileUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowStationBaseInfos(0);
    }

    protected string ShowOnClientClick(object profileID)
    {
        return "return openPhotos('" + profileID.ToString() + "');return false;";
    }
}