﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowPhotos.aspx.cs"
    Inherits="DisasterManagement_ShowPhotos" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%--<%@ Register TagPrefix="uc" TagName="ucDateTime" Src="~/_uc/ucDisDateTime.ascx" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <%--    <link id="Link1" rel="Stylesheet" runat="server" href="~/App_Themes/Css/template.css" type="text/css" />--%>
    <style type="text/css">
        #rfvDateTime {
            color: #ff0000;
            animation: twinkling 1.5s infinite ease-in-out;
            -o-animation: twinkling 1.5s infinite ease-in-out;
            -moz-animation: twinkling 1.5s infinite ease-in-out;
            -webkit-animation: twinkling 1.5s infinite ease-in-out;
        }

        @-webkit-keyframes twinkling {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @-o-keyframes twinkling {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @-moz-keyframes twinkling {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @keyframes twinkling {
            from {
                color: #ff8d8d;
                opacity: 0;
            }

            to {
                color: red;
                opacity: 1;
            }
        }

        .auto-style1 {
            font-size: 14px;
            font-family: "Verdana";
            border-right: #c6c6c6 1px solid;
            padding-right: 10px;
            padding-left: 10px;
            padding-bottom: 3px;
            padding-top: 5px;
            border-bottom: #c6c6c6 1px solid;
            background-color: white;
            text-align: center;
            color: dimgray;
            font-weight: bold;
            width: 500px;
        }

        .auto-style2 {
            font-size: 12px;
            font-family: "Verdana";
            border-right: #c6c6c6 1px solid;
            padding-right: 10px;
            padding-left: 10px;
            padding-bottom: 3px;
            padding-top: 5px;
            border-bottom: #c6c6c6 1px solid;
            background-color: white;
            text-align: left;
            color: dimgray;
            width: 500px;
        }
    </style>
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <title>地籍現勘-圖片</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" />
        <div>
            <table align="center" class="DataTable" cellpadding="0" cellspacing="0">
                <%--                <tr>
                    <td class="auto-style1" align="center">
                        <asp:Label ID="lblQLakeName" runat="server" Font-Bold="True" Font-Size="12pt"></asp:Label>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <asp:GridView ID="gvImages" runat="server" AutoGenerateColumns="False" DataKeyNames="ProfileID,ImageID"
                            CssClass="fancytable" Width="100%" OnRowDeleting="gvImages_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="照片" SortExpression="ImagePath">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlnkImg" runat="server" ImageUrl='<%# showFilePath(Eval("ImagePath")) %>'
                                            NavigateUrl='<%# showFilePath2(Eval("ImagePath")) %>'>圖片</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="說明" SortExpression="ImageDescription">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("ImageDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="照片時間" SortExpression="ImageTime">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("ImageTime", "{0:g}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" runat="server" Text="刪除" OnClick="btnDelete_Click" CommandArgument='<%#Eval("PK_DisasterImageGUID")%>' />
                                    <ajax:ConfirmButtonExtender ID="cbeDelete" runat="server" TargetControlID="btnDelete" ConfirmText="確定要刪除嗎？" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="刪除" CausesValidation="false" OnClientClick="javascript:return confirm('確定要刪除此照片?');">  
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnUpdate" runat="server" Text="編輯" OnClick="btnUpdate_Click" CommandArgument='<%#Eval("PK_DisasterImageGUID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                            <EmptyDataTemplate>
                                無照片上傳
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <%--<asp:ObjectDataSource ID="ODSQLake" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByQLID" TypeName="QLakeImageBLL">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="-1" Name="QLID" QueryStringField="QLID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>--%>
                        <%--                    <asp:ObjectDataSource ID="ODSQLake" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByDisasterBaseDataID" TypeName="DisasterImagesBLL">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="-1" Name="QLID" QueryStringField="QLID" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>--%>

                        <br />
                        <hr />
                        <table class="form">
                            <tr>
                                <th>
                                    <asp:Label ID="fuImage_label" runat="server" Text="※上傳照片"></asp:Label></th>
                                <td>
                                    <asp:FileUpload ID="fuImage" runat="server" />
                                </td>
                            </tr>
                            <%--<tr>
                                <th>※照片時間</th>
                                <td>
                                    <uc:ucDateTime ID="ucDateTime" runat="server" />
                                    <asp:RequiredFieldValidator ID="rfvDateTime" runat="server" Display="Dynamic" ControlToValidate="ucDateTime" ErrorMessage="※ 必填欄位" ValidationGroup="Form" />
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <th>說明</th>
                                <td>
                                    <asp:TextBox ID="imageDescription" runat="server" CssClass="txt" TextMode="MultiLine" Width="250px" />
                                </td>
                            </tr>--%>
                            <tr>
                                <td></td>
                                <td colspan="1" style="text-align: left;">
                                    <asp:Label ID="WarningMessage" runat="server" Text="" Visible="false"></asp:Label>&nbsp;
                                    <asp:Button ID="btnSbumit" runat="server" Text="上傳" OnClick="btnSbumit_Click" ValidationGroup="Form" CssClass="buttonStyle" />
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="buttonStyle" OnClientClick="window.close(); return false;">關閉</asp:LinkButton>
                                    <%--  <asp:Button ID="btnUpdateSubmit" runat="server" Text="更新" OnClick="btnUpdateSubmit_Click" ValidationGroup="Form" />--%>
                                    <%--    <asp:Button ID="btnCancel" runat="server" Text="取消" OnClick="btnCancel_Click" />--%>
                                    <asp:TextBox ID="imageID" runat="server" Visible="False"></asp:TextBox>
                                    <asp:TextBox ID="imagePath" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
<%--                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    </td>
                            </tr>--%>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
