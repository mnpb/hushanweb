﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;

using System.Security.Principal;
using System.Runtime.InteropServices;

public class ImageHandler : IHttpHandler
{
    //定義縮圖的大小
    private const int WIDTH = 300;
    private const int HEIGHT = 225;


    public const int LOGON32_LOGON_INTERACTIVE = 2;
    public const int LOGON32_PROVIDER_DEFAULT = 0;

    public void ProcessRequest(HttpContext context)
    {

        MemoryStream ms = null;
        string fileName = null;
        try
        {
            //string path = WebConfigurationManager.AppSettings["UploadImgPath"];

            //Get the FileName
            fileName = context.Request.QueryString["fn"];
            //Get the PhotoType(TYPE1:800x600 or TYPE2:300x225)
            string type = context.Request.QueryString["t"];
            if (type == null) type = "2"; //預設是TYPE2

            //Get the original image file data
            string sFilePath = HttpContext.Current.Request.MapPath("~/") + "OnSiteLandImages\\" + fileName;
            Byte[] PhotoImage = File.ReadAllBytes(sFilePath);

            if (type == "2")  //QLakeManager用(縮成200*150)
            {
                Bitmap thumbBitmap = Thumbnails.thumbnails(PhotoImage, 200, 150);
                ms = new MemoryStream();
                thumbBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);  //convert thumbBitmap to MemoryStream
            }
            //else if (type == "3")  //QueryData用(縮成120*90)
            //{
            //    Bitmap thumbBitmap = RNTS.Thumbnails.thumbnails(PhotoImage, 120, 90);
            //    ms = new MemoryStream();
            //    thumbBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);  //convert thumbBitmap to MemoryStream
            //}
            else  //不縮圖
            {
                ms = new MemoryStream(PhotoImage, false); //不可變更大小的MemoryStream(maybe save some performance)
            }
        }
        catch (Exception ex)
        {

        }

        if (ms != null)
        {

            //取得影像MemoryStream大小
            int bufferSize = (int)ms.Length;
            //建立 buffer
            byte[] buffer = ms.ToArray();
            //呼叫MemoryStream.Read，自MemoryStream 讀取至buffer，並傳回count
            //int countSize = ms.Read(buffer, 0, bufferSize);
            context.Response.ContentType = "image/jpeg";
            context.Response.AddHeader("Content-Disposition", "filename= download" + fileName.Substring(15, 3) + ".jpg");
            //傳回影像buffer
            context.Response.OutputStream.Write(buffer, 0, buffer.Length);
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }




    /// <summary>
    /// Reads data from a stream until the end is reached. The
    /// data is returned as a byte array. An IOException is
    /// thrown if any of the underlying IO calls fail.
    /// </summary>
    /// <param name="stream">The stream to read data from</param>
    public static byte[] ReadStreamFully(Stream stream)
    {
        byte[] buffer = new byte[32768];
        using (MemoryStream ms = new MemoryStream())
        {
            while (true)
            {
                int read = stream.Read(buffer, 0, buffer.Length);
                if (read <= 0)
                    return ms.ToArray();
                ms.Write(buffer, 0, read);
            }
        }
    }
}