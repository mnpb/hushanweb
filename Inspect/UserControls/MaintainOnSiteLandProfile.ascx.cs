﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inspect_UserControls_MaintainOnSiteLandProfile : System.Web.UI.UserControl
{
    #region Properties

    private int profileID;
    public int ProfileID
    {
        get
        {
            return profileID;
        }
        set
        {
            profileID = value;
            ShowStationBaseInfo(StationBaseInfo);
        }
    }

    private OnSiteLandProfile StationBaseInfo
    {
        get
        {
            return (from info in InspectController.Instance.GetAllOnSiteLandProfiles()
                    where info.ProfileID == ProfileID
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    private void EmptyAllControls()
    {
        ControlUtility.EmptyAllControls(this.Controls);
    }

    /// <summary>
    /// 顯示基本資料
    /// </summary>
    /// <param name="stationBaseInfo"></param>
    private void ShowStationBaseInfo(OnSiteLandProfile stationBaseInfo)
    {
        if (stationBaseInfo == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            ProfileIDTextBox.Text = stationBaseInfo.ProfileID.ToString();
            ProfileTimeTextBox.Text = stationBaseInfo.ProfileTime.ToString("yyyy年MM月dd日HH時mm分ss秒");
            StaffTextBox.Text = stationBaseInfo.Staff;
            LongitudeTextBox.Text = stationBaseInfo.Longitude.ToString();
            LatitudeTextBox.Text = stationBaseInfo.Latitude.ToString();
            TWD97XTextBox.Text = stationBaseInfo.TWD97X.ToString();
            TWD97YTextBox.Text = stationBaseInfo.TWD97Y.ToString();
            DescriptionTextBox.Text = stationBaseInfo.Description;
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newProfile = new OnSiteLandProfile
            {
                ProfileID = Convert.ToInt32(ProfileIDTextBox.Text),
                //ProfileTime = Convert.ToDateTime(ProfileTimeTextBox.Text),
                Staff = StaffTextBox.Text,
                Description = DescriptionTextBox.Text,
                TWD97X = NumericUtility.ToHydValue(TWD97XTextBox.Text),
                TWD97Y = NumericUtility.ToHydValue(TWD97YTextBox.Text),
                Longitude = NumericUtility.ToHydValue(LongitudeTextBox.Text),
                Latitude = NumericUtility.ToHydValue(LatitudeTextBox.Text),
            };
            if (!InspectController.Instance.UpdateOnSiteLandProfile(newProfile))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}