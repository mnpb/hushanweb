﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainOnSiteLandProfile.ascx.cs" Inherits="Inspect_UserControls_MaintainOnSiteLandProfile" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable" style="width: 450px">
    <tr style="display:none">
        <th style="text-align: right">ID</th>
        <td style="text-align: left">
            <asp:TextBox ID="ProfileIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">資料時間</th>
        <td style="text-align: left">
            <asp:TextBox ID="ProfileTimeTextBox" runat="server" CssClass="textBoxStyle" Enabled="false" /></td>
    </tr>
    <tr>
        <th style="text-align: right">填寫人員</th>
        <td style="text-align: left">
            <asp:TextBox ID="StaffTextBox" runat="server" CssClass="textBoxStyle" Width="200" /></td>
    </tr>
    <tr>
        <th style="text-align: right">經度</th>
        <td style="text-align: left">
            <asp:TextBox ID="LongitudeTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">緯度</th>
        <td style="text-align: left">
            <asp:TextBox ID="LatitudeTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">TWD97X</th>
        <td style="text-align: left">
            <asp:TextBox ID="TWD97XTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">TWD97Y</th>
        <td style="text-align: left">
            <asp:TextBox ID="TWD97YTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
        <tr>
        <th style="text-align: right">描述</th>
        <td style="text-align: left">
            <asp:TextBox ID="DescriptionTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>