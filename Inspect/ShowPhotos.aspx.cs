﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using RNTS;
//using DC.Net.Extension;
//using Model;
using System.IO;
using System.Collections.Generic;
using Models;
using DC.Net.Extension;

public partial class DisasterManagement_ShowPhotos : System.Web.UI.Page
{
    //private QLakeBLL qlakeBLL = new QLakeBLL();
    //QLakeDBEntities db = new QLakeDBEntities();
    string ProfileID;

    //private string Account
    //{
    //    get
    //    {
    //        if (Session["Account"] != null)
    //        {
    //            return Session["Account"].ToString();
    //        }
    //        return string.Empty;
    //    }
    //}

    protected void Page_Init(object sender, EventArgs e)
    {
        int a = 0;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        int a = 0;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileID = Request.QueryString["ProfileID"];
        //if (string.IsNullOrEmpty(QLID))
        //{ // 未綁某個災情，預設為帳號相關數據
        //    QLID = "NEW_QLAKE_TEMP_" + Account;
        //}
        btnSbumit.Visible = true;
        WarningMessage.Visible = false;
        //btnUpdateSubmit.Visible = false;
        //btnCancel.Visible = false;

        ShowImageGridView(ProfileID);
    }
    public string ShowImage(object filepath)
    {
        return "~/" + filepath.ToString();
    }
    public string showFilePath(object fileName)
    {
        //縮圖當小圖(200*150)
        string path = "ImageHandler.ashx" + "?fn=" + fileName.ToString() + "&t=2";
        return path;
    }

    public string showFilePath2(object fileName)
    {
        //原圖
        string path = "ImageHandler.ashx" + "?fn=" + fileName.ToString() + "&t=1";
        return path;
    }
    //protected void gvImages_DataBound(object sender, EventArgs e)
    //{
    //    //int iQLID = 0;
    //    //if (gvImages.DataKeys.Count > 0)
    //    //    iQLID = Convert.ToInt32(gvImages.DataKeys[0].Value);
    //    //QLakeDB.TB_QLakeDataDataTable dt = qlakeBLL.GetDataByQLID(iQLID);
    //    //if (dt.Rows.Count > 0)
    //    //{
    //    //    lblQLakeName.Text = dt.Rows[0]["QLName"].ToString();
    //    //}

    //    string disasterID = "";
    //    if (gvImages.DataKeys.Count > 0)
    //        disasterID = gvImages.DataKeys[0].Values["DisasterBaseDataID"].ToString();//gvImages.DataKeys[0].Value.ToString();

    //    DisasterBaseDatasBLL baseBLL = new DisasterBaseDatasBLL();
    //    CalamityManage.TB_DisasterBaseDatasDataTable dt = baseBLL.GetDataByDisasterID(disasterID);
    //    if (dt.Rows.Count > 0)
    //    {
    //        lblQLakeName.Text = dt.Rows[0]["DisasterName"].ToString();
    //    }
    //}

    #region < -> btnSbumit_Click >
    protected void btnSbumit_Click(object sender, EventArgs e)
    {
        if (fuImage.HasFile == false)
        {
            WarningMessage.Text = "請選擇檔案！";
            WarningMessage.Visible = true;
            return;
        }

        if (fuImage.Check(FileExtensions.Image) == false)
        {
            WarningMessage.Text = "請用以下格式！(*.jpg, *.gif, *.png)";
            WarningMessage.Visible = true;
            return;
        }

        DateTime now = DateTime.Now;
        string imagePath = string.Format("OnSiteLandImages/{0}.jpg", now.ToString("yyyyMMddHHmmss"));
        fuImage.SaveAs(imagePath, 500, 500);
        //if (string.IsNullOrEmpty(QLID))
        //{ // 圖片尚未綁定災情
        //    AddTempImageWithNoDisaster(QLID, imagePath, imageDescription.Text, ucDateTime.Value);
        //    BindTempImagesWithNoDisaster();
        //}
        //else
        //{ // 圖片已綁災情
        //    DisasterImagesBLL imagesBLL = new DisasterImagesBLL();
        //    imagesBLL.Insert(QLID, imagePath, imageDescription.Text, ucDateTime.Value);
        //    BindDisasterImages(QLID);
        //    //gvImages.DataBind();
        //}


        InspectController.Instance.AddImage(Convert.ToInt32(ProfileID), now);
        BindDisasterImages(ProfileID);

        fuImage.Attributes.Clear();
        //ucDateTime.Value = DateTime.Now;
        //imageDescription.Text = string.Empty;

    }
    #endregion
    //#region < -> btnDelete_Click >
    //protected void btnDelete_Click(object sender, EventArgs e)
    //{
    //    //LinkButton btn = sender as LinkButton;
    //    //int QIID = btn.CommandArgument.ToInt32();
    //    //var query = db.TB_QLakeImage.Where(a => a.QIID == QIID);
    //    //if (query.Count() > 0)
    //    //{
    //    //    db.DeleteObject(query.First());
    //    //    db.SaveChanges();
    //    //    gvImages.DataBind();
    //    //}

    //    try
    //    {
    //        string imageGUID = ((LinkButton)sender).CommandArgument;
    //        if (string.IsNullOrEmpty(QLID))
    //        { // 圖片尚未綁定災情
    //            var oldInfo = GetTempImagesWithNoDisaster(imageGUID);
    //            if (oldInfo != null)
    //            {
    //                DeleteTempImageWithNoDisaster(imageGUID);
    //                BindTempImagesWithNoDisaster();
    //                File.Delete(Path.Combine(Server.MapPath("~"), oldInfo.ImagePath)); // 刪除實體圖檔
    //            }
    //        }
    //        else
    //        { // 圖片已綁災情
    //            var disasterImageInfo = DisasterImageController.Instance.GetDisasterImage(imageGUID);
    //            if (disasterImageInfo != null)
    //            {
    //                try
    //                {
    //                    File.Delete(Path.Combine(Server.MapPath("~"), disasterImageInfo.ImagePath));
    //                }
    //                catch (Exception ex)
    //                {
    //                    BasePage.showMsgAjax("刪除圖檔發生錯誤！" + ex.Message, Page, this);
    //                    return;
    //                }

    //                if (!DisasterImageController.Instance.DeleteDisasterImage(imageGUID))
    //                {
    //                    BasePage.showMsgAjax("於資料庫刪除圖檔資訊發生錯誤！", Page, this);
    //                    return;
    //                }
    //            }
    //            BindDisasterImages(QLID);
    //            //DisasterImagesBLL imageBLL = new DisasterImagesBLL();
    //            //imageBLL.Delete(imageGUID);
    //            //gvImages.DataBind();
    //        }
    //    }
    //    catch (Exception exx)
    //    {
    //        BasePage.showMsgAjax(exx.Message, Page, this);
    //    }
    //}
    //#endregion   
    protected void gvImages_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string profileID = gvImages.DataKeys[e.RowIndex].Values["ProfileID"].ToString();
            string imageID = gvImages.DataKeys[e.RowIndex].Values["ImageID"].ToString();
            var disasterImageInfo = InspectController.Instance.GetOnSiteLandImage(Convert.ToInt32(imageID));
            if (disasterImageInfo != null)
            {
                File.Delete(Path.Combine(Server.MapPath("~/OnSiteLandImages"), disasterImageInfo.ImagePath));

                if (!InspectController.Instance.DeleteImage(Convert.ToInt32(imageID)))
                {
                    WarningMessage.Text = "於資料庫刪除圖檔資訊發生錯誤！";
                    WarningMessage.Visible = true;
                    return;
                }
            }
            BindDisasterImages(profileID);
        }
        catch (Exception ex)
        {
            //BasePage.showMsgAjax(ex.Message, Page, this);
        }
    }
    //protected void btnUpdateSubmit_Click(object sender, EventArgs e)
    //{
    //    DisasterImagesBLL imagesBLL = new DisasterImagesBLL();
    //    imagesBLL.Update(imageID.Text, QLID, imagePath.Text, imageDescription.Text, ucDateTime.Value);

    //    clearForm();
    //    BindDisasterImages(ProfileID);
    //    //gvImages.DataBind();
    //    fuImage.Visible = true;
    //    fuImage_label.Visible = true;
    //    btnSbumit.Visible = true;
    //    btnUpdateSubmit.Visible = false;
    //    btnCancel.Visible = false;
    //}
    //protected void btnCancel_Click(object sender, EventArgs e)
    //{
    //    clearForm();

    //    fuImage.Visible = true;
    //    fuImage_label.Visible = true;
    //    btnSbumit.Visible = true;
    //    btnUpdateSubmit.Visible = false;
    //    btnCancel.Visible = false;
    //}
    //protected void btnUpdate_Click(object sender, EventArgs e)
    //{
    //    string imageGUID = ((LinkButton)sender).CommandArgument;
    //    DisasterImagesBLL imageBLL = new DisasterImagesBLL();
    //    CalamityManage.TB_DisasterImagesDataTable dt = imageBLL.GetDataByDisasterBaseDataID(QLID);
    //    if (dt.Rows.Count > 0)
    //    {
    //        var query = dt.AsQueryable().Where(x => x.PK_DisasterImageGUID == Guid.Parse(imageGUID));
    //        if (query.Any())
    //        {
    //            var query2 = from x in query.ToList()
    //                         select new
    //                         {
    //                             ImagePath = x.ImagePath,
    //                             ImageDescription = x.IsImageDescriptionNull() ? "" : x.ImageDescription,
    //                             ImageTime = x.IsImageTimeNull() ? DateTime.Now : x.ImageTime
    //                         };
    //            imagePath.Text = query2.FirstOrDefault().ImagePath;
    //            imageDescription.Text = query2.FirstOrDefault().ImageDescription;
    //            ucDateTime.Value = query2.FirstOrDefault().ImageTime;
    //        }

    //        fuImage.Visible = false;
    //        fuImage_label.Visible = false;
    //        btnSbumit.Visible = false;
    //        btnUpdateSubmit.Visible = true;
    //        btnCancel.Visible = true;
    //        imageID.Text = imageGUID;
    //    }
    //    else
    //    {
    //        fuImage.Visible = true;
    //        fuImage_label.Visible = true;
    //        btnSbumit.Visible = true;
    //        btnUpdateSubmit.Visible = false;
    //        btnCancel.Visible = false;
    //        imageID.Text = string.Empty;
    //    }
    //}

    private void clearForm()
    {
        fuImage.Attributes.Clear();
        //ucDateTime.Value = DateTime.Now;
        //imageDescription.Text = string.Empty;
        imageID.Text = string.Empty;
        imagePath.Text = string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="qLakeID"></param>
    private void ShowImageGridView(string profileID)
    {
        BindDisasterImages(profileID);
        //bool withDisaster = false;
        //if (string.IsNullOrEmpty(profileID))
        //{ // 圖片尚未綁定災情
        //    withDisaster = false;
        //    BindTempImagesWithNoDisaster();
        //}
        //else
        //{
        //    withDisaster = true;
        //    BindDisasterImages(profileID);
        //}
        //gvImages.Visible = withDisaster;
        //gvImagesWithNoDisaster.Visible = !withDisaster;
    }

    private void BindDisasterImages(string profileID)
    {
        try
        {
            List<OnSiteLandImage> allImages = InspectController.Instance.GetOnSiteLandImages(Convert.ToInt32(profileID));
            gvImages.DataSource = allImages;
            gvImages.DataBind();

            //DisasterBaseDatasBLL baseBLL = new DisasterBaseDatasBLL();
            //CalamityManage.TB_DisasterBaseDatasDataTable dt = baseBLL.GetDataByDisasterID(disasterID);
            //if (dt.Rows.Count > 0)
            //{
            //    lblQLakeName.Text = dt.Rows[0]["DisasterName"].ToString();
            //}
        }
        catch
        {
        }
    }

    //#region 上傳暫存圖片(未綁災情)

    //private string TempImagesWithNoDisasterSessionKey = DisasterImageController.TempImagesWithNoDisasterSessionKey;
    //private List<TB_DisasterImage> TempImagesWithNoDisaster
    //{
    //    get
    //    {
    //        if (Session[TempImagesWithNoDisasterSessionKey] != null)
    //        {
    //            return Session[TempImagesWithNoDisasterSessionKey] as List<TB_DisasterImage>;
    //        }
    //        return new List<TB_DisasterImage>();
    //    }
    //}
    //private void BindTempImagesWithNoDisaster()
    //{
    //    try
    //    {
    //        gvImagesWithNoDisaster.DataSource = TempImagesWithNoDisaster;
    //        gvImagesWithNoDisaster.DataBind();
    //    }
    //    catch
    //    {
    //    }
    //}

    ///// <summary>
    ///// 取得暫存之上傳圖片資訊
    ///// </summary>
    ///// <param name="disasterImageID"></param>
    ///// <returns></returns>
    //private TB_DisasterImage GetTempImagesWithNoDisaster(string disasterImageID)
    //{
    //    try
    //    {
    //        Guid guid = new Guid(disasterImageID);
    //        return (from info in TempImagesWithNoDisaster
    //                where info.PK_DisasterImageGUID == guid
    //                select info).FirstOrDefault();
    //    }
    //    catch
    //    {
    //        return null;
    //    }
    //}
    ///// <summary>
    ///// 將上傳圖片資訊暫存於session
    ///// </summary>
    ///// <param name="disasterID"></param>
    ///// <param name="imagePath"></param>
    ///// <param name="imageDescription"></param>
    ///// <param name="imageTime"></param>
    //private void AddTempImageWithNoDisaster(string disasterID, string imagePath, string imageDescription,
    //    DateTime imageTime)
    //{
    //    var images = TempImagesWithNoDisaster;
    //    images.Add(new TB_DisasterImage
    //        {
    //            PK_DisasterImageGUID = Guid.NewGuid(),
    //            DisasterBaseDataID = disasterID,
    //            ImagePath = imagePath,
    //            ImageDescription = imageDescription,
    //            ImageTime = imageTime,
    //        });
    //    Session[TempImagesWithNoDisasterSessionKey] = images; // 回存session
    //}

    ///// <summary>
    ///// 刪除暫存之上傳圖片資訊
    ///// </summary>
    ///// <param name="disasterImageID"></param>
    //private void DeleteTempImageWithNoDisaster(string disasterImageID)
    //{
    //    Guid guid = new Guid(disasterImageID);
    //    var images = TempImagesWithNoDisaster;
    //    var oldInfo = (from info in images
    //                   where info.PK_DisasterImageGUID == guid
    //                   select info).FirstOrDefault();
    //    if (oldInfo != null)
    //    {
    //        images.Remove(oldInfo);
    //        Session[TempImagesWithNoDisasterSessionKey] = images; // 回存session
    //    }
    //}

    //protected void gvImagesWithNoDisaster_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    try
    //    {
    //        string imageGUID = gvImagesWithNoDisaster.DataKeys[e.RowIndex].Values["PK_DisasterImageGUID"].ToString();
    //        if (string.IsNullOrEmpty(QLID))
    //        { // 圖片尚未綁定災情
    //            var oldInfo = GetTempImagesWithNoDisaster(imageGUID);
    //            if (oldInfo != null)
    //            {
    //                DeleteTempImageWithNoDisaster(imageGUID);
    //                BindTempImagesWithNoDisaster();
    //                File.Delete(Path.Combine(Server.MapPath("~"), oldInfo.ImagePath)); // 刪除實體圖檔
    //            }
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}

    //#endregion
}
