﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inspect_ShowAllRadios : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowLatestInfos();
        }
    }

    private void ShowLatestInfos()
    {
        try
        {
            using (MySqlConnection con = new MySqlConnection(ConfigInfo.InspectConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM radio"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            RadioGridView.DataSource = dt;
                            RadioGridView.DataBind();
                        }
                    }
                }
            }
        }
        catch
        {
        }
    }

    protected void QueryButton_Click(object sender, EventArgs e)
    {
        ShowLatestInfos();
    }
}