﻿<%@ Page Title="地籍現勘資料維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainOnSiteLandProfile.aspx.cs" Inherits="Inspect_MaintainOnSiteLandProfile" %>

<%--<%@ Register Src="~/WebUserControlers/Meters/SearchStation.ascx" TagPrefix="uc1" TagName="SearchStation" %>--%>
<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Inspect/UserControls/MaintainOnSiteLandProfile.ascx" TagPrefix="uc1" TagName="MaintainOnSiteLandProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
    <script type="text/javascript">
        function openPhotos(profileID) {
            window.open('ShowPhotos.aspx?ProfileID=' + profileID, 'letter', 'scrollbars=yes,resizable=yes,width=800,height=600'); return false;
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainOnSiteLandProfile runat="server" ID="MaintainOnSiteLandProfileUserControl" OnCompleted="MaintainOnSiteLandProfileUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <%--    <uc1:SearchStation runat="server" ID="SearchStationUserControl" OnFilterStation="SearchStationUserControl_FilterStation" OnClearFilter="SearchStationUserControl_ClearFilter" />--%>
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="ProfileID" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="ID" Visible="false">
                        <ItemTemplate><%# Eval("ProfileID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="資料時間">
                        <ItemTemplate><%# Eval("ProfileTime") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="填寫人員">
                        <ItemTemplate><%# Eval("Staff") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="經度">
                        <ItemTemplate><%# Eval("Longitude") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="緯度">
                        <ItemTemplate><%# Eval("Latitude") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TWD97X">
                        <ItemTemplate><%# Eval("TWD97X") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TWD97Y">
                        <ItemTemplate><%# Eval("TWD97Y") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="描述">
                        <ItemTemplate><%# Eval("Description") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="瀏覽圖片">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnPhoto" runat="server" Font-Underline="True" Text='瀏覽圖片' OnClientClick='<%# ShowOnClientClick(Eval("ProfileID")) %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

