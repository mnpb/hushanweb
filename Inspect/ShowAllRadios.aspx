﻿<%@ Page Title="調度無線電話務系統資料" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShowAllRadios.aspx.cs" Inherits="Inspect_ShowAllRadios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
            <asp:Button ID="QueryButton" runat="server" OnClick="QueryButton_Click" Text="取得最新資料" CssClass="buttonStyle" />
            <br />  <br />
            <asp:GridView ID="RadioGridView" runat="server" CssClass="fancytable"></asp:GridView>
                </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

