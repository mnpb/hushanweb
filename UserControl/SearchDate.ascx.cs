﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_SearchDate : System.Web.UI.UserControl
{
    #region Methods

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public string SetDateTime
    {
        set
        {
            date.Value = value;
        }
    }
    public DateTime GetDateTime
    {
        get
        {
            return DateTime.Parse(date.Value);
        }
    }
    public string SetTitle
    {
        set
        {
            title.InnerText = value;
        }
    }
    #endregion
}