﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%--<%@ Register Src="WebUserControlers/Modal.ascx" TagName="Modal" TagPrefix="uc1" %>
<%@ Register Src="WebUserControlers/Popup.ascx" TagName="Popup" TagPrefix="uc2" %>
<%@ Register Src="WebUserControlers/AccountManagement/ChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc3" %>--%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>湖山水庫營運管理系統平台</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:400,300italic,300,400italic,700,900' rel='stylesheet' type='text/css'>
    <link href="css/normalize.css" rel="stylesheet" />
    <link href="css/loginStyle.css" rel="stylesheet" />

    <%--    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:400,700" />
    <link rel="stylesheet" href="assets/css/reset.css" />
    <link rel="stylesheet" href="assets/css/style.css" />--%>
</head>
<body>
    <form id="form1" runat="server">
    <div style="z-index: 99999; position: absolute; left: 22%; top: 9%;">
        <img src="assets/img/title.png" />
    </div>
    <div class="auth-wrap-background">
        <div class="auth-center">
            <div class="auth-blur auth-size">
                <div class="auth-wrap-background"></div>
            </div>
        </div>
        <div class="auth-center">
            <div class="auth__inner auth-size">
                <div class="auth__sidebar auth__inner__section"></div>
                <div class="auth__form auth__inner__section">
                    <div class="form">
                        <h1 class="form__title">使用者登入</h1>
                        <label class="form__label">
                            <span class="form__label__text">帳號</span>
                     <%--       <input class="form__input" />--%>
                            <asp:TextBox ID="LoginName" runat="server" CssClass="form__input"></asp:TextBox>
                            <div class="form__input-border"></div>
                        </label>
                        <label class="form__label">
                            <span class="form__label__text">密碼</span>
                          <%--  <input class="form__input" />--%>
                            <asp:TextBox ID="Password" runat="server" CssClass="form__input" TextMode="Password"></asp:TextBox>
                            <div class="form__input-border"></div>
                        </label>
                     <%--   <button class="button">登入</button>--%>
                        <asp:Button ID="Button_ok" runat="server" Text="登入" type="submit" CssClass="button" OnClick="Button_ok_Click" />
                         <asp:Label ID="ErrorMessage" runat="server" Style="color: Red; line-height: 1.6; font-weight: bold;"></asp:Label>
                    </div>
                    <div id="lakes">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sky">
        <div class="clouds_one"></div>
        <div class="clouds_two"></div>
        <div class="clouds_three"></div>
    </div>
        </form>
    <%--<div class="page-container">
        <img src="assets/img/title.png" />
        <h1>使用者登入</h1>
        <form id="form1" runat="server" class="contact-us">
            <asp:TextBox ID="LoginName" runat="server" placeholder="請輸入帳號"></asp:TextBox>
            <asp:TextBox ID="Password" runat="server" placeholder="請輸入密碼" TextMode="Password"></asp:TextBox>
            <asp:Button ID="Button_ok" runat="server" Text="確認" type="submit" OnClick="Button_ok_Click" />
            <asp:Label ID="ErrorMessage" runat="server" Style="color: Red; line-height: 1.6; font-weight: bold;"></asp:Label>
        </form>
    </div>--%>
</body>
</html>
