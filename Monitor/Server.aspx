﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="Server.aspx.cs" Inherits="Monitor_Server" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #canvasMemory {
            margin-top: 50px;
        }

        canvas {
            border: solid 1px gray;
            margin: 20px;
            padding: 20px;
            width: 80%;
        }

        #canvasdiv {
            width: 92%;
        }

        .level-Normal {
            background-image: url("../images/Icons/circle-green.png");
        }

        .level-Danger {
            background-image: url("../images/Icons/circle-red.png");
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="monitoredserver-tabs cs-tab">
        <ul>
            <li><a href="#tabs-server">伺服器主機設備狀態</a></li>
            <li><a href="#tabs-resource">資源使用狀態歷史</a></li>
        </ul>
        <div id="tabs-server">
            <img alt="" src="../images/Icons/circle-green.png" />(正常)&nbsp;
                <img alt="" src="../images/Icons/circle-red.png" />(異常)，
            資料更新時間 : 時間:<%=eDatetime.ToString("HH:mm") %>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="fancytable" OnRowDataBound="GridView1_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="ServerIP" HeaderText="IP" />
                    <asp:BoundField DataField="Name" HeaderText="設備名稱" />
                    <asp:BoundField DataField="Location" HeaderText="設備位置" />
                    <asp:BoundField DataField="CPUTemperature" HeaderText="CPU溫度(℃)" />
                    <asp:BoundField DataField="PowerStatus" HeaderText="電源供應器" />
                    <asp:BoundField DataField="MemoryUsage" HeaderText="記憶體使用率(%)" />
                    <asp:BoundField DataField="HD_C" HeaderText="硬碟C使用率%" />
                    <asp:BoundField DataField="HD_D" HeaderText="硬碟D使用率%" />
                    <asp:BoundField DataField="Power" HeaderText="硬碟" DataFormatString="正常" />
                    <asp:BoundField DataField="Net1" HeaderText="網路1傳輸率(Kb/s)" />
                    <asp:BoundField DataField="Net2" HeaderText="網路2傳輸率(Kb/s)" />
                    <asp:BoundField DataField="Net3" HeaderText="網路3傳輸率(Kb/s)" />
                    <asp:BoundField DataField="Net4" HeaderText="網路4傳輸率(Kb/s)" />
                    <%--<asp:BoundField DataField="CreateDatetime" HeaderText="時間" DataFormatString="{0:T}" />--%>
                    <asp:TemplateField HeaderText="狀態">
                        <ItemTemplate>
                            <div id="Label1" runat="server" title='<%#Eval("PowerStatusText") %>' class='<%#"level-"+ Eval("Level") %>' style="width: 24px; height: 24px;"></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tabs-resource">
            <label class="monitoredserver-searchDatetime">日期：</label>
            <input type="text" runat="server" class="datepicker monitoredserver-startDatetime" name="startDatetime" id="startDatetime" />
            <asp:Button ID="ButtonQuery" runat="server" Text="查詢" OnClick="ButtonQuery_Click" />
            <div id="canvasdiv">
                <canvas id="canvasCPU"></canvas>
                <canvas id="canvasMemory"></canvas>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
    <script src="../Scripts/MonitoredServer.js"></script>
    <script>
        $(function () {
            $("#<%= ButtonQuery.ClientID%>").click(function () {
                if ($("#<%= startDatetime.ClientID%>").val() == '') {
                    console.log(1);
                    return false;
                }
            })

            $(".monitoredserver-tabs").tabs();
            $(".datepicker").datepicker({
                option: "zh-TW",
                dateFormat: 'yy/mm/dd'
            });

            $("#canvasCPU").MoniteredServerHistoryChart(<%=Newtonsoft.Json.JsonConvert.SerializeObject(ChartCPU.XLabels.Select(a=>a.Substring(0,2)).ToList()) %>,
            <%=Newtonsoft.Json.JsonConvert.SerializeObject(ChartCPU.Datas) %> , {
                    yLabel: "溫度(℃)",
                    xLabel: "小時",
                    title: "CPU溫度"
                });
            $("#canvasMemory").MoniteredServerHistoryChart(<%=Newtonsoft.Json.JsonConvert.SerializeObject(ChartMem.XLabels.Select(a=>a.Substring(0,2)).ToList()) %>,
                <%=Newtonsoft.Json.JsonConvert.SerializeObject(ChartMem.Datas) %>, {
                    yLabel: "使用百分比",
                    xLabel: "小時",
                    title: "記憶體使用率"
                });
        });
    </script>
</asp:Content>

