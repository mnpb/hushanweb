﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DischargeWarnReport.aspx.cs" Inherits="Monitor_DischargeWarnReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="StationID,RecDateTime" DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="StationID" HeaderText="StationID" ReadOnly="True" SortExpression="StationID"></asp:BoundField>
            <asp:BoundField DataField="StationName" HeaderText="設備名稱" SortExpression="StationName"></asp:BoundField>
            <asp:BoundField DataField="CmdName" HeaderText="CmdName" SortExpression="CmdName"></asp:BoundField>
            <asp:BoundField DataField="AC" HeaderText="AC" SortExpression="AC"></asp:BoundField>
            <asp:BoundField DataField="DC" HeaderText="DC" SortExpression="DC"></asp:BoundField>
            <asp:BoundField DataField="Door" HeaderText="門禁狀態" SortExpression="Door"></asp:BoundField>
            <asp:BoundField DataField="Amp" HeaderText="Amp" SortExpression="Amp"></asp:BoundField>
            <asp:BoundField DataField="Trumpet" HeaderText="喇叭迴路狀態" SortExpression="Trumpet"></asp:BoundField>
            <asp:BoundField DataField="SN" HeaderText="SN" SortExpression="SN"></asp:BoundField>
            <asp:BoundField DataField="RecDateTime" HeaderText="資料時間" ReadOnly="True" SortExpression="RecDateTime"></asp:BoundField>
        </Columns>
        <EditRowStyle BackColor="#2461BF"></EditRowStyle>

        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

        <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

        <RowStyle BackColor="#EFF3FB"></RowStyle>

        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

        <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

        <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

        <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

        <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString='<%$ ConnectionStrings:HushanConnectionString %>' SelectCommand="SELECT * FROM [WarningRealStatus]"></asp:SqlDataSource>
</asp:Content>

