﻿<%@ WebHandler Language="C#" Class="ServerStatus" %>

using System;
using System.Web;

public class ServerStatus : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            //http://localhost:1961/monitor/serverstatus.ashx?IP=192.168.55.2&CPUTemperature=3&MemoryUsage=1&Power=1&Power1=0&HD_C=33&HD_D=44&net1=1.1&net2=2.2&net3=3.3&net4=4.4
            MonitoredServer ms = new MonitoredServer();
            var IPs = context.Request["IP"];//1.1.1.1;2.2.2.2
            string ip = "";
            if (ms.IsExist(IPs, out ip))
            {
                short? CPUTemperature =
                string.IsNullOrEmpty(context.Request["CPUTemperature"]) ?
                    CPUTemperature = null :
                    CPUTemperature = (short)float.Parse(context.Request["CPUTemperature"]);

                short? MemoryUsage =
                string.IsNullOrEmpty(context.Request["MemoryUsage"]) ?
                    MemoryUsage = null :
                    MemoryUsage = (short)float.Parse(context.Request["MemoryUsage"]);

                short? HD_C =
                string.IsNullOrEmpty(context.Request["HD_C"]) ?
                    HD_C = null :
                    HD_C = (short)float.Parse(context.Request["HD_C"]);

                short? HD_D =
                string.IsNullOrEmpty(context.Request["HD_D"]) ?
                    HD_D = null :
                    HD_D = (short)float.Parse(context.Request["HD_D"]);

                short? Power =
                string.IsNullOrEmpty(context.Request["Power"]) ?
                    Power = null :
                    Power = short.Parse(context.Request["Power"]);

                short? Power1 =
                string.IsNullOrEmpty(context.Request["Power1"]) ?
                    Power1 = null :
                    Power1 = short.Parse(context.Request["Power1"]);

                double? net1 =
                string.IsNullOrEmpty(context.Request["net1"]) ?
                    net1 = null :
                    net1 = double.Parse(context.Request["net1"]);

                double? net2 =
                string.IsNullOrEmpty(context.Request["net2"]) ?
                    net2 = null :
                    net2 = double.Parse(context.Request["net2"]);

                double? net3 =
                string.IsNullOrEmpty(context.Request["net3"]) ?
                    net3 = null :
                    net3 = double.Parse(context.Request["net3"]);

                double? net4 =
                string.IsNullOrEmpty(context.Request["net4"]) ?
                    net4 = null :
                    net4 = double.Parse(context.Request["net4"]);

                ms.InsertStatus(ip, CPUTemperature, MemoryUsage, HD_C, HD_D, Power, Power1, net1, net2, net3, net4);
                context.Response.Write(ip +
                    ", CPUTemperature" + (CPUTemperature.HasValue ? CPUTemperature.Value.ToString() : "") +
                    ", MemoryUsage" + (MemoryUsage.HasValue ? MemoryUsage.Value.ToString() : "") +
                    ", HD" + (HD_C.HasValue ? HD_C.Value.ToString() : "") +
                    ", Power" + (Power.HasValue ? Power.Value.ToString() : "") +
                    ", Power1" + (Power1.HasValue ? Power1.Value.ToString() : "") +
                    ", net1" + (net1.HasValue ? net1.Value.ToString() : "") +
                    ", net2" + (net2.HasValue ? net2.Value.ToString() : "") +
                    ", net3" + (net3.HasValue ? net3.Value.ToString() : "") +
                    ", net4" + (net4.HasValue ? net4.Value.ToString() : "")
                    );
            }
            else
            {
                context.Response.Write(IPs + ":not in white list");
            }
        }
        catch (Exception ex)
        {
            context.Response.Write(ex);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}