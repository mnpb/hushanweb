﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Monitor_Server : SuperPage
{
    public MonitedServerChartModel ChartCPU = new MonitedServerChartModel();
    public MonitedServerChartModel ChartMem = new MonitedServerChartModel();
    //public DateTime sDatetime = DatetimeUtility.MinDownTo0or10(DateTime.Parse("2016/12/22 06:50"));
    //public DateTime eDatetime = DatetimeUtility.MinDownTo0or10(DateTime.Parse("2016/12/23 06:50"));
    public DateTime sDatetime = DatetimeUtility.MinDownTo0or10(DateTime.Now.AddHours(-23));
    public DateTime eDatetime = DatetimeUtility.MinDownTo0or10(DateTime.Now);
    MonitoredServer dal = new MonitoredServer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();

        }
    }

    private void BindData()
    {
        AlertServerModel alertmodel = CacheUtil.GetFromCache("AlertServer", "",
            () =>
            {
                AlertServerDAL dal = new AlertServerDAL();
                return dal.Get();
            }, 10);//cache十分鐘

        List<MonitoredServerStatusInfo> data = CacheUtil.GetFromCache("MonitoredServerStatusInfo",
            eDatetime.ToString("yyyyMMddHHmm"), () =>
            {
                List<MonitoredServerStatusInfo> ans = dal.GetServerStatus(sDatetime, eDatetime);
                return ans;
            });

        List<MonitoredServerStatusInfo> lastestData = data.Where(a => a.DatetimeStamp == data.Max(b => b.DatetimeStamp)).ToList();
        lastestData.ForEach(a =>//顯示在前台的燈號狀態
        {
            a.PowerStatusText = a.Power + "," + a.Power1;

            MonitoredServerLevelEnum cpu, mem;
            if (a.CPUTemperature < alertmodel.CPUTemp)
                cpu = MonitoredServerLevelEnum.Normal;
            else
                cpu = MonitoredServerLevelEnum.Danger;

            if (a.MemoryUsage < alertmodel.MemUsage)
                mem = MonitoredServerLevelEnum.Normal;
            else
                mem = MonitoredServerLevelEnum.Danger;

            if (a.Power > 0 || a.Power1 > 0)
                a.PowerStatus = MonitoredServerLevelEnum.Normal;
            else if (a.Power == 0 && a.Power1 == 0)
                a.PowerStatus = MonitoredServerLevelEnum.None;
            else
                a.PowerStatus = MonitoredServerLevelEnum.Danger;

            short ans = Math.Max((short)cpu, (short)mem);
            ans = Math.Max(ans, (short)a.PowerStatus);

            a.Level = (MonitoredServerLevelEnum)ans;

        }
        );
        GridView1.DataSource = lastestData;
        GridView1.DataBind();

        //畫圖
        string cacheKey = sDatetime.ToString("yyyyMMddHHmm") + eDatetime.ToString("yyyyMMddHHmm");
        ChartCPU = CacheUtil.GetFromCache("ChartCPU", cacheKey, () =>
            {
                return InitChartModel(data, (model) => { return model.CPUTemperature; });
            });
        ChartMem = CacheUtil.GetFromCache("ChartMem", cacheKey, () =>
            {
                return InitChartModel(data, (model) => { return model.MemoryUsage; });
            });
    }

    private MonitedServerChartModel InitChartModel(List<MonitoredServerStatusInfo> ans, Func<MonitoredServerStatusInfo, short> returnProperty)
    {
        ans = ans.Where(a => a.DatetimeStamp.EndsWith("00")).ToList();
        MonitedServerChartModel chartModel = new MonitedServerChartModel();

        //X軸的值
        chartModel.InitXLabels(sDatetime, eDatetime);

        List<MonitedServerChartModel.Series> datas = new List<MonitedServerChartModel.Series>();
        IEnumerable<string> machines = ans.Where(a => a.Name != null).Select(a => a.Name).Distinct();
        foreach (var machineName in machines)
        {
            MonitedServerChartModel.Series s = new MonitedServerChartModel.Series();
            foreach (string time in chartModel.XLabels)
            {
                var model = ans.FirstOrDefault(a => a.DatetimeStamp.EndsWith(time) && a.Name == machineName);
                if (model == null)
                {
                    s.Data.Add(0);
                }
                else
                {
                    s.Data.Add(returnProperty(model));
                }
            }
            s.Label = machineName;
            datas.Add(s);
        }
        chartModel.Datas = datas;
        return chartModel;
    }

    protected void ButtonQuery_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "buttonStartupBySM",
            "$('.cs-tab').tabs({  active: 1});", true);
        if (string.IsNullOrEmpty(startDatetime.Value))
        {
            return;
        }
        else
        {
            sDatetime = DateTime.Parse(startDatetime.Value + " 00:00:00");
            eDatetime = DateTime.Parse(startDatetime.Value + " 23:59:00");
            BindData();
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        AlertServerModel alertmodel = CacheUtil.GetFromCache("AlertServer", "",
            () =>
            {
                AlertServerDAL dal = new AlertServerDAL();
                return dal.Get();
            }, 10);//cache十分鐘

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CPU
            if (e.Row.Cells[3].Text == "0")
                e.Row.Cells[3].Text = "";
            else
            {
                int limit;
                if (int.TryParse(e.Row.Cells[3].Text, out limit) && limit > alertmodel.CPUTemp)
                    e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
            }

            //Power
            if (MonitoredServerLevelEnum.None.ToString() == e.Row.Cells[4].Text)
                e.Row.Cells[4].Text = "";
            else if (MonitoredServerLevelEnum.Normal.ToString() == e.Row.Cells[4].Text)
            {
                e.Row.Cells[4].Text = "正常";
            }
            else if (MonitoredServerLevelEnum.Danger.ToString() == e.Row.Cells[4].Text)
            {
                e.Row.Cells[4].Text = "故障";
                e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
            }

            //MEMORY
            if (e.Row.Cells[5].Text == "0")
                e.Row.Cells[5].Text = "";
            else
            {
                int limit;
                if (int.TryParse(e.Row.Cells[5].Text, out limit) && limit > alertmodel.MemUsage)
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
            }


        }
    }
}