﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Communicate.aspx.cs" Inherits="Monitor_Communicate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="u162" class="ax_droplist">
        <select id="u162_input">
            <option value="資訊網路節點">資訊網路節點</option>
            <option value="CCTV監視站台">CCTV監視站台</option>
            <option value="無限寬頻節點">無限寬頻節點</option>
            <option value="無線電水文站台">無線電水文站台</option>
            <option value="無線電警報站台">無線電警報站台</option>
            <option value="地震儀">地震儀</option>
        </select>
    </div>
    <table>
        <tr>
            <td>IP位置</td>
            <td></td>
            <td>設備名稱</td>
            <td></td>
            <td>設備位置</td>
            <td></td>
            <td>網路狀態</td>
            <td></td>
            <td>設備說明</td>
            <td></td>
        </tr>
        <tr>
            <td>10.56.181.100</td>
            <td></td>
            <td>NVR影像監控伺服主機</td>
            <td></td>
            <td>營管中心</td>
            <td></td>
        </tr>
    </table>
</asp:Content>

