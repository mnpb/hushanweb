﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Message_EditSmsGroupAndMember : SuperPage
{
    private List<SmsGroup> SmsGroupList
    {
        get
        {
            return MessageController.Instance.GetAllSmsGroups();
        }
    }
    private List<SmsMember> AllSmsMembers
    {
        get
        {
            return MessageController.Instance.GetAllSmsMembers();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ControlUtility.FillData(SmsGroupRadioButtonList, SmsGroupList, "Name", "SmsGroupID");
            ControlUtility.FillData(SmsMemberCheckBoxList, AllSmsMembers, "Name", "SmsMemberID");
        }
    }
    protected void SmsGroupRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList radioButtonList = (RadioButtonList)sender;
        var allSmsGroups = SmsGroupList;
        SmsGroup selectedSmsGroup =
            allSmsGroups.Where(SG => SG.SmsGroupID == short.Parse(radioButtonList.SelectedValue)).FirstOrDefault();
        ChangeSelectedMembers(selectedSmsGroup);
    }
    private void ChangeSelectedMembers(SmsGroup selectedSmsGroup)
    {
        // reset
        foreach (ListItem listItem in SmsMemberCheckBoxList.Items)
        {
            listItem.Selected = false;
        }

        List<SmsMember> selectedSmsMembers =
            MessageController.Instance.GetSmsMembersByGroup(new List<SmsGroup> { selectedSmsGroup });
        var selectedSmsMemberIDs = selectedSmsMembers.Select(x => x.SmsMemberID).ToList();
        foreach (ListItem listItem in SmsMemberCheckBoxList.Items)
        {
            if (selectedSmsMemberIDs.Contains(Convert.ToInt32(listItem.Value)))
            {
                listItem.Selected = true;
            }
        }
    }

    protected void UpdateSmsGroupToMemberButton_Click(object sender, EventArgs e)
    {
        short groupID = Convert.ToInt16(SmsGroupRadioButtonList.SelectedValue);
        var memberIDs = SmsMemberCheckBoxList.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => Convert.ToInt32(x.Value)).ToList();

        if (memberIDs.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(
                UpdateSmsGroupToMemberButton, UpdateSmsGroupToMemberButton.GetType(), "", "alert('請選擇一位簡訊接收人員');", true);
        }
        else
        {
            if (MessageController.Instance.SetMemberToGroup(groupID, memberIDs))
            {
                ScriptManager.RegisterClientScriptBlock(UpdateSmsGroupToMemberButton, UpdateSmsGroupToMemberButton.GetType(), "", "alert('更改成功');", true);
            }
        }
    }

    protected void EditSmsGroupButton_Click(object sender, EventArgs e)
    {
        SmsGroupPopup.Title = "簡訊群組編輯";
        SmsGroupPopup.Show();
    }

    protected void EditSmsMemberButton_Click(object sender, EventArgs e)
    {
        SmsMemberPopup.Title = "簡訊接收人員編輯";
        SmsMemberPopup.Show();
    }

    protected void SmsGroupList_Completed(object sender, EventArgs e)
    {
        var selectedSmsGroupID = SmsGroupRadioButtonList.SelectedValue;
        var allSmsGroups = SmsGroupList;
        ControlUtility.FillData(SmsGroupRadioButtonList, allSmsGroups, "Name", "SmsGroupID");
        SmsGroup selectedSmsGroup =
            allSmsGroups.Where(SG => SG.SmsGroupID == short.Parse(selectedSmsGroupID)).FirstOrDefault();
        SmsGroupRadioButtonList.SelectedValue = selectedSmsGroupID;
        ChangeSelectedMembers(selectedSmsGroup);
    }
    protected void SmsMemberList_Completed(object sender, EventArgs e)
    {
        var selectedMemberIDs = 
            SmsMemberCheckBoxList.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => Convert.ToInt32(x.Value)).ToList();
        ControlUtility.FillData(SmsMemberCheckBoxList, AllSmsMembers, "Name", "SmsMemberID");
        foreach (ListItem listItem in SmsMemberCheckBoxList.Items)
        {
            if (selectedMemberIDs.Contains(Convert.ToInt32(listItem.Value)))
            {
                listItem.Selected = true;
            }
        }
    }
}