﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Message_SendSms : SuperPage
{
    private List<SmsGroup> SmsGroupList
    {
        get
        {
            return MessageController.Instance.GetAllSmsGroups();
        }
    }
    private List<SmsMember> AllSmsMembers
    {
        get
        {
            return MessageController.Instance.GetAllSmsMembers();
        }
    }

    private List<string> MobileNoList
    {
        get
        {
            return ViewState["MobileNoList"] == null ? new List<string>() : (List<string>)ViewState["MobileNoList"];
        }
        set
        {
            ViewState["MobileNoList"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ControlUtility.FillData(SmsGroupCheckBoxList, SmsGroupList, "Name", "SmsGroupID");
            ControlUtility.FillData(SmsMemberCheckBoxList, AllSmsMembers, "Name", "SmsMemberID");
            FillAllSmsContentsTemplates(SmsContentsTemplateDropDownList);
        }
    }

    private void FillAllSmsContentsTemplates(dynamic dropDownList)
    {
        ControlUtility.FillData(dropDownList, MessageController.Instance.GetAllSmsContentsTemplates(), "Name", "SmsContentsTemplateID");
        dropDownList.Items.Insert(0, new ListItem("請選擇", ""));

    }

    protected void SmsGroupCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
    {
        CheckBoxList CheckBoxList = (CheckBoxList)sender;
        var allSmsGroups = SmsGroupList;
        List<SmsGroup> selectedGroups = new List<SmsGroup>();
        foreach (ListItem listItem in CheckBoxList.Items)
        {
            if (listItem.Selected)
            {
                SmsGroup selectedSmsGroup = allSmsGroups.Where(SG => SG.SmsGroupID == short.Parse(listItem.Value)).Single();
                selectedGroups.Add(selectedSmsGroup);
            }
        }
        ChangeSelectedMembers(selectedGroups);
    }

    protected void AddMobileNoButton_Click(object sender, EventArgs e)
    {
        List<string> MobileNoList = this.MobileNoList;

        MobileNoList.Add(MobileNoTextBox.Text);
        this.MobileNoList = MobileNoList;

        MobileNoCheckBoxList.DataSource = this.MobileNoList;
        MobileNoCheckBoxList.DataBind();
    }

    protected void DeleteMobileNoButton_Click(object sender, EventArgs e)
    {
        bool IsSelected = false;

        List<string> MobileNoList = this.MobileNoList;

        foreach (ListItem ListItem in MobileNoCheckBoxList.Items)
        {
            if (ListItem.Selected)
            {
                IsSelected = true;
                MobileNoList.Remove(ListItem.Value);
            }
        }

        if (!IsSelected)
            ScriptManager.RegisterClientScriptBlock(DeleteMobileNoButton, DeleteMobileNoButton.GetType(), "", "alert('請選擇欲刪除的號碼');", true);
        else
        {
            this.MobileNoList = MobileNoList;

            MobileNoCheckBoxList.DataSource = this.MobileNoList;
            MobileNoCheckBoxList.DataBind();
        }
    }


    protected void SmsContentsTemplateDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dropDownList = (DropDownList)sender;

        SmsContentsTemplate thisSmsContentsTemplate =
            MessageController.Instance.GetSmsContentsTemplate(short.Parse(dropDownList.SelectedValue));
        if (thisSmsContentsTemplate != null)
        {
            SmsContentsTextBox.Text = thisSmsContentsTemplate.ContentsTemplate;
        }
        else
        {
            SmsContentsTextBox.Text = string.Empty;
        }
    }

    protected void AddNewSmsContentsTemplateButton_Click(object sender, EventArgs e)
    {
        string ErrorMessage = string.Empty;

        if (string.IsNullOrEmpty(SmsContentsTextBox.Text))
            ErrorMessage += "簡訊範本內容不可以空白\\n";

        if (string.IsNullOrEmpty(TemplateTextBox.Text))
            ErrorMessage += "簡訊範本名稱不可以空白\\n";

        if (!string.IsNullOrEmpty(ErrorMessage))
            ScriptManager.RegisterClientScriptBlock(AddNewSmsContentsTemplateButton, AddNewSmsContentsTemplateButton.GetType(), "", "alert('" + ErrorMessage + "');", true);
        else
        {
            MessageController.Instance.AddSmsContentsTemplate(TemplateTextBox.Text, SmsContentsTextBox.Text);

            ScriptManager.RegisterClientScriptBlock(AddNewSmsContentsTemplateButton, AddNewSmsContentsTemplateButton.GetType(), "", "alert('新增簡訊範本成功');", true);
        }
    }

    protected void SendSmsButton_Click(object sender, EventArgs e)
    {
        bool IsSend = true;
        string ErrorMessage = string.Empty;

        //List<SmsGroup> SmsGroupList = CheckBoxSmsGroup1.SelectedSmsGroupList;
        var recerivers = GetSendMembers(GetSelectedMemberIDs(), MobileNoCheckBoxList);

        int SmsSendRecordID = 0;

        //發簡訊之前要做的檢查
        //1. 檢查是否有 選擇有選擇簡訊成員或手動新增手機號碼
        //2. 檢查是否有 簡訊內容

        //if (SmsMemberList.Count == 0 && MobileNoCheckBoxList.Items.Count == 0)
        if (recerivers.Count == 0)
        {
            IsSend = false;
            ErrorMessage += "請選擇簡訊成員或手動新增手機號碼\\n";
        }

        if (string.IsNullOrEmpty(SmsContentsTextBox.Text))
        {
            IsSend = false;
            ErrorMessage += "請輸入簡訊內容\\n";
        }


        if (IsSend)
        {
            foreach (var member in recerivers)
            {
                string returnMessage = string.Empty;
                MessageController.Instance.SendSms(
                    AccountController.Instance.GetAccountName(), member, SmsContentsTextBox.Text, ref returnMessage);
            }

            //using (SmsSendRecord SmsSendRecord = new SmsSendRecord())
            //    SmsSendRecordID = SmsSendRecord.AddSmsSendRecordReturnSmsSendRecordID(LoginAccount.AccountID, 2, SmsContentsTextBox.Text, DateTime.Now);

            ////發簡訊，將發簡訊的記錄寫入Table
            //using (Sms Sms = new Sms())
            //{
            //    //認證
            //    Sms.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmsWebServiceAccount"], ConfigurationManager.AppSettings["SmsWebServicePassword"]);

            //    using (SmsSendMember SmsSendMember = new SmsSendMember())
            //    {

            //        foreach (SmsMember SmsMember in SmsMemberList)
            //        {
            //            Result Result = Sms.Send(SmsMember.MobileNo, SmsContentsTextBox.Text);
            //            SmsSendMember.AddSmsSendMember(SmsSendRecordID, SmsMember.Name, SmsMember.MobileNo, Result.ReturnCode == 0, Result.ReturnMessage, DateTime.Now);
            //        }

            //        foreach (ListItem ListItem in MobileNoCheckBoxList.Items)
            //        {
            //            Result Result = Sms.Send(ListItem.Value, SmsContentsTextBox.Text);
            //            SmsSendMember.AddSmsSendMember(SmsSendRecordID, string.Empty, ListItem.Value, Result.ReturnCode == 0, Result.ReturnMessage, DateTime.Now);
            //        }
            //    }

            //    using (SmsSendGroup SmsSendGroup = new SmsSendGroup())
            //        foreach (SmsGroup SmsGroup in SmsGroupList)
            //            SmsSendGroup.AddSmsSendGroup(SmsSendRecordID, SmsGroup.SmsGroupID);
            //}

            ScriptManager.RegisterClientScriptBlock(SendSmsButton, SendSmsButton.GetType(), "", "alert('手動簡訊發佈成功');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(SendSmsButton, SendSmsButton.GetType(), "", "alert('" + ErrorMessage + "');", true);
        }
    }

    private void ChangeSelectedMembers(List<SmsGroup> selectedSmsGroups)
    {
        // reset
        foreach (ListItem listItem in SmsMemberCheckBoxList.Items)
        {
            listItem.Selected = false;
        }

        List<SmsMember> selectedSmsMembers = MessageController.Instance.GetSmsMembersByGroup(selectedSmsGroups);
        var selectedSmsMemberIDs = selectedSmsMembers.Select(x => x.SmsMemberID).ToList();
        foreach (ListItem listItem in SmsMemberCheckBoxList.Items)
        {
            if (selectedSmsMemberIDs.Contains(Convert.ToInt32(listItem.Value)))
            {
                listItem.Selected = true;
            }
        }
    }

    private List<int> GetSelectedMemberIDs()
    {
        List<int> IDs = new List<int>();
        foreach (ListItem listItem in SmsMemberCheckBoxList.Items)
        {
            if (listItem.Selected)
            {
                IDs.Add(Convert.ToInt32(listItem.Value));
            }
        }
        return IDs;
    }
    private List<SmsMember> GetSendMembers(List<int> selectedMemberIDs, CheckBoxList checkBoxList)
    {
        List<SmsMember> receivers = (from member in AllSmsMembers
                                     where selectedMemberIDs.Contains(member.SmsMemberID)
                                     select member).ToList();
        foreach (ListItem listItem in checkBoxList.Items)
        {
            receivers.Add(new SmsMember
            {
                Name = string.Empty,
                MobileNumber = listItem.Value,
            });
        }
        return receivers;
    }
}