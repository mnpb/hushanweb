﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Message_MaintainSmsCondition : SuperPage
{
    #region Properties

    List<AutoSmsConditionCompoundInfo> AllAutoSmsConditionCompoundInfos
    {
        get
        {
            return MessageController.Instance.GetAllAutoSmsConditionCompoundInfos();
        }
    }

    string PopupTitle = "編輯自動簡訊群組";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowInfos();
        }
    }
    private void ShowInfos()
    {
        InfoGridView.DataSource = AllAutoSmsConditionCompoundInfos;
        InfoGridView.DataBind();
    }

    #region GridView Events

    protected void InfoGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            short autoSmsConditionID = Convert.ToInt16(InfoGridView.DataKeys[e.NewEditIndex].Values["AutoSmsConditionID"].ToString());
            MaintainSmsConditionUserControl.AutoSmsConditionID = autoSmsConditionID;
            Popup.Title = PopupTitle;
            Popup.Show();
        }
        catch
        {
        }
    }

    #endregion

    #region UserControl Events

    protected void MaintainSmsConditionUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowInfos();
    }

    #endregion

    #region Utilities

    protected string GetActiveDescription(string active)
    {
        try
        {
            bool activeBoolean = Convert.ToBoolean(active);
            string image = string.Format("<img src={0} style=vertical-align:middle />", ResolveUrl(IconUtility.GetLightIcon(activeBoolean)));
            return string.Format("{0}({1})", image, IconUtility.GetActiveDescription(activeBoolean));
        }
        catch
        {
            return string.Empty;
        }
    }

    protected string GetSmsGroupsDescription(object o)
    {
        return MessageController.Instance.GetSmsGroupsDescription(o);
    }

    #endregion

    protected void InfoGridView_PreRender(object sender, EventArgs e)
    {
        var allRows = (sender as GridView).Rows;
        var allInfos = AllAutoSmsConditionCompoundInfos;
        List<short> allTypeIDs = allInfos.Select(x => x.AutoSmsTypeID).Distinct().ToList();
        int rowIndex = 0;
        foreach (var typeID in allTypeIDs)
        {
            int count = (from info in allInfos
                         where info.AutoSmsTypeID == typeID
                         select info).Count();
            allRows[rowIndex].Cells[1].RowSpan = count;
            allRows[rowIndex].Cells[1].BackColor = System.Drawing.Color.FromArgb(222, 222, 222);
            for (int idx = 1; idx < count; idx++)
            {
                allRows[rowIndex+idx].Cells[1].Visible = false;
            }
            rowIndex += count;
        }
        //foreach (GridViewRow gridViewRow in (sender as GridView).Rows)
        //{
        //    int autoSmsConditionID = Convert.ToInt32(gridViewRow.Cells[2].Text);
        //    var theTypeID = (from info in allInfos
        //                     where info.AutoSmsConditionID == autoSmsConditionID
        //                     select info.AutoSmsTypeID).FirstOrDefault();
        //    //gridViewRow.Cells[1].RowSpan = 4;
        //    //gridViewRow[1].Cells[1].Visible = false;
        //    //gridViewRow[2].Cells[1].Visible = false;
        //    //gridViewRow[3].Cells[1].Visible = false;
        //    //gridViewRow[GridViewRowIndex].Cells[0].BackColor = System.Drawing.Color.FromArgb(222, 222, 222);
        //}


    }
}