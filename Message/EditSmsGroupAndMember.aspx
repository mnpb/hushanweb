﻿<%@ Page Title="通訊錄編輯" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditSmsGroupAndMember.aspx.cs" Inherits="Message_EditSmsGroupAndMember" %>

<%--<%@ Register Src="../WebUserControlers/SMSManagement/RadioButtonSmsGroup.ascx" TagName="RadioButtonSmsGroup" TagPrefix="uc1" %>
<%@ Register Src="../WebUserControlers/SMSManagement/CheckBoxSmsMember.ascx" TagName="CheckBoxSmsMember" TagPrefix="uc2" %>--%>
<%@ Register Src="~/WebUserControlers/Popup.ascx" TagName="Popup" TagPrefix="uc3" %>
<%@ Register Src="~/Message/UserControls/SmsGroupList.ascx" TagName="SmsGroupList" TagPrefix="uc4" %>
<%@ Register Src="~/Message/UserControls/SmsMemberList.ascx" TagName="SmsMemberList" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/cleartable.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <table class="fancytable">
            <tr>
                <th>步驟一：選取欲編輯的簡訊群組</th>
                <th>步驟二：設定所選簡訊群組的接收人員清單</th>
                <th>最後步驟：執行設定內容</th>
            </tr>
            <tr>
                <th>現有簡訊群組清單</th>
                <th>現有簡訊接收人員清單</th>
                <th>更新群組資訊</th>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <asp:Panel ID="Panel1" runat="server" Width="380px" Height="360px" ScrollBars="Auto" BackColor="White">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                  <asp:RadioButtonList ID="SmsGroupRadioButtonList" runat="server" AutoPostBack="True" RepeatColumns="3" CssClass="cleartable" OnSelectedIndexChanged="SmsGroupRadioButtonList_SelectedIndexChanged"></asp:RadioButtonList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </td>
                <td style="vertical-align: top">
                    <asp:Panel ID="Panel2" runat="server" Width="450px" Height="360px" ScrollBars="Auto" BackColor="White">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="SmsMemberCheckBoxList" runat="server" CssClass="cleartable" RepeatColumns="4" RepeatDirection="Horizontal" ></asp:CheckBoxList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </td>
                <td rowspan="2" style="vertical-align: top">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Button ID="UpdateSmsGroupToMemberButton" runat="server" Text="確認修改" OnClick="UpdateSmsGroupToMemberButton_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Button ID="EditSmsGroupButton" runat="server" Text="編輯簡訊群組" OnClick="EditSmsGroupButton_Click" />
                </td>
                <td style="text-align: left">
                    <asp:Button ID="EditSmsMemberButton" runat="server" Text="編輯簡訊接收人員" OnClick="EditSmsMemberButton_Click" />
                </td>
            </tr>
        </table>
    </center>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc3:Popup ID="SmsGroupPopup" runat="server">
                <ContentTemplate>
                    <uc4:SmsGroupList ID="SmsGroupList1" runat="server"  OnCompleted="SmsGroupList_Completed" />
                </ContentTemplate>
            </uc3:Popup>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="EditSmsGroupButton" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc3:Popup ID="SmsMemberPopup" runat="server">
                <ContentTemplate>
                    <uc5:SmsMemberList ID="SmsMemberList1" runat="server" OnCompleted="SmsMemberList_Completed" />
                </ContentTemplate>
            </uc3:Popup>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="EditSmsMemberButton" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

