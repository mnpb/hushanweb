﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Message_UserControls_SmsMemberList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowAllSmsMembers(SmsMemberGridView, 0);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        SmsMemberPanel.Visible = !SmsMemberFormView.Visible;
    }

    private void ShowAllSmsMembers(GridView gridView, int pageIndex)
    {
        try
        {
            gridView.DataSource = MessageController.Instance.GetAllSmsMembers();
            gridView.PageIndex = pageIndex;
            gridView.DataBind();
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    protected void SmsMemberGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView gridView = (sender as GridView);

        // 更新動作
        string newName = ((TextBox)gridView.Rows[e.RowIndex].FindControl("NameTextBox")).Text;
        if (string.IsNullOrEmpty(newName))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "alert('姓名不可空白');", true);
            return;
        }
        string newMobileNumber = ((TextBox)gridView.Rows[e.RowIndex].FindControl("MobileNumberTextBox")).Text;
        if (string.IsNullOrEmpty(newMobileNumber))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "alert('手機號碼不可空白');", true);
            return;
        }

        if (!MessageController.Instance.AddSmsMember(new SmsMember
        {
            SmsMemberID = Convert.ToInt16(gridView.DataKeys[e.RowIndex].Values["SmsMemberID"].ToString()),
            Name = newName,
            MobileNumber = newMobileNumber,
        }))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "window.setTimeout(\"alert('更新群組失敗')\", 10);", true);
        }
        else
        {
            InvokeCompleted();
        }
        gridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
        ShowAllSmsMembers(gridView, gridView.PageIndex);
    }
    protected void SmsMemberGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView gridView = (sender as GridView);
        gridView.EditIndex = e.NewEditIndex;
        ShowAllSmsMembers(gridView, gridView.PageIndex);
    }
    protected void SmsMemberGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView gridView = (sender as GridView);
        gridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
        ShowAllSmsMembers(gridView, gridView.PageIndex);
    }
    protected void SmsMemberGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gridView = (sender as GridView);
        ShowAllSmsMembers(gridView, e.NewPageIndex);
    }
    protected void SmsMemberGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gridView = sender as GridView;
        try
        {
            int smsMemberID = Convert.ToInt32(gridView.DataKeys[e.RowIndex].Values["SmsMemberID"].ToString());
            if (!MessageController.Instance.DeletetSmsMember(smsMemberID))
            {
                ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "window.setTimeout(\"alert('刪除簡訊接收人員失敗')\", 10);", true);
            }
            else
            {
                InvokeCompleted();
            }
        }
        catch
        {
        }
        finally
        {
            gridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowAllSmsMembers(gridView, 0);
        }
    }

    protected void AddNewSmsMemberButton_Click(object sender, EventArgs e)
    {
        SmsMemberFormView.Visible = true;
    }
    protected void InsertCancelButton_Click(object sender, EventArgs e)
    {
        SmsMemberFormView.Visible = false;
    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(NameTextBox.Text))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "alert('姓名不可空白');", true);
            return;
        }
        if (string.IsNullOrEmpty(MobileNumberTextBox.Text))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "alert('手機號碼不可空白');", true);
            return;
        }

        if (!MessageController.Instance.AddSmsMember(new SmsMember
        {
            Name = NameTextBox.Text,
            MobileNumber = MobileNumberTextBox.Text,
        }))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "window.setTimeout(\"alert('新增簡訊接收人員失敗')\", 10);", true);
        }
        else
        {
            InvokeCompleted();
        }
        SmsMemberFormView.Visible = false;
        ShowAllSmsMembers(SmsMemberGridView, SmsMemberGridView.PageIndex);
    }

    private void InvokeCompleted()
    {
        if (Completed != null)
        {
            Completed(this, null);
        }
    }
}