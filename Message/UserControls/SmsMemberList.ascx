﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmsMemberList.ascx.cs" Inherits="Message_UserControls_SmsMemberList" %>

<asp:Panel ID="SmsMemberPanel" runat="server">
    <asp:Button ID="AddNewSmsMemberButton" runat="server" Text="新增人員" OnClick="AddNewSmsMemberButton_Click" />
    <asp:GridView ID="SmsMemberGridView" runat="server" CssClass="fancytable" AutoGenerateColumns="False"
        Width="350px" DataKeyNames="SmsMemberID" OnRowUpdating="SmsMemberGridView_RowUpdating" AllowPaging="True" PageSize="5" OnRowEditing="SmsMemberGridView_RowEditing" OnRowCancelingEdit="SmsMemberGridView_RowCancelingEdit" OnPageIndexChanging="SmsMemberGridView_PageIndexChanging" OnRowDeleting="SmsMemberGridView_RowDeleting">
        <Columns>
            <asp:TemplateField HeaderText="姓名" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="10" Width="90px"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="手機號碼" SortExpression="MobileNumber">
                <EditItemTemplate>
                    <asp:TextBox ID="MobileNumberTextBox" runat="server" Text='<%# Bind("MobileNumber") %>' MaxLength="15" Width="90px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="MobileNumberTextBox_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="MobileNumberTextBox">
                    </ajaxToolkit:FilteredTextBoxExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="MobileNumberLabel" runat="server" Text='<%# Bind("MobileNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="編輯" ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="UpdateLinkButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="CancelLinkButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="刪除LinkButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" OnClientClick="return confirm('您確定要刪除該簡訊接收人員嗎？');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="cleartable" />
    </asp:GridView>
</asp:Panel>

<table runat="server" id="SmsMemberFormView" class="fancytable" style="width: 280px" visible="false">
    <tr>
        <th style="text-align: right">接收人姓名</th>
        <td style="text-align: left">
            <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="10" Width="90px" /></td>
    </tr>
    <tr>
        <th style="text-align: right">手機號碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="MobileNumberTextBox" runat="server" Text='<%# Bind("MobileNumber") %>' MaxLength="10" Width="90px" />
            <ajaxToolkit:FilteredTextBoxExtender ID="MobileNumberTextBox_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="MobileNumberTextBox">
            </ajaxToolkit:FilteredTextBoxExtender>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="新增" OnClick="InsertButton_Click" />&nbsp;
                    <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" OnClick="InsertCancelButton_Click" />
        </td>
    </tr>
</table>