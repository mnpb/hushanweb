﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmsGroupList.ascx.cs" Inherits="Message_UserControls_SmsGroupList" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/cleartable.css") %>" rel="stylesheet" />

<asp:Panel ID="SmsGroupGridViewPanel" runat="server">
    <asp:Button ID="AddNewSmsGroupButton" runat="server" Text="新增群組" OnClick="AddNewSmsGroupButton_Click" />
    <asp:GridView ID="SmsGroupGridView" runat="server" CssClass="fancytable" AutoGenerateColumns="False" Width="350px"
        DataKeyNames="SmsGroupID" AllowPaging="True" OnRowEditing="SmsGroupGridView_RowEditing" OnRowUpdating="SmsGroupGridView_RowUpdating" OnRowCancelingEdit="SmsGroupGridView_RowCancelingEdit" PageSize="5" OnPageIndexChanging="SmsGroupGridView_PageIndexChanging" OnRowDeleting="SmsGroupGridView_RowDeleting">
        <Columns>
            <asp:TemplateField HeaderText="群組名稱" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="20" Width="120px" />
                    <asp:TextBox ID="SmsGroupIDTextBox" runat="server" Text='<%# Bind("SmsGroupID") %>' Visible="false" />
                    <asp:TextBox ID="IsEnableTextBox" runat="server" Text='<%# Bind("IsEnable") %>' Visible="false" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Width="200px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="編輯" ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="UpdateLinkButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="CancelLinkButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="EditLinkButton" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯"></asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle Width="80px" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="DeleteLinkButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="cleartable" />
    </asp:GridView>
</asp:Panel>
<table runat="server" id="SmsGroupFormView" class="fancytable" style="width: 280px" visible="false">
    <tr>
        <th style="text-align: right">新群組名稱</th>
        <td style="text-align: left">
            <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="20" Width="120px" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="新增" OnClick="InsertButton_Click" />
            <asp:Button ID="CancelButton" runat="server" CausesValidation="False" CommandName="Cancel" OnClick="CancelButton_Click" Text="取消" />
        </td>
    </tr>
</table>