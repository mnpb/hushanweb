﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Message_UserControls_MaintainSmsCondition : System.Web.UI.UserControl
{
    #region Properties

    private int autoSmsConditionID;
    public int AutoSmsConditionID
    {
        get
        {
            return autoSmsConditionID;
        }
        set
        {
            autoSmsConditionID = value;
            ShowAutoSmsConditionCompoundInfo(AutoSmsConditionInfo);
        }
    }

    private AutoSmsConditionCompoundInfo AutoSmsConditionInfo
    {
        get
        {
            return MessageController.Instance.GetAutoSmsConditionCompoundInfo(autoSmsConditionID);
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// 顯示資料
    /// </summary>
    /// <param name="info"></param>
    private void ShowAutoSmsConditionCompoundInfo(AutoSmsConditionCompoundInfo info)
    {
        if (info == null)
        { // 新增
            return;
        }

        try
        {
            ConditionIDLabel.Text = info.AutoSmsConditionID.ToString();
            TypeNameLabel.Text = info.AutoSmsTypeName;
            ConditionNameLabel.Text = info.AutoSmsConditionName;
            IsActiveRadioButtonList.SelectedValue = info.IsActive.ToString();

            ControlUtility.FillData(SmsGroupCheckBoxList, MessageController.Instance.GetAllSmsGroups(), "Name", "SmsGroupID");
            FillAlreadaySetSmsGroups(SmsGroupCheckBoxList, info.SmsGroups);
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newInfo = new AutoSmsConditionCompoundInfo
            {
                AutoSmsConditionID = Convert.ToInt32(ConditionIDLabel.Text),
                IsActive = Boolean.Parse(IsActiveRadioButtonList.SelectedValue),
                SmsGroups = GetCheckedGroups(SmsGroupCheckBoxList),
            };
            if (!MessageController.Instance.SetAutoSmsConditionCompoundInfo(newInfo))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    private void FillAlreadaySetSmsGroups(CheckBoxList checkBoxList, List<SmsGroup> smsGroups)
    {
        foreach (ListItem item in checkBoxList.Items)
        {
            if (smsGroups.Exists(x => x.SmsGroupID == Convert.ToInt32(item.Value)))
            {
                item.Selected = true;
            }
        }
    }
    private List<SmsGroup> GetCheckedGroups(CheckBoxList checkBoxList)
    {
        List<SmsGroup> groups = new List<SmsGroup>();
        foreach (ListItem item in checkBoxList.Items)
        {
            if (item.Selected)
            {
                groups.Add(new SmsGroup
                {
                    SmsGroupID = Convert.ToInt16(item.Value)
                });
            }
        }
        return groups;
    }

    #endregion
}