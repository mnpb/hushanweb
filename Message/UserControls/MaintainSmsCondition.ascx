﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainSmsCondition.ascx.cs" Inherits="Message_UserControls_MaintainSmsCondition" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/cleartable.css") %>" rel="stylesheet" />

<table class="fancytable">
    <tr>
        <th style="text-align: right">警戒項目</th>
        <td style="text-align: left">
            <asp:Label runat="server" ID="TypeNameLabel"></asp:Label>
        </td>
    </tr>
        <tr style="display: none;">
        <th style="text-align: right">簡訊發佈條件ID</th>
        <td style="text-align: left">
            <asp:Label runat="server" ID="ConditionIDLabel"></asp:Label>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">簡訊發佈條件</th>
        <td style="text-align: left">
            <asp:Label runat="server" ID="ConditionNameLabel"></asp:Label>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">是否啟用</th>
        <td style="text-align: left">
            <asp:RadioButtonList ID="IsActiveRadioButtonList" runat="server" CssClass="cleartable" RepeatDirection="Horizontal">
                <asp:ListItem Value="True">是</asp:ListItem>
                <asp:ListItem Value="False">否</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">自動簡訊群組</th>
        <td style="text-align: left">
           <%-- <div style="height: 200px; overflow-y: scroll; text-align: left;">
                <asp:CheckBoxList ID="SmsGroupCheckBoxList" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal" RepeatColumns="5"></asp:CheckBoxList>
            </div>--%>  
                   <asp:CheckBoxList ID="SmsGroupCheckBoxList" runat="server" RepeatColumns="3"
                    RepeatDirection="Vertical" CssClass="cleartable"></asp:CheckBoxList>
        </td>
       
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
