﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Message_UserControls_SmsGroupList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowAllSmsGroups(SmsGroupGridView, 0);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        SmsGroupGridViewPanel.Visible = !SmsGroupFormView.Visible;
    }

    private void ShowAllSmsGroups(GridView gridView, int pageIndex)
    {
        try
        {
            gridView.DataSource = MessageController.Instance.GetAllSmsGroups();
            gridView.PageIndex = pageIndex;
            gridView.DataBind();
        }
        catch
        {
        }
    }

    //protected void SmsGroupGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    GridView GridView = (GridView)sender;

    //    if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
    //    {
    //        LinkButton DeleteLinkButton = (LinkButton)e.Row.FindControl("DeleteLinkButton");

    //        DeleteLinkButton.OnClientClick = "return confirm('您確定要刪除該群組？該群組一旦被刪除後，其對應的簡訊接收人員亦一將被刪除。'); ";
    //    }
    //}

    public event EventHandler Completed;
    protected void SmsGroupGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView gridView = (sender as GridView);

        string newName = ((TextBox)gridView.Rows[e.RowIndex].FindControl("NameTextBox")).Text;
        if (string.IsNullOrEmpty(newName))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "alert('群組名稱不可為空白');", true);
            return;
        }

        if (!MessageController.Instance.AddSmsGroup(new SmsGroup
        {
            SmsGroupID = Convert.ToInt16(gridView.DataKeys[e.RowIndex].Values["SmsGroupID"].ToString()),
            Name = newName,
            IsEnable = true,
        }))
        {
            ScriptManager.RegisterClientScriptBlock(
                InsertButton, InsertButton.GetType(), "", "window.setTimeout(\"alert('更新群組失敗')\", 10);", true);
        }
        else
        {
            InvokeCompleted();
        }
        gridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
        ShowAllSmsGroups(gridView, gridView.PageIndex);
    }
    protected void SmsGroupGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowAllSmsGroups(SmsGroupGridView, e.NewPageIndex);
    }
    protected void SmsGroupGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView gridView = (sender as GridView);
        gridView.EditIndex = e.NewEditIndex;
        ShowAllSmsGroups(gridView, gridView.PageIndex);
    }
    protected void SmsGroupGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView gridView = (sender as GridView);
        gridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
        ShowAllSmsGroups(gridView, gridView.PageIndex);
    }
    protected void SmsGroupGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gridView = sender as GridView;
        try
        {
            short smsGroupID = Convert.ToInt16(gridView.DataKeys[e.RowIndex].Values["SmsGroupID"].ToString());
            if (!MessageController.Instance.DeletetSmsGroup(smsGroupID))
            {
                ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "window.setTimeout(\"alert('刪除群組失敗')\", 10);", true);
            }
            else
            {
                InvokeCompleted();
            }
        }
        catch
        {
        }
        finally
        {
            gridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowAllSmsGroups(gridView, 0);
        }
    }

    protected void AddNewSmsGroupButton_Click(object sender, EventArgs e)
    {
        SmsGroupFormView.Visible = true;
    }
    protected void CancelButton_Click(object sender, EventArgs e)
    {
        SmsGroupFormView.Visible = false;
    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(NameTextBox.Text))
        {
            ScriptManager.RegisterClientScriptBlock(InsertButton, InsertButton.GetType(), "", "alert('群組名稱不可為空白');", true);
            return;
        }

        if (!MessageController.Instance.AddSmsGroup(new SmsGroup
         {
             Name = NameTextBox.Text,
             IsEnable = true,
         }))
        {
            ScriptManager.RegisterClientScriptBlock(
                InsertButton, InsertButton.GetType(), "", "window.setTimeout(\"alert('新增新群組失敗')\", 10);", true);
        }
        else
        {
            InvokeCompleted();
        }
        SmsGroupFormView.Visible = false;
        ShowAllSmsGroups(SmsGroupGridView, SmsGroupGridView.PageIndex);
    }

    private void InvokeCompleted()
    {
        if (Completed != null)
        {
            Completed(this, null);
        }
    }
}