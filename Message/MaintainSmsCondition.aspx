﻿<%@ Page Title="簡訊條件設定" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainSmsCondition.aspx.cs" Inherits="Message_MaintainSmsCondition" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Message/UserControls/MaintainSmsCondition.ascx" TagPrefix="uc1" TagName="MaintainSmsCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainSmsCondition runat="server" ID="MaintainSmsConditionUserControl" OnCompleted="MaintainSmsConditionUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <asp:GridView ID="InfoGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="AutoSmsConditionID" EmptyDataText="沒有任何資料" CssClass="fancytable" OnRowEditing="InfoGridView_RowEditing" OnPreRender="InfoGridView_PreRender">
                <Columns>
                    <asp:TemplateField HeaderText="警戒項目ID" Visible="false">
                        <ItemTemplate><%# Eval("AutoSmsTypeID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="警戒項目" ItemStyle-Width="20%">
                        <ItemTemplate><%# Eval("AutoSmsTypeName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="編號">
                        <ItemTemplate><%# Eval("AutoSmsConditionID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="簡訊發佈條件">
                        <ItemTemplate><%# Eval("AutoSmsConditionName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="是否啟用">
                        <ItemTemplate><%# GetActiveDescription(Eval("IsActive").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="自動簡訊群組"  ItemStyle-Width="30%">
                        <ItemTemplate><%# GetSmsGroupsDescription(Eval("SmsGroups")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

