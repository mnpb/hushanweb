﻿<%@ Page Title="手動簡訊發佈" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SendSms.aspx.cs" Inherits="Message_SendSms" %>

<%--<%@ Register Src="~/Message/UserControls/CheckBoxSmsMember.ascx" TagName="CheckBoxSmsMember" TagPrefix="uc2" %>
<%@ Register Src="~/Message/UserControls/CheckBoxSmsGroup.ascx" TagName="CheckBoxSmsGroup" TagPrefix="uc3" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/cleartable.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <table width="950px" class="fancytable">
            <tr>
                <th colspan="3">步驟一：設定簡訊傳送對象</th>
            </tr>
            <tr>
                <th>1a：選擇簡訊群組</th>
                <th colspan="2">1b：調整簡訊內人員</th>
            </tr>
            <tr>
                <td style="text-align: left; vertical-align: top">
                    <asp:Panel ID="Panel1" runat="server" Height="100px" ScrollBars="Vertical" BackColor="White">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                        <asp:CheckBoxList ID="SmsGroupCheckBoxList" runat="server" AutoPostBack="True" RepeatColumns="2" CssClass="cleartable" OnSelectedIndexChanged="SmsGroupCheckBoxList_SelectedIndexChanged"></asp:CheckBoxList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </td>
                <td colspan="2" style="text-align: left; vertical-align: top">
                    <asp:Panel ID="Panel2" runat="server" Height="100px" ScrollBars="Vertical" BackColor="White">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="SmsMemberCheckBoxList" runat="server" CssClass="cleartable" RepeatColumns="4" RepeatDirection="Horizontal" ></asp:CheckBoxList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <th colspan="3">步驟二：手動加入臨時性簡訊傳送對象</th>
            </tr>
            <tr>
                <th>2a：手動輸入簡訊傳送手機號碼</th>
                <th colspan="2">2b：調整手動輸入手機號碼</th>
            </tr>
            <tr>
                <td style="text-align: left; vertical-align: top">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:TextBox ID="MobileNoTextBox" runat="server" MaxLength="15" Width="120px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="MobileNoTextBox_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="MobileNoTextBox">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <asp:Button ID="AddMobileNoButton" runat="server" Text="手動加入" OnClick="AddMobileNoButton_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td colspan="2" style="text-align: left">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <span style="float: right">
                                <asp:Button ID="DeleteMobileNoButton" runat="server" Text="刪除" OnClick="DeleteMobileNoButton_Click" /></span>
                            <asp:Panel ID="Panel3" runat="server" Width="525px" Height="50px" ScrollBars="Vertical" BackColor="White">
                                <asp:CheckBoxList ID="MobileNoCheckBoxList" runat="server" CssClass="cleartable" RepeatColumns="4" RepeatDirection="Horizontal">
                                </asp:CheckBoxList>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <th colspan="3">
                步驟三：選擇及調整簡訊內容範本
            </tr>
            <tr>
                <th style="width: 35%">3a：選擇範本</th>
                <th style="width: 40%">3b：編輯簡訊傳送內容</th>
                <th style="width: 25%">3c：另存為新範本</th>
            </tr>
            <tr>
                <td style="text-align: left; vertical-align: top">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:DropDownList ID="SmsContentsTemplateDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SmsContentsTemplateDropDownList_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="text-align: left; vertical-align: top">
                    <asp:UpdatePanel ID="UpdatePanel16" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:TextBox ID="SmsContentsTextBox" runat="server" Rows="3" TextMode="MultiLine" Width="450px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="text-align: left; vertical-align: top">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            範本名稱：<asp:TextBox ID="TemplateTextBox" runat="server" MaxLength="30" Width="100px"></asp:TextBox>
                            <asp:Button ID="AddNewSmsContentsTemplateButton" runat="server" Text="另存範本" OnClick="AddNewSmsContentsTemplateButton_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <th colspan="3">最後步驟：執行簡訊傳送</th>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Button ID="SendSmsButton" runat="server" Text="執行簡訊傳送" OnClick="SendSmsButton_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </center>
</asp:Content>

