﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_KeyForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //GateOpraOutFlow2Tr.Visible = false;
        //GateOpraDiv16.Visible = false;
        if (!IsPostBack)
        {
            AreaValidator(true);
            GateApplyValidator(false);
            GateOpraValidator(false);
        }
        InitForm();
    }

    private void InitForm()
    {
        DateTime now = DateTime.Now;
        AreaDateLb.Text = now.ToString("yyyy/MM/dd HH:mm:ss");
        GateApplyDateLb.Text = now.ToString("yyyy/MM/dd HH:mm:ss");
        GateOpraDateLb.Text = now.ToString("yyyy/MM/dd HH:mm:ss");
    }

    private void AreaValidator(bool ivEnable)
    {
        AreaJobValidator.Enabled = ivEnable;
        AreaPlaceValidator.Enabled = ivEnable;
        AreaPepoleCountFieldValidator.Enabled = ivEnable;
        AreaPepoleCountCompareValidator.Enabled = ivEnable;
        AreaSTDateTimeFieldValidator.Enabled = ivEnable;
        AreaEndDateTimeFieldValidator.Enabled = ivEnable;
        AreaApplyCompanyFieldValidator.Enabled = ivEnable;
        AreaApplyPersonPhoneFieldValidator.Enabled = ivEnable;
        AreaGuardFieldValidator.Enabled = ivEnable;
        AreaDealPepoleFieldValidator.Enabled = ivEnable;
        AreaOfficerFieldValidator.Enabled = ivEnable;
        AreaOrgTbFieldValidator.Enabled = ivEnable;
    }

    private void GateApplyValidator(bool ivEnable)
    {
        GateApplySTDateTimeValidator.Enabled = ivEnable;
        GateApplyEndDateTimeValidator.Enabled = ivEnable;
        GateApplyPlaceTbValidator.Enabled = ivEnable;
        GateApplyJobTbValidator.Enabled = ivEnable;
        GateApplyCompanyTbValidator.Enabled = ivEnable;
        GateApplyPersonTbValidator.Enabled = ivEnable;
        GateApplyPersonPhoneTbValidator.Enabled = ivEnable;
        GateApplyGuardTbValidator.Enabled = ivEnable;
        GateApplyGateOpratePersonTbValidator.Enabled = ivEnable;
        GateApplyOfficerTbValidator.Enabled = ivEnable;
        GateApplyFinishJobTimeTbValidator.Enabled = ivEnable;
        VS2.Enabled = ivEnable;
    }

    private void GateOpraValidator(bool ivEnable)
    {
        GateOpraJobValidator.Enabled = ivEnable;
        GateOpraLYTWaterLevelValidator.Enabled = ivEnable;
        GateOpraLYTWaterLevelCompareValidator.Enabled = ivEnable;
        GateOpraYanWaterLevelValidator.Enabled = ivEnable;
        GateOpraYanWaterLevelCompareValidator.Enabled = ivEnable;
        GateOpraSwitchDateTimeValidator.Enabled = ivEnable;
        GateOpraGateValidator.Enabled = ivEnable;
        GateOpraHoursValidator.Enabled = ivEnable;
        GateOpraHoursCompareValidator.Enabled = ivEnable;
        GateOpraWaterSupplyValidator.Enabled = ivEnable;
        GateOpraWaterSupplyCompareValidator.Enabled = ivEnable;
        GateOpraTimeValidator.Enabled = ivEnable;
        GateOpraOutFlow1_1CompareValidator.Enabled = ivEnable;
        GateOpraOutFlow1_2CompareValidator.Enabled = ivEnable;
        GateOpraOutFlow2_1CompareValidator.Enabled = ivEnable;
        //GateOpraOutFlow2_2CompareValidator.Enabled = ivEnable;
        GateOpraOutFlowValidator.Enabled = ivEnable;
        GateOpraOutFlowCompareValidator.Enabled = ivEnable;
        GateOpraDeployPersonValidator.Enabled = ivEnable;
        GateOpraPoliceValidator.Enabled = ivEnable;
        GateOpraPersonValidator.Enabled = ivEnable;
        GateOpraManagerValidator.Enabled = ivEnable;
        VS3.Enabled = ivEnable;
    }

    protected void Tab_OnActiveTabChanged(object sender, EventArgs e)
    {
        int tabIndex = TabContainer1.ActiveTabIndex;
        switch (tabIndex)
        {
            case 0:
                AreaValidator(true);
                GateApplyValidator(false);
                GateOpraValidator(false);
                break;
            case 1:
                AreaValidator(false);
                GateApplyValidator(true);
                GateOpraValidator(false);
                break;
            case 2:
                AreaValidator(false);
                GateApplyValidator(false);
                GateOpraValidator(true);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 湖山水庫管制區進入作業申請單
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AreaOKBt_Click(object sender, EventArgs e)
    {
        DateTime beginTime = Convert.ToDateTime(AreaSTDateTime.Value.ToString());
        DateTime endTime = Convert.ToDateTime(AreaEndDateTime.Value.ToString());

        if (beginTime > endTime)
        {
            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "預定時間(起)不得大於預定時間(訖)");
            return;
        }

        FormArea formArea = new FormArea
        {
            AreaJob = AreaJobTb.Text,
            AreaPlace = AreaPlaceTb.Text,
            AreaPepoleCount = Convert.ToInt16(AreaPepoleCountTb.Text),
            AreaSTDateTime = beginTime,
            AreaEndDateTime = endTime,
            AreaApplyCompany = AreaApplyCompanyTb.Text,
            AreaApplyPersonPhone = AreaApplyPersonPhoneTb.Text,
            AreaGuard = AreaGuardTb.Text,
            AreaDealPepole = AreaDealPepoleTb.Text,
            AreaOfficer = AreaOfficerTb.Text,
            AreaOrg = AreaOrgTb.Text,
            ApplyTime = Convert.ToDateTime(AreaDateLb.Text),
            CarNo = CarNoTb.Text,
            Sentry = SentryTb.Text,
        };
        bool addSuccess = FormController.Instance.AddAreaForm(formArea);
        if (addSuccess)
        {
            AreaJobTb.Text = string.Empty;
            AreaPlaceTb.Text = string.Empty;
            AreaPepoleCountTb.Text = string.Empty;
            AreaSTDateTime.Value = string.Empty;
            AreaEndDateTime.Value = string.Empty;
            AreaApplyCompanyTb.Text = string.Empty;
            AreaApplyPersonPhoneTb.Text = string.Empty;
            AreaGuardTb.Text = string.Empty;
            AreaDealPepoleTb.Text = string.Empty;
            AreaOfficerTb.Text = string.Empty;
            AreaOrgTb.Text = string.Empty;
            CarNoTb.Text = string.Empty;
            SentryTb.Text = string.Empty;

            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "申請成功！");
        }
        else
        {
            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "申請失敗！");
        }
    }

    /// <summary>
    /// 湖山水庫管理中心－閘門啟閉申請單
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GateApplyOKBt_Click(object sender, EventArgs e)
    {
        DateTime beginTime = Convert.ToDateTime(GateApplySTDateTime.Value);
        DateTime endTime = Convert.ToDateTime(GateApplyEndDateTime.Value);
        if (beginTime > endTime)
        {
            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "時間(起)不得大於時間(訖)");
            return;
        }

        FormGate formGate = new FormGate
        {
            GateApplySTDateTime = beginTime,
            GateApplyEndDateTime = endTime,
            GateApplyPlace = GateApplyPlaceTb.Text,
            GateApplyJob = GateApplyJobTb.Text,
            GateApplyCompany = GateApplyCompanyTb.Text,
            GateApplyPerson = GateApplyPersonTb.Text,
            GateApplyPersonPhone = GateApplyPersonPhoneTb.Text,
            GateApplyGuard = GateApplyGuardTb.Text,
            GateApplyGateOpratePerson = GateApplyGateOpratePersonTb.Text,
            GateApplyOfficer = GateApplyOfficerTb.Text,
            GateApplyFinishJobTime = Convert.ToDateTime(GateApplyFinishJobTimeTb.Value),
            ApplyTime = Convert.ToDateTime(GateApplyDateLb.Text),
        };
        bool addSuccess = FormController.Instance.AddGateApplyForm(formGate);
        if (addSuccess)
        {
            GateApplySTDateTime.Value = string.Empty;
            GateApplyEndDateTime.Value = string.Empty;
            GateApplyPlaceTb.Text = string.Empty;
            GateApplyJobTb.Text = string.Empty;
            GateApplyCompanyTb.Text = string.Empty;
            GateApplyPersonTb.Text = string.Empty;
            GateApplyPersonPhoneTb.Text = string.Empty;
            GateApplyGuardTb.Text = string.Empty;
            GateApplyGateOpratePersonTb.Text = string.Empty;
            GateApplyOfficerTb.Text = string.Empty;
            GateApplyFinishJobTimeTb.Value = string.Empty;

            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "申請成功！");
        }
        else
        {
            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "申請失敗！");
        }
    }

    //湖山水庫出水工閘門操作紀錄表 
    protected void GateOpraOKBt_Click(object sender, EventArgs e)
    {
        FormGateOperate formGateOperate = new FormGateOperate
        {
            ApplyTime = Convert.ToDateTime(GateOpraDateLb.Text),
            GateOpraJob = GateOpraJobTb.Text,
            GateOpraLYTWaterLevel = Convert.ToDouble(GateOpraLYTWaterLevelTb.Text),
            GateOpraYanWaterLevel = Convert.ToDouble(GateOpraYanWaterLevelTb.Text),
            GateOpraSwitchDateTime = Convert.ToDateTime(GateOpraSwitchDateTimeTb.Value),
            GateOpraGate = GateOpraGateTb.Text,
            GateOpraHours = Convert.ToDouble(GateOpraHoursTb.Text),
            GateOpraWaterSupply = Convert.ToDouble(GateOpraWaterSupplyTb.Text),
            GateOpraDeployPerson = GateOpraDeployPersonTb.Text,
            GateOpraPolice = GateOpraPoliceTb.Text,
            GateOpraManager = GateOpraManagerTb.Text,
            GateOpraOutFlow1_1 = GateOpraOutFlow1_1Tb.Text,
            GateOpraOutFlow1_2 = GateOpraOutFlow1_2Tb.Text,
            GateOpraOutFlow2_1 = GateOpraOutFlow2_1Tb.Text,
            //GateOpraOutFlow2_2 = GateOpraOutFlow2_2Tb.Text,
            GateOpraTime = Convert.ToDateTime(GateOpraTimeTb.Value),
            GateOpraOutFlow = Convert.ToDouble( GateOpraOutFlowTb.Text),
            GateOpraPerson = GateOpraPersonTb.Text,
        };
        bool addSuccess = FormController.Instance.AddGateOpraForm(formGateOperate);
        if (addSuccess)
        {
            GateOpraJobTb.Text = string.Empty;
            GateOpraLYTWaterLevelTb.Text = string.Empty;
            GateOpraYanWaterLevelTb.Text = string.Empty;
            GateOpraSwitchDateTimeTb.Value = string.Empty;
            GateOpraGateTb.Text = string.Empty;
            GateOpraHoursTb.Text = string.Empty;
            GateOpraWaterSupplyTb.Text = string.Empty;
            GateOpraTimeTb.Value = string.Empty;
            GateOpraOutFlow1_1Tb.Text = string.Empty;
            GateOpraOutFlow1_2Tb.Text = string.Empty;
            GateOpraOutFlow2_1Tb.Text = string.Empty;
            //GateOpraOutFlow2_2Tb.Text = string.Empty;
            GateOpraOutFlowTb.Text = string.Empty;
            GateOpraDeployPersonTb.Text = string.Empty;
            GateOpraPoliceTb.Text = string.Empty;
            GateOpraPersonTb.Text = string.Empty;
            GateOpraManagerTb.Text = string.Empty;

            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "申請成功！");
        }
        else
        {
            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "申請失敗！");
        }
    }
}