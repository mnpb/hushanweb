﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_ViewPrintArea : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int areaID = Convert.ToInt32(Request.QueryString["AreaID"].ToString());
        var info = FormController.Instance.GetAreaForm(areaID);
        if (info == null)
        {
            msg("無法瀏覽與列印");
        }
        else
        {
            Area(info);
        }
    }

    private void msg(string ss)
    {
        this.Response.Write("<script>");
        this.Response.Write("alert('" + ss + "')");
        this.Response.Write("</script>");
    }

    private void Area(FormArea info)
    {
        try
        {
            string output = string.Empty;
            using (StreamReader sr = new StreamReader(Server.MapPath("Data/Area.html"), Encoding.GetEncoding("BIG5")))
            {
                while (sr.EndOfStream == false)
                {
                    string line = sr.ReadLine();
                    output = output + line;
                }
            }

            //申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //工作內容
            output = output.Replace("$Job", info.AreaJob);

            //位置
            output = output.Replace("$Place", info.AreaPlace);

            //人數
            output = output.Replace("$Pepole", info.AreaPepoleCount.ToString());

            //預定時間
            DateTime sttime = info.AreaSTDateTime;
            DateTime endtime = info.AreaEndDateTime;

            output = output.Replace("$SYear", (sttime.Year - 1911).ToString());
            output = output.Replace("$SMonth", sttime.Month.ToString());
            output = output.Replace("$SDay", sttime.Day.ToString());
            output = output.Replace("$STime", sttime.Hour.ToString() + ":" + sttime.Minute.ToString() + ":" + sttime.Second.ToString());

            output = output.Replace("$EYear", (endtime.Year - 1911).ToString());
            output = output.Replace("$EMonth", endtime.Month.ToString());
            output = output.Replace("$EDay", endtime.Day.ToString());
            output = output.Replace("$ETime", endtime.Hour.ToString() + ":" + endtime.Minute.ToString() + ":" + endtime.Second.ToString());

            // 申請單位或廠商
            output = output.Replace("$company", info.AreaApplyCompany);

            //申請人手機
            output = output.Replace("$Phone", info.AreaApplyPersonPhone);

            //管制區經辦人
            output = output.Replace("$DealPepole", info.AreaDealPepole);

            //主任
            output = output.Replace("$Officer", info.AreaOfficer);

            //中水局主辦課室
            output = output.Replace("$Org", info.AreaOrg);

            //警勤室
            output = output.Replace("$Guard", info.AreaGuard);

            //登陸車牌
            output = output.Replace("$CarNo", info.CarNo);

            //保全崗哨
            output = output.Replace("$Sentry", info.Sentry + "&nbsp;&nbsp;");

            Literal outresult = new Literal();
            outresult.Text = output;

            block.Controls.Add(outresult);
        }
        catch
        {
        }
    }
}