﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_ViewPrintGateApply : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int gateApplyID = Convert.ToInt32(Request.QueryString["GateApplyID"].ToString());
        var info = FormController.Instance.GetGateApplyForm(gateApplyID);
        if (info == null)
        {
            msg("無法瀏覽與列印");
        }
        else
        {
            GateApply(info);
        }
    }

    private void msg(string ss)
    {
        this.Response.Write("<script>");
        this.Response.Write("alert('" + ss + "')");
        this.Response.Write("</script>");
    }

    private void GateApply(FormGate info)
    {
        try
        {
            string output = string.Empty;
            using (StreamReader sr = new StreamReader(Server.MapPath("Data/GateApply.html"), Encoding.GetEncoding("BIG5")))
            {
                while (sr.EndOfStream == false)
                {
                    string line = sr.ReadLine();
                    output = output + line;
                }
            }

            //申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //預定時間
            DateTime sttime = info.GateApplySTDateTime;
            DateTime endtime = info.GateApplyEndDateTime;

            output = output.Replace("$SYear", (sttime.Year - 1911).ToString());
            output = output.Replace("$SMonth", sttime.Month.ToString());
            output = output.Replace("$SDay", sttime.Day.ToString());
            output = output.Replace("$STime", sttime.Hour.ToString() + " : " + sttime.Minute.ToString() + " : " + sttime.Second.ToString());

            output = output.Replace("$EYear", (endtime.Year - 1911).ToString());
            output = output.Replace("$EMonth", endtime.Month.ToString());
            output = output.Replace("$EDay", endtime.Day.ToString());
            output = output.Replace("$ETime", endtime.Hour.ToString() + " : " + endtime.Minute.ToString() + " : " + endtime.Second.ToString());

            //地點
            output = output.Replace("$Place", info.GateApplyPlace);

            //事由
            output = output.Replace("$Job", info.GateApplyJob);

            //申請單位
            output = output.Replace("$Conpany", info.GateApplyCompany);

            //申請人
            output = output.Replace("$ApplePerson", info.GateApplyPerson);

            //聯絡電話
            output = output.Replace("$ApplePhone", info.GateApplyPersonPhone);

            //警勤室
            output = output.Replace("$Guard", info.GateApplyGuard);

            //閘門操控人員
            output = output.Replace("$GatePseron", info.GateApplyGateOpratePerson);

            //單位主管
            output = output.Replace("$Officer", info.GateApplyOfficer);

            //廠商確實完成工作時間
            DateTime finishTime = info.GateApplyFinishJobTime;
            string finTimestr = (finishTime.Year - 1911).ToString() + "年" + (finishTime.Month).ToString() + "月" + (finishTime.Day).ToString() + "日 時間：" + finishTime.Hour.ToString() + " : " + finishTime.Minute.ToString() + " : " + finishTime.Second.ToString();
            output = output.Replace("$FinishTime", finTimestr);

            Literal outresult = new Literal();
            outresult.Text = output;

            block.Controls.Add(outresult);
        }
        catch
        {
        }
    }
}