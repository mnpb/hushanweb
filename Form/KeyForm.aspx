﻿<%@ Page Title="日常表單登打" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="KeyForm.aspx.cs" Inherits="Form_KeyForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <link href="../css/AjaxTab/ajaxtab.css" rel="stylesheet" />

    <script src="../ThirdPartyComponents/My97DatePicker/WdatePicker.js"></script>
    <script src="../Scripts/WindowsOpen.js"></script>

    <script type="text/javascript">

        //清除湖山水庫管制區進入作業申請單
        function clearAreaMessage() {
            var AreaJobTbobj = document.getElementById("<%= AreaJobTb.ClientID %>");
            AreaJobTbobj.value = "";

            var AreaPlaceTbobj = document.getElementById("<%= AreaPlaceTb.ClientID %>");
            AreaPlaceTbobj.value = "";

            var AreaPepoleCountTbobj = document.getElementById("<%= AreaPepoleCountTb.ClientID %>");
            AreaPepoleCountTbobj.value = "";

            var AreaSTDateTimeobj = document.getElementById("<%= AreaSTDateTime.ClientID %>");
            AreaSTDateTimeobj.value = "";

            var AreaEndDateTimeobj = document.getElementById("<%= AreaEndDateTime.ClientID %>");
            AreaEndDateTimeobj.value = "";

            var AreaApplyCompanyTbobj = document.getElementById("<%= AreaApplyCompanyTb.ClientID %>");
            AreaApplyCompanyTbobj.value = "";

            var AreaApplyPersonPhoneTbobj = document.getElementById("<%= AreaApplyPersonPhoneTb.ClientID %>");
            AreaApplyPersonPhoneTbobj.value = "";

            var AreaGuardTbobj = document.getElementById("<%= AreaGuardTb.ClientID %>");
            AreaGuardTbobj.value = "";

            var AreaDealPepoleTbobj = document.getElementById("<%= AreaDealPepoleTb.ClientID %>");
            AreaDealPepoleTbobj.value = "";

            var AreaOfficerTbobj = document.getElementById("<%= AreaOfficerTb.ClientID %>");
            AreaOfficerTbobj.value = "";

            var AreaOrgTbobj = document.getElementById("<%= AreaOrgTb.ClientID %>");
            AreaOrgTbobj.value = "";

            var CarNoTbobj = document.getElementById("<%= CarNoTb.ClientID %>");
            CarNoTbobj.value = "";

            var SentryTbobj = document.getElementById("<%= SentryTb.ClientID %>");
            SentryTbobj.value = "";
        }

        //清除湖山水庫管理中心－閘門啟閉申請單
        function clearGateApplyMessage() {
            var GateApplySTDateTimeobj = document.getElementById("<%= GateApplySTDateTime.ClientID %>");
            GateApplySTDateTimeobj.value = "";

            var GateApplyEndDateTimeobj = document.getElementById("<%= GateApplyEndDateTime.ClientID %>");
            GateApplyEndDateTimeobj.value = "";

            var GateApplyPlaceTbobj = document.getElementById("<%= GateApplyPlaceTb.ClientID %>");
            GateApplyPlaceTbobj.value = "";

            var GateApplyJobTbobj = document.getElementById("<%= GateApplyJobTb.ClientID %>");
            GateApplyJobTbobj.value = "";

            var GateApplyCompanyTbobj = document.getElementById("<%= GateApplyCompanyTb.ClientID %>");
            GateApplyCompanyTbobj.value = "";

            var GateApplyPersonTbobj = document.getElementById("<%= GateApplyPersonTb.ClientID %>");
            GateApplyPersonTbobj.value = "";

            var GateApplyPersonPhoneTbobj = document.getElementById("<%= GateApplyPersonPhoneTb.ClientID %>");
            GateApplyPersonPhoneTbobj.value = "";

            var GateApplyGuardTbobj = document.getElementById("<%= GateApplyGuardTb.ClientID %>");
            GateApplyGuardTbobj.value = "";

            var GateApplyGateOpratePersonTbobj = document.getElementById("<%= GateApplyGateOpratePersonTb.ClientID %>");
            GateApplyGateOpratePersonTbobj.value = "";

            var GateApplyOfficerTbobj = document.getElementById("<%= GateApplyOfficerTb.ClientID %>");
            GateApplyOfficerTbobj.value = "";

            var GateApplyFinishJobTimeTbobj = document.getElementById("<%= GateApplyFinishJobTimeTb.ClientID %>");
            GateApplyFinishJobTimeTbobj.value = "";
        }


        //清除湖山水庫出水工閘門操作紀錄表
        function clearGateOpraMessage() {
            var GateOpraJobTbobj = document.getElementById("<%= GateOpraJobTb.ClientID %>");
            GateOpraJobTbobj.value = "";

            var GateOpraLYTWaterLevelTbobj = document.getElementById("<%= GateOpraLYTWaterLevelTb.ClientID %>");
            GateOpraLYTWaterLevelTbobj.value = "";

            var GateOpraYanWaterLevelTbobj = document.getElementById("<%= GateOpraYanWaterLevelTb.ClientID %>");
            GateOpraYanWaterLevelTbobj.value = "";

            var GateOpraSwitchDateTimeTbobj = document.getElementById("<%= GateOpraSwitchDateTimeTb.ClientID %>");
            GateOpraSwitchDateTimeTbobj.value = "";

            var GateOpraGateTbobj = document.getElementById("<%= GateOpraGateTb.ClientID %>");
            GateOpraGateTbobj.value = "";

            var GateOpraHoursTbobj = document.getElementById("<%= GateOpraHoursTb.ClientID %>");
            GateOpraHoursTbobj.value = "";

            var GateOpraWaterSupplyTbobj = document.getElementById("<%= GateOpraWaterSupplyTb.ClientID %>");
            GateOpraWaterSupplyTbobj.value = "";

            var GateOpraTimeTbobj = document.getElementById("<%= GateOpraTimeTb.ClientID %>");
            GateOpraTimeTbobj.value = "";

            var GateOpraOutFlow1_1Tbobj = document.getElementById("<%= GateOpraOutFlow1_1Tb.ClientID %>");
            GateOpraOutFlow1_1Tbobj.value = "";

            var GateOpraOutFlow1_2Tbobj = document.getElementById("<%= GateOpraOutFlow1_2Tb.ClientID %>");
            GateOpraOutFlow1_2Tbobj.value = "";

            var GateOpraOutFlow2_1Tbobj = document.getElementById("<%= GateOpraOutFlow2_1Tb.ClientID %>");
            GateOpraOutFlow2_1Tbobj.value = "";

<%--            var GateOpraOutFlow2_2Tbobj = document.getElementById("<%= GateOpraOutFlow2_2Tb.ClientID %>");
            GateOpraOutFlow2_2Tbobj.value = "";--%>

            var GateOpraOutFlowTbobj = document.getElementById("<%= GateOpraOutFlowTb.ClientID %>");
            GateOpraOutFlowTbobj.value = "";

            var GateOpraDeployPersonTbobj = document.getElementById("<%= GateOpraDeployPersonTb.ClientID %>");
            GateOpraDeployPersonTbobj.value = "";

            var GateOpraPoliceTbobj = document.getElementById("<%= GateOpraPoliceTb.ClientID %>");
            GateOpraPoliceTbobj.value = "";

            var GateOpraPersonTbobj = document.getElementById("<%= GateOpraPersonTb.ClientID %>");
            GateOpraPersonTbobj.value = "";

            var GateOpraManagerTbobj = document.getElementById("<%= GateOpraManagerTb.ClientID %>");
            GateOpraManagerTbobj.value = "";
        }
    </script>
    <script type="text/javascript">

        //預覽湖山水庫管制區進入作業申請單
        function AreaView(url) {

            var keys = [];
            var values = [];

            var AreaJobTbobj = document.getElementById("<%= AreaJobTb.ClientID %>");
            var AreaPlaceTbobj = document.getElementById("<%= AreaPlaceTb.ClientID %>");
            var AreaPepoleCountTbobj = document.getElementById("<%= AreaPepoleCountTb.ClientID %>");
            var AreaSTDateTimeobj = document.getElementById("<%= AreaSTDateTime.ClientID %>");
            var AreaEndDateTimeobj = document.getElementById("<%= AreaEndDateTime.ClientID %>");
            var AreaApplyCompanyTbobj = document.getElementById("<%= AreaApplyCompanyTb.ClientID %>");
            var AreaApplyPersonPhoneTbobj = document.getElementById("<%= AreaApplyPersonPhoneTb.ClientID %>");
            var AreaGuardTbobj = document.getElementById("<%= AreaGuardTb.ClientID %>");
            var AreaDealPepoleTbobj = document.getElementById("<%= AreaDealPepoleTb.ClientID %>");
            var AreaOfficerTbobj = document.getElementById("<%= AreaOfficerTb.ClientID %>");
            var AreaOrgTbobj = document.getElementById("<%= AreaOrgTb.ClientID %>");
            var CarNoTbobj = document.getElementById("<%= CarNoTb.ClientID %>");
            var SentryTbobj = document.getElementById("<%= SentryTb.ClientID %>");

            keys[0] = "AreaJobTb";
            keys[1] = "AreaPlaceTb";
            keys[2] = "AreaPepoleCountTb";
            keys[3] = "AreaSTDateTime";
            keys[4] = "AreaEndDateTime";
            keys[5] = "AreaApplyCompanyTb";
            keys[6] = "AreaApplyPersonPhoneTb";
            keys[7] = "AreaGuardTb";
            keys[8] = "AreaDealPepoleTb";
            keys[9] = "AreaOfficerTb";
            keys[10] = "AreaOrgTb";
            keys[11] = "CarNoTb";
            keys[12] = "SentryTb";

            values[0] = AreaJobTbobj.value;
            values[1] = AreaPlaceTbobj.value;
            values[2] = AreaPepoleCountTbobj.value;
            values[3] = AreaSTDateTimeobj.value;
            values[4] = AreaEndDateTimeobj.value;
            values[5] = AreaApplyCompanyTbobj.value;
            values[6] = AreaApplyPersonPhoneTbobj.value;
            values[7] = AreaGuardTbobj.value;
            values[8] = AreaDealPepoleTbobj.value;
            values[9] = AreaOfficerTbobj.value;
            values[10] = AreaOrgTbobj.value;
            values[11] = CarNoTbobj.value;
            values[12] = SentryTbobj.value;

            //保全崗哨非必填 所以迴圈-1
            for (var i = 0; i < keys.length - 1; i++) {
                if (values[i] == null || values[i] == "") {
                    alert("資料填寫不完整！");
                    return;
                }
            }

            openWindowWithPost(url, "view", keys, values);
        }

        //預覽湖山水庫管理中心－閘門啟閉申請單
        function GateApplyView(url) {

            var keys = [];
            var values = [];

            var GateApplySTDateTimeobj = document.getElementById("<%= GateApplySTDateTime.ClientID %>");
            var GateApplyEndDateTimeobj = document.getElementById("<%= GateApplyEndDateTime.ClientID %>");
            var GateApplyPlaceTbobj = document.getElementById("<%= GateApplyPlaceTb.ClientID %>");
            var GateApplyJobTbobj = document.getElementById("<%= GateApplyJobTb.ClientID %>");
            var GateApplyCompanyTbobj = document.getElementById("<%= GateApplyCompanyTb.ClientID %>");
            var GateApplyPersonTbobj = document.getElementById("<%= GateApplyPersonTb.ClientID %>");
            var GateApplyPersonPhoneTbobj = document.getElementById("<%= GateApplyPersonPhoneTb.ClientID %>");
            var GateApplyGuardTbobj = document.getElementById("<%= GateApplyGuardTb.ClientID %>");
            var GateApplyGateOpratePersonTbobj = document.getElementById("<%= GateApplyGateOpratePersonTb.ClientID %>");
            var GateApplyOfficerTbobj = document.getElementById("<%= GateApplyOfficerTb.ClientID %>");
            var GateApplyFinishJobTimeTbobj = document.getElementById("<%= GateApplyFinishJobTimeTb.ClientID %>");


            keys[0] = "GateApplySTDateTime";
            keys[1] = "GateApplyEndDateTime";
            keys[2] = "GateApplyPlaceTb";
            keys[3] = "GateApplyJobTb";
            keys[4] = "GateApplyCompanyTb";
            keys[5] = "GateApplyPersonTb";
            keys[6] = "GateApplyPersonPhoneTb";
            keys[7] = "GateApplyGuardTb";
            keys[8] = "GateApplyGateOpratePersonTb";
            keys[9] = "GateApplyOfficerTb";
            keys[10] = "GateApplyFinishJobTimeTb";


            values[0] = GateApplySTDateTimeobj.value;
            values[1] = GateApplyEndDateTimeobj.value;
            values[2] = GateApplyPlaceTbobj.value;
            values[3] = GateApplyJobTbobj.value;
            values[4] = GateApplyCompanyTbobj.value;
            values[5] = GateApplyPersonTbobj.value;
            values[6] = GateApplyPersonPhoneTbobj.value;
            values[7] = GateApplyGuardTbobj.value;
            values[8] = GateApplyGateOpratePersonTbobj.value;
            values[9] = GateApplyOfficerTbobj.value;
            values[10] = GateApplyFinishJobTimeTbobj.value;


            for (var i = 0; i < keys.length; i++) {
                if (values[i] == null || values[i] == "") {
                    alert("資料填寫不完整！");
                    return;
                }
            }

            openWindowWithPost(url, "view", keys, values);

        }

        //預覽湖山水庫出水工閘門操作紀錄表
        function GateOpraView(url) {

            var keys = [];
            var values = [];

            var GateOpraJobTbobj = document.getElementById("<%= GateOpraJobTb.ClientID %>");
            var GateOpraLYTWaterLevelTbobj = document.getElementById("<%= GateOpraLYTWaterLevelTb.ClientID %>");
            var GateOpraYanWaterLevelTbobj = document.getElementById("<%= GateOpraYanWaterLevelTb.ClientID %>");
            var GateOpraSwitchDateTimeTbobj = document.getElementById("<%= GateOpraSwitchDateTimeTb.ClientID %>");
            var GateOpraGateTbobj = document.getElementById("<%= GateOpraGateTb.ClientID %>");
            var GateOpraHoursTbobj = document.getElementById("<%= GateOpraHoursTb.ClientID %>");

            var GateOpraWaterSupplyTbobj = document.getElementById("<%= GateOpraWaterSupplyTb.ClientID %>");
            var GateOpraTimeTbobj = document.getElementById("<%= GateOpraTimeTb.ClientID %>");

            var GateOpraOutFlow1_1Tbobj = document.getElementById("<%= GateOpraOutFlow1_1Tb.ClientID %>");
            var GateOpraOutFlow1_2Tbobj = document.getElementById("<%= GateOpraOutFlow1_2Tb.ClientID %>");
            var GateOpraOutFlow2_1Tbobj = document.getElementById("<%= GateOpraOutFlow2_1Tb.ClientID %>");
   <%--         var GateOpraOutFlow2_2Tbobj = document.getElementById("<%= GateOpraOutFlow2_2Tb.ClientID %>");--%>

            var GateOpraOutFlowTbobj = document.getElementById("<%= GateOpraOutFlowTb.ClientID %>");
            var GateOpraDeployPersonTbobj = document.getElementById("<%= GateOpraDeployPersonTb.ClientID %>");
            var GateOpraPoliceTbobj = document.getElementById("<%= GateOpraPoliceTb.ClientID %>");
            var GateOpraPersonTbobj = document.getElementById("<%= GateOpraPersonTb.ClientID %>");
            var GateOpraManagerTbobj = document.getElementById("<%= GateOpraManagerTb.ClientID %>");



            keys[0] = "GateOpraJobTb";
            keys[1] = "GateOpraLYTWaterLevelTb";
            keys[2] = "GateOpraYanWaterLevelTb";
            keys[3] = "GateOpraSwitchDateTimeTb";
            keys[4] = "GateOpraGateTb";
            keys[5] = "GateOpraHoursTb";
            keys[6] = "GateOpraWaterSupplyTb";
            keys[7] = "GateOpraTimeTb";
            keys[8] = "GateOpraOutFlow1_1Tb";
            keys[9] = "GateOpraOutFlow1_2Tb";
            keys[10] = "GateOpraOutFlow2_1Tb";
            //keys[11] = "GateOpraOutFlow2_2Tb";
            keys[11] = "GateOpraOutFlowTb";
            keys[12] = "GateOpraDeployPersonTb";
            keys[13] = "GateOpraPoliceTb";
            keys[14] = "GateOpraPersonTb";
            keys[15] = "GateOpraManagerTb";


            values[0] = GateOpraJobTbobj.value;
            values[1] = GateOpraLYTWaterLevelTbobj.value;
            values[2] = GateOpraYanWaterLevelTbobj.value;
            values[3] = GateOpraSwitchDateTimeTbobj.value;
            values[4] = GateOpraGateTbobj.value;
            values[5] = GateOpraHoursTbobj.value;
            values[6] = GateOpraWaterSupplyTbobj.value;
            values[7] = GateOpraTimeTbobj.value;
            values[8] = GateOpraOutFlow1_1Tbobj.value;
            values[9] = GateOpraOutFlow1_2Tbobj.value;
            values[10] = GateOpraOutFlow2_1Tbobj.value;
            //values[11] = GateOpraOutFlow2_2Tbobj.value;
            values[11] = GateOpraOutFlowTbobj.value;
            values[12] = GateOpraDeployPersonTbobj.value;
            values[13] = GateOpraPoliceTbobj.value;
            values[14] = GateOpraPersonTbobj.value;
            values[15] = GateOpraManagerTbobj.value;

            for (var i = 8 ; i <= 11 ; i++) {

                if (values[i] == null || values[i] == "") {
                    values[i] = "--";
                }

            }


            for (var i = 0; i < keys.length; i++) {
                if (values[i] == null || values[i] == "") {
                    alert("資料填寫不完整！");
                    return;
                }
            }

            openWindowWithPost(url, "view", keys, values);

        }

    </script>

    <style type="text/css">
        .style1 {
            height: 20px;
            text-align: center;
        }

        p.11 {
            margin-top: 3.0pt;
            margin-right: 20.6pt;
            margin-bottom: 3.0pt;
            margin-left: 54.0pt;
            text-align: justify;
            text-justify: inter-ideograph;
            line-height: 18.0pt;
            font-size: 16.0pt;
            font-family: "Times New Roman";
        }

        .style2 {
            height: 19px;
            width: 344px;
        }

        .style3 {
            text-align: right;
        }

        .style10 {
        }

        .style12 {
            width: 220px;
        }

        .style14 {
            text-align: center;
        }

        .style15 {
            width: 168px;
        }

        .style16 {
            width: 180px;
            height: 19px;
            text-align: left;
        }

        .style17 {
            width: 144px;
            height: 8px;
            text-align: left;
        }

        .style18 {
            height: 6px;
        }

        .style19 {
            width: 344px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                Width="1000px" AutoPostBack="true"
                CssClass="ajax__tab_technorati-theme"
                OnActiveTabChanged="Tab_OnActiveTabChanged">
                <cc1:TabPanel runat="server" HeaderText="湖山水庫管制區進入作業申請單" ID="TabPanel1">
                    <HeaderTemplate>
                        湖山水庫管制區進入作業申請單-表單登打
                    </HeaderTemplate>
                    <ContentTemplate>
                        <center>
             <table>
             <tr>
                <td valign="top">
                        <table  border=1 style="width:550px; height: 198px;">
            <tr>
                <td class="style1" colspan="2">
                        湖山水庫管制區進入作業申請單
                </td>
            </tr>
            <tr>
                <td class="style3" colspan="2">
                    申請時間：<asp:Label ID="AreaDateLb" runat="server" 
                        Text="2011/03/30"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    工作內容
                    <asp:RequiredFieldValidator   ID="AreaJobValidator" runat="server" ControlToValidate="AreaJobTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaJobTb" runat="server" BackColor="#FFFFCC" Height="50px" 
                        TextMode="MultiLine" Width="344px"></asp:TextBox> 
                </td>
            </tr>
             <tr>
                <td class="style12">
                    工作地點<asp:RequiredFieldValidator ID="AreaPlaceValidator" runat="server" ErrorMessage="" ControlToValidate="AreaPlaceTb" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaPlaceTb" runat="server" BackColor="#FFFFCC" Height="41px" 
                        TextMode="MultiLine" Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    人數<asp:RequiredFieldValidator ID="AreaPepoleCountFieldValidator" runat="server" ControlToValidate="AreaPepoleCountTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="AreaPepoleCountCompareValidator" runat="server" ControlToValidate="AreaPepoleCountTb" Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                </td>
                 <td class="style2">
                    <asp:TextBox ID="AreaPepoleCountTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    預定時間(起訖)<br />
                    <asp:RequiredFieldValidator ID="AreaSTDateTimeFieldValidator" runat="server" ControlToValidate="AreaSTDateTime" ErrorMessage="" ForeColor="Red">(起時不得空白)</asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="AreaEndDateTimeFieldValidator" runat="server" ControlToValidate="AreaEndDateTime" ErrorMessage="" ForeColor="Red">(訖時不得空白)</asp:RequiredFieldValidator>
                </td>
                <td class="style19">
                    <input id="AreaSTDateTime" class="Wdate" 
                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 150px" 
                        type="text"  runat="server" />~<input ID="AreaEndDateTime" runat="server" class="Wdate" 
                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 150px" 
                        type="text" runat="server"> </td>
            </tr>
             <tr>
                <td class="style12">
                    申請單位或廠商<asp:RequiredFieldValidator ID="AreaApplyCompanyFieldValidator" runat="server" ControlToValidate="AreaApplyCompanyTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaApplyCompanyTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    申請人手機<asp:RequiredFieldValidator ID="AreaApplyPersonPhoneFieldValidator" runat="server" ControlToValidate="AreaApplyPersonPhoneTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaApplyPersonPhoneTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    登錄車牌<br /></td>
                <td class="style19">
                    <asp:TextBox ID="CarNoTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    警勤室<asp:RequiredFieldValidator ID="AreaGuardFieldValidator" runat="server" ControlToValidate="AreaGuardTb"  ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaGuardTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    管制區經辦人<asp:RequiredFieldValidator ID="AreaDealPepoleFieldValidator" runat="server" ControlToValidate="AreaDealPepoleTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaDealPepoleTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    主任(或代理人)<asp:RequiredFieldValidator ID="AreaOfficerFieldValidator" runat="server" ControlToValidate="AreaOfficerTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaOfficerTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    中水局主辦課室<asp:RequiredFieldValidator ID="AreaOrgTbFieldValidator" runat="server" ControlToValidate="AreaOrgTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td class="style19">
                    <asp:TextBox ID="AreaOrgTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
            <tr>
                <td class="style12">
                    保全崗哨<br /></td>
                <td class="style19">
                    <asp:TextBox ID="SentryTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style10" align="center" colspan="2">
                    <asp:Button ID="AreaOKBt" runat="server" onclick="AreaOKBt_Click" Text="送出申請單" />
                    <asp:Button ID="AreaClearBt" runat="server"  OnClientClick="clearAreaMessage()" 
                        Text="清除表單內容" />
                    <asp:Button ID="AreaViewBt" runat="server"    OnClientClick="AreaView('AreaView.aspx')"  
                        Text="預覽申請單內容" />
                 </td>
            </tr>
             </table>
                </td>
                <td align="left" valign="top">
                    &nbsp;&nbsp;&nbsp; 備註：<br />
                    <ol>
                        <li>進入人員應由湖山水庫管理中心指派之人員會同開啟入口鑰匙（或操作水門）。&nbsp;</li>
                        <li>奉核後警勤室需通知現場保全人員。</li>
                        <li>所有進入危險區域之工作人員均應穿戴安全防護配備。</li>
                        <li>現場應有負責連繫人員（備妥通訊設備）隨時與本中心值勤人員通知作業地點及作業人數。</li>
                        <li>工作完成，申請人平日必須通知鑰匙管理人員簽名確認。假日必須通知值勤人員簽名確認。</li>
                        <li>申請閘門維護廠商、務必填寫閘門啟閉申請單。</li>    
                    </ol>
                </td>
              </tr>
             </table>
             </center>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" HeaderText="閘門啟閉申請單表格" ID="TabPanel2">
                    <HeaderTemplate>
                        閘門啟閉申請單表格-表單登打
                    </HeaderTemplate>
                    <ContentTemplate>
                        <center>
                <table>
                <tr>
                <td align="left" valign="top">
                 <table  border=1 style="width:570px; height: 198px;">
            <tr>
                <td class="style1" colspan="2">
                       湖山水庫管理中心－閘門啟閉申請單
                </td>
            </tr>
            <tr>
                <td class="style3" colspan="2">
                    申請時間：<asp:Label ID="GateApplyDateLb" runat="server" 
                        Text="2011/03/30"></asp:Label>
                </td>
            </tr>
              <tr>
                <td class="style12">
                    時間(起訖)<br />
                    <asp:RequiredFieldValidator ID="GateApplySTDateTimeValidator" runat="server" ControlToValidate="GateApplySTDateTime" ErrorMessage="" ForeColor="Red">(起時不得空白)</asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="GateApplyEndDateTimeValidator" runat="server" ControlToValidate="GateApplyEndDateTime" ErrorMessage="" ForeColor="Red">(訖時不得空白)</asp:RequiredFieldValidator>
                </td>
                <td>
                    <input id="GateApplySTDateTime" class="Wdate" 
                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 150px;" 
                        type="text"  runat="server" />~<input ID="GateApplyEndDateTime" 
                        runat="server" class="Wdate" 
                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 150px" 
                        type="text"></input> </input>
                    </input>
                    </input>
                    </input>
                   </td>
            </tr>
             <tr>
                <td class="style12">
                    地點<asp:RequiredFieldValidator ID="GateApplyPlaceTbValidator" runat="server" ControlToValidate="GateApplyPlaceTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyPlaceTb" runat="server" BackColor="#FFFFCC" Height="41px" 
                        TextMode="MultiLine" Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    事由<asp:RequiredFieldValidator ID="GateApplyJobTbValidator" runat="server" ControlToValidate="GateApplyJobTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyJobTb" runat="server" BackColor="#FFFFCC" Height="50px" 
                        TextMode="MultiLine" Width="344px"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td class="style12">
                    申請單位（廠商）<asp:RequiredFieldValidator ID="GateApplyCompanyTbValidator" runat="server" ControlToValidate="GateApplyCompanyTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyCompanyTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    申請人員<asp:RequiredFieldValidator ID="GateApplyPersonTbValidator" runat="server" ControlToValidate="GateApplyPersonTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyPersonTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    申請人聯絡電話<asp:RequiredFieldValidator ID="GateApplyPersonPhoneTbValidator" runat="server" ControlToValidate="GateApplyPersonPhoneTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyPersonPhoneTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    警勤室<asp:RequiredFieldValidator ID="GateApplyGuardTbValidator" runat="server" ControlToValidate="GateApplyGuardTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyGuardTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    閘門操控人員<asp:RequiredFieldValidator ID="GateApplyGateOpratePersonTbValidator" runat="server" ControlToValidate="GateApplyGateOpratePersonTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyGateOpratePersonTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    單位主管<asp:RequiredFieldValidator ID="GateApplyOfficerTbValidator" runat="server" ControlToValidate="GateApplyOfficerTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="GateApplyOfficerTb" runat="server" BackColor="#FFFFCC" 
                        Width="344px"></asp:TextBox>
                 </td>
            </tr>
             <tr>
                <td class="style12">
                    廠商確實完成工作時間<asp:RequiredFieldValidator ID="GateApplyFinishJobTimeTbValidator" runat="server" ControlToValidate="GateApplyFinishJobTimeTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                    </td>
                <td>
                    <input id="GateApplyFinishJobTimeTb" class="Wdate" 
                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 160px" 
                        type="text" runat="server"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
             <tr>
                <td class="style10" align="center" colspan="2">
                    <asp:Button ID="GateApplyOKBt" runat="server"  
                        Text="送出申請單" onclick="GateApplyOKBt_Click" />
                    <asp:Button ID="GateApplyClearBt" runat="server" 
                        Text="清除表單內容"  OnClientClick="clearGateApplyMessage()"  />
                    <asp:Button ID="GateApplyViewBt" runat="server" 
                        Text="預覽申請單內容" OnClientClick="GateApplyView('GateApplyView.aspx')" />
                 </td>
            </tr>
             </table>	
             </td>
                <td align="left" valign="top">
                    &nbsp;&nbsp; 備註：
                    <ol>
                        <li>開啟閘門（20CMS以上）時請控制室播放放流警報，並由警勤人員巡查下游安全後，始可操作閘門啟閉。</li>
                        <li>本表一式二份：一份鯉管中心存檔、一份承辦人留存。 </li>
                    </ol> 
                    <asp:ValidationSummary ID="VS2" runat="server" />
                </td>
                </tr>
                </table>
             </center>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" HeaderText="湖山水庫出水工閘門操作紀錄表" ID="TabPanel3">
                    <HeaderTemplate>
                        湖山水庫出水工閘門操作紀錄表-表單登打
                    </HeaderTemplate>
                    <ContentTemplate>
                        <table>
                            <tr align="left">
                                <td align="left" valign="top">
                                    <table border="1" style="width: 600px; height: 180px;">
                                        <tr>
                                            <td class="style1" colspan="2">湖山水庫出水工閘門操作紀錄表
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style3" colspan="2">申請時間：<asp:Label ID="GateOpraDateLb" runat="server"
                                                Text="2011/03/30"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style12">操作事由<asp:RequiredFieldValidator ID="GateOpraJobValidator" runat="server" ControlToValidate="GateOpraJobTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                            <td>
                                                <asp:TextBox ID="GateOpraJobTb" runat="server" BackColor="#FFFFCC" Height="35px"
                                                    TextMode="MultiLine" Width="344px"></asp:TextBox>
                                            </td>
                                            <tr>
                                                <td class="style16">水庫水位(公尺)
                                                    <br /><asp:RequiredFieldValidator ID="GateOpraLYTWaterLevelValidator" runat="server" ControlToValidate="GateOpraLYTWaterLevelTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="GateOpraLYTWaterLevelCompareValidator" runat="server" ControlToValidate="GateOpraLYTWaterLevelTb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                                                </td>
                                                <td class="style2">
                                                    <asp:TextBox ID="GateOpraLYTWaterLevelTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">桶頭堰水位(公尺)
                                                    <br /><asp:RequiredFieldValidator ID="GateOpraYanWaterLevelValidator" runat="server" ControlToValidate="GateOpraYanWaterLevelTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="GateOpraYanWaterLevelCompareValidator" runat="server" ControlToValidate="GateOpraYanWaterLevelTb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraYanWaterLevelTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style17">啟閉時間<asp:RequiredFieldValidator ID="GateOpraSwitchDateTimeValidator" runat="server" ControlToValidate="GateOpraSwitchDateTimeTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td class="style18">
                                                    <input id="GateOpraSwitchDateTimeTb" class="Wdate"
                                                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 160px"
                                                        type="text" runat="server" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></input></td>
                                            </tr>
                                            <tr>
                                                <td class="style12">操作閘門<asp:RequiredFieldValidator ID="GateOpraGateValidator" runat="server" ControlToValidate="GateOpraGateTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraGateTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">放流時間(小時)
                                                    <br /><asp:RequiredFieldValidator ID="GateOpraHoursValidator" runat="server" ControlToValidate="GateOpraHoursTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="GateOpraHoursCompareValidator" runat="server" ControlToValidate="GateOpraHoursTb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraHoursTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">供水量(cms)                                        
                                                    <asp:RequiredFieldValidator ID="GateOpraWaterSupplyValidator" runat="server" ControlToValidate="GateOpraWaterSupplyTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="GateOpraWaterSupplyCompareValidator" runat="server" ControlToValidate="GateOpraWaterSupplyTb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraWaterSupplyTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">資料時間<asp:RequiredFieldValidator ID="GateOpraTimeValidator" runat="server" ControlToValidate="GateOpraTimeTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td>
                                                    <input id="GateOpraTimeTb" runat="server" class="Wdate"
                                                        onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" style="width: 160px"
                                                        type="text" />
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">出水工1-1閘門開度(公分)
                                                    <asp:CompareValidator ID="GateOpraOutFlow1_1CompareValidator" runat="server" ControlToValidate="GateOpraOutFlow1_1Tb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                                                    </td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraOutFlow1_1Tb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">出水工1-2閘門開度(公分)
                                                    <asp:CompareValidator ID="GateOpraOutFlow1_2CompareValidator" runat="server" ControlToValidate="GateOpraOutFlow1_2Tb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraOutFlow1_2Tb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">出水工2-1閘門開度(公分)
                                                    <asp:CompareValidator ID="GateOpraOutFlow2_1CompareValidator" runat="server" ControlToValidate="GateOpraOutFlow2_1Tb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraOutFlow2_1Tb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <%--<tr runat="server" id="GateOpraOutFlow2Tr">
                                                <td class="style12">出水工2-2閘門開度<br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; （公分）</td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraOutFlow2_2Tb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>--%><%--<div id="GateOpraDiv16" runat="server"><asp:CompareValidator ID="GateOpraOutFlow2_2CompareValidator" runat="server" ControlToValidate="GateOpraOutFlow2_2Tb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="＊出水工2-2閘門開度輸入錯誤">＊出水工2-2閘門開度輸入錯誤</asp:CompareValidator><br /></div>--%>
                                            <tr>
                                                <td class="style12">放流量(cms)
                                                <asp:RequiredFieldValidator ID="GateOpraOutFlowValidator" runat="server" ControlToValidate="GateOpraOutFlowTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="GateOpraOutFlowCompareValidator" runat="server" ControlToValidate="GateOpraOutFlowTb" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ErrorMessage="" ForeColor="Red">(輸入錯誤)</asp:CompareValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraOutFlowTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">調配人員
                                                    <asp:RequiredFieldValidator ID="GateOpraDeployPersonValidator" runat="server" ControlToValidate="GateOpraDeployPersonTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraDeployPersonTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">警勤室人員<asp:RequiredFieldValidator ID="GateOpraPoliceValidator" runat="server" ControlToValidate="GateOpraPoliceTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraPoliceTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">操作人員<asp:RequiredFieldValidator ID="GateOpraPersonValidator" runat="server" ControlToValidate="GateOpraPersonTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraPersonTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style12">單位主管<asp:RequiredFieldValidator ID="GateOpraManagerValidator" runat="server" ControlToValidate="GateOpraManagerTb" ErrorMessage="" ForeColor="Red">(不得空白)</asp:RequiredFieldValidator></td>
                                                <td>
                                                    <asp:TextBox ID="GateOpraManagerTb" runat="server" BackColor="#FFFFCC"
                                                        Width="344px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style10" align="center" colspan="2">
                                                    <asp:Button ID="GateOpraOKBt" runat="server"
                                                        Text="送出申請單" OnClick="GateOpraOKBt_Click" />
                                                    <asp:Button ID="GateOpraClearBt" runat="server"
                                                        Text="清除表單內容" OnClientClick="clearGateOpraMessage()" />
                                                    <asp:Button ID="GateOpraViewBt" runat="server"
                                                        Text="預覽申請單內容" OnClientClick="GateOpraView('GateOpraView.aspx')" />
                                                </td>
                                            </tr>
                                    </table>
                                </td>
                                <td align="left" valign="top" class="style15">&nbsp;&nbsp;&nbsp; 備註：
                     <ol>
                         <li>敬會警勤室人員</li>
                         <li>敬會閘門操作人員 </li>
                     </ol>
                                    <asp:ValidationSummary ID="VS3" runat="server" Height="34px" Width="276px" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </cc1:TabPanel>
            </cc1:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
    </center>
</asp:Content>
