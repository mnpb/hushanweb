﻿<%@ Page Title="日常表單紀錄" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SearchForm.aspx.cs" Inherits="Form_SearchForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<%--    <link href="../css/AjaxTab/ajaxtab.css" rel="stylesheet" />--%>

    <script src="../ThirdPartyComponents/My97DatePicker/WdatePicker.js"></script>
    <script src="../Scripts/WindowsOpen.js"></script>

    <style type="text/css">
        p.11 {
            margin-top: 3.0pt;
            margin-right: 20.6pt;
            margin-bottom: 3.0pt;
            margin-left: 54.0pt;
            text-align: justify;
            text-justify: inter-ideograph;
            line-height: 18.0pt;
            font-size: 16.0pt;
            font-family: "Times New Roman";
        }

        .style14 {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<%--        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <asp:Label ID="StTimeLb" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="EndTimeLb" runat="server" Visible="False"></asp:Label>
             查詢時間： 
             <asp:DropDownList ID="StYear" runat="server" 
                 onselectedindexchanged="Date_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>年 <asp:DropDownList ID="StMonth" runat="server"  onselectedindexchanged="Date_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>月 <asp:DropDownList ID="StDay" runat="server"  onselectedindexchanged="Date_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>日 ~ <asp:DropDownList ID="EndYear" runat="server"  onselectedindexchanged="Date_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>年 <asp:DropDownList ID="EndMonth" runat="server"  onselectedindexchanged="Date_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>月 <asp:DropDownList ID="EndDay" runat="server"  onselectedindexchanged="Date_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>日

    <br />
    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
        Width="980px" AutoPostBack="true"
        OnActiveTabChanged="Tab_OnActiveTabChanged">
        <cc1:TabPanel runat="server" HeaderText="湖山水庫管制區進入作業申請單" ID="TabPanel1">
            <HeaderTemplate>
                湖山水庫管制區進入作業申請單-記錄查詢
            </HeaderTemplate>
            <ContentTemplate>
                <table>
                    <tr>
                        <td valign="top">
                            <asp:GridView ID="AreaGridView" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="AreaID" DataSourceID="SqlDataSource1"
                                EnableModelValidation="True" GridLines="Vertical"
                                EmptyDataText="此時段無資料" Width="432px">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <Columns>
                                    <asp:BoundField DataField="AreaID" HeaderText="AreaID" InsertVisible="False"
                                        ReadOnly="True" SortExpression="AreaID" Visible="False" />
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True">
                                        <HeaderStyle Width="2%" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="AreaApplyCompany" HeaderText="申請人"
                                        SortExpression="AreaApplyCompany">
                                        <HeaderStyle Width="3%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaApplyPersonPhone" HeaderText="聯絡電話"
                                        SortExpression="AreaApplyPersonPhone">
                                        <HeaderStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ApplyTime" HeaderText="申請時間" SortExpression="ApplyTime">
                                        <HeaderStyle Width="7%" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#999999" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </td>
                        <td valign="top">
                            <asp:DetailsView ID="AreaDetailsView" runat="server"
                                CellPadding="3" DataSourceID="SqlDataSource2" EnableModelValidation="True"
                                GridLines="Horizontal" Height="50px" Width="529px"
                                AutoGenerateRows="False" BackColor="White" BorderColor="#E7E7FF"
                                BorderStyle="None" BorderWidth="1px" DataKeyNames="AreaID"
                                OnItemCommand="AreaDetailsView_RowCommand"
                                OnItemCreated="AreaDetailsView_ItemCreated"
                                OnItemUpdated="AreaDetailsView_ItemUpdated">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <EditRowStyle BackColor="#E7E7FF" Font-Bold="True"
                                    Width="100%" />
                                <%--      ForeColor="#F7F7F7"--%>
                                <Fields>
                                    <asp:BoundField DataField="AreaID" HeaderText="申請單編號" ReadOnly="True"
                                        SortExpression="AreaID">
                                        <ControlStyle Width="400px" />
                                        <HeaderStyle Width="120px" Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="申請時間">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ApplyTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("ApplyTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="ApplyTime" runat="server" Text='<%# Bind("ApplyTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AreaApplyCompany" HeaderText="申請人"
                                        SortExpression="AreaApplyCompany">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaApplyPersonPhone"
                                        HeaderText="聯絡電話" SortExpression="AreaApplyPersonPhone">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaPlace" HeaderText="工作地點"
                                        SortExpression="AreaPlace">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaPepoleCount" HeaderText="工作人數"
                                        SortExpression="AreaPepoleCount">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="工作內容">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="AreaJob" Rows="3" TextMode="MultiLine" runat="server" Text='<%# Bind("AreaJob") %>' Width="425px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="AreaJob" runat="server" Text='<%# Bind("AreaJob") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="預定時間（起）">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="AreaSTDateTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("AreaSTDateTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="AreaSTDateTime" runat="server" Text='<%# Bind("AreaSTDateTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="預定時間（迄）">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="AreaEndDateTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("AreaEndDateTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="AreaEndDateTime" runat="server" Text='<%# Bind("AreaEndDateTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CarNo" HeaderText="登錄車牌"
                                        SortExpression="CarNo">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Sentry" HeaderText="保全崗哨"
                                        SortExpression="Sentry">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaGuard" HeaderText="警勤室"
                                        SortExpression="AreaGuard">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaDealPepole" HeaderText="管制區經辦人"
                                        SortExpression="AreaDealPepole">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaOfficer" HeaderText="主任（代理人）"
                                        SortExpression="AreaOfficer">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AreaOrg" HeaderText="中水局主辦課室"
                                        SortExpression="AreaOrg">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" ShowHeader="False">
                                        <EditItemTemplate>
                                            <center>
                                <asp:Button ID="Button1" runat="server" CausesValidation="True" 
                                    CommandName="Update" Text="更新" />
                                &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" 
                                    CommandName="Cancel" Text="取消" />
                            </center>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <center>
                                <asp:Button ID="EditBtn" runat="server" CausesValidation="False" 
                                    CommandName="Edit" Text="編輯" />
                                 <asp:Button ID="DelBtn" runat="server" CausesValidation="false" CommandName="Area_Del" 
                                    Text="刪除" onclientclick="return confirm(&quot;您確定要刪除該筆資料嗎？&quot;);" />
                                <asp:Button ID="DownBtn" runat="server" CausesValidation="false" CommandName="Area_Download" 
                                    Text="下載建檔" />
                                    <asp:Button ID="DownBtnToSign" runat="server" CausesValidation="false" CommandName="Area_DownloadToSign" 
                                    Text="下載簽單" />
                                <asp:Button ID="ViewPrintBtn" runat="server" CausesValidation="false" CommandName="Area_ViewAndPrint" 
                                    Text="瀏覽與列印" />
                             </center>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="True" ForeColor="#C00000" />
                                    </asp:TemplateField>
                                </Fields>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"
                                    HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" />
                                <%--ForeColor="#4A3C8C"--%>
                            </asp:DetailsView>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                                ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>"
                                SelectCommand="SELECT * FROM [FormArea] WHERE [AreaID]=@AreaID"
                                UpdateCommand="UPDATE [FormArea] SET [AreaPepoleCount] = @AreaPepoleCount,[AreaJob]=@AreaJob,[AreaPlace]=@AreaPlace,
                                  [AreaSTDateTime]=@AreaSTDateTime,[AreaEndDateTime]=@AreaEndDateTime,[AreaApplyCompany]=@AreaApplyCompany,
                                  [AreaApplyPersonPhone] = @AreaApplyPersonPhone,[AreaGuard]=@AreaGuard,[AreaDealPepole]=@AreaDealPepole,
                                  [AreaOfficer]=@AreaOfficer,[AreaOrg]=@AreaOrg,[ApplyTime]=@ApplyTime,[CarNo]=@CarNo,[Sentry]=@Sentry WHERE AreaID=@AreaID ">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="AreaGridView" Name="AreaID" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AreaPepoleCount" Type="Int32" />
                                    <asp:Parameter Name="AreaJob" Type="String" />
                                    <asp:Parameter Name="AreaPlace" Type="String" />
                                    <asp:Parameter Name="AreaSTDateTime" Type="DateTime" />
                                    <asp:Parameter Name="AreaEndDateTime" Type="DateTime" />
                                    <asp:Parameter Name="AreaApplyCompany" Type="String" />
                                    <asp:Parameter Name="AreaApplyPersonPhone" Type="String" />
                                    <asp:Parameter Name="AreaGuard" Type="String" />
                                    <asp:Parameter Name="AreaDealPepole" Type="String" />
                                    <asp:Parameter Name="AreaOfficer" Type="String" />
                                    <asp:Parameter Name="AreaOrg" Type="String" />
                                    <asp:Parameter Name="ApplyTime" Type="DateTime" />
                                    <asp:Parameter Name="CarNo" Type="String" />
                                    <asp:Parameter Name="Sentry" Type="String" />
                                    <asp:ControlParameter ControlID="AreaGridView" Name="AreaID" PropertyName="SelectedValue" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                </center>
            </ContentTemplate>
        </cc1:TabPanel>

        <cc1:TabPanel runat="server" HeaderText="閘門啟閉申請單表格" ID="TabPanel2">
            <HeaderTemplate>
                閘門啟閉申請單表格-記錄查詢
            </HeaderTemplate>
            <ContentTemplate>
                <table>
                    <tr>
                        <td valign="top">
                            <asp:GridView ID="GateApplyGridView" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="GateApplyID" DataSourceID="SqlDataSource3"
                                EnableModelValidation="True" GridLines="Vertical"
                                EmptyDataText="此時段無資料" Width="432px">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <Columns>
                                    <asp:BoundField DataField="GateApplyID" HeaderText="GateApplyID" InsertVisible="False"
                                        ReadOnly="True" SortExpression="GateApplyID" Visible="False" />
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True">
                                        <HeaderStyle Width="2%" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="GateApplyPerson" HeaderText="申請人"
                                        SortExpression="GateApplyPerson">
                                        <HeaderStyle Width="3%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateApplyPersonPhone" HeaderText="聯絡電話"
                                        SortExpression="GateApplyPersonPhone">
                                        <HeaderStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ApplyTime" HeaderText="申請時間" SortExpression="ApplyTime">
                                        <HeaderStyle Width="7%" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#999999" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </td>
                        <td valign="top">
                            <asp:DetailsView ID="GateApplyDetailsView" runat="server"
                                CellPadding="3" DataSourceID="SqlDataSource4" EnableModelValidation="True"
                                GridLines="Horizontal" Height="50px" Width="529px"
                                AutoGenerateRows="False" BackColor="White" BorderColor="#E7E7FF"
                                BorderStyle="None" BorderWidth="1px" DataKeyNames="GateApplyID"
                                OnItemCommand="GateApplyDetailsView_RowCommand"
                                OnItemCreated="GateApplyDetailsView_ItemCreated"
                                OnItemUpdated="GateApplyDetailsView_ItemUpdated">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <EditRowStyle BackColor="#E7E7FF" Font-Bold="True"
                                    Width="100%" />
                                <%--     ForeColor="#F7F7F7"--%>
                                <Fields>
                                    <asp:BoundField DataField="GateApplyID" HeaderText="申請單編號" ReadOnly="True"
                                        SortExpression="GateApplyID">
                                        <ControlStyle Width="400px" />
                                        <HeaderStyle Width="120px" Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="申請時間">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ApplyTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("ApplyTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="ApplyTime" runat="server" Text='<%# Bind("ApplyTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GateApplyCompany" HeaderText="申請單位"
                                        SortExpression="GateApplyCompany">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateApplyPerson" HeaderText="申請人"
                                        SortExpression="GateApplyPerson">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateApplyPersonPhone"
                                        HeaderText="聯絡電話" SortExpression="GateApplyPersonPhone">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateApplyPlace" HeaderText="工作地點"
                                        SortExpression="GateApplyPlace">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="事由">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateApplyJob" Rows="3" TextMode="MultiLine" runat="server" Text='<%# Bind("GateApplyJob") %>' Width="425px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateApplyJob" runat="server" Text='<%# Bind("GateApplyJob") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="預定時間（起）">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateApplySTDateTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("GateApplySTDateTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateApplySTDateTime" runat="server" Text='<%# Bind("GateApplySTDateTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="預定時間（迄）">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateApplyEndDateTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("GateApplyEndDateTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateApplyEndDateTime" runat="server" Text='<%# Bind("GateApplyEndDateTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="廠商完工時間">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateApplyFinishJobTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("GateApplyFinishJobTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateApplyFinishJobTime" runat="server" Text='<%# Bind("GateApplyFinishJobTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GateApplyGuard" HeaderText="警勤室"
                                        SortExpression="GateApplyGuard">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateApplyGateOpratePerson" HeaderText="閘門操控人員"
                                        SortExpression="GateApplyGateOpratePerson">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateApplyOfficer" HeaderText="單位主管"
                                        SortExpression="GateApplyOfficer">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" ShowHeader="False">
                                        <EditItemTemplate>
                                            <center>
                                <asp:Button ID="Button1" runat="server" CausesValidation="True" 
                                    CommandName="Update" Text="更新" />
                                &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" 
                                    CommandName="Cancel" Text="取消" />
                            </center>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <center>
                                <asp:Button ID="EditBtn" runat="server" CausesValidation="False" 
                                    CommandName="Edit" Text="編輯" />
                                 <asp:Button ID="DelBtn" runat="server" CausesValidation="false" CommandName="GateApply_Del" 
                                    Text="刪除" onclientclick="return confirm(&quot;您確定要刪除該筆資料嗎？&quot;);" />
                                <asp:Button ID="DownBtn" runat="server" CausesValidation="false" CommandName="GateApply_Download" 
                                    Text="下載建檔" />
                                     <asp:Button ID="DownBtnToSign" runat="server" CausesValidation="false" CommandName="GateApply_DownloadToSign" 
                                    Text="下載簽單" />
                                <asp:Button ID="ViewPrintBtn" runat="server" CausesValidation="false" CommandName="GateApply_ViewAndPrint" 
                                    Text="瀏覽與列印" />
                             </center>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="True" ForeColor="#C00000" />
                                    </asp:TemplateField>
                                </Fields>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"
                                    HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" />
                                <%--ForeColor="#4A3C8C"--%>
                            </asp:DetailsView>
                            <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                                ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>"
                                SelectCommand="SELECT * FROM [FormGate] WHERE [GateApplyID]=@GateApplyID"
                                UpdateCommand="UPDATE [FormGate] SET [GateApplySTDateTime] = @GateApplySTDateTime,
					[GateApplyEndDateTime] = @GateApplyEndDateTime,[GateApplyPlace] = @GateApplyPlace,
					[GateApplyJob] = @GateApplyJob,[GateApplyCompany] = @GateApplyCompany,
					[GateApplyPerson] = @GateApplyPerson,[GateApplyGuard] = @GateApplyGuard,[GateApplyPersonPhone] = @GateApplyPersonPhone,
					[GateApplyGateOpratePerson] = @GateApplyGateOpratePerson,[GateApplyOfficer] = @GateApplyOfficer,
					[GateApplyFinishJobTime] = @GateApplyFinishJobTime,[ApplyTime] = @ApplyTime WHERE GateApplyID=@GateApplyID ">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="GateApplyGridView" Name="GateApplyID" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="GateApplySTDateTime" Type="DateTime" />
                                    <asp:Parameter Name="GateApplyEndDateTime" Type="DateTime" />
                                    <asp:Parameter Name="GateApplyPlace" Type="String" />
                                    <asp:Parameter Name="GateApplyJob" Type="String" />
                                    <asp:Parameter Name="GateApplyCompany" Type="String" />
                                    <asp:Parameter Name="GateApplyPerson" Type="String" />
                                    <asp:Parameter Name="GateApplyPersonPhone" Type="String" />
                                    <asp:Parameter Name="GateApplyGuard" Type="String" />
                                    <asp:Parameter Name="GateApplyGateOpratePerson" Type="String" />
                                    <asp:Parameter Name="GateApplyOfficer" Type="String" />
                                    <asp:Parameter Name="GateApplyFinishJobTime" Type="DateTime" />
                                    <asp:Parameter Name="ApplyTime" Type="DateTime" />
                                    <asp:ControlParameter ControlID="GateApplyGridView" Name="GateApplyID" PropertyName="SelectedValue" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </cc1:TabPanel>

        <cc1:TabPanel runat="server" HeaderText="湖山水庫出水工閘門操作紀錄表" ID="TabPanel3">
            <HeaderTemplate>湖山水庫出水工閘門操作紀錄表-記錄查詢</HeaderTemplate>
            <ContentTemplate>
                <table>
                    <tr>
                        <td valign="top">
                            <asp:GridView ID="GateOpraGridView" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="GateOpraID" DataSourceID="SqlDataSource5"
                                EnableModelValidation="True" GridLines="Vertical"
                                EmptyDataText="此時段無資料" Width="432px">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <Columns>
                                    <asp:BoundField DataField="GateOpraID" HeaderText="GateOpraID" InsertVisible="False"
                                        ReadOnly="True" SortExpression="GateOpraID" Visible="False" />
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True">
                                        <HeaderStyle Width="2%" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="GateOpraDeployPerson" HeaderText="調配人員"
                                        SortExpression="GateOpraDeployPerson">
                                        <HeaderStyle Width="3%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraPerson" HeaderText="閘門操作人員"
                                        SortExpression="GateOpraPerson">
                                        <HeaderStyle Width="3%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ApplyTime" HeaderText="申請時間" SortExpression="ApplyTime">
                                        <HeaderStyle Width="7%" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#999999" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </td>
                        <td valign="top">
                            <asp:DetailsView ID="GateOpraDetailsView" runat="server"
                                CellPadding="3" DataSourceID="SqlDataSource6" EnableModelValidation="True"
                                GridLines="Horizontal" Height="50px" Width="530px"
                                AutoGenerateRows="False" BackColor="White" BorderColor="#E7E7FF"
                                BorderStyle="None" BorderWidth="1px" DataKeyNames="GateOpraID"
                                OnItemCommand="GateOpraDetailsView_RowCommand"
                                OnItemCreated="GateOpraDetailsView_ItemCreated"
                                OnItemUpdated="GateOpraDetailsView_ItemUpdated">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <EditRowStyle BackColor="#E7E7FF" Font-Bold="True"
                                    Width="100%" />
                                <%--ForeColor="#F7F7F7"--%>
                                <Fields>
                                    <asp:BoundField DataField="GateOpraID" HeaderText="申請單編號" ReadOnly="True"
                                        SortExpression="GateOpraID">
                                        <ControlStyle Width="380px" />
                                        <HeaderStyle Width="175px" Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="申請時間">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ApplyTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("ApplyTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="ApplyTime" runat="server" Text='<%# Bind("ApplyTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="操作事由">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateOpraJob" Rows="3" TextMode="MultiLine" runat="server" Text='<%# Bind("GateOpraJob") %>' Width="425px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateOpraJob" runat="server" Text='<%# Bind("GateOpraJob") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GateOpraLYTWaterLevel" HeaderText="水庫水位(公尺)"
                                        SortExpression="GateOpraLYTWaterLevel">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraYanWaterLevel"
                                        HeaderText="後池堰水位(公尺)" SortExpression="GateOpraYanWaterLevel">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="啟閉時間">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateOpraSwitchDateTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("GateOpraSwitchDateTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateOpraSwitchDateTime" runat="server" Text='<%# Bind("GateOpraSwitchDateTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GateOpraGate" HeaderText="操作閘門"
                                        SortExpression="GateOpraGate">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraHours" HeaderText="放流時間(小時)"
                                        SortExpression="GateOpraHours">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraWaterSupply" HeaderText="供水量(cms)"
                                        SortExpression="GateOpraWaterSupply">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" HeaderText="資料時間">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="GateOpraTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:ss'})" runat="server" Text='<%# Bind("GateOpraTime") %>' Width="200px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <itemtemplate>
                                    <asp:Label ID="GateOpraTime" runat="server" Text='<%# Bind("GateOpraTime") %>' style="color:black"></asp:Label>
                                </itemtemplate>
                                        </ItemTemplate>
                                        <ControlStyle Font-Bold="False" />
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle Font-Bold="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GateOpraOutFlow1_1" HeaderText="出水工1-1閘門開度（公分）"
                                        SortExpression="GateOpraOutFlow1_1">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraOutFlow1_2" HeaderText="出水工1-2閘門開度（公分）"
                                        SortExpression="GateOpraOutFlow1_2">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraOutFlow2_1" HeaderText="出水工2-1閘門開度（公分）"
                                        SortExpression="GateOpraOutFlow2_1">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <%--                         <asp:BoundField DataField="GateOpraOutFlow2_2" HeaderText="出水工2-2閘門開度（公分）" 
                            SortExpression="GateOpraOutFlow2_2" >                   
                        <ControlStyle Width="415px" />
                        <HeaderStyle Font-Bold="True" />
                        </asp:BoundField>--%>
                                    <asp:BoundField DataField="GateOpraOutFlow" HeaderText="放流量(cms)"
                                        SortExpression="GateOpraOutFlow">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraDeployPerson" HeaderText="調配人員"
                                        SortExpression="GateOpraDeployPerson">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraPolice" HeaderText="警勤室人員"
                                        SortExpression="GateOpraPolice">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraPerson" HeaderText="操作人員"
                                        SortExpression="GateOpraPerson">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GateOpraManager" HeaderText="單位主管"
                                        SortExpression="GateOpraManager">
                                        <ControlStyle Width="415px" />
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField InsertVisible="False" ShowHeader="False">
                                        <EditItemTemplate>
                                            <center>
                                <asp:Button ID="Button1" runat="server" CausesValidation="True" 
                                    CommandName="Update" Text="更新" />
                                &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" 
                                    CommandName="Cancel" Text="取消" />
                            </center>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <center>
                                <asp:Button ID="EditBtn" runat="server" CausesValidation="False" 
                                    CommandName="Edit" Text="編輯" />
                                 <asp:Button ID="DelBtn" runat="server" CausesValidation="false" CommandName="GateOpra_Del" 
                                    Text="刪除" onclientclick="return confirm(&quot;您確定要刪除該筆資料嗎？&quot;);" />
                                <asp:Button ID="DownBtn" runat="server" CausesValidation="false" CommandName="GateOpra_Download" 
                                    Text="下載建檔" />
                                <asp:Button ID="DownToSignBtn" runat="server" CausesValidation="false" CommandName="GateOpra_DownloadToSign" 
                                    Text="下載簽單" />
                                <asp:Button ID="ViewPrintBtn" runat="server" CausesValidation="false" CommandName="GateOpra_ViewAndPrint" 
                                    Text="瀏覽與列印" />
                             </center>
                                        </ItemTemplate>
                                        <ItemStyle Font-Bold="True" ForeColor="#C00000" />
                                    </asp:TemplateField>
                                </Fields>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"
                                    HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" />
                                <%--ForeColor="#4A3C8C"--%>
                            </asp:DetailsView>
                            <asp:SqlDataSource ID="SqlDataSource6" runat="server"
                                ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>"
                                SelectCommand="SELECT * FROM [FormGateOperate] WHERE [GateOpraID]=@GateOpraID"
                                UpdateCommand="UPDATE [FormGateOperate] SET 
					[GateOpraJob] = @GateOpraJob,
					[GateOpraLYTWaterLevel] = @GateOpraLYTWaterLevel,
                    [GateOpraYanWaterLevel] = @GateOpraYanWaterLevel,
                    [GateOpraSwitchDateTime] = @GateOpraSwitchDateTime,
                    [GateOpraGate] = @GateOpraGate,
                    [GateOpraHours] = @GateOpraHours,
                    [GateOpraWaterSupply] = @GateOpraWaterSupply,
                    [GateOpraTime] = @GateOpraTime,
                    [GateOpraOutFlow1_1] = @GateOpraOutFlow1_1,
                    [GateOpraOutFlow1_2] = @GateOpraOutFlow1_2,
                    [GateOpraOutFlow2_1] = @GateOpraOutFlow2_1,
                    [GateOpraOutFlow2_2] = @GateOpraOutFlow2_2,
                    [GateOpraOutFlow] = @GateOpraOutFlow,
                    [GateOpraDeployPerson] = @GateOpraDeployPerson,
                    [GateOpraPolice] = @GateOpraPolice,
                    [GateOpraPerson] = @GateOpraPerson,
					[GateOpraManager] = @GateOpraManager
					 WHERE GateOpraID=@GateOpraID ">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="GateOpraGridView" Name="GateOpraID" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="GateOpraJob" Type="String" />
                                    <asp:Parameter Name="GateOpraLYTWaterLevel" Type="Double" />
                                    <asp:Parameter Name="GateOpraYanWaterLevel" Type="Double" />
                                    <asp:Parameter Name="GateOpraSwitchDateTime" Type="DateTime" />
                                    <asp:Parameter Name="GateOpraGate" Type="String" />
                                    <asp:Parameter Name="GateOpraHours" Type="Double" />
                                    <asp:Parameter Name="GateOpraWaterSupply" Type="Double" />
                                    <asp:Parameter Name="GateOpraTime" Type="DateTime" />
                                    <asp:Parameter Name="GateOpraOutFlow1_1" Type="Double" />
                                    <asp:Parameter Name="GateOpraOutFlow1_2" Type="Double" />
                                    <asp:Parameter Name="GateOpraOutFlow2_1" Type="Double" />
                                    <asp:Parameter Name="GateOpraOutFlow2_2" Type="Double" />
                                    <asp:Parameter Name="GateOpraOutFlow" Type="Double" />
                                    <asp:Parameter Name="GateOpraDeployPerson" Type="String" />
                                    <asp:Parameter Name="GateOpraPolice" Type="String" />
                                    <asp:Parameter Name="GateOpraPerson" Type="String" />
                                    <asp:Parameter Name="GateOpraManager" Type="String" />
                                    <asp:Parameter Name="ApplyTime" Type="DateTime" />
                                    <asp:ControlParameter ControlID="GateOpraGridView" Name="GateOpraID" PropertyName="SelectedValue" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </cc1:TabPanel>

    </cc1:TabContainer>

<%--            </ContentTemplate>
    </asp:UpdatePanel>--%>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
        ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>"
        SelectCommand="SELECT * FROM [FormArea] WHERE ([ApplyTime] >= @StTime) AND ([ApplyTime] <= @EdTime)">
        <SelectParameters>
            <asp:ControlParameter ControlID="StTimeLb" Name="StTime" Type="String" />
            <asp:ControlParameter ControlID="EndTimeLb" Name="EdTime" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
        ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>"
        SelectCommand="SELECT * FROM [FormGate] WHERE ([ApplyTime] >= @StTime) AND ([ApplyTime] <= @EdTime)">
        <SelectParameters>
            <asp:ControlParameter ControlID="StTimeLb" Name="StTime" Type="String" />
            <asp:ControlParameter ControlID="EndTimeLb" Name="EdTime" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server"
        ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>"
        SelectCommand="SELECT * FROM [FormGateOperate] WHERE ([ApplyTime] >= @StTime) AND ([ApplyTime] <= @EdTime)">
        <SelectParameters>
            <asp:ControlParameter ControlID="StTimeLb" Name="StTime" Type="String" />
            <asp:ControlParameter ControlID="EndTimeLb" Name="EdTime" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

