﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_GateOpraView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DateTime Date = DateTime.Now;
            yearTb.Text = Date.Year.ToString();
            monthTb.Text = Date.Month.ToString();
            dayTb.Text = Date.Day.ToString();

            if (Request.Form["GateOpraJobTb"] != null)
            {
                GateOpraJobTb.Text = Request.Form["GateOpraJobTb"].ToString();
            }

            if (Request.Form["GateOpraLYTWaterLevelTb"] != null)
            {
                GateOpraLYTWaterLevelTb.Text = Request.Form["GateOpraLYTWaterLevelTb"].ToString();
            }

            if (Request.Form["GateOpraYanWaterLevelTb"] != null)
            {
                GateOpraYanWaterLevelTb.Text = Request.Form["GateOpraYanWaterLevelTb"].ToString();
            }

            if (Request.Form["GateOpraSwitchDateTimeTb"] != null)
            {
                GateOpraSwitchDateTimeTb.Text = Request.Form["GateOpraSwitchDateTimeTb"].ToString();
            }

            if (Request.Form["GateOpraGateTb"] != null)
            {
                GateOpraGateTb.Text = Request.Form["GateOpraGateTb"].ToString();
            }

            if (Request.Form["GateOpraHoursTb"] != null)
            {
                GateOpraHoursTb.Text = Request.Form["GateOpraHoursTb"].ToString();
            }

            if (Request.Form["GateOpraWaterSupplyTb"] != null)
            {
                GateOpraWaterSupplyTb.Text = Request.Form["GateOpraWaterSupplyTb"].ToString();
            }

            if (Request.Form["GateOpraTimeTb"] != null)
            {
                GateOpraTimeTb.Text = Request.Form["GateOpraTimeTb"].ToString();
            }

            if (Request.Form["GateOpraOutFlow1_1Tb"] != null)
            {
                GateOpraOutFlow1_1Tb.Text = Request.Form["GateOpraOutFlow1_1Tb"].ToString();
            }

            if (Request.Form["GateOpraOutFlow1_2Tb"] != null)
            {
                GateOpraOutFlow1_2Tb.Text = Request.Form["GateOpraOutFlow1_2Tb"].ToString();
            }

            if (Request.Form["GateOpraOutFlow2_1Tb"] != null)
            {
                GateOpraOutFlow2_1Tb.Text = Request.Form["GateOpraOutFlow2_1Tb"].ToString();
            }

            //if (Request.Form["GateOpraOutFlow2_2Tb"] != null)
            //{
            //    GateOpraOutFlow2_2Tb.Text = Request.Form["GateOpraOutFlow2_2Tb"].ToString();
            //}

            if (Request.Form["GateOpraDeployPersonTb"] != null)
            {
                GateOpraDeployPersonTb.Text = Request.Form["GateOpraDeployPersonTb"].ToString();
            }

            if (Request.Form["GateOpraPoliceTb"] != null)
            {
                GateOpraPoliceTb.Text = Request.Form["GateOpraPoliceTb"].ToString();
            }

            if (Request.Form["GateOpraPersonTb"] != null)
            {
                GateOpraPersonTb.Text = Request.Form["GateOpraPersonTb"].ToString();
            }

            if (Request.Form["GateOpraManagerTb"] != null)
            {
                GateOpraManagerTb1.Text = Request.Form["GateOpraManagerTb"].ToString();
            }

            if (Request.Form["GateOpraManagerTb"] != null)
            {
                GateOpraManagerTb2.Text = Request.Form["GateOpraManagerTb"].ToString();
            }

            if (Request.Form["GateOpraOutFlowTb"] != null)
            {
                GateOpraOutFlowTb.Text = Request.Form["GateOpraOutFlowTb"].ToString();
            }
        }
        catch (Exception ee)
        {
        }
    }
}