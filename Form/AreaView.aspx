﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AreaView.aspx.cs" Inherits="Form_AreaView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;}
@font-face
	{font-family:標楷體;
	panose-1:3 0 5 9 0 0 0 0 0 0;}
@font-face
	{font-family:"\@標楷體";
	panose-1:3 0 5 9 0 0 0 0 0 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 2 5 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	line-height:18.0pt;
	font-size:16.0pt;
	font-family:"Times New Roman";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{margin:0cm;
	margin-bottom:.0001pt;
	line-height:18.0pt;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"Times New Roman";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{margin:0cm;
	margin-bottom:.0001pt;
	line-height:18.0pt;
	layout-grid-mode:char;
	font-size:10.0pt;
	font-family:"Times New Roman";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{margin:0cm;
	margin-bottom:.0001pt;
	line-height:18.0pt;
	font-size:9.0pt;
	font-family:Arial;}
p.11, li.11, div.11
	{margin-top:3.0pt;
	margin-right:20.6pt;
	margin-bottom:3.0pt;
	margin-left:54.0pt;
	text-align:justify;
	text-justify:inter-ideograph;
	line-height:18.0pt;
	font-size:16.0pt;
	font-family:"Times New Roman";}
 /* Page Definitions */
 @page Section1
	{size:21.0cm 842.0pt;
	margin:2.0cm 42.55pt 2.0cm 42.55pt;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
      .style1
      {
          width: 58.85pt;
          height: 32pt;
      }
      .style2
      {
          width: 432.55pt;
          height: 32pt;
      }
-->
</style>
</head>
<body lang=ZH-TW style='text-justify-trim:punctuation'>
    <form id="form1" runat="server">
<div class=Section1>

<p class=11 align=center style='margin-left:0cm;text-align:center; width: 652px;'><span
style='font-size:18.0pt;font-family:標楷體'>湖山水庫管制區進入作業申請單</span></p>

<p class=11 style='margin-left:12.0pt;text-indent:72.0pt; width: 634px;'><span lang=EN-US
style='font-size:18.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style='font-size:14.0pt;font-family:標楷體'>申請時間：</span><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="ApplyYearLb" runat="server"></asp:Label>
    </span></span>&nbsp;</span><span style='font-size:14.0pt;
font-family:標楷體'>年</span><span lang=EN-US style='font-size:14.0pt'><span lang="EN-US" 
            style="font-size:14.0pt"><span style="mso-spacerun:yes"><asp:Label ID="ApplyMonthLb" runat="server"></asp:Label>
    </span></span>&nbsp;</span><span
style='font-size:14.0pt;font-family:標楷體'>月</span><span lang=EN-US
style='font-size:14.0pt'>&nbsp;<span lang="EN-US" 
            style="font-size:14.0pt"><span style="mso-spacerun:yes"><asp:Label ID="ApplyDayLb" runat="server"></asp:Label>
    </span></span> </span><span style='font-size:14.0pt;
font-family:標楷體'>日</span><span lang=EN-US style='font-size:14.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=655
 style='width:491.4pt;border-collapse:collapse;border:none'>
 <tr style='height:45.55pt'>
  <td width=102 colspan=2 style='width:76.65pt;border:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:45.55pt'>
  <p class=11 align=center style='margin-top:3.0pt;margin-right:9.15pt;
  margin-bottom:3.0pt;margin-left:0cm;text-align:center'><span 
                        style="font-size:14.0pt;font-family:
  標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;">工作內容</span></p>
  </td>
  <td width=553 colspan=3 valign=top style='width:414.75pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 2.85pt 0cm 2.85pt;height:45.55pt'>
  <p class=11 style='margin-left:0cm'><span lang=EN-US style='font-size:14.0pt'>&nbsp;<span
  style='font-size:14.0pt;font-family:標楷體'><asp:Label ID="AreaJobLb" 
                        runat="server"></asp:Label>
                    </span>
                </span></p>
  </td>
 </tr>
 <tr style='height:46.9pt'>
  <td width=102 colspan=2 style='width:76.65pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 2.85pt 0cm 2.85pt;height:46.9pt'>
  <p class=11 align=center style='margin-top:3.0pt;margin-right:9.15pt;
  margin-bottom:3.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:14.0pt;font-family:標楷體'>地點人數</span></p>
  </td>
  <td width=553 colspan=3 valign=top style='width:414.75pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:46.9pt'>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>位置：<span 
                        style="font-size:14.0pt;font-family:
  標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;"><asp:Label ID="AreaPlacebLb" 
                        runat="server"></asp:Label>
                    </span></span></p>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>人數：<span 
                        style="font-size:14.0pt;font-family:
  標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaPepoleCountLb" runat="server"></asp:Label>
                    </span></span></p>
  </td>
 </tr>
 <tr style='height:48.3pt'>
  <td width=102 colspan=2 style='width:76.65pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 2.85pt 0cm 2.85pt;height:48.3pt'>
  <p class=11 align=center style='margin-top:3.0pt;margin-right:9.15pt;
  margin-bottom:3.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:14.0pt;font-family:標楷體'>預定時間</span></p>
  </td>
  <td width=553 colspan=3 valign=top style='width:414.75pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:48.3pt'>
  <p class=11 style='margin-left:0cm'><span lang=EN-US style='font-size:14.0pt'>&nbsp;
  &nbsp;</span><span style='font-size:14.0pt;font-family:標楷體'><span style="font-size:14.0pt;font-family:標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;
  mso-hansi-font-family:&quot;Times New Roman&quot;"><span lang="EN-US" style="font-size:14.0pt"><span 
                        style="mso-spacerun:yes"><asp:Label ID="AreaSTDateTimeYearLb" 
                        runat="server"></asp:Label>
                    </span></span></span>年</span><span
  lang=EN-US style='font-size:14.0pt'>&nbsp; &nbsp;</span><span
  style='font-size:14.0pt;font-family:標楷體'><span lang="EN-US" style="font-size:14.0pt"><span 
                        style="mso-spacerun:yes"><span style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label ID="AreaSTDateTimeMonthLb" runat="server"></asp:Label>
                    </span></span></span>月</span><span lang=EN-US
  style='font-size:14.0pt'>&nbsp; &nbsp;</span><span style='font-size:14.0pt;
  font-family:標楷體'><span lang="EN-US" style="font-size:14.0pt"><span 
                        style="mso-spacerun:yes"><span style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label ID="AreaSTDateTimeDayLb" runat="server"></asp:Label>
                    </span></span></span>日</span><span lang=EN-US style='font-size:14.0pt'>&nbsp; </span><span
  style='font-size:14.0pt;font-family:標楷體'>時間：<span style="font-size:14.0pt;font-family:標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;
  mso-hansi-font-family:&quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaSTDateTimeLb" runat="server"></asp:Label>
                    </span></span><span lang=EN-US
  style='font-size:14.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
  style='font-size:14.0pt;font-family:標楷體'>起</span></p>
  <p class=11 style='margin-left:0cm'><span lang=EN-US style='font-size:14.0pt'>&nbsp;
  &nbsp;</span><span style='font-size:14.0pt;font-family:標楷體'><span style="font-size:14.0pt;font-family:標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;
  mso-hansi-font-family:&quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaEndDateTimeYearLb" runat="server"></asp:Label>
      </span>年</span><span
  lang=EN-US style='font-size:14.0pt'>&nbsp; &nbsp;</span><span
  style='font-size:14.0pt;font-family:標楷體'><span lang="EN-US" style="font-size:14.0pt"><span style="mso-spacerun:yes"><span style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label ID="AreaEndDateTimeMonthLb" runat="server"></asp:Label>
                    </span></span></span>月</span><span lang=EN-US
  style='font-size:14.0pt'>&nbsp; &nbsp;</span><span style='font-size:14.0pt;
  font-family:標楷體'><span lang="EN-US" style="font-size:14.0pt"><span 
                        style="mso-spacerun:yes"><span style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label ID="AreaEndDateTimeDayLb" runat="server"></asp:Label>
                    </span></span></span>日</span><span lang=EN-US style='font-size:14.0pt'>&nbsp; </span><span
  style='font-size:14.0pt;font-family:標楷體'>時間：<span style="font-size:14.0pt;font-family:標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;
  mso-hansi-font-family:&quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaEndDateTimeLb" runat="server"></asp:Label>
                    </span></span><span lang=EN-US
  style='font-size:14.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
  style='font-size:14.0pt;font-family:標楷體'>訖</span></p>
  </td>
 </tr>
 <tr style='height:34.8pt'>
  <td width=238 colspan=3 style='width:178.85pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 2.85pt 0cm 2.85pt;height:34.8pt'>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>申請單位或廠商：<span lang="EN-US" style="font-size:14.0pt"><span 
                        style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><br><asp:Label 
                        ID="AreaApplyCompanyLb" runat="server"></asp:Label>
                    </span></span>
                </span></p>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>申請人手機：<span lang="EN-US" style="font-size:14.0pt"><span 
                        style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaApplyPersonPhoneLb" runat="server"></asp:Label>
                    </span></span>
                </span></p>

 <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>登錄車牌：<span lang="EN-US" style="font-size:14.0pt"><span 
                        style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label 
                        ID="CarNoTb" runat="server"></asp:Label>
                    </span></span>
                </span></p>


  </td>
  <td width=235 style='width:176.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:34.8pt'>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>管制區經辦人：<span lang="EN-US" style="font-size:14.0pt"><span 
                        style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaDealPepoleLb" runat="server"></asp:Label>
                    </span></span>
                </span></p>
  </td>
  <td width=182 rowspan=2 valign=top style='width:136.55pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:34.8pt'>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>主任：<span 
                        style="font-size:14.0pt;font-family:
  標楷體;mso-ascii-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:&quot;Times New Roman&quot;"><span lang="EN-US" style="font-size:14.0pt"><asp:Label 
                        ID="AreaOfficerLb" runat="server"></asp:Label>
                    </span>
                    </span></span></p>
  <p class=11 style='margin-left:0cm'><span lang=EN-US style='font-size:14.0pt'>(</span><span
  style='font-size:14.0pt;font-family:標楷體'>或代理人</span><span lang=EN-US
  style='font-size:14.0pt'>)</span></p>
  </td>
 </tr>
 <tr style='height:31.55pt'>
  <td width=238 colspan=3 style='width:178.85pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 2.85pt 0cm 2.85pt;height:31.55pt'>
  <p class=11 style='margin-left:0cm'><span style='font-size:12.0pt;font-family:
  標楷體'>中水局主辦課室：<span lang="EN-US" style="font-size:12.0pt"><span lang="EN-US" style="font-size:14.0pt"><span 
                        style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaOrgLb" runat="server"></asp:Label>
                    </span></span>
                    </span></span></p>
  </td>
  <td width=235 style='width:176.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:31.55pt'>
  <p class=11 style='margin-left:0cm'><span style='font-size:14.0pt;font-family:
  標楷體'>警勤室：<span lang="EN-US" style="font-size:14.0pt"><span 
                        style="font-size: 14.0pt; font-family: 標楷體; mso-ascii-font-family: &quot;Times New Roman&quot;; mso-hansi-font-family: &quot;Times New Roman&quot;"><asp:Label 
                        ID="AreaGuardLb" runat="server"></asp:Label>
                    </span></span></span></p>
  </td>
 </tr>
 <tr style='height:86.15pt'>
  <td width=78 style='width:58.85pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 2.85pt 0cm 2.85pt;height:86.15pt'>
  <p class=11 style='margin-left:0cm;text-align:justify;text-justify:distribute-all-lines'><span
  style='font-size:14.0pt;font-family:標楷體'>平日</span></p>
  </td>
  <td width=577 colspan=4 valign=top style='width:432.55pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:86.15pt'>
  <p class=11 style='margin-top:3.0pt;margin-right:20.7pt;margin-bottom:3.0pt;
  margin-left:0cm;line-height:150%'><span lang=EN-US style='font-size:14.0pt;
  line-height:150%'>1</span><span style='font-size:14.0pt;line-height:150%;
  font-family:標楷體'>、向鑰匙管理人員領取鑰匙</span></p>
  <p class=11 style='margin-top:3.0pt;margin-right:20.7pt;margin-bottom:3.0pt;
  margin-left:0cm;line-height:150%'><span lang=EN-US style='font-size:14.0pt;
  line-height:150%'>2</span><span style='font-size:14.0pt;line-height:150%;
  font-family:標楷體'>、工作完畢鑰匙繳回鑰匙管理人員</span></p>
  <p class=11 style='margin-left:0cm;line-height:150%'><span lang=EN-US
  style='font-size:14.0pt;line-height:150%'>3</span><span style='font-size:
  14.0pt;line-height:150%;font-family:標楷體'>、管理中心鑰匙管理人員核章：</span></p>
  </td>
 </tr>
 <tr style='height:81.3pt'>
  <td width=78 style='width:58.85pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 2.85pt 0cm 2.85pt;height:81.3pt'>
  <p class=11 style='margin-left:0cm;text-align:justify;text-justify:distribute-all-lines'><span
  style='font-size:14.0pt;font-family:標楷體'>假日</span></p>
  </td>
  <td width=577 colspan=4 valign=top style='width:432.55pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:81.3pt'>
  <p class=11 style='margin-top:3.0pt;margin-right:20.7pt;margin-bottom:3.0pt;
  margin-left:0cm;line-height:150%'><span lang=EN-US style='font-size:14.0pt;
  line-height:150%'>1</span><span style='font-size:14.0pt;line-height:150%;
  font-family:標楷體'>、向值勤人員領取鑰匙</span></p>
  <p class=11 style='margin-top:3.0pt;margin-right:20.7pt;margin-bottom:3.0pt;
  margin-left:0cm;line-height:150%'><span lang=EN-US style='font-size:14.0pt;
  line-height:150%'>2</span><span style='font-size:14.0pt;line-height:150%;
  font-family:標楷體'>、工作完畢鑰匙繳回值勤人員</span></p>
  <p class=11 style='margin-top:3.0pt;margin-right:20.7pt;margin-bottom:3.0pt;
  margin-left:0cm;line-height:150%'><span
  lang=EN-US style='font-size:14.0pt;line-height:150%'>3</span><span
  style='font-size:14.0pt;line-height:150%;font-family:標楷體'>、值勤人員核章：</span></p>
  </td>
 </tr>
 <tr style='height:56.0pt'>
  <td width=78 valign=top style='width:58.85pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 2.85pt 0cm 2.85pt;height:56.0pt'>
  <p class=11 style='margin-top:3.0pt;margin-right:9.15pt;margin-bottom:3.0pt;
  margin-left:0cm'><span style='font-size:14.0pt;font-family:標楷體'>備</span><span
  style='font-size:14.0pt'> </span><span style='font-size:14.0pt;font-family:
  標楷體'>註</span></p>
  </td>
  <td width=577 colspan=4 valign=top style='width:432.55pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt;height:56.0pt'>
  <p class=11 style='margin-left:26.15pt;text-indent:-26.15pt'><span
  style='font-size:12.0pt;font-family:標楷體'>一、進入人員應由湖山水庫管理中心指派之人員會同開啟入口鑰匙（或操作水門）。</span></p>
  <p class=11 style='margin-left:26.2pt;text-indent:-25.55pt'><span
  style='font-size:12.0pt;font-family:標楷體'>二、奉核後警勤室需通知現場保全人員。</span></p>
  <p class=11 style='margin-left:26.2pt;text-indent:-25.55pt'><span
  style='font-size:12.0pt;font-family:標楷體'>三、所有進入危險區域之工作人員均應穿戴安全防護配備。</span></p>
  <p class=11 style='margin-left:26.2pt;text-indent:-25.55pt'><span
  style='font-size:12.0pt;font-family:標楷體'>四、現場應有負責連繫人員（備妥通訊設備）隨時與本中心值勤人員通知作業地點及作業人數。</span></p>
  <p class=11 style='margin-left:26.2pt;text-indent:-25.55pt'><span
  style='font-size:12.0pt;font-family:標楷體'>五、工作完成，申請人<b>平日</b>必須通知鑰匙管理人員簽名確認。</span></p>
  <p class=11 style='margin-left:26.2pt;text-indent:-25.55pt'><span lang=EN-US
  style='font-size:12.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span><b><span style='font-size:12.0pt;font-family:標楷體'>假日</span></b><span
  style='font-size:12.0pt;font-family:標楷體'>必須通知值勤人員簽名確認。</span></p>
  <p class=11 style='margin-left:26.25pt;text-indent:-25.6pt'><b><span
  style='font-size:12.0pt;font-family:標楷體'>六、申請閘門維護廠商、務必填寫閘門啟閉申請單。</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=78 
         style='border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding: 0cm 2.85pt; border-top-style: none; border-top-color: inherit; border-top-width: medium;' 
         class="style1">
  <p class=11 
          style='margin-left:0cm;text-align:justify;text-justify:distribute-all-lines; width: 86px;'><span
  style='font-size:14.0pt;font-family:標楷體'>保全崗哨</span></p>
  </td>
  <td width=577 colspan=4 valign=top 
         style='border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 2.85pt 0cm 2.85pt; border-left-style: none; border-left-color: inherit; border-left-width: medium; border-top-style: none; border-top-color: inherit; border-top-width: medium;' 
         class="style2">
  <p class=11 style='margin-top:3.0pt;margin-right:20.7pt;margin-bottom:3.0pt;
  margin-left:0cm;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
  font-family:標楷體'><asp:Label ID="SentryTb" runat="server"></asp:Label></span></p>  
  </td>
 </tr>
 <tr height=0>
  <td width=78 style='border:none'></td>
  <td width=24 style='border:none'></td>
  <td width=136 style='border:none'></td>
  <td width=235 style='border:none'></td>
  <td width=182 style='border:none'></td>
 </tr>
</table>

<p class=11 style='margin-left:0cm'><span lang=EN-US style='font-size:12.0pt'>&nbsp;</span></p>

</div>
    </form>
     <center>
        <input type="button" VALUE="關閉視窗" onClick="window.close()" />
      </center>
</body>
</html>
