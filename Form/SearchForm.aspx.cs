﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_SearchForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            InitYear();
            InitMonth();
            InitStratDay(Convert.ToInt32(StYear.SelectedItem.Text), Convert.ToInt32(StMonth.SelectedItem.Text));
            InitEndDay(Convert.ToInt32(EndYear.SelectedItem.Text), Convert.ToInt32(EndMonth.SelectedItem.Text));
            select();
            setLb();
        }
    }

    protected void Tab_OnActiveTabChanged(object sender, EventArgs e)
    {

        int tab = TabContainer1.ActiveTabIndex;

        switch (tab)
        {
            case 0:

                setLb();
                AreaGridView.DataBind();

                break;
            case 1:

                setLb();
                GateApplyGridView.DataBind();

                break;

            case 2:

                setLb();
                GateOpraGridView.DataBind();

                break;

            default:

                break;
        }


    }

    private void setLb()
    {
        int syear = Convert.ToInt32(StYear.SelectedItem.Text);
        int smonth = Convert.ToInt32(StMonth.SelectedItem.Text);
        int sday = Convert.ToInt32(StDay.SelectedItem.Text);
        int eyear = Convert.ToInt32(EndYear.SelectedItem.Text);
        int emonth = Convert.ToInt32(EndMonth.SelectedItem.Text);
        int eday = Convert.ToInt32(EndDay.SelectedItem.Text);

        DateTime begDateTime = new DateTime(syear, smonth, sday, 0, 0, 0);
        DateTime endDateTime = new DateTime(eyear, emonth, eday, 23, 0, 0);

        StTimeLb.Text = begDateTime.ToString("yyyy/MM/dd HH:mm:ss");
        EndTimeLb.Text = endDateTime.ToString("yyyy/MM/dd HH:mm:ss");
    }

    /// <summary>
    /// 初始年份
    /// </summary>
    private void InitYear()
    {
        StYear.Items.Clear();
        EndYear.Items.Clear();

        int begYear = 2010;
        int endYear = DateTime.Today.Year;
        for (int year = begYear; year <= endYear; year++)
        {
            StYear.Items.Add(year.ToString());
            EndYear.Items.Add(year.ToString());
        }
    }

    /// <summary>
    /// 初始月份
    /// </summary>
    private void InitMonth()
    {
        StMonth.Items.Clear();
        EndMonth.Items.Clear();


        for (int month = 1; month <= 12; month++)
        {
            StMonth.Items.Add(month.ToString());
            EndMonth.Items.Add(month.ToString());

        }
    }

    /// <summary>
    /// 初始日
    /// </summary>
    /// <param name="year"></param>
    /// <param name="month"></param>
    private void InitStratDay(int year, int month)
    {
        StDay.Items.Clear();



        for (int day = 1; day <= DateTime.DaysInMonth(year, month); day++)
        {
            StDay.Items.Add(day.ToString());

        }
    }

    private void InitEndDay(int year, int month)
    {

        EndDay.Items.Clear();


        for (int day = 1; day <= DateTime.DaysInMonth(year, month); day++)
        {
            EndDay.Items.Add(day.ToString());

        }
    }

    private void select()
    {
        string year = DateTime.Now.Year.ToString();
        string month = DateTime.Now.Month.ToString();
        string day = DateTime.Now.Day.ToString();

        for (int i = 0; i < StYear.Items.Count; i++)
        {
            if (StYear.Items[i].Text.Equals(year))
            {
                StYear.SelectedIndex = i;
                break;
            }
        }



        for (int i = 0; i < StMonth.Items.Count; i++)
        {

            if (StMonth.Items[i].Text.Equals(month))
            {
                StMonth.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < StDay.Items.Count; i++)
        {

            if (StDay.Items[i].Text.Equals(day))
            {
                StDay.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < EndYear.Items.Count; i++)
        {
            if (EndYear.Items[i].Text.Equals(year))
            {
                EndYear.SelectedIndex = i;
                break;
            }
        }



        for (int i = 0; i < EndMonth.Items.Count; i++)
        {

            if (EndMonth.Items[i].Text.Equals(month))
            {
                EndMonth.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < EndDay.Items.Count; i++)
        {

            if (EndDay.Items[i].Text.Equals(day))
            {
                EndDay.SelectedIndex = i;
                break;
            }
        }
    }


    protected void AreaDetailsView_ItemCreated(Object sender, EventArgs e)
    {
        foreach (DetailsViewRow Row in AreaDetailsView.Rows)
        {
            string DataKey = AreaDetailsView.DataKey[0].ToString();
            if (DataKey != "")
            {
                Button btn = (Button)Row.FindControl("ViewPrintBtn");
                if (btn != null)
                {
                    btn.Attributes.Add("onclick", "window.open('ViewPrintArea.aspx?AreaID=" + DataKey + "', 'ViewPrint', config='fullscreen=1,scrollbars=yes')");
                }
            }
        }
    }

    protected void GateApplyDetailsView_ItemCreated(Object sender, EventArgs e)
    {
        foreach (DetailsViewRow Row in GateApplyDetailsView.Rows)
        {
            string DataKey = GateApplyDetailsView.DataKey[0].ToString();
            if (DataKey != "")
            {
                Button btn = (Button)Row.FindControl("ViewPrintBtn");
                if (btn != null)
                {
                    btn.Attributes.Add("onclick", "window.open('ViewPrintGateApply.aspx?GateApplyID=" + DataKey + "', 'ViewPrint', config='fullscreen=1,scrollbars=yes')");
                }
            }
        }
    }

    protected void GateOpraDetailsView_ItemCreated(Object sender, EventArgs e)
    {
        foreach (DetailsViewRow Row in GateOpraDetailsView.Rows)
        {
            string DataKey = GateOpraDetailsView.DataKey[0].ToString();
            if (DataKey != "")
            {
                Button btn = (Button)Row.FindControl("ViewPrintBtn");
                if (btn != null)
                {
                    btn.Attributes.Add("onclick", "window.open('ViewPrintGateOpra.aspx?GateOpraID=" + DataKey + "', 'ViewPrint', config='fullscreen=1,scrollbars=yes')");
                }
            }
        }
    }

    protected void AreaDetailsView_RowCommand(Object sender, DetailsViewCommandEventArgs e)
    {
        DetailsViewRow row = AreaDetailsView.Rows[0];
        int areaID = Convert.ToInt32(row.Cells[1].Text);

        if (e.CommandName == "Area_Del")
        {
            if (FormController.Instance.DeleteAreaForm(areaID))
            {
                AreaGridView.DataBind();
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "刪除成功！");
            }
            else
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "刪除失敗！");
            }
        }

        if (e.CommandName == "Area_Download")
        {
            FormArea formArea = FormController.Instance.GetAreaForm(areaID);
            if (formArea == null)
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "無法下載檔案！");
            }
            else
            {
                DownloadArea(formArea);
            }
        }

        if (e.CommandName == "Area_DownloadToSign")
        {
            FormArea formArea = FormController.Instance.GetAreaForm(areaID);
            if (formArea == null)
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "無法下載檔案！");
            }
            else
            {
                DownloadAreaToSign(formArea);
            }
        }
    }

    protected void GateApplyDetailsView_RowCommand(Object sender, DetailsViewCommandEventArgs e)
    {
        DetailsViewRow row = GateApplyDetailsView.Rows[0];
        int gateApplyID = Convert.ToInt32(row.Cells[1].Text);

        if (e.CommandName == "GateApply_Del")
        {
            if (FormController.Instance.DeleteGateApplyForm(gateApplyID))
            {
                GateApplyGridView.DataBind();
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "刪除成功！");
            }
            else
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "刪除失敗！");
            }
        }

        if (e.CommandName == "GateApply_Download")
        {
            FormGate info = FormController.Instance.GetGateApplyForm(gateApplyID);
            if (info == null)
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "無法下載檔案！");
            }
            else
            {
                DownloadGateApply(info);
            }
        }

        if (e.CommandName == "GateApply_DownloadToSign")
        {
            FormGate info = FormController.Instance.GetGateApplyForm(gateApplyID);
            if (info == null)
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "無法下載檔案！");
            }
            else
            {
                DownloadGateApplyToSign(info);
            }
        }

    }

    protected void GateOpraDetailsView_RowCommand(Object sender, DetailsViewCommandEventArgs e)
    {
        DetailsViewRow row = GateOpraDetailsView.Rows[0];
        int gateOpraID = Convert.ToInt32(row.Cells[1].Text);

        if (e.CommandName == "GateOpra_Del")
        {
            if (FormController.Instance.DeleteGateOpraForm(gateOpraID))
            {
                GateOpraGridView.DataBind();
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "刪除成功！");
            }
            else
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "刪除失敗！");
            }
        }

        if (e.CommandName == "GateOpra_Download")
        {
            FormGateOperate info = FormController.Instance.GetGateOpraForm(gateOpraID);
            if (info == null)
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "無法下載檔案！");
            }
            else
            {
                DownloadGateOpra(info);
            }
        }

        if (e.CommandName == "GateOpra_DownloadToSign")
        {
            FormGateOperate info = FormController.Instance.GetGateOpraForm(gateOpraID);
            if (info == null)
            {
                IISI.Util.WebClientMessage.ShowMsg(this.Page, "無法下載檔案！");
            }
            else
            {
                DownloadGateOpraToSign(info);
            }
        }
    }

    //protected void AreaGridView_RowCommand(Object sender, GridViewCommandEventArgs e)
    //{
    //    //int index = Convert.ToInt32(e.CommandArgument);
    //    //string DataKey =  AreaGridView.DataKeys[index].Value.ToString();




    //    //if (e.CommandName == "Area_Download")
    //    //{
    //    //    DataSet ds = IET.MLIA.DB.Form.getAreaform(DataKey, dbconn);

    //    //    if (ds == null) 
    //    //    {
    //    //       msg("無法下載檔案！");
    //    //    }
    //    //    else 
    //    //    {                
    //    //      DownloadArea(ds);
    //    //    }  
    //    //}


    //    //if (e.CommandName == "Area_ViewAndPrint")
    //    //{

    //    //}

    //}

    private void DownloadAreaToSign(FormArea info)
    {
        try
        {
            string fileName = "AreaApply" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".doc";
            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.ContentType = "application/msword";
            Page.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");


            string output = string.Empty;
            using (System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("Data/Area.xml")))
            {
            output = sr.ReadToEnd();
            }


            //申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //工作內容
            output = output.Replace("$Job", info.AreaJob);

            //位置
            output = output.Replace("$Place", info.AreaPlace);

            //人數
            output = output.Replace("$Pepole", info.AreaPepoleCount.ToString());

            //預定時間
            DateTime sttime = info.AreaSTDateTime;
            DateTime endtime = info.AreaEndDateTime;

            output = output.Replace("$SYear", (sttime.Year - 1911).ToString());
            output = output.Replace("$SMonth", sttime.Month.ToString());
            output = output.Replace("$SDay", sttime.Day.ToString());
            output = output.Replace("$STime", sttime.Hour.ToString() + " : " + sttime.Minute.ToString() + " : " + sttime.Second.ToString());

            output = output.Replace("$EYear", (endtime.Year - 1911).ToString());
            output = output.Replace("$EMonth", endtime.Month.ToString());
            output = output.Replace("$EDay", endtime.Day.ToString());
            output = output.Replace("$ETime", endtime.Hour.ToString() + " : " + endtime.Minute.ToString() + " : " + endtime.Second.ToString());

            // 申請單位或廠商
            output = output.Replace("$company", string.Empty);

            //申請人手機
            output = output.Replace("$Phone", info.AreaApplyPersonPhone);

            //管制區經辦人
            output = output.Replace("$DealPepole", string.Empty);

            //主任
            output = output.Replace("$Officer", string.Empty);

            //中水局主辦課室
            output = output.Replace("$Org", string.Empty);

            //警勤室
            output = output.Replace("$Guard", string.Empty);

            //登陸車牌
            output = output.Replace("$CarNo", info.CarNo);

            //保全崗哨
            output = output.Replace("$Sentry", string.Empty);

            Response.Write(output);
            Response.End();

        }
        catch
        {

        }
    }

    private void DownloadArea(FormArea info)
    {
        try
        {
            string fileName = "AreaApply" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".doc";
            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.ContentType = "application/msword";
            Page.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");


            string output = string.Empty;
            using (System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("Data/Area.xml")))
            {
                output = sr.ReadToEnd();
            }

            //申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //工作內容
            output = output.Replace("$Job", info.AreaJob);

            //位置
            output = output.Replace("$Place", info.AreaPlace);

            //人數
            output = output.Replace("$Pepole", info.AreaPepoleCount.ToString());

            //預定時間
            DateTime sttime = info.AreaSTDateTime;
            DateTime endtime = info.AreaEndDateTime;

            output = output.Replace("$SYear", (sttime.Year - 1911).ToString());
            output = output.Replace("$SMonth", sttime.Month.ToString());
            output = output.Replace("$SDay", sttime.Day.ToString());
            output = output.Replace("$STime", sttime.Hour.ToString() + " : " + sttime.Minute.ToString() + " : " + sttime.Second.ToString());

            output = output.Replace("$EYear", (endtime.Year - 1911).ToString());
            output = output.Replace("$EMonth", endtime.Month.ToString());
            output = output.Replace("$EDay", endtime.Day.ToString());
            output = output.Replace("$ETime", endtime.Hour.ToString() + " : " + endtime.Minute.ToString() + " : " + endtime.Second.ToString());

            // 申請單位或廠商
            output = output.Replace("$company", info.AreaApplyCompany);

            //申請人手機
            output = output.Replace("$Phone", info.AreaApplyPersonPhone);

            //管制區經辦人
            output = output.Replace("$DealPepole", info.AreaDealPepole);

            //主任
            output = output.Replace("$Officer", info.AreaOfficer);

            //中水局主辦課室
            output = output.Replace("$Org", info.AreaOrg);

            //警勤室
            output = output.Replace("$Guard", info.AreaGuard);

            //登陸車牌
            output = output.Replace("$CarNo", info.CarNo);

            //保全崗哨
            output = output.Replace("$Sentry", info.Sentry);

            Response.Write(output);
            Response.End();

        }
        catch (Exception e)
        {

        }
    }

    private void DownloadGateApplyToSign(FormGate info)
    {
        try
        {
            string fileName = "GateApply" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".doc";
            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.ContentType = "application/msword";
            Page.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");

            string output = string.Empty;
            System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("Data/GateApply.xml"));
            output = sr.ReadToEnd();
            sr.Close();

            ////申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //預定時間
            DateTime sttime = info.GateApplySTDateTime;
            DateTime endtime = info.GateApplyEndDateTime;

            output = output.Replace("$SYear", (sttime.Year - 1911).ToString());
            output = output.Replace("$SMonth", sttime.Month.ToString());
            output = output.Replace("$SDay", sttime.Day.ToString());
            output = output.Replace("$STime", sttime.Hour.ToString() + " : " + sttime.Minute.ToString() + " : " + sttime.Second.ToString());

            output = output.Replace("$EYear", (endtime.Year - 1911).ToString());
            output = output.Replace("$EMonth", endtime.Month.ToString());
            output = output.Replace("$EDay", endtime.Day.ToString());
            output = output.Replace("$ETime", endtime.Hour.ToString() + " : " + endtime.Minute.ToString() + " : " + endtime.Second.ToString());

            //地點
            output = output.Replace("$Place", info.GateApplyPlace);

            //事由
            output = output.Replace("$Job", info.GateApplyJob);

            //申請單位
            output = output.Replace("$Conpany", info.GateApplyCompany);

            //申請人
            output = output.Replace("$ApplePerson", string.Empty);

            //聯絡電話
            output = output.Replace("$ApplePhone", info.GateApplyPersonPhone);

            //警勤室
            output = output.Replace("$Guard", string.Empty);

            //閘門操控人員
            output = output.Replace("$GatePseron", string.Empty);

            //單位主管
            output = output.Replace("$Officer", string.Empty);

            //廠商確實完成工作時間
            DateTime finishTime = info.GateApplyFinishJobTime;
            string finTimestr = (finishTime.Year - 1911).ToString() + "年" + (finishTime.Month).ToString() + "月" + (finishTime.Day).ToString() + "日 時間：" + finishTime.Hour.ToString() + " : " + finishTime.Minute.ToString() + " : " + finishTime.Second.ToString();
            output = output.Replace("$FinishTime", finTimestr);

            Response.Write(output);
            Response.End();
        }
        catch
        {
        }
    }

    private void DownloadGateApply(FormGate info)
    {
        try
        {
            string fileName = "GateApply" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".doc";
            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.ContentType = "application/msword";
            Page.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");

            string output = string.Empty;
            System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("Data/GateApply.xml"));
            output = sr.ReadToEnd();
            sr.Close();

            ////申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //預定時間
            DateTime sttime = info.GateApplySTDateTime;
            DateTime endtime = info.GateApplyEndDateTime;

            output = output.Replace("$SYear", (sttime.Year - 1911).ToString());
            output = output.Replace("$SMonth", sttime.Month.ToString());
            output = output.Replace("$SDay", sttime.Day.ToString());
            output = output.Replace("$STime", sttime.Hour.ToString() + " : " + sttime.Minute.ToString() + " : " + sttime.Second.ToString());

            output = output.Replace("$EYear", (endtime.Year - 1911).ToString());
            output = output.Replace("$EMonth", endtime.Month.ToString());
            output = output.Replace("$EDay", endtime.Day.ToString());
            output = output.Replace("$ETime", endtime.Hour.ToString() + " : " + endtime.Minute.ToString() + " : " + endtime.Second.ToString());

            //地點
            output = output.Replace("$Place", info.GateApplyPlace);

            //事由
            output = output.Replace("$Job", info.GateApplyJob);

            //申請單位
            output = output.Replace("$Conpany", info.GateApplyCompany);

            //申請人
            output = output.Replace("$ApplePerson", info.GateApplyPerson);

            //聯絡電話
            output = output.Replace("$ApplePhone", info.GateApplyPersonPhone);

            //警勤室
            output = output.Replace("$Guard", info.GateApplyGuard);

            //閘門操控人員
            output = output.Replace("$GatePseron", info.GateApplyGateOpratePerson);

            //單位主管
            output = output.Replace("$Officer", info.GateApplyOfficer);

            //廠商確實完成工作時間
            DateTime finishTime = info.GateApplyFinishJobTime;
            string finTimestr = (finishTime.Year - 1911).ToString() + "年" + (finishTime.Month).ToString() + "月" + (finishTime.Day).ToString() + "日 時間：" + finishTime.Hour.ToString() + " : " + finishTime.Minute.ToString() + " : " + finishTime.Second.ToString();
            output = output.Replace("$FinishTime", finTimestr);

            Response.Write(output);
            Response.End();

        }
        catch
        {

        }
    }

    private void DownloadGateOpraToSign(FormGateOperate info)
    {
        try
        {
            string fileName = "GateOpra" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".doc";
            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.ContentType = "application/msword";
            Page.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");

            string output = string.Empty;
            System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("Data/GateOpra.xml"));
            output = sr.ReadToEnd();
            sr.Close();

            ////申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("Year", (appDate.Year - 1911).ToString());
            output = output.Replace("Month", appDate.Month.ToString());
            output = output.Replace("Day", appDate.Day.ToString());

            //操  作  事  由
            output = output.Replace("GateOpraJob", info.GateOpraJob);

            //水庫水位
            output = output.Replace("GateOpraLYTWaterLevel", info.GateOpraLYTWaterLevel.ToString());

            //後池堰水位
            output = output.Replace("GateOpraYanWaterLevel", info.GateOpraYanWaterLevel.ToString());

            //啟閉時間
            output = output.Replace("GateOpraSwitchDateTime", info.GateOpraSwitchDateTime.ToString("yyyy/MM/dd HH:mm:ss"));

            //操作閘門
            output = output.Replace("GateOpraGate", info.GateOpraGate);

            //放流時間
            output = output.Replace("GateOpraHours", info.GateOpraHours.ToString());

            //供水量
            output = output.Replace("GateOpraWaterSupply", info.GateOpraWaterSupply.ToString());

            //閘門操作人員
            output = output.Replace("GateOpraPerson", "         ");

            //警勤室
            output = output.Replace("GateOpraPolice", "   ");

            //調配人員
            output = output.Replace("GateOpraDeployPerson", "        ");

            //單位主管
            output = output.Replace("GateOpraManager", "         ");

            //出水工1-1閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow1_1))
            {
                output = output.Replace("GateOpraOutFlow1_1", string.Empty);
            }
            else
            {
                output = output.Replace("GateOpraOutFlow1_1", info.GateOpraOutFlow1_1);
            }

            //出水工1-2閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow1_2))
            {
                output = output.Replace("GateOpraOutFlow1_2", string.Empty);
            }
            else
            {
                output = output.Replace("GateOpraOutFlow1_2", info.GateOpraOutFlow1_2);
            }

            //出水工2-1閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow2_1))
            {
                output = output.Replace("GateOpraOutFlow2_1", string.Empty);
            }
            else
            {
                output = output.Replace("GateOpraOutFlow2_1", info.GateOpraOutFlow2_1);
            }

            ////出水工2-2閘門開度
            //if (Convert.DBNull.Equals(ds.Tables[0].Rows[0]["GateOpraOutFlow2_2"].ToString()))
            //{
            //    output = output.Replace("GateOpraOutFlow2_2", "");
            //}
            //else
            //{
            //    output = output.Replace("GateOpraOutFlow2_2", ds.Tables[0].Rows[0]["GateOpraOutFlow2_2"].ToString());
            //}

            //資料時間
            output = output.Replace("GateOpraTime", info.GateOpraTime.ToString("yyyy/MM/dd HH:mm:ss"));

            //放流量
            output = output.Replace("GateOpraOutFlow", info.GateOpraOutFlow.ToString());

            Response.Write(output);
            Response.End();
        }
        catch
        {
        }
    }

    private void DownloadGateOpra(FormGateOperate info)
    {
        try
        {
            string fileName = "GateOpra" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".doc";
            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.ContentType = "application/msword";
            Page.Response.HeaderEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            Page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");

            string output = string.Empty;
            System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("Data/GateOpra.xml"));
            output = sr.ReadToEnd();
            sr.Close();

            //申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("Year", (appDate.Year - 1911).ToString());
            output = output.Replace("Month", appDate.Month.ToString());
            output = output.Replace("Day", appDate.Day.ToString());

            //操  作  事  由
            output = output.Replace("GateOpraJob", info.GateOpraJob);

            //水庫水位
            output = output.Replace("GateOpraLYTWaterLevel", info.GateOpraLYTWaterLevel.ToString());

            //後池堰水位
            output = output.Replace("GateOpraYanWaterLevel", info.GateOpraYanWaterLevel.ToString());

            //啟閉時間
            output = output.Replace("GateOpraSwitchDateTime", info.GateOpraSwitchDateTime.ToString("yyyy/MM/dd HH:mm:ss"));

            //操作閘門
            output = output.Replace("GateOpraGate", info.GateOpraGate);

            //放流時間
            output = output.Replace("GateOpraHours", info.GateOpraHours.ToString());

            //供水量
            output = output.Replace("GateOpraWaterSupply", info.GateOpraWaterSupply.ToString());

            //閘門操作人員
            output = output.Replace("GateOpraPerson", info.GateOpraPerson);

            //警勤室
            output = output.Replace("GateOpraPolice", info.GateOpraPolice);

            //調配人員
            output = output.Replace("GateOpraDeployPerson", info.GateOpraDeployPerson);

            //單位主管
            output = output.Replace("GateOpraManager", info.GateOpraManager);

            //出水工1-1閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow1_1))
            {
                output = output.Replace("GateOpraOutFlow1_1", string.Empty);
            }
            else
            {
                output = output.Replace("GateOpraOutFlow1_1", info.GateOpraOutFlow1_1);
            }

            //出水工1-2閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow1_2))
            {
                output = output.Replace("GateOpraOutFlow1_2", string.Empty);
            }
            else
            {
                output = output.Replace("GateOpraOutFlow1_2", info.GateOpraOutFlow1_2);
            }

            //出水工2-1閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow2_1))
            {
                output = output.Replace("GateOpraOutFlow2_1", string.Empty);
            }
            else
            {
                output = output.Replace("GateOpraOutFlow2_1", info.GateOpraOutFlow2_1);
            }

            ////出水工2-2閘門開度
            //if (Convert.DBNull.Equals(ds.Tables[0].Rows[0]["GateOpraOutFlow2_2"].ToString()))
            //{
            //    output = output.Replace("GateOpraOutFlow2_2", "");
            //}
            //else
            //{
            //    output = output.Replace("GateOpraOutFlow2_2", ds.Tables[0].Rows[0]["GateOpraOutFlow2_2"].ToString());
            //}

            //資料時間
            output = output.Replace("GateOpraTime", info.GateOpraTime.ToString("yyyy/MM/dd HH:mm:ss"));

            //放流量
            output = output.Replace("GateOpraOutFlow", info.GateOpraOutFlow.ToString());

            Response.Write(output);
            Response.End();
        }
        catch
        {
        }
    }

    protected void AreaDetailsView_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        AreaGridView.DataBind();
    }

    protected void GateApplyDetailsView_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        GateApplyGridView.DataBind();
    }

    protected void GateOpraDetailsView_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        GateOpraGridView.DataBind();
    }

    protected void Date_SelectedIndexChanged(object sender, EventArgs e)
    {
        setLb();
        AreaGridView.DataBind();
        GateApplyGridView.DataBind();
        GateOpraGridView.DataBind();
    }
}