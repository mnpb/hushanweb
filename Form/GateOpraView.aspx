﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GateOpraView.aspx.cs" Inherits="Form_GateOpraView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
<!--
.style1 {font-size: 16px}
.style2 {font-size: 18px}
.style3 {font-size: 12px; }
-->
</style>
</head>
<body>
<form id="form1" runat="server">
    <table width="937" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
  <tr>
    <td colspan="8" nowrap="nowrap" class="style1"><p align="center" class="style2">湖山水庫出水工閘門操作紀錄表 </p></td>
  </tr>
  <tr>
  <td colspan="8" nowrap="nowrap" class="style1"><p align="right" class="style3">日期:<asp:Label 
          ID="yearTb" runat="server"></asp:Label>
      年<asp:Label ID="monthTb" runat="server"></asp:Label>
      月<asp:Label ID="dayTb" runat="server"></asp:Label>
      日 </p></td>
  </tr>
  <tr>
    <td width="108" nowrap="nowrap"><p align="center" class="style3">操作事由 </p></td>
    <td width="129"><p align="center" class="style3">水庫水位(公尺)</p></td>
    <td width="126"><p align="center" class="style3">桶頭堰水位(公尺)</p></td>
    <td width="129"><p align="center" class="style3">啟閉時間 </p></td>
    <td width="74"><p align="center" class="style3">操作閘門</p></td>
    <td width="91"><p align="center" class="style3">放流時間(小時)</p></td>
    <td width="119"><p align="center" class="style3">供水量</p></td>
    <td width="143" nowrap="nowrap"><p align="center" class="style3">備註 </p></td>
  </tr>
  <tr>
    <td width="108" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraJobTb" runat="server"></asp:Label>
&nbsp;</p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3">&nbsp;</p></td>
    <td width="129" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraLYTWaterLevelTb" runat="server"></asp:Label>
&nbsp;</p></td>
    <td width="126" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraYanWaterLevelTb" runat="server"></asp:Label>
&nbsp;</p></td>
    <td width="129" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraSwitchDateTimeTb" runat="server"></asp:Label>
&nbsp;</p></td>
    <td width="74" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraGateTb" runat="server"></asp:Label>
&nbsp;</p></td>
    <td width="91" nowrap="nowrap" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraHoursTb" runat="server"></asp:Label>
        </p></td>
    <td width="119" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraWaterSupplyTb" runat="server"></asp:Label>
        </p></td>
    <td width="143" rowspan="5" valign="top"><p class="style3">壹、敬會警勤室人員</p>
    <p class="style3"> 貳、敬會閘門操作人員</p>    </td>
  </tr>
  
  <tr>
    <td colspan="7" valign="bottom" nowrap="nowrap"><p class="style3">
        調配人員：<asp:Label ID="GateOpraDeployPersonTb" runat="server"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;警勤室：<asp:Label ID="GateOpraPoliceTb" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        單位主管：<asp:Label ID="GateOpraManagerTb1" runat="server"></asp:Label>
        </p>
    <p class="style3">&nbsp;</p>
    <p class="style3">&nbsp;</p></td>
  </tr>
  <tr>
    <td nowrap="nowrap" valign="bottom"><p align="center" class="style3">出水工1-1閘門開度<Br>(公分) </p></td>
    <td nowrap="nowrap" valign="bottom"><p align="center" class="style3">出水工1-2閘門開度<Br>(公分)</p></td>
    <td nowrap="nowrap" valign="bottom"><p align="center" class="style3">出水工2-1閘門開度<Br>(公分)</p></td>
  <%--  <td nowrap="nowrap" valign="bottom"><p align="center" class="style3">出水工2-2閘門開度<Br>(公分)</p></td>--%>
    <td nowrap="nowrap" colspan="2" valign="bottom"><p align="center" class="style3">資料時間</div></td>
    <td nowrap="nowrap" valign="bottom"><p align="center" class="style3">放流量</p></td>
  </tr>
  <tr>
    <td nowrap="nowrap" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraOutFlow1_1Tb" runat="server"></asp:Label>
        </p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3">&nbsp;</p></td>
    <td nowrap="nowrap" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraOutFlow1_2Tb" runat="server"></asp:Label>
        </p></td>
    <td nowrap="nowrap" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraOutFlow2_1Tb" runat="server"></asp:Label>
        </p></td>
<%--    <td nowrap="nowrap" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraOutFlow2_2Tb" runat="server"></asp:Label>
        </p></td>--%>
    <td nowrap="nowrap" colspan="2" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraTimeTb" runat="server"></asp:Label>
        </p></td>
    <td nowrap="nowrap" valign="top"><p align="center" class="style3">
        <asp:Label ID="GateOpraOutFlowTb" runat="server"></asp:Label>
        </p></td>
  </tr>
  <tr>
    <td colspan="7" valign="bottom" nowrap="nowrap"><p class="style3">操作人員：<asp:Label 
            ID="GateOpraPersonTb" runat="server"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;單位主管：<asp:Label 
            ID="GateOpraManagerTb2" runat="server"></asp:Label>
        </p>
    <p class="style3">&nbsp;</p>
    <p class="style3">&nbsp;</p></td>
  </tr>
</table>
 
        <center>
        <input type="button" VALUE="關閉視窗" onClick="window.close()">
      </center>
    </form>
</body>
</html>
