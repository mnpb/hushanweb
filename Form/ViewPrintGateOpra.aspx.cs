﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_ViewPrintGateOpra : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int gateOpraID = Convert.ToInt32(Request.QueryString["GateOpraID"].ToString());
        var info = FormController.Instance.GetGateOpraForm(gateOpraID);
        if (info == null)
        {
            msg("無法瀏覽與列印");
        }
        else
        {
            GateOpra(info);
        }
    }

    private void msg(string ss)
    {
        this.Response.Write("<script>");
        this.Response.Write("alert('" + ss + "')");
        this.Response.Write("</script>");
    }

    private void GateOpra(FormGateOperate info)
    {
        try
        {
            string output = string.Empty;
            using (StreamReader sr = new StreamReader(Server.MapPath("Data/GateOpra.html"), Encoding.GetEncoding("BIG5")))
            {
                while (sr.EndOfStream == false)
                {
                    string line = sr.ReadLine();
                    output = output + line;
                }
            }

            //申請時間
            DateTime appDate = info.ApplyTime;
            output = output.Replace("$Year", (appDate.Year - 1911).ToString());
            output = output.Replace("$Month", appDate.Month.ToString());
            output = output.Replace("$Day", appDate.Day.ToString());

            //操  作  事  由
            output = output.Replace("$GateOpraJob", info.GateOpraJob);

            //水庫水位
            output = output.Replace("$GateOpraLYTWaterLevel", info.GateOpraLYTWaterLevel.ToString());

            //後池堰水位
            output = output.Replace("$GateOpraYanWaterLevel", info.GateOpraYanWaterLevel.ToString());

            //啟閉時間
            output = output.Replace("$GateOpraSwitchDateTime", info.GateOpraSwitchDateTime.ToString("yyyy/MM/dd HH:mm:ss"));

            //操作閘門
            output = output.Replace("$GateOpraGate", info.GateOpraGate);

            //放流時間
            output = output.Replace("$GateOpraHours", info.GateOpraHours.ToString());

            //供水量
            output = output.Replace("$GateOpraWaterSupply", info.GateOpraWaterSupply.ToString());

            //閘門操作人員
            output = output.Replace("$GateOpraPerson", info.GateOpraPerson);

            //警勤室
            output = output.Replace("$GateOpraPolice", info.GateOpraPolice);

            //調配人員
            output = output.Replace("$GateOpraDeployPerson", info.GateOpraDeployPerson);

            //單位主管
            output = output.Replace("$GateOpraManager", info.GateOpraManager);

            //出水工1-1閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow1_1))
            {
                output = output.Replace("$GateOpraOutFlow1_1", string.Empty);
            }
            else
            {
                output = output.Replace("$GateOpraOutFlow1_1", info.GateOpraOutFlow1_1);
            }

            //出水工1-2閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow1_2))
            {
                output = output.Replace("$GateOpraOutFlow1_2", string.Empty);
            }
            else
            {
                output = output.Replace("$GateOpraOutFlow1_2", info.GateOpraOutFlow1_2);
            }

            //出水工2-1閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow2_1))
            {
                output = output.Replace("$GateOpraOutFlow2_1", string.Empty);
            }
            else
            {
                output = output.Replace("$GateOpraOutFlow2_1", info.GateOpraOutFlow2_1);
            }

            //出水工2-2閘門開度
            if (string.IsNullOrEmpty(info.GateOpraOutFlow2_2))
            {
                output = output.Replace("$GateOpraOutFlow2_2", string.Empty);
            }
            else
            {
                output = output.Replace("$GateOpraOutFlow2_2", info.GateOpraOutFlow2_2);
            }

            //資料時間
            output = output.Replace("$GateOpraTime", info.GateOpraTime.ToString("yyyy/MM/dd HH:mm:ss"));

            //放流量
            output = output.Replace("$GateOpraOutFlow", info.GateOpraOutFlow.ToString());

            Literal outresult = new Literal();
            outresult.Text = output;

            block.Controls.Add(outresult);
        }
        catch
        {
        }
    }
}