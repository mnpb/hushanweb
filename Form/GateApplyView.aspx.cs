﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_GateApplyView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DateTime Date = DateTime.Now;
            ApplyYearLb.Text = Date.Year.ToString();
            ApplyMonthLb.Text = Date.Month.ToString();
            ApplyDayLb.Text = Date.Day.ToString();


            if (Request.Form["GateApplySTDateTime"] != null)
            {
                DateTime STDate = Convert.ToDateTime(Request.Form["GateApplySTDateTime"].ToString());
                GateApplySTDateTimeYearLb.Text = STDate.Year.ToString();
                GateApplySTDateTimeMonthLb.Text = STDate.Month.ToString();
                GateApplySTDateTimeDayLb.Text = STDate.Day.ToString();
                GateApplySTDateTimeLb.Text = STDate.Hour.ToString() + ":" + STDate.Minute.ToString() + ":" + STDate.Second.ToString();
            }


            if (Request.Form["GateApplyEndDateTime"] != null)
            {
                DateTime STDate = Convert.ToDateTime(Request.Form["GateApplyEndDateTime"].ToString());
                GateApplyEndDateTimeYearLb.Text = STDate.Year.ToString();
                GateApplyEndDateTimeMonthLb.Text = STDate.Month.ToString();
                GateApplyEndDateTimeDayLb.Text = STDate.Day.ToString();
                GateApplyEndDateTimeLb.Text = STDate.Hour.ToString() + ":" + STDate.Minute.ToString() + ":" + STDate.Second.ToString();
            }

            if (Request.Form["GateApplyPlaceTb"] != null)
            {
                GateApplyPlaceLb.Text = Request.Form["GateApplyPlaceTb"].ToString();
            }

            if (Request.Form["GateApplyJobTb"] != null)
            {
                GateApplyJobLb.Text = Request.Form["GateApplyJobTb"].ToString();
            }

            if (Request.Form["GateApplyCompanyTb"] != null)
            {
                GateApplyCompanyLb.Text = Request.Form["GateApplyCompanyTb"].ToString();
            }

            if (Request.Form["GateApplyPersonTb"] != null)
            {
                GateApplyPersonLb.Text = Request.Form["GateApplyPersonTb"].ToString();
            }

            if (Request.Form["GateApplyPersonPhoneTb"] != null)
            {
                GateApplyPersonPhoneLb.Text = Request.Form["GateApplyPersonPhoneTb"].ToString();
            }

            if (Request.Form["GateApplyGuardTb"] != null)
            {
                GateApplyGuardLb.Text = Request.Form["GateApplyGuardTb"].ToString();
            }

            if (Request.Form["GateApplyGateOpratePersonTb"] != null)
            {
                GateApplyGateOpratePersonLb.Text = Request.Form["GateApplyGateOpratePersonTb"].ToString();
            }

            if (Request.Form["GateApplyOfficerTb"] != null)
            {
                GateApplyOfficerLb.Text = Request.Form["GateApplyOfficerTb"].ToString();
            }

            if (Request.Form["GateApplyFinishJobTimeTb"] != null)
            {
                GateApplyFinishJobTimeLb.Text = Request.Form["GateApplyFinishJobTimeTb"].ToString();
            }
        }
        catch (Exception ee)
        {
        }
    }
}