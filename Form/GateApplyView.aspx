﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GateApplyView.aspx.cs" Inherits="Form_GateApplyView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <style type="text/css">
        p.MsoNormal
	{margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";
	        text-align: left;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-top: 0cm;
        }
 table.MsoNormalTable
	{font-size:10.0pt;
	font-family:"Times New Roman";
	}
        .style1
        {
            width: 107.15pt;
            height: 33pt;
        }
        .style2
        {
            width: 366.2pt;
            height: 33pt;
        }
        .style3
        {
            width: 272.6pt;
            height: 54pt;
        }
        .style4
        {
            width: 200.75pt;
            height: 54pt;
        }
        .style5
        {
            width: 272.6pt;
            height: 26pt;
        }
        .style6
        {
            width: 200.75pt;
            height: 26pt;
        }
        .style7
        {
            width: 473.35pt;
            height: 56pt;
        }
    </style>
</head>
<body style="width: 640px">
    <form id="form1" runat="server">


    <p align="center" class="MsoNormal" style="text-align:center">
        <b><span style="font-size:22.0pt;font-family:標楷體">湖山水庫管理中心－閘門啟閉申請單<span 
            lang="EN-US"><o:p></o:p></span></span></b></p>
    <p align="right" class="MsoNormal" 
        style="margin-right: 21.0pt; text-align: right; word-break: break-all">
        <span style="font-size:14.0pt;mso-bidi-font-size:12.0pt;
font-family:標楷體">申請時間：<span lang="EN-US"><span style="mso-spacerun:yes"><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><asp:Label ID="ApplyYearLb" runat="server"></asp:Label>
        </span></span></span></span>年<span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"> <asp:Label ID="ApplyMonthLb" runat="server"></asp:Label>
    </span></span></span>月<span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"> <asp:Label ID="ApplyDayLb" runat="server"></asp:Label>
    </span></span></span>日<span lang="EN-US"><o:p></o:p></span></span></p>
    <div align="center">
        <table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="margin-left:1.4pt;border-collapse:collapse;border:none;mso-border-alt:
 solid windowtext .5pt;mso-padding-alt:0cm 1.4pt 0cm 1.4pt;mso-border-insideh:
 .5pt solid windowtext;mso-border-insidev:.5pt solid windowtext">
            <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">
                <td class="style1" colspan="2" 
                    style="border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt;" 
                    width="143">
                    <p align="center" class="MsoNormal" style="text-align:center">
                        <span style="font-size:16.0pt;font-family:標楷體">時間<span lang="EN-US"><o:p></o:p></span></span></p>
                </td>
                <td class="style2" colspan="2" 
                    style="border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt; border-left-style: none; border-left-color: inherit; border-left-width: medium;" 
                    valign="top" width="488">
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:標楷體">
                        <span style="mso-spacerun:yes">&nbsp;<span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><asp:Label 
                            ID="GateApplySTDateTimeYearLb" runat="server"></asp:Label>
                        </span></span></span></span><span style="font-size:16.0pt;
  font-family:標楷體">年<span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"> <asp:Label ID="GateApplySTDateTimeMonthLb" runat="server"></asp:Label>
    </span></span></span>月<span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"> <asp:Label ID="GateApplySTDateTimeDayLb" runat="server"></asp:Label>
    </span></span></span>日 時間：<span lang="EN-US"><span style="mso-spacerun:yes"><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><asp:Label 
                            ID="GateApplySTDateTimeLb" runat="server"></asp:Label>
                        </span></span>&nbsp;</span></span>起<span lang="EN-US"><o:p></o:p></span></span></p>
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;
  font-family:標楷體"><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt">
                        <span 
            style="mso-spacerun:yes">&nbsp;<asp:Label ID="GateApplyEndDateTimeYearLb" runat="server"></asp:Label>
    </span></span></span>年<span style="mso-spacerun:yes"><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt">
                        <asp:Label ID="GateApplyEndDateTimeMonthLb" runat="server"></asp:Label>
                        </span></span></span>月 <span style="mso-spacerun:yes">
                        <span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt">
                        <asp:Label ID="GateApplyEndDateTimeDayLb" runat="server"></asp:Label>
                        </span></span></span>日 時間：<span lang="EN-US"><span style="mso-spacerun:yes"><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><asp:Label 
                            ID="GateApplyEndDateTimeLb" runat="server"></asp:Label>
    &nbsp;</span></span></span></span>訖<span lang="EN-US"><o:p></o:p></span></span></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:1;height:41.35pt">
                <td colspan="2" style="width:107.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 1.4pt 0cm 1.4pt;height:41.35pt" width="143">
                    <p align="center" class="MsoNormal" style="text-align:center">
                        <span style="font-size:16.0pt;font-family:標楷體">地點<span lang="EN-US"><o:p></o:p></span></span></p>
                </td>
                <td colspan="2" style="width:366.2pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 1.4pt 0cm 1.4pt;height:41.35pt" 
                    valign="top" width="488">
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:標楷體">
                        <span style="mso-spacerun:yes">&nbsp;</span><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyPlaceLb" runat="server"></asp:Label>
    </span></span></span></o:p></span>
                    </p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:2;height:49.6pt">
                <td colspan="2" style="width:107.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 1.4pt 0cm 1.4pt;height:49.6pt" width="143">
                    <p align="center" class="MsoNormal" style="text-align:center">
                        <span style="font-size:16.0pt;font-family:標楷體">事由<span lang="EN-US"><o:p></o:p></span></span></p>
                </td>
                <td colspan="2" style="width:366.2pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 1.4pt 0cm 1.4pt;height:49.6pt" valign="top" 
                    width="488">
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:標楷體">
                        <span style="mso-spacerun:yes">&nbsp;</span><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyJobLb" runat="server"></asp:Label>
    </span></span></span></o:p></span>
                    </p>
                </td>
            </tr>
            <tr style="mso-yfti-irow: 3;">
                <td class="style3" colspan="3" 
                    style="border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt; border-top-style: none; border-top-color: inherit; border-top-width: medium;" 
                    valign="top" width="363">
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;font-family:標楷體">申請單位（廠商）：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyCompanyLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;font-family:標楷體">申請人員：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyPersonLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;font-family:標楷體">聯絡電話：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyPersonPhoneLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                </td>
                <td class="style4" 
                    style="border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt; border-left-style: none; border-left-color: inherit; border-left-width: medium; border-top-style: none; border-top-color: inherit; border-top-width: medium;" 
                    valign="top" width="268">
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:標楷體"><o:p>&nbsp;</o:p></span></p>
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph">
                        <span style="font-size:16.0pt;font-family:標楷體">警勤室：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyGuardLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:標楷體"><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow: 4;">
                <td class="style5" colspan="3" 
                    style="border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt; border-top-style: none; border-top-color: inherit; border-top-width: medium;" 
                    valign="top" width="363">
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;font-family:標楷體">閘門操控人員：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyGateOpratePersonLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                </td>
                <td class="style6" 
                    style="border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt; border-left-style: none; border-left-color: inherit; border-left-width: medium; border-top-style: none; border-top-color: inherit; border-top-width: medium;" 
                    valign="top" width="268">
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;font-family:標楷體">單位主管：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyOfficerLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                </td>
            </tr>
            <tr style="mso-yfti-irow: 5;">
                <td class="style7" colspan="4" 
                    style="border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 1.4pt 0cm 1.4pt; border-top-style: none; border-top-color: inherit; border-top-width: medium;" 
                    valign="top" width="631">
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-line-height-alt:0pt">
                        <span style="font-size:16.0pt;font-family:標楷體">廠商<b 
                            style="mso-bidi-font-weight:normal">確實完成</b>工作時間：<span lang="EN-US"><o:p><span
lang=EN-US style='font-size:14.0pt'><span lang="EN-US" style="font-size:14.0pt"><span 
            style="mso-spacerun:yes"><asp:Label ID="GateApplyFinishJobTimeLb" runat="server"></asp:Label>
    </span></span></span></o:p></span></span>
                    </p>
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-line-height-alt:0pt">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:
  標楷體"><o:p>&nbsp;</o:p></span></p>
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-line-height-alt:0pt">
                        <span style="font-size:16.0pt;font-family:標楷體">申請廠商確認：<span lang="EN-US"><span 
                            style="mso-spacerun:yes">&nbsp; </span><o:p></o:p></span></span>
                    </p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:6;height:61.4pt">
                <td colspan="4" style="width:473.35pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 1.4pt 0cm 1.4pt;height:61.4pt" valign="top" width="631">
                    <p class="MsoNormal">
                        <span style="font-size:16.0pt;font-family:標楷體">注意事項：<span lang="EN-US"><o:p></o:p></span></span></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:7;mso-yfti-lastrow:yes;height:81.75pt">
                <td style="width:40.4pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 1.4pt 0cm 1.4pt;height:81.75pt" valign="top" width="54">
                    <p class="MsoNormal">
                        <span style="font-size:18.0pt;font-family:標楷體">備<span lang="EN-US"><o:p></o:p></span></span></p>
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:18.0pt;font-family:標楷體"><o:p>&nbsp;</o:p></span></p>
                    <p class="MsoNormal">
                        <span style="font-size:18.0pt;font-family:標楷體">註<span lang="EN-US"><o:p></o:p></span></span></p>
                </td>
                <td colspan="3" style="width:432.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 1.4pt 0cm 1.4pt;height:81.75pt" width="577">
                    <p class="MsoNormal" style="margin-left:.1pt;text-align:justify;text-justify:
  inter-ideograph;mso-line-height-alt:0pt">
                        <span lang="EN-US" style="font-size:
  16.0pt;font-family:標楷體">1.</span><span style="font-size:16.0pt;font-family:
  標楷體">開啟閘門（<span lang="EN-US">20CMS</span>以上）時請控制室播放<b style="mso-bidi-font-weight:
  normal">放流警報</b>，並由警勤人員巡查下游安全後，始可操作閘門啟閉。<span lang="EN-US"><o:p></o:p></span></span></p>
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-line-height-alt:0pt">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:
  標楷體"><o:p>&nbsp;</o:p></span></p>
                    <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-line-height-alt:0pt">
                        <span lang="EN-US" style="font-size:16.0pt;font-family:
  標楷體">2.</span><span style="font-size:16.0pt;font-family:標楷體">本表一式一份：影印本承辦人留存<span 
                            lang="EN-US"><o:p></o:p></span></span></p>
                </td>
            </tr>
            <![if !supportMisalignedColumns]>
            <tr height="0">
                <td style="border:none" width="54">
                </td>
                <td style="border:none" width="89">
                </td>
                <td style="border:none" width="221">
                </td>
                <td style="border:none" width="268">
                </td>
            </tr>
            <![endif]>
        </table>
    </div>
    <p class="MsoNormal" style="text-indent:21.0pt;mso-char-indent-count:1.5;
tab-stops:list 36.0pt">
        <span style="font-size:14.0pt;font-family:標楷體">製表人：<span lang="EN-US"><o:p></o:p></span></span></p>


    </form>
     <center>
        <input type="button" VALUE="關閉視窗" onClick="window.close()">
      </center>
</body>
</html>
