﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewPrintGateOpra.aspx.cs" Inherits="Form_ViewPrintGateOpra" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Button ID="ViewBtn" runat="server" onclientclick="PreviewScreen(block)" Text="預覽列印" />
    <asp:Button ID="PrintBtn" runat="server" onclientclick="PrintScreen(block)" Text="直接列印" />
    <asp:Button ID="CloseBtn" runat="server" onclientclick="Close()" Text="關閉視窗" />
    
    <div id="block" runat="server">
    </div>
    </form>
</body>
</html>

<script type="text/javascript">

    function Close() {
        window.close();
    }


    function PrintScreen(block) {
        var value = block.innerHTML;
        var printPage = window.open("", "printPage", "");
        printPage.document.open();
        printPage.document.write("<object id=factory viewastext style='display:none' classid='clsid:1663ed61-23eb-11d2-b92f-008048fdd814' codebase='smsx.cab#Version=6,6,440,26'></object>");
        printPage.document.write("<HTML><head></head><BODY onload='factory.printing.portrait = false;factory.printing.leftMargin = 25; factory.printing.topMargin =15.0;factory.printing.rightMargin = 1.0;factory.printing.bottomMargin = 1.0; factory.printing.header =\"\"; factory.printing.footer = \"\"; window.print();window.close()'>");
        // printPage.document.write("<PRE>");
        printPage.document.write(value);
        //printPage.document.write("</PRE>");
        printPage.document.close("</BODY></HTML>");
    }

    function PreviewScreen(block) {
        var value = block.innerHTML;
        var printPage = window.open("", "printPage", "");
        printPage.document.open();
        printPage.document.write("<OBJECT classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2' height=0 id=wc name=wc width=0></OBJECT>");
        printPage.document.write("<object id=factory viewastext style='display:none' classid='clsid:1663ed61-23eb-11d2-b92f-008048fdd814' codebase='smsx.cab#Version=6,6,440,26'></object>");
        printPage.document.write("<HTML><head></head><BODY onload='javascript:factory.printing.portrait = false;factory.printing.header =\"\";factory.printing.footer =\"\";factory.printing.leftMargin = 25; factory.printing.topMargin =15.0;factory.printing.rightMargin = 1.0;factory.printing.bottomMargin = 1.0;wc.execwb(7,1);window.close()'>");
        printPage.document.write(value);
        printPage.document.close("</BODY></HTML>");

    }


</script>
