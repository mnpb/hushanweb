﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_AreaView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DateTime Date = DateTime.Now;
            ApplyYearLb.Text = Date.Year.ToString();
            ApplyMonthLb.Text = Date.Month.ToString();
            ApplyDayLb.Text = Date.Day.ToString();

            if (Request.Form["AreaJobTb"] != null)
            {
                AreaJobLb.Text = Request.Form["AreaJobTb"].ToString();
            }


            if (Request.Form["AreaPlaceTb"] != null)
            {
                AreaPlacebLb.Text = Request.Form["AreaPlaceTb"].ToString();
            }


            if (Request.Form["AreaPepoleCountTb"] != null)
            {
                AreaPepoleCountLb.Text = Request.Form["AreaPepoleCountTb"].ToString();
            }

            if (Request.Form["AreaSTDateTime"] != null)
            {
                DateTime STDate = Convert.ToDateTime(Request.Form["AreaSTDateTime"].ToString());
                AreaSTDateTimeYearLb.Text = STDate.Year.ToString();
                AreaSTDateTimeMonthLb.Text = STDate.Month.ToString();
                AreaSTDateTimeDayLb.Text = STDate.Day.ToString();
                AreaSTDateTimeLb.Text = STDate.Hour.ToString() + ":" + STDate.Minute.ToString() + ":" + STDate.Second.ToString();
            }

            if (Request.Form["AreaEndDateTime"] != null)
            {
                DateTime ETDate = Convert.ToDateTime(Request.Form["AreaEndDateTime"].ToString());
                AreaEndDateTimeYearLb.Text = ETDate.Year.ToString();
                AreaEndDateTimeMonthLb.Text = ETDate.Month.ToString();
                AreaEndDateTimeDayLb.Text = ETDate.Day.ToString();
                AreaEndDateTimeLb.Text = ETDate.Hour.ToString() + ":" + ETDate.Minute.ToString() + ":" + ETDate.Second.ToString();
            }


            if (Request.Form["AreaApplyCompanyTb"] != null)
            {
                AreaApplyCompanyLb.Text = Request.Form["AreaApplyCompanyTb"].ToString();
            }

            if (Request.Form["AreaApplyPersonPhoneTb"] != null)
            {
                AreaApplyPersonPhoneLb.Text = Request.Form["AreaApplyPersonPhoneTb"].ToString();
            }

            if (Request.Form["AreaDealPepoleTb"] != null)
            {
                AreaDealPepoleLb.Text = Request.Form["AreaDealPepoleTb"].ToString();
            }


            if (Request.Form["AreaOfficerTb"] != null)
            {
                AreaOfficerLb.Text = Request.Form["AreaOfficerTb"].ToString();
            }

            if (Request.Form["AreaGuardTb"] != null)
            {
                AreaGuardLb.Text = Request.Form["AreaGuardTb"].ToString();
            }

            if (Request.Form["AreaOrgTb"] != null)
            {
                AreaOrgLb.Text = Request.Form["AreaOrgTb"].ToString();
            }

            if (Request.Form["CarNoTb"] != null)
            {
                CarNoTb.Text = Request.Form["CarNoTb"].ToString();
            }

            if (Request.Form["SentryTb"] != null)
            {
                SentryTb.Text = Request.Form["SentryTb"].ToString();
            }
        }
        catch
        {
        }
    }
}