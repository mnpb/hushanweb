﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FloodEvent_MaintainStation : System.Web.UI.Page
{
    #region Properties

    List<FloodEvent> AllFloodEvents
    {
        get
        {
            return FloodEventController.Instance.GetAllFloodEvents();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowAllFloodEvents(0);
        }
    }
    private void ShowAllFloodEvents(int pageIndex)
    {
        StationBaseGridView.PageIndex = pageIndex;
        StationBaseGridView.DataSource = AllFloodEvents;
        StationBaseGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainStationUserControl.EventID = string.Empty;
        Popup.Title = "颱洪事件基本資料";
        Popup.Show();
    }

    #region GridView Events
    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowAllFloodEvents(e.NewPageIndex);
    }

    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string eventID = StationBaseGridView.DataKeys[e.NewEditIndex].Values["ID"].ToString();
        MaintainStationUserControl.EventID = eventID;
        Popup.Title = "颱洪事件基本資料";
        Popup.Show();
    }

    protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string eventID = StationBaseGridView.DataKeys[e.RowIndex].Values["ID"].ToString();
            if (!FloodEventController.Instance.DeleteEvent(eventID))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowAllFloodEvents(StationBaseGridView.PageIndex);
        }
    }

    #endregion

    protected void MaintainStationUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowAllFloodEvents(StationBaseGridView.PageIndex);
    }
}