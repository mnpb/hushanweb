﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainStation.ascx.cs" Inherits="FloodEvent_UserControls_MaintainStation" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />
<script type="text/javascript" src="<%=ResolveUrl("~/ThirdPartyComponents/My97DatePicker/WdatePicker.js") %>"></script>

<table class="fancytable" style="width: 450px">
    <tr>
        <th style="text-align: right">事件編號</th>
        <td style="text-align: left">
            <asp:TextBox ID="EventIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">事件名稱</th>
        <td style="text-align: left">
            <asp:TextBox ID="EventNameTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">成立時間</th>
        <td style="text-align: left">
            <input id="StartTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:00'})" style="width: 200px" type="text"  runat="server" /></td>
    </tr>
     <tr>
        <th style="text-align: right">撤除時間</th>
        <td style="text-align: left">
            <input id="EndTime" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy/MM/dd HH:mm:00'})" style="width: 200px" type="text"  runat="server" /></td>
    </tr>
    <tr>
        <th style="text-align: right">備註</th>
        <td style="text-align: left">
            <asp:TextBox ID="NoteTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>