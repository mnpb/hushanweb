﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FloodEvent_UserControls_MaintainStation : System.Web.UI.UserControl
{
    #region Properties

    private string eventID;
    public string EventID
    {
        get
        {
            return eventID;
        }
        set
        {
            eventID = value;
            ShowFloodEventInfo(FloodEventInfo);
        }
    }

    private FloodEvent FloodEventInfo
    {
        get
        {
            return FloodEventController.Instance.GetFloodEvent(EventID);
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitTime();
        }
    }

    private void InitTime()
    {
        DateTime now = DateTime.Now;
        StartTime.Value = now.ToString("yyyy/MM/dd HH:mm:00");
        EndTime.Value = now.ToString("yyyy/MM/dd HH:mm:00");
    }

    private void EmptyAllControls()
    {
        foreach (Control c in this.Controls)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                drp.SelectedIndex = 0;
            }
        }
        EventIDTextBox.Enabled = true;
        InitTime();
    }

    /// <summary>
    /// 顯示基本資料
    /// </summary>
    /// <param name="stationBaseInfo"></param>
    private void ShowFloodEventInfo(FloodEvent floodEvent)
    {
        if (floodEvent == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            EventIDTextBox.Text = floodEvent.ID;
            EventIDTextBox.Enabled = false; // 不可修改
            EventNameTextBox.Text = floodEvent.Name;
            StartTime.Value = floodEvent.StartTime.ToString("yyyy/MM/dd HH:mm:00");
            EndTime.Value = floodEvent.EndTime.ToString("yyyy/MM/dd HH:mm:00");
            NoteTextBox.Text = floodEvent.Note;
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newEvent = new FloodEvent
            {
                ID = EventIDTextBox.Text,
                Name = EventNameTextBox.Text,
                StartTime = Convert.ToDateTime(StartTime.Value),
                EndTime = Convert.ToDateTime(EndTime.Value),
                Note = NoteTextBox.Text,
            };
            if (!FloodEventController.Instance.AddEvent(newEvent))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}