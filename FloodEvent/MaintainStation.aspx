﻿<%@ Page Title="颱洪事件維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainStation.aspx.cs" Inherits="FloodEvent_MaintainStation" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/FloodEvent/UserControls/MaintainStation.ascx" TagPrefix="uc1" TagName="MaintainStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainStation runat="server" ID="MaintainStationUserControl" OnCompleted="MaintainStationUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增事件" CssClass="buttonStyle" /><br />
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="ID" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing" OnRowDeleting="StationBaseGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="事件編號">
                        <ItemTemplate><%# Eval("ID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="事件名稱">
                        <ItemTemplate><%# Eval("Name") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="成立時間">
                        <ItemTemplate><%# Eval("StartTime", "{0:yyyy/MM/dd HH:mm:00}") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="撤除時間">
                        <ItemTemplate><%# Eval("EndTime", "{0:yyyy/MM/dd HH:mm:00}") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="備註">
                        <ItemTemplate><%# Eval("Note") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

