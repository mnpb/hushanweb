﻿<%@ WebHandler Language="C#" Class="ShowTHChartHandler" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Reflection;

public class ShowTHChartHandler : IHttpHandler
{

    /// <summary>
    /// 取得報表類型
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private string GetChartType(HttpContext context)
    {
        return GetQueryValue(context, "ChartType"); // 時、日、月
    }
    private string GetDataType(HttpContext context)
    {
        return GetQueryValue(context, "DataType"); // 溫度、濕度
    }

    private string GetDateTime(HttpContext context, string fieldName)
    {
        return GetQueryValue(context, fieldName);
    }

    private string GetQueryValue(HttpContext context, string fieldName)
    {
        try
        {
            return context.Request.QueryString[fieldName].ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// 取得圖寬
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private int GetChartWidth(HttpContext context)
    {
        try
        {
            return Convert.ToInt32(GetQueryValue(context, "Width"));
        }
        catch
        {
            return 0;
        }
    }
    /// <summary>
    /// 取得圖高
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private int GetChartHeight(HttpContext context)
    {
        try
        {
            return Convert.ToInt32(GetQueryValue(context, "Height"));
        }
        catch
        {
            return 0;
        }
    }

    private List<TemperatureHumidityReportInfo> GetInfos(HttpContext context, string chartType)
    {
        switch (chartType)
        {
            case "H":
                return EnvCtrlController.Instance.GetTemperatureHumidityDayReport(Convert.ToDateTime(GetDateTime(context, "Date")));
            case "D":
                return EnvCtrlController.Instance.GetTemperatureHumidityMonthReport(Convert.ToInt32(GetDateTime(context, "Year")), Convert.ToInt32(GetDateTime(context, "Month")));
            case "M":
                return EnvCtrlController.Instance.GetTemperatureHumidityYearReport(Convert.ToInt32(GetDateTime(context, "Year")));
            default:
                return new List<TemperatureHumidityReportInfo>();
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string chartType = GetChartType(context);
            string dataType = GetDataType(context);
            int width = GetChartWidth(context);
            int height = GetChartHeight(context);
            var chart = GenChart(
                context,
                chartType,
                dataType,
                width,
                height,
                GetInfos(context, chartType));
            context.Response.ContentType = "image/png";
            chart.Bitmap().Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        }
        catch
        {
        }
    }


    #region 歷線圖

    /// <summary>
    /// 
    /// </summary>
    /// <param name="width">圖寬</param>
    /// <param name="height">圖高</param>
    /// <returns></returns>
    private Steema.TeeChart.Chart InitChart(int width, int height)
    {
        var webChart = new Steema.TeeChart.Web.WebChart();
        webChart.Width = width;
        webChart.Height = height;
        webChart.PictureFormat = Steema.TeeChart.Export.PictureFormats.JPEG;
        webChart.AutoPostback = false;
        webChart.TempChart = Steema.TeeChart.Web.TempChartStyle.Session;
        webChart.GetChartFile = "../TeeChartFiles/GetChart.aspx";

        Steema.TeeChart.Chart chart = webChart.Chart;
        chart.Series.Clear();
        chart.Axes.Custom.Clear();
        chart.Chart.Aspect.View3D = false;
        chart.Series.RemoveAllSeries();

        return chart;
    }

    /// <summary>
    /// 產生歷線圖
    /// </summary>
    /// <param name="chartType"></param>
    /// <param name="width">圖寬</param>
    /// <param name="height">圖高</param>
    /// <param name="allInfos"></param>
    private Steema.TeeChart.Chart GenChart(HttpContext context, string chartType, string dataType, int width, int height, List<TemperatureHumidityReportInfo> allInfos)
    {
        Steema.TeeChart.Chart chart = InitChart(width, height);
        GenLine(chartType, dataType, chart, allInfos, "#,##0.00");

        SetYCustom(chart);
        SetLinesColor(chart);
        SetAxeBottom(context, chartType, allInfos, chart, dataType);
        return chart;
    }

    //private void GenMinuteLine(string chartType, Steema.TeeChart.Chart chart, dynamic allInfos, string unit, string valueFormat)
    //{
    //    chart.Axes.Custom.Add(new Steema.TeeChart.Axis());

    //    chart.Axes.Custom[chart.Axes.Custom.Count - 1].Title.Text = "水位" + unit;
    //    chart.Axes.Custom[chart.Axes.Custom.Count - 1].Title.Angle = 90;

    //    string fieldName = "level_out";
    //    // 設定上下限界
    //    double maxBound = ChartUtility.GetMaxBound(GetMaxValue(allInfos, "WaterMinute", fieldName));
    //    double minBound = ChartUtility.GetMinBound(GetMinValue(allInfos, "WaterMinute", fieldName));

    //    if (maxBound < 5)
    //    {
    //        valueFormat = "#,##0.000";
    //    }
    //    else if (maxBound > 10)
    //    {
    //        valueFormat = "#,##0";
    //    }
    //    chart.Axes.Custom[chart.Axes.Custom.Count - 1].Labels.ValueFormat = valueFormat;

    //    chart.Series.Add(GenMinuteLine(allInfos, fieldName, "水位"));
    //    chart.Axes.Custom[chart.Axes.Custom.Count - 1].SetMinMax(minBound, maxBound);
    //}

    private void GenLine(string chartType, string dataType, Steema.TeeChart.Chart chart, List<TemperatureHumidityReportInfo> allInfos, string valueFormat)
    {
        string fieldDescription = string.Empty, unit = string.Empty;
        double maxBound = 0;
        double minBound = 0;
        if (dataType.Equals("T"))
        {
            maxBound = ChartUtility.GetMaxBound(GetMaxTemperatureValue(allInfos));
            minBound = ChartUtility.GetMinBound(GetMinTemperatureValue(allInfos));
            fieldDescription = "溫度";
            unit = "(°C)";
        }
        else if (dataType.Equals("H"))
        {
            maxBound = ChartUtility.GetMaxBound(GetMaxHumidityValue(allInfos));
            minBound = ChartUtility.GetMinBound(GetMinHumidityValue(allInfos));
            fieldDescription = "濕度";
            unit = "(%)";
        }
        chart.Axes.Custom.Add(new Steema.TeeChart.Axis());

        chart.Axes.Custom[chart.Axes.Custom.Count - 1].Title.Text = fieldDescription + unit;
        chart.Axes.Custom[chart.Axes.Custom.Count - 1].Title.Angle = 90;

        if (maxBound < 5)
        {
            valueFormat = "#,##0.000";
        }
        else if (maxBound > 10)
        {
            valueFormat = "#,##0";
        }
        chart.Axes.Custom[chart.Axes.Custom.Count - 1].Labels.ValueFormat = valueFormat;

        chart.Series.Add(GenLine(chartType, allInfos, "C" + dataType, "電腦機房" + fieldDescription));
        chart.Series.Add(GenLine(chartType, allInfos, "U" + dataType, "UPS機房" + fieldDescription));
        chart.Axes.Custom[chart.Axes.Custom.Count - 1].SetMinMax(minBound, maxBound);
        //chart.Series[chart.Series.Count - 1].ShowInLegend = false; // 下限線之legend不顯示
    }

    private void SetYCustom(Steema.TeeChart.Chart chart)
    {
        try
        {
            //chart.Series[1].CustomVertAxis = chart.Axes.Custom[1];
            chart.Axes.Custom[0].StartPosition = 1;
            chart.Axes.Custom[0].EndPosition = 100;
            chart.Axes.Left.StartPosition = 1;
            chart.Axes.Left.EndPosition = 100;
            //chart.Axes.Custom[1].StartPosition = 52;
            //chart.Axes.Custom[1].EndPosition = 100;
            chart.Axes.Left.SetMinMax(chart.Axes.Custom[0].Minimum, chart.Axes.Custom[0].Maximum);
            chart.Axes.Left.Title.Text = chart.Axes.Custom[0].Title.Text;

            //chart.Series[0].VertAxis = Steema.TeeChart.Styles.VerticalAxis.Left;
            //chart.Series[0].CustomVertAxis = chart.Axes.Custom[0];
        }
        catch
        {
        }
    }

    private void SetLinesColor(Steema.TeeChart.Chart chart)
    {
        try
        {
            chart.Series[0].Color = Color.Blue;
        }
        catch
        {
        }
    }

    private double GetMinTemperatureValue(List<TemperatureHumidityReportInfo> allInfos)
    {
        try
        {
            double cTemp = allInfos.Where(x => !x.Central_CRoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_CRoom_Temp).Min();
            double uTemp = allInfos.Where(x => !x.Central_URoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_URoom_Temp).Min();
            return Math.Min(cTemp, uTemp);
        }
        catch
        {
        }
        return 0;
    }
    private double GetMaxTemperatureValue(List<TemperatureHumidityReportInfo> allInfos)
    {
        try
        {
            double cTemp = allInfos.Where(x => !x.Central_CRoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_CRoom_Temp).Max();
            double uTemp = allInfos.Where(x => !x.Central_URoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_URoom_Temp).Max();
            return Math.Max(cTemp, uTemp);
        }
        catch
        {
        }
        return 0;
    }
    private double GetMinHumidityValue(List<TemperatureHumidityReportInfo> allInfos)
    {
        try
        {
            double cTemp = allInfos.Where(x => !x.Central_CRoom_RH.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_CRoom_RH).Min();
            double uTemp = allInfos.Where(x => !x.Central_URoom_RH.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_URoom_RH).Min();
            return Math.Min(cTemp, uTemp);
        }
        catch
        {
        }
        return 0;
    }
    private double GetMaxHumidityValue(List<TemperatureHumidityReportInfo> allInfos)
    {
        try
        {
            double cTemp = allInfos.Where(x => !x.Central_CRoom_RH.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_CRoom_RH).Max();
            double uTemp = allInfos.Where(x => !x.Central_URoom_RH.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Central_URoom_RH).Max();
            return Math.Max(cTemp, uTemp);
        }
        catch
        {
        }
        return 0;
    }

    /// <summary>
    /// 畫線
    /// </summary>
    /// <param name="chartType"></param>
    /// <param name="allInfos"></param>
    /// <param name="fieldName"></param>
    /// <param name="title"></param>
    /// <returns></returns>
    private Steema.TeeChart.Styles.Line GenLine(string chartType, List<TemperatureHumidityReportInfo> allInfos, string dataType, string title)
    {
        Steema.TeeChart.Styles.Line line = new Steema.TeeChart.Styles.Line();
        line.LinePen.Width = 3;
        line.Pointer.Visible = true;
        line.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Sphere;
        line.Title = title;
        string className = string.Empty;
        try
        {
            switch (chartType)
            {
                case "H":
                    foreach (var info in allInfos)
                    {
                        if (!info.Central_CRoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData))
                        {
                            if (dataType.Equals("CT"))
                            {
                                line.Add(info.Hour, info.Central_CRoom_Temp);
                            }
                            else if (dataType.Equals("UT"))
                            {
                                line.Add(info.Hour, info.Central_URoom_Temp);
                            }
                            else if (dataType.Equals("CH"))
                            {
                                line.Add(info.Hour, info.Central_CRoom_RH);
                            }
                            else if (dataType.Equals("UH"))
                            {
                                line.Add(info.Hour, info.Central_URoom_RH);
                            }
                        }
                    }
                    return line;
                case "D":
                    foreach (var info in allInfos)
                    {
                        if (!info.Central_CRoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData))
                        {
                            if (dataType.Equals("CT"))
                            {
                                line.Add(info.Day, info.Central_CRoom_Temp);
                            }
                            else if (dataType.Equals("UT"))
                            {
                                line.Add(info.Day, info.Central_URoom_Temp);
                            }
                            else if (dataType.Equals("CH"))
                            {
                                line.Add(info.Day, info.Central_CRoom_RH);
                            }
                            else if (dataType.Equals("UH"))
                            {
                                line.Add(info.Day, info.Central_URoom_RH);
                            }
                        }
                    }
                    return line;
                case "M":
                    foreach (var info in allInfos)
                    {
                        if (!info.Central_CRoom_Temp.Equals(IISI.Util.NumericUtility.NoHydData))
                        {
                            if (dataType.Equals("CT"))
                            {
                                line.Add(info.Month, info.Central_CRoom_Temp);
                            }
                            else if (dataType.Equals("UT"))
                            {
                                line.Add(info.Month, info.Central_URoom_Temp);
                            }
                            else if (dataType.Equals("CH"))
                            {
                                line.Add(info.Month, info.Central_CRoom_RH);
                            }
                            else if (dataType.Equals("UH"))
                            {
                                line.Add(info.Month, info.Central_URoom_RH);
                            }
                        }
                    }
                    return line;
                default:
                    return line;
            }
        }
        catch
        {
        }
        return line;
    }

    private void SetAxeBottom(HttpContext context, string chartType, List<TemperatureHumidityReportInfo> allInfos, Steema.TeeChart.Chart chart, string dataType)
    {
        string title = string.Empty, dateTimeFormat = string.Empty, headerText = string.Empty;
        double increment = 1;
        string fieldDescription = string.Empty;
        if (dataType.Equals("T"))
        {
            fieldDescription = "溫度";
        }
        else if (dataType.Equals("H"))
        {
            fieldDescription = "濕度";
        }
        switch (chartType)
        {
            case "H":
                DateTime dateTime = Convert.ToDateTime(GetDateTime(context, "Date"));
                headerText = string.Format("{0}趨勢歷線-{1}", fieldDescription, dateTime.ToString("yyyy年MM月dd日"));
                title = "時間(時)";
                dateTimeFormat = "HH";
                //increment = Steema.TeeChart.Utils.DateTimeStep[(int)Steema.TeeChart.DateTimeSteps.OneHour];
                break;
            case "D":
                int year = Convert.ToInt32(GetDateTime(context, "Year"));
                int month = Convert.ToInt32(GetDateTime(context, "Month"));
                headerText = string.Format("{0}趨勢歷線-{1}年{2}月", fieldDescription, year, month);
                title = "時間(日)";
                //dateTimeFormat = "dd";
                //increment = Steema.TeeChart.Utils.DateTimeStep[(int)Steema.TeeChart.DateTimeSteps.OneDay];
                break;
            case "M":
                headerText = string.Format("{0}趨勢歷線-{1}年", fieldDescription, Convert.ToInt32(GetDateTime(context, "Year")));
                title = "時間(月)";
                //dateTimeFormat = "MM";
                //increment = Steema.TeeChart.Utils.DateTimeStep[(int)Steema.TeeChart.DateTimeSteps.OneMonth];
                break;
        }

        chart.Header.Text = headerText;
        chart.Axes.Bottom.Title.Text = title;
        chart.Axes.Bottom.Labels.DateTimeFormat = dateTimeFormat;
        chart.Axes.Bottom.Increment = increment;
        switch (chartType)
        {
            case "H":
                chart.Axes.Bottom.SetMinMax(0, 23);
                break;
            case "D":
                int year = Convert.ToInt32(GetDateTime(context, "Year"));
                int month = Convert.ToInt32(GetDateTime(context, "Month"));
                chart.Axes.Bottom.SetMinMax(1, DateTime.DaysInMonth(year, month));
                break;
            case "M":
                chart.Axes.Bottom.SetMinMax(1, 12);
                break;
        }
    }

    #endregion

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}