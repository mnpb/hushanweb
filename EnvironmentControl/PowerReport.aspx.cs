﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EnvironmentControl_PowerReport : SuperPage
{
    #region Properties

    string DataClass
    {
        get
        {
            return DataClassRadioButtonList.SelectedItem.Value;
        }
    }
    string DataClassName
    {
        get
        {
            return DataClassRadioButtonList.SelectedItem.Text;
        }
    }

    /// <summary>
    /// GridView欄位名稱資訊
    /// </summary>
    List<string> ColumnInfos
    {
        get
        {
            List<string> infos = new List<string>();
            if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Day)
            {
                infos.Add("Hour;時間(小時)");
            }
            else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Month)
            {
                infos.Add("Day;時間(日)");
            }
            else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Year)
            {
                infos.Add("Month;時間(月)");
            }
            infos.Add("Vab;R-S線電壓");
            infos.Add("Vbc;S-T線電壓");
            infos.Add("Vca;T-R線電壓");
            infos.Add("Ia;R相電流");
            infos.Add("Ib;S相電流");
            infos.Add("Ic;T相電流");
            infos.Add("Kw;瞬時總電能");
            infos.Add("Factor;功率因素");
            return infos;
        }
    }

    DateTime InfoDate
    {
        get
        {
            return ReportSelectorUserControl.InfoDate;
        }
    }
    int InfoYear
    {
        get
        {
            return ReportSelectorUserControl.InfoYear;
        }
    }
    int InfoMonth
    {
        get
        {
            return ReportSelectorUserControl.InfoMonth;
        }
    }

    List<PowerReportInfo> PowerReportInfo
    {
        get
        {
            if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Day)
            {
                return EnvCtrlController.Instance.GetPowerDayReport(InfoDate, DataClass);
            }
            else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Month)
            {
                return EnvCtrlController.Instance.GetPowerMonthReport(InfoYear, InfoMonth, DataClass);
            }
            else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Year)
            {
                return EnvCtrlController.Instance.GetPowerYearReport(InfoYear, DataClass);
            }
            return new List<PowerReportInfo>();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Show();
        }
    }

    /// <summary>
    /// 動態產生gridview欄位
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="meterType"></param>
    private void BuildTemplateColumnsDynamically(GridView gridView)
    {
        gridView.Columns.Clear();
        try
        {
            foreach (string columnItem in ColumnInfos)
            {
                TemplateField lname = new TemplateField();
                lname.HeaderText = columnItem.Split(';')[1];
                lname.ItemTemplate = new AddTemplateToGridView(ListItemType.Item, columnItem.Split(';')[0]);
                gridView.Columns.Add(lname);
            }
        }
        catch
        {
        }
    }
    private void ShowReport(GridView gridView)
    {
        BuildTemplateColumnsDynamically(gridView); // 動態產生gridview欄位
        try
        {
            gridView.DataSource = PowerReportInfo;
            gridView.DataBind();
        }
        catch
        {
        }
    }

    protected void ReportGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            List<List<HeaderInfo>> allHeaderInfos = new List<List<HeaderInfo>>
            {
                 MainHeader, 
            };

            int index = 0;
            foreach (var headerInfo in allHeaderInfos)
            {
                GridViewRow headerGridViewRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                foreach (var item in headerInfo)
                {
                    TableCell headerCell = new TableCell();
                    headerCell.BackColor = System.Drawing.Color.FromArgb(75, 120, 176);
                    headerCell.ForeColor = System.Drawing.Color.White;
                    headerCell.Text = item.Text;
                    headerCell.RowSpan = item.RowSpan;
                    headerCell.ColumnSpan = item.ColumnSpan;
                    headerGridViewRow.Cells.Add(headerCell);
                }
                ReportGridView.Controls[0].Controls.AddAt(index, headerGridViewRow);
                index++;
            }
        }
    }
    private List<HeaderInfo> MainHeader
    {
        get
        {
            string header = DataClassName + "-電力";
            if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Day)
            {
                header += string.Format("日報表({0})", InfoDate.ToString("yyyy年MM月dd日"));
            }
            else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Month)
            {
                header += string.Format("月報表({0}年{1}月)", InfoYear, InfoMonth);
            }
            else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Year)
            {
                header += string.Format("年報表({0}年)", InfoYear);
            }
            return new List<HeaderInfo>
            {
                 new HeaderInfo{ Text = header, RowSpan=1, ColumnSpan = ReportGridView.Columns.Count},
            };
        }
    }

    private void ShowChart(Image image, string dataClass, string dataType)
    {
        string imageUrl = string.Empty;
        int width = 1000, height = 300;
        if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Day)
        {
            imageUrl = string.Format("ShowPowerChartHandler.ashx?ChartType={0}&DataClass={1}&DataType={2}&Date={3}&Width={4}&Height={5}&now={6}",
                "H",
                dataClass,
                dataType,
                InfoDate.ToString("yyyy/MM/dd"),
                width,
                height,
                DateTime.Now.ToString("yyyyMMddHHmmss"));
        }
        else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Month)
        {
            imageUrl = string.Format("ShowPowerChartHandler.ashx?ChartType={0}&DataClass={1}&DataType={2}&Year={3}&Month={4}&Width={5}&Height={6}&now={7}",
                "D",
                dataClass,
                dataType,
                InfoYear,
                InfoMonth,
                width,
                height,
                DateTime.Now.ToString("yyyyMMddHHmmss"));
        }
        else if (ReportSelectorUserControl.ReportType == EnvironmentControl_UserControls_ReportSelector.ReportTypeEnum.Year)
        {
            imageUrl = string.Format("ShowPowerChartHandler.ashx?ChartType={0}&DataClass={1}&DataType={2}&Year={3}&Width={4}&Height={5}&now={6}",
                "M",
                dataClass,
                dataType,
                InfoYear,
                width,
                height,
                DateTime.Now.ToString("yyyyMMddHHmmss"));
        }
        image.ImageUrl = imageUrl;
    }

    protected void QueryButton_Click(object sender, EventArgs e)
    {
        Show();
    }
    protected void SaveFileButton_Click(object sender, EventArgs e)
    {
        ShowReport(ReportGridView);
        ReportUtility.ExportFile(this, ReportGridView);
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        // '處理'GridView' 的控制項 'GridView' 必須置於有 runat=server 的表單標記之中   
    }

    protected void ReportSelectorUserControl_ReportTypeChanged(object sender, EventArgs e)
    {
        Show();
    }
    protected void DataClassRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Show();
    }

    private void Show()
    {
        ShowReport(ReportGridView);
        ShowChart(VoltChartImage, DataClass, "V");
        ShowChart(CurrentChartImage, DataClass, "C");
    }
}