﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportSelector.ascx.cs" Inherits="EnvironmentControl_UserControls_ReportSelector" %>

<script src="<%=ResolveUrl("~/Scripts/jquery-1.10.2.min.js") %>"></script>
<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.css") %>" />
<script src="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js") %>"></script>
<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" />
<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/SelectStyle.css") %>" />

<script type="text/javascript">

    function load() {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    }
    function loadDatePicker() {
        $("#<%= DateTextBox.ClientID %>").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy年mm月dd日',
            dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
            monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
        });
    }
    function EndRequestHandler() {
        loadDatePicker();
    }
    $(document).ready(function () {
        EndRequestHandler();
        window.onload = load;
    });
</script>

<style type="text/css">
    #ui-datepicker-div {
        z-index: 9999999 !important;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        報表：
        <asp:RadioButton ID="DayReportRadioButton" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="DayReportRadioButton_CheckedChanged" />日報表<asp:TextBox ID="DateTextBox" runat="server" Width="120px" CssClass="textBoxStyle"></asp:TextBox>&nbsp;
        <asp:RadioButton ID="MonthReportRadioButton" runat="server" AutoPostBack="true" OnCheckedChanged="MonthReportRadioButton_CheckedChanged" />月報表<div class="selectStyle">
            <asp:DropDownList ID="YearInMonthReportDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList></div>
        年<div class="selectStyle">
            <asp:DropDownList ID="MonthDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList></div>
        月&nbsp;
        <asp:RadioButton ID="YearReportRadioButton" runat="server" AutoPostBack="true" OnCheckedChanged="YearReportRadioButton_CheckedChanged" />年報表<div class="selectStyle">
            <asp:DropDownList ID="YearInYearReportDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList></div>
        年
    </ContentTemplate>
</asp:UpdatePanel>