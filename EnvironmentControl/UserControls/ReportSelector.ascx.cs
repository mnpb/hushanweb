﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EnvironmentControl_UserControls_ReportSelector : System.Web.UI.UserControl
{
    #region Properties

    public enum ReportTypeEnum
    {
        Day = 1,
        Month,
        Year,
    }


    int BeginYear = 2016;

    public DateTime InfoDate
    {
        get
        {
            try
            {
                return Convert.ToDateTime(DateTextBox.Text);
            }
            catch
            {
                return DateTime.Today.Date;
            }
        }
    }

    public int InfoYear
    {
        get
        {
            if (MonthReportRadioButton.Checked)
            {
                return Convert.ToInt32(YearInMonthReportDropDownList.SelectedItem.Text);
            }
            else if (YearReportRadioButton.Checked)
            {
                return Convert.ToInt32(YearInYearReportDropDownList.SelectedItem.Text);
            }
            return DateTime.Today.Year;
        }
    }

    public int InfoMonth
    {
        get
        {
            return Convert.ToInt32(MonthDropDownList.SelectedItem.Text);
        }
    }

    public ReportTypeEnum ReportType
    {
        get
        {
            if (YearReportRadioButton.Checked)
            {
                return ReportTypeEnum.Year;
            }
            else if (MonthReportRadioButton.Checked)
            {
                return ReportTypeEnum.Month;
            }
            else
            {
                return ReportTypeEnum.Day;
            }
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTextBox.Text = DateTime.Today.ToString("yyyy年MM月dd日");
            InitYears(YearInMonthReportDropDownList);
            InitYears(YearInYearReportDropDownList);
            InitMonths(MonthDropDownList);
        }
    }

    private void InitYears(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();
        for (int year = BeginYear; year <= DateTime.Now.Year; year++)
        {
            dropDownList.Items.Add(year.ToString());
        }
        dropDownList.SelectedIndex = dropDownList.Items.Count - 1;
    }

    private void InitMonths(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();
        for (int month = 1; month <= 12; month++)
        {
            dropDownList.Items.Add(month.ToString());
        }
        dropDownList.SelectedIndex = 0;
    }

    public event EventHandler ReportTypeChanged;
    protected void DayReportRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        MonthReportRadioButton.Checked = false;
        YearReportRadioButton.Checked = false;

        DateTextBox.Enabled = true;
        YearInMonthReportDropDownList.Enabled = false;
        MonthDropDownList.Enabled = false;
        YearInYearReportDropDownList.Enabled = false;

        if (ReportTypeChanged != null)
        {
            ReportTypeChanged(this, e);
        }
    }
    protected void MonthReportRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        DayReportRadioButton.Checked = false;
        YearReportRadioButton.Checked = false;

        DateTextBox.Enabled = false;
        YearInMonthReportDropDownList.Enabled = true;
        MonthDropDownList.Enabled = true;
        YearInYearReportDropDownList.Enabled = false;

        if (ReportTypeChanged != null)
        {
            ReportTypeChanged(this, e);
        }
    }
    protected void YearReportRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        DayReportRadioButton.Checked = false;
        MonthReportRadioButton.Checked = false;

        DateTextBox.Enabled = false;
        YearInMonthReportDropDownList.Enabled = false;
        MonthDropDownList.Enabled = false;
        YearInYearReportDropDownList.Enabled = true;

        if (ReportTypeChanged != null)
        {
            ReportTypeChanged(this, e);
        }
    }

    #endregion
}