﻿<%@ WebHandler Language="C#" Class="ShowPowerChartHandler" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Reflection;

public class ShowPowerChartHandler : IHttpHandler {

    /// <summary>
    /// 取得報表類型
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private string GetChartType(HttpContext context)
    {
        return GetQueryValue(context, "ChartType"); // 時、日、月
    }
    private string GetDataClass(HttpContext context)
    {
        return GetQueryValue(context, "DataClass"); // 類別(機械設備機房電力、電腦機房UPS盤電力)
    }
    private string GetDataType(HttpContext context)
    {
        return GetQueryValue(context, "DataType"); // 電壓、電流
    }

    private string GetDateTime(HttpContext context, string fieldName)
    {
        return GetQueryValue(context, fieldName);
    }

    private string GetQueryValue(HttpContext context, string fieldName)
    {
        try
        {
            return context.Request.QueryString[fieldName].ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// 取得圖寬
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private int GetChartWidth(HttpContext context)
    {
        try
        {
            return Convert.ToInt32(GetQueryValue(context, "Width"));
        }
        catch
        {
            return 0;
        }
    }
    /// <summary>
    /// 取得圖高
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    private int GetChartHeight(HttpContext context)
    {
        try
        {
            return Convert.ToInt32(GetQueryValue(context, "Height"));
        }
        catch
        {
            return 0;
        }
    }

    private List<PowerReportInfo> GetInfos(HttpContext context, string chartType, string dataClass)
    {
        switch (chartType)
        {
            case "H":
                return EnvCtrlController.Instance.GetPowerDayReport(Convert.ToDateTime(GetDateTime(context, "Date")), dataClass);
            case "D":
                return EnvCtrlController.Instance.GetPowerMonthReport(Convert.ToInt32(GetDateTime(context, "Year")), Convert.ToInt32(GetDateTime(context, "Month")), dataClass);
            case "M":
                return EnvCtrlController.Instance.GetPowerYearReport(Convert.ToInt32(GetDateTime(context, "Year")), dataClass);
            default:
                return new List<PowerReportInfo>();
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string chartType = GetChartType(context);
            string dataClass = GetDataClass(context);
            string dataType = GetDataType(context);
            int width = GetChartWidth(context);
            int height = GetChartHeight(context);
            var chart = GenChart(
                context,
                dataClass,
                chartType,
                dataType,
                width,
                height,
                GetInfos(context, chartType, dataClass));
            context.Response.ContentType = "image/png";
            chart.Bitmap().Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        }
        catch
        {
        }
    }


    #region 歷線圖

    /// <summary>
    /// 
    /// </summary>
    /// <param name="width">圖寬</param>
    /// <param name="height">圖高</param>
    /// <returns></returns>
    private Steema.TeeChart.Chart InitChart(int width, int height)
    {
        var webChart = new Steema.TeeChart.Web.WebChart();
        webChart.Width = width;
        webChart.Height = height;
        webChart.PictureFormat = Steema.TeeChart.Export.PictureFormats.JPEG;
        webChart.AutoPostback = false;
        webChart.TempChart = Steema.TeeChart.Web.TempChartStyle.Session;
        webChart.GetChartFile = "../TeeChartFiles/GetChart.aspx";

        Steema.TeeChart.Chart chart = webChart.Chart;
        chart.Series.Clear();
        chart.Axes.Custom.Clear();
        chart.Chart.Aspect.View3D = false;
        chart.Series.RemoveAllSeries();

        return chart;
    }

    /// <summary>
    /// 產生歷線圖
    /// </summary>
    /// <param name="chartType"></param>
    /// <param name="width">圖寬</param>
    /// <param name="height">圖高</param>
    /// <param name="allInfos"></param>
    private Steema.TeeChart.Chart GenChart(HttpContext context, string dataClass, string chartType, string dataType, int width, int height, List<PowerReportInfo> allInfos)
    {
        Steema.TeeChart.Chart chart = InitChart(width, height);
        GenLine(chartType, dataType, chart, allInfos, "#,##0.00");

        SetYCustom(chart);
        SetAxeBottom(context, dataClass, chartType, allInfos, chart, dataType);
        return chart;
    }

    private void GenLine(string chartType, string dataType, Steema.TeeChart.Chart chart, List<PowerReportInfo> allInfos, string valueFormat)
    {
        string fieldDescription = string.Empty, unit = string.Empty;
        List<string> allFieldNames = new List<string>();
        List<Color> allColors = new List<Color> { Color.Red, Color.Green, Color.Blue };
        double maxBound = 0;
        double minBound = 0;
        if (dataType.Equals("V"))
        {
            allFieldNames.AddRange(new List<string> { "Vab", "Vbc", "Vca", });
            maxBound = GetMaxBound(GetMaxVoltValue(allInfos));
            minBound = GetMinBound(GetMinVoltValue(allInfos));
            fieldDescription = "線電壓";
        }
        else if (dataType.Equals("C"))
        {
            allFieldNames.AddRange(new List<string> { "Ia", "Ib", "Ic", });
            maxBound = GetMaxBound(GetMaxCurrentValue(allInfos));
            minBound = GetMinBound(GetMinCurrentValue(allInfos));
            fieldDescription = "相電流";
        }
        chart.Axes.Custom.Add(new Steema.TeeChart.Axis());

        chart.Axes.Custom[chart.Axes.Custom.Count - 1].Title.Text = fieldDescription + unit;
        chart.Axes.Custom[chart.Axes.Custom.Count - 1].Title.Angle = 90;

        if (maxBound < 5)
        {
            valueFormat = "#,##0.000";
        }
        else if (maxBound > 10)
        {
            valueFormat = "#,##0";
        }
        chart.Axes.Custom[chart.Axes.Custom.Count - 1].Labels.ValueFormat = valueFormat;

        for (int idx = 0; idx < 3; idx++)
        {
            chart.Series.Add(GenLine(chartType, allInfos, allFieldNames[idx], fieldDescription, allColors[idx]));
        }
        chart.Axes.Custom[chart.Axes.Custom.Count - 1].SetMinMax(minBound, maxBound);
        //chart.Series[chart.Series.Count - 1].ShowInLegend = false; // 下限線之legend不顯示
    }

    private void SetYCustom(Steema.TeeChart.Chart chart)
    {
        try
        {
            //chart.Series[1].CustomVertAxis = chart.Axes.Custom[1];
            chart.Axes.Custom[0].StartPosition = 1;
            chart.Axes.Custom[0].EndPosition = 100;
            chart.Axes.Left.StartPosition = 1;
            chart.Axes.Left.EndPosition = 100;
            //chart.Axes.Custom[1].StartPosition = 52;
            //chart.Axes.Custom[1].EndPosition = 100;
            chart.Axes.Left.SetMinMax(chart.Axes.Custom[0].Minimum, chart.Axes.Custom[0].Maximum);
            chart.Axes.Left.Title.Text = chart.Axes.Custom[0].Title.Text;

            //chart.Series[0].VertAxis = Steema.TeeChart.Styles.VerticalAxis.Left;
            //chart.Series[0].CustomVertAxis = chart.Axes.Custom[0];
        }
        catch
        {
        }
    }

    private double GetMinVoltValue(List<PowerReportInfo> allInfos)
    {
        try
        {
            double vab = allInfos.Where(x => !x.Vab.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Vab).Min();
            double vbc = allInfos.Where(x => !x.Vbc.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Vbc).Min();
            double vca = allInfos.Where(x => !x.Vca.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Vca).Min();
            return Math.Min(vab, Math.Min(vbc, vca));
        }
        catch
        {
        }
        return 0;
    }
    private double GetMaxVoltValue(List<PowerReportInfo> allInfos)
    {
        try
        {
            double vab = allInfos.Where(x => !x.Vab.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Vab).Max();
            double vbc = allInfos.Where(x => !x.Vbc.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Vbc).Max();
            double vca = allInfos.Where(x => !x.Vca.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Vca).Max();
            return Math.Max(vab, Math.Max(vbc, vca));
        }
        catch
        {
        }
        return 0;
    }
    private double GetMinCurrentValue(List<PowerReportInfo> allInfos)
    {
        try
        {
            double ia = allInfos.Where(x => !x.Ia.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Ia).Min();
            double ib = allInfos.Where(x => !x.Ib.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Ib).Min();
            double ic = allInfos.Where(x => !x.Ic.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Ic).Min();
            return Math.Min(ia, Math.Min(ib, ic));
        }
        catch
        {
        }
        return 0;
    }
    private double GetMaxCurrentValue(List<PowerReportInfo> allInfos)
    {
        try
        {
            double ia = allInfos.Where(x => !x.Ia.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Ia).Max();
            double ib = allInfos.Where(x => !x.Ib.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Ib).Max();
            double ic = allInfos.Where(x => !x.Ic.Equals(IISI.Util.NumericUtility.NoHydData)).Select(x => x.Ic).Max();
            return Math.Max(ia, Math.Max(ib, ic));
        }
        catch
        {
        }
        return 0;
    }

    /// <summary>
    /// 畫線
    /// </summary>
    /// <param name="chartType"></param>
    /// <param name="allInfos"></param>
    /// <param name="fieldName"></param>
    /// <param name="title"></param>
    /// <returns></returns>
    private Steema.TeeChart.Styles.Line GenLine(string chartType, List<PowerReportInfo> allInfos, string fieldName, string title, Color color)
    {
        Steema.TeeChart.Styles.Line line = new Steema.TeeChart.Styles.Line();
        line.LinePen.Width = 3;
        line.Pointer.Visible = true;
        line.Pointer.Style = Steema.TeeChart.Styles.PointerStyles.Sphere;
        line.Title = GetRealElectricDescription(fieldName) + title;
        line.Color = color;
        string className = string.Empty;
        try
        {
            switch (chartType)
            {
                case "H":
                    foreach (var info in allInfos)
                    {
                        PropertyInfo pi = info.GetType().GetProperty(fieldName);
                        double value = Convert.ToDouble(pi.GetValue(info, null));
                        if (!value.Equals(IISI.Util.NumericUtility.NoHydData))
                        {
                            line.Add(info.Hour, value);
                        }
                    }
                    return line;
                case "D":
                    foreach (var info in allInfos)
                    {
                        PropertyInfo pi = info.GetType().GetProperty(fieldName);
                        double value = Convert.ToDouble(pi.GetValue(info, null));
                        if (!value.Equals(IISI.Util.NumericUtility.NoHydData))
                        {
                            line.Add(info.Day, value);
                        }
                    }
                    return line;
                case "M":
                    foreach (var info in allInfos)
                    {
                        PropertyInfo pi = info.GetType().GetProperty(fieldName);
                        double value = Convert.ToDouble(pi.GetValue(info, null));
                        if (!value.Equals(IISI.Util.NumericUtility.NoHydData))
                        {
                            line.Add(info.Month, value);
                        }
                    }
                    return line;
                default:
                    return line;
            }
        }
        catch
        {
        }
        return line;
    }

    private void SetAxeBottom(HttpContext context, string dataClass, string chartType, List<PowerReportInfo> allInfos, Steema.TeeChart.Chart chart, string dataType)
    {
        string title = string.Empty, dateTimeFormat = string.Empty, headerText = string.Empty;
        double increment = 1;
        string dataClassName = GetDataClassName(dataClass);
        string fieldDescription = string.Empty;
        if (dataType.Equals("V"))
        {
            fieldDescription = "線電壓";
        }
        else if (dataType.Equals("C"))
        {
            fieldDescription = "相電流";
        }
        switch (chartType)
        {
            case "H":
                DateTime dateTime = Convert.ToDateTime(GetDateTime(context, "Date"));
                headerText = string.Format("{0}{1}趨勢歷線-{2}", dataClassName, fieldDescription, dateTime.ToString("yyyy年MM月dd日"));
                title = "時間(時)";
                dateTimeFormat = "HH";
                //increment = Steema.TeeChart.Utils.DateTimeStep[(int)Steema.TeeChart.DateTimeSteps.OneHour];
                break;
            case "D":
                int year = Convert.ToInt32(GetDateTime(context, "Year"));
                int month = Convert.ToInt32(GetDateTime(context, "Month"));
                headerText = string.Format("{0}{1}趨勢歷線-{2}年{3}月", dataClassName, fieldDescription, year, month);
                title = "時間(日)";
                //dateTimeFormat = "dd";
                //increment = Steema.TeeChart.Utils.DateTimeStep[(int)Steema.TeeChart.DateTimeSteps.OneDay];
                break;
            case "M":
                headerText = string.Format("{0}{1}趨勢歷線-{2}年", dataClassName, fieldDescription, Convert.ToInt32(GetDateTime(context, "Year")));
                title = "時間(月)";
                //dateTimeFormat = "MM";
                //increment = Steema.TeeChart.Utils.DateTimeStep[(int)Steema.TeeChart.DateTimeSteps.OneMonth];
                break;
        }

        chart.Header.Text = headerText;
        chart.Axes.Bottom.Title.Text = title;
        chart.Axes.Bottom.Labels.DateTimeFormat = dateTimeFormat;
        chart.Axes.Bottom.Increment = increment;
        switch (chartType)
        {
            case "H":
                chart.Axes.Bottom.SetMinMax(0, 23);
                break;
            case "D":
                int year = Convert.ToInt32(GetDateTime(context, "Year"));
                int month = Convert.ToInt32(GetDateTime(context, "Month"));
                chart.Axes.Bottom.SetMinMax(1, DateTime.DaysInMonth(year, month));
                break;
            case "M":
                chart.Axes.Bottom.SetMinMax(1, 12);
                break;
        }
    }

    private string GetDataClassName(string dataClass)
    {
        return dataClass.Equals("0") ? "機械設備機房電力" : "電腦機房UPS盤電力";
    }

    private double GetMaxBound(double value)
    {
        if (value < 0)
        {
            return value * 0.99;
        }
        return value * 1.01;
    }
    private double GetMinBound(double value)
    {
        if (value < 0)
        {
            return value * 1.01;
        }
        return value * 0.99;
    }

    private string GetRealElectricDescription(string fieldName)
    {
        switch (fieldName)
        {
            case "Ia":
                return "R";
            case "Ib":
                return "S";
            case "Ic":
                return "T";
            case "Vab":
                return "R-S";
            case "Vbc":
                return "S-T";
            case "Vca":
                return "T-R";
            default:
                return string.Empty;
        }
    } 
    
    #endregion
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}