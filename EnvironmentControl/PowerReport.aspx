﻿<%@ Page Title="電力報表" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PowerReport.aspx.cs" Inherits="EnvironmentControl_PowerReport" %>

<%@ Register Src="~/EnvironmentControl/UserControls/ReportSelector.ascx" TagPrefix="uc1" TagName="ReportSelector" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
<%--    <link href="../css/tableStyle.css" rel="stylesheet" />--%>
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.10.2.min.js"></script>
    <script src="../css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
    <%--    <link href="../css/jqueryUITabs.css" rel="stylesheet" />--%>
    <link href="../css/ui.jqgrid.table.css" rel="stylesheet" />

    <script>
        $(function () {
            EndRequest();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                EndRequest();
            });
        });
        function EndRequest() {
            $("#tabs").tabs();
            $("#DataClassRadioButtonList").buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <uc1:ReportSelector runat="server" ID="ReportSelectorUserControl" OnReportTypeChanged="ReportSelectorUserControl_ReportTypeChanged" />
                類型 :
                    <asp:RadioButtonList ID="DataClassRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="DataClassRadioButtonList_SelectedIndexChanged">
                        <asp:ListItem Text="機械設備機房" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="電腦機房UPS盤" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>&nbsp;
                    <asp:Button ID="QueryButton" runat="server" Text="查詢" OnClick="QueryButton_Click" CssClass="buttonStyle" />&nbsp;
                    <asp:Button ID="SaveFileButton" runat="server" Text="另存報表" CssClass="buttonStyle" OnClick="SaveFileButton_Click" />&nbsp;
                <%--   <input type="button" value="列印" class="buttonStyle" onclick="getPreviewPrint('<%=ReportGridView.ClientID%>');" />--%>
            </div>
            <br />
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">報表</a></li>
                    <li><a href="#tabs-2">趨勢圖</a></li>
                </ul>
                <div id="tabs-1">
                    <div style="width: 100%; height: 500px; overflow: auto">
                        <asp:GridView ID="ReportGridView" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="fancytable"
                            EmptyDataText="沒有任何資料" OnRowCreated="ReportGridView_RowCreated">
                        </asp:GridView>
                    </div>
                </div>
                <div id="tabs-2" style="text-align: center">
                    <asp:Image runat="server" ID="VoltChartImage"></asp:Image>
                    <asp:Image runat="server" ID="CurrentChartImage"></asp:Image>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="SaveFileButton" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

