﻿<%@ WebHandler Language="C#" Class="GetAreaXMLFromPoint" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.IO;

public class GetAreaXMLFromPoint : IHttpHandler
{
    const string INNERBOUNDARYIS = @"
          <innerBoundaryIs>
            <LinearRing>
              <coordinates>{0}
              </coordinates>
            </LinearRing>
          </innerBoundaryIs>";
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            context.Response.ContentType = "application/vnd.google-earth.kml+xml";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=xy.kml");
            context.Response.Charset = "utf-8";

            var x = context.Request.QueryString["x"];
            var y = context.Request.QueryString["y"];
            //120.34863129 23.82135189
            //x = "120.34853129";
            //y = "23.82136189";
            HushanSect hs = new HushanSect();
            System.Collections.Generic.List<GISPointInfo> areas = hs.AreasContainPoint(x, y);
            if (areas == null || areas.Count == 0)
            {
                context.Response.Write(@"");
            }
            else
            {
                //context.Response.ContentType = "application/vnd.google-earth.kml+xml";
                var firstArea = areas[0];
                string kmlTemplate = @"<?xml version='1.0' encoding='UTF-8'?>
<kml xmlns='http://www.opengis.net/kml/2.2'>
  <Document>
    <name>KmlFile</name>
    <Style id='transRedPoly'>
      <LineStyle>
        <width>1.5</width>
      </LineStyle>
      <PolyStyle>
        <color>7d00ffff</color>
      </PolyStyle>
    </Style>
    <Folder>
      <name>Hushan</name>
      <visibility>1</visibility>
      <description>{2}</description>
      <Placemark>
        <name>Building 40</name>
        <visibility>1</visibility>
        <styleUrl>#transRedPoly</styleUrl>
        <Polygon>
          <extrude>1</extrude>
          <altitudeMode>relativeToGround</altitudeMode>
          <outerBoundaryIs>
            <LinearRing>
              <coordinates>{0}
              </coordinates>
            </LinearRing>
          </outerBoundaryIs>
{1}
        </Polygon>
      </Placemark>
     
    </Folder>
  </Document>
</kml>
";
                List<string> xys = firstArea.ToKMLJSLatLngArray();
                if (xys.Count == 0)
                {
                    context.Response.Write("");
                }
                else if (xys.Count == 1)
                {
                    context.Response.Write(string.Format(kmlTemplate,
                        xys[0], "", "地段：" + firstArea.SectNo + "，地號：" + firstArea.LandNo));
                }
                else
                {
                    string inner = "";
                    for (int i = 1; i < xys.Count; i++)
                    {
                        inner += string.Format(INNERBOUNDARYIS, xys[i]);
                    }
                    context.Response.Write(string.Format(kmlTemplate, xys[0], inner,
                        "地段：" + firstArea.SectNo + "，地號：" + firstArea.LandNo));
                }
            }
        }
        catch (Exception ex)
        {
            context.Response.Write(ex);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}