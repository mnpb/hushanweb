﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchStation.ascx.cs" Inherits="WebUserControllers_Meters_SearchStation" %>

<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" />
<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" />

<div id="QueryDiv" style="height: 50px">
    <fieldset>
        <legend>測站搜尋</legend>
        請輸入站名 : <asp:TextBox ID="FilterStationTextBox" runat="server" CssClass="textBoxStyle" onkeydown="return (event.keyCode!=13);"></asp:TextBox>&nbsp;
    <asp:Button ID="FilterStationButton" runat="server" Text="查詢測站" OnClick="FilterStationButton_Click" CssClass="buttonStyle" />
        <asp:Button ID="ClearFilterButton" runat="server" Text="清空查詢條件" OnClick="ClearFilterButton_Click" CssClass="buttonStyle" />
    </fieldset>
</div>
