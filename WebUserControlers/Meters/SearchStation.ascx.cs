﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControllers_Meters_SearchStation : System.Web.UI.UserControl
{
    #region Properties

    public string FilterStationName
    {
        get
        {
            return FilterStationTextBox.Text;
        }
        set
        {
            FilterStationTextBox.Text = value;
        }
    }

    #endregion

    #region Methods

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public event EventHandler FilterStation;
    protected void FilterStationButton_Click(object sender, EventArgs e)
    {
        if (FilterStation != null)
        {
            FilterStation(this, e);
        }
    }

    public event EventHandler ClearFilter;
    protected void ClearFilterButton_Click(object sender, EventArgs e)
    {
        FilterStationName = string.Empty;
        if (ClearFilter != null)
        {
            ClearFilter(this, e);
        }
    }

    #endregion
}