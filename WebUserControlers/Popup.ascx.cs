﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// 這是彈出式使用者控制項外面都需要包的一層，使用 ASP.NET AJAX Toolkit 來達成獨佔模式的彈出式視窗。
/// 使用者控制項放至於該控制項的 ContentTemplate 之中。
/// </summary>
public partial class UserControls_Popup : System.Web.UI.UserControl
{
    #region 欄位 屬性
    private ITemplate _ContentTemplate = null;

    /// <summary>
    /// 取得或設定 TargetControlID
    /// </summary>
    public string TargetControlID { get; set; }

    /// <summary>
    /// 設定CancelControlID
    /// </summary>
    public string CancelControlID
    {
        set
        {
            ModalPopup.CancelControlID = value;
        }
    }

    /// <summary>
    /// 彈出視窗的標頭文字。
    /// </summary>
    public string Title
    {
        get { return ViewState["Title"] == null ? string.Empty : (string)ViewState["Title"]; }
        set { ViewState["Title"] = value; }
    }

    /// <summary>
    /// 取得或設定寬度。
    /// </summary>
    public Unit Width { get; set; }

    /// <summary>
    /// 取得或設定高度。
    /// </summary>
    public Unit Height { get; set; }

    /// <summary>
    /// 取得或設定ContentTemplate。
    /// </summary>
    [TemplateContainer(typeof(UserControls_Popup))]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate ContentTemplate
    {
        get { return _ContentTemplate; }
        set { _ContentTemplate = value; }
    }

    /// <summary>
    /// 記錄是否為顯示狀態。
    /// </summary>
    private bool IsDisplay
    {
        get { return ViewState["IsDisplay"] == null ? false : (bool)ViewState["IsDisplay"]; }
        set { ViewState["IsDisplay"] = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_ContentTemplate != null)
        {
            UserControls_Popup UserControls_Popup = new UserControls_Popup();
            _ContentTemplate.InstantiateIn(UserControls_Popup);
            ContentPlaceHolder.Controls.Add(UserControls_Popup);
            TitleLiteral.Text = Title;
            PopupPanel.Width = Width;
            PopupPanel.Height = Height;

            if (string.IsNullOrEmpty(TargetControlID) == false)
                ModalPopup.TargetControlID = TargetControlID;

            if (IsDisplay)
                ModalPopup.Show();
            else
                ModalPopup.Hide();
        }
    }

    /// <summary>
    /// 按下關閉按鈕的動作。
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <remarks>
    /// 其實就是設定視窗的顯示屬性
    /// </remarks>
    protected void CloseLinkButton_Click(object sender, EventArgs e)
    {
        this.Hide();
    }

    /// <summary>
    /// 顯示彈出視窗。
    /// </summary>
    public void Show()
    {
        TitleLiteral.Text = Title;
        ModalPopup.Show();
        IsDisplay = true;
    }

    /// <summary>
    /// 隱藏彈出視窗。
    /// </summary>
    public void Hide()
    {
        ModalPopup.Hide();
        IsDisplay = false;
    }
}
