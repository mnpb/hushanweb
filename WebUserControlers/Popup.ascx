﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Popup.ascx.cs" Inherits="UserControls_Popup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Panel ID="PopupPanel" runat="server" CssClass="popup" stlye="display:none;">
    <div class="cmd">
        <asp:LinkButton ID="CloseLinkButton" runat="server" CssClass="popup_close" ToolTip="關閉"
            OnClick="CloseLinkButton_Click"></asp:LinkButton></div>
    <asp:Panel ID="TitlePanel" runat="server" CssClass="popup_title">
        <asp:Literal ID="TitleLiteral" runat="server" />
    </asp:Panel>
    <div class="popup_content">
        <asp:PlaceHolder ID="ContentPlaceHolder" runat="server" EnableViewState="true"></asp:PlaceHolder>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopup" runat="server" RepositionMode="None"
    PopupControlID="PopupPanel" BackgroundCssClass="PopupBackground" TargetControlID="PopupHiddenField"
    PopupDragHandleControlID="TitlePanel">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="PopupHiddenField" runat="server" />
