﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_DateTimes_SingleDateSelector : System.Web.UI.UserControl
{
    #region Properties

    public DateTime InfoDate
    {
        get
        {
            try
            {
                return Convert.ToDateTime(DateTextBox.Text);
            }
            catch
            {
                return DateTime.Today;
            }
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTextBox.Text = DateTime.Today.ToString("yyyy年MM月dd日");
        }
    }

    #endregion
}