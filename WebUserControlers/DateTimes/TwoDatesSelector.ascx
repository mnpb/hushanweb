﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TwoDatesSelector.ascx.cs" Inherits="WebUserControlers_DateTimes_TwoDatesSelector" %>

<script src="<%=ResolveUrl("~/Scripts/jquery-1.10.2.min.js") %>"></script>
<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.css") %>" />
<script src="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js") %>"></script>

<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" />

<script type="text/javascript">

    function load() {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    }
    function loadDatePicker() {
        $("#<%= BeginDateTextBox.ClientID %>").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy年mm月dd日',
            dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
            monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
        });
        $("#<%= EndDateTextBox.ClientID %>").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy年mm月dd日',
            dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
            monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
        });
    }
    function EndRequestHandler() {
        loadDatePicker();
    }
    $(document).ready(function () {
        EndRequestHandler();
        window.onload = load;
    });
</script>

<style type="text/css">
    #ui-datepicker-div {
        z-index: 9999999 !important;
    }
</style>

日期:<asp:TextBox ID="BeginDateTextBox" runat="server" Width="160px" CssClass="textBoxStyle"></asp:TextBox>~<asp:TextBox ID="EndDateTextBox" runat="server" Width="160px" CssClass="textBoxStyle"></asp:TextBox>