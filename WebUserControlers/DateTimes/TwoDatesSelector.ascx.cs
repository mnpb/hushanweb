﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_DateTimes_TwoDatesSelector : System.Web.UI.UserControl
{
    #region Properties

    public DateTime BeginTime
    {
        get
        {
            try
            {
                return Convert.ToDateTime(BeginDateTextBox.Text);
            }
            catch
            {
                return DateTime.Today;
            }
        }
    }
    public DateTime EndTime
    {
        get
        {
            try
            {
                return Convert.ToDateTime(EndDateTextBox.Text).AddDays(1).AddMilliseconds(-100);
            }
            catch
            {
                return DateTime.Today.AddDays(1).AddMilliseconds(-100);
            }
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BeginDateTextBox.Text = DateTime.Today.ToString("yyyy年MM月dd日");
            EndDateTextBox.Text = DateTime.Today.ToString("yyyy年MM月dd日");
        }
    }

    #endregion
}