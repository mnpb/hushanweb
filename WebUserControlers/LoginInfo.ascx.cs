﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_LoginInfo : System.Web.UI.UserControl
{
    //public static string LoginIP = string.Empty;

    //public static DateTime LoginDateTime = DateTime.Now;

    private Account LoginAccount
    {
        get
        {
            if (ViewState["LoginAccount"] == null)
                using (Account Account = new Account())
                    ViewState["LoginAccount"] = Account.GetAccount(int.Parse(Session["AccountID"].ToString()));
            
            return (Account)ViewState["LoginAccount"]; 
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<AccountLogin> AccountLoginList = LoginAccount.AccountLoginList;
            AccountLogin LatestAccountLogin = AccountLoginList.Where(AL => AL.Aaction == 1).OrderByDescending(AL => AL.ActionDateTime).First();

            LoginAccountLiteral.Text = LoginAccount.LoginName;
            LoginIPLiteral.Text = LatestAccountLogin.LoginIP;
            LoginDateTimeLiteral.Text = string.Format("{0:yyyy/MM/dd HH:mm}", LatestAccountLogin.ActionDateTime);
        }
    }

    protected void LogoutLinkButton_Click(object sender, EventArgs e)
    {
        //寫入一筆登出記錄
        //寫入一筆登錄記錄
        using (AccountLogin AccountLogin = new AccountLogin())
            AccountLogin.AddAccountLogin((int)Session["AccountID"]
                                         , 2
                                         , Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null ? HttpContext.Current.Request.UserHostAddress : Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString()
                                         , DateTime.Now);

        Session["AccountID"] = null;

        Response.Redirect("~/index.aspx");
    }
}