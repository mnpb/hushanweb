﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupToAccountList.ascx.cs" Inherits="WebUserControlers_AccountManagement_GroupToAccountList" %>
<asp:GridView ID="AccountToGroupGridView" runat="server" CssClass="cleartable" AutoGenerateColumns="False" ShowHeader="False" 
    OnRowDataBound="AccountToGroupGridView_RowDataBound" Width="200px">
    <Columns>
        <asp:TemplateField HeaderText="AccountID" SortExpression="AccountID">
            <ItemTemplate>
                <asp:Literal ID="AccountNameLiteral" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>  
</asp:GridView>


<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAccountToGroupByGroupID" TypeName="AccountToGroup" OnSelecting="ObjectDataSource1_Selecting">
    <SelectParameters>
        <asp:Parameter Name="GroupID" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>




