﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_CheckBoxGroupList : System.Web.UI.UserControl, IGroupObject
{
    public IAccountObject AccountObject { get; set; }

    public int RepeatColumns
    {
        get { return ViewState["RepeatColumns"] == null ? 1 : (int)ViewState["RepeatColumns"]; }
        set { ViewState["RepeatColumns"] = value; }
    }

    public RepeatDirection RepeatDirection
    {
        get { return ViewState["RepeatDirection"] == null ? RepeatDirection.Vertical : (RepeatDirection)ViewState["RepeatDirection"]; }
        set { ViewState["RepeatDirection"] = value; }
    }

    private List<Group> GroupList
    {
        get
        {
            List<Group> GroupList = new List<Group>();

            using (Group Group = new Group())
                GroupList = Group.GetGroupList();

            return GroupList;
        }
    }

    public bool IsFirstLoad
    {
        get { return ViewState["IsFirstLoad"] == null ? false : (bool)ViewState["IsFirstLoad"]; }
        set { ViewState["IsFirstLoad"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GenerateDepartmentTable();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {        
        if (IsFirstLoad)
        {
            for (int i = 0; i < GroupTable.Rows.Count; i++)
            {
                for (int j = 0; j < RepeatColumns; j++)
                {
                    int GroupListIndex = 0;

                    switch (RepeatDirection)
                    {
                        case RepeatDirection.Vertical:
                            GroupListIndex = i + j * RepeatColumns;
                            break;

                        case RepeatDirection.Horizontal:
                            GroupListIndex = i * RepeatColumns + j;
                            break;
                    }

                    if (GroupListIndex >= GroupList.Count)
                        continue;
                    else
                    {
                        CheckBox GroupCheckBox = (CheckBox)GroupTable.Rows[i].FindControl(string.Format("GroupCheckBox{0}", GroupList[GroupListIndex].GroupID));
                        try { GroupCheckBox.Checked = false; }
                        catch { }
                    }
                }
            }
        }

        IsFirstLoad = false;
    }

    private void GenerateDepartmentTable()
    {
        for (int i = 0; i < Math.Ceiling(float.Parse(GroupList.Count.ToString()) / RepeatColumns); i++)
        {
            TableRow TableRow = new TableRow();

            for (int j = 0; j < RepeatColumns; j++)
                TableRow.Cells.Add(new TableCell());

            GroupTable.Rows.Add(TableRow);
        }

        for (int i = 0; i < GroupTable.Rows.Count; i++)
        {
            for (int j = 0; j < RepeatColumns; j++)
            {
                int GroupListIndex = 0;

                switch (RepeatDirection)
                {
                    case RepeatDirection.Vertical:
                        GroupListIndex = i + j * RepeatColumns;
                        break;

                    case RepeatDirection.Horizontal:
                        GroupListIndex = i * RepeatColumns + j;
                        break;
                }

                if (GroupListIndex >= GroupList.Count)
                    continue;
                else
                {
                    CheckBox GroupCheckBox = new CheckBox();

                    GroupCheckBox.ID = string.Format("GroupCheckBox{0}", GroupList[GroupListIndex].GroupID);
                    GroupCheckBox.Text = GroupList[GroupListIndex].Name;                    

                    GroupCheckBox.AutoPostBack = true;
                    GroupCheckBox.CheckedChanged += GroupCheckBox_CheckedChanged;

                    GroupTable.Rows[i].Cells[j].Controls.Add(GroupCheckBox);
                }
            }
        }

    }

    protected void GroupCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox CheckBox = (CheckBox)sender;

        List<AccountToGroup> AccountToGroupList = GetAccountToGroupList(int.Parse(CheckBox.ID.Replace("GroupCheckBox", string.Empty)));

        foreach (AccountToGroup AccountToGroup in AccountToGroupList)
            GroupSelected(AccountObject, AccountToGroup.AccountID, CheckBox.Checked);
    }

    public void GroupSelected(IAccountObject AccountObject, int AccountID, bool IsSelected)
    {
        AccountObject.AccountSelected(AccountID, IsSelected);
    }

    private List<AccountToGroup> GetAccountToGroupList(int GroupID)
    {
        List<AccountToGroup> AccountToGroupList = new List<AccountToGroup>();

        using (AccountToGroup AccountToGroup = new AccountToGroup())
            AccountToGroupList = AccountToGroup.GetAccountToGroupByGroupID(GroupID);

        return AccountToGroupList;
    }

}