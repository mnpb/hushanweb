﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_CheckBoxMenuList : System.Web.UI.UserControl
{
    #region 全域變數
    public bool IsFirstLoad
    {
        get { return ViewState["IsFirstLoad"] == null ? false : (bool)ViewState["IsFirstLoad"]; }
        set { ViewState["IsFirstLoad"] = value; }
    }

    public int GroupID
    {
        get { return ViewState["GroupID"] == null ? 0 : (int)ViewState["GroupID"]; }
        set { ViewState["GroupID"] = value; }
    }

    /// <summary>
    /// 回傳已勾選的選單
    /// </summary>
    public List<Menu> SelectedMenuList
    {
        get
        {
            List<Menu> ReturnSelectedMenuList = new List<Menu>();

            foreach (CheckBox CheckBox in CheckBoxList)
            {
                if (CheckBox.Checked)
                {
                    Menu SelectedMenu = MenuList.Where(Mu => Mu.MenuID == int.Parse(CheckBox.ID.Replace("MenuCheckBox", string.Empty))).Single();
                    ReturnSelectedMenuList.Add(SelectedMenu);
                }
            }

            return ReturnSelectedMenuList;
        }
    }

    private List<CheckBox> CheckBoxList = new List<CheckBox>();

    private HtmlGenericControl TreeUL = new HtmlGenericControl("ul");

    private List<Menu> FirstNodeMenuList
    {
        get
        {
            if (ViewState["FirstNodeMenuList"] == null)
                using (Menu Menu = new Menu())
                    ViewState["FirstNodeMenuList"] = Menu.GetFirstNodeMenuList();

            return (List<Menu>)ViewState["FirstNodeMenuList"];
        }
    }

    private List<Menu> MenuList
    {
        get
        {
            if (ViewState["MenuList"] == null)
                using (Menu Menu = new Menu())
                    ViewState["MenuList"] = Menu.GetMenuList();

            return (List<Menu>)ViewState["MenuList"];
        }
    }

    private List<GroupToMenu> GroupToMenuList
    {
        get
        {
            List<GroupToMenu> ReturnGroupToMenuList = new List<GroupToMenu>();

            if (GroupID > 0)

                using (Group Group = new Group())
                    ReturnGroupToMenuList = Group.GetGroup(GroupID).GroupToMenuList;

            return ReturnGroupToMenuList;
        }
    }
    #endregion 

    protected void Page_Load(object sender, EventArgs e)
    {
        GenerateMenu();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        CheckMenu();
    }
     
    private void GenerateMenu()
    {
        if (FirstNodeMenuList.Count > 0)
        {
            GenerateNextNode(FirstNodeMenuList, out TreeUL);
            TreeUL.Attributes["id"] = "MenuCheckBoxTree";
        }

        MenuPlaceHolder.Controls.Add(TreeUL);        
    }

    private void CheckMenu()
    {
        if (IsFirstLoad)  //第一次載入時才檢查該群組是否有勾選此選項
        {
            foreach (CheckBox CheckBox in this.CheckBoxList)
            {
                if (GroupID > 0)
                    CheckBox.Checked = GroupToMenuList.Where(GTM => GTM.MenuID == int.Parse(CheckBox.ID.Replace("MenuCheckBox", string.Empty))).Count() > 0;
                else
                    CheckBox.Checked = false;
            }

            IsFirstLoad = false;
        }

        // I.系統管理要強制勾選，並且不得編輯
        CheckBox AccountManagementCheckBox = CheckBoxList.Where(CB => CB.Text == "I、系統管理").Single();
        AccountManagementCheckBox.Checked = true;
        AccountManagementCheckBox.Enabled = false;

        // 個人資料編輯要強制勾選，並且不得編輯
        CheckBox EditAccountCheckBox = CheckBoxList.Where(CB => CB.Text == "I1.個人資料編輯").Single();
        EditAccountCheckBox.Checked = true;
        EditAccountCheckBox.Enabled = false;


        LoadTreeViewCss();   
    }

    public void GenerateNextNode(List<Menu> MenuList, out HtmlGenericControl ReturnHtmlGenericControl)
    {
        HtmlGenericControl UL = new HtmlGenericControl("ul");

        foreach (Menu Menu in MenuList)
        {
            HtmlGenericControl LI = new HtmlGenericControl("li");
            CheckBox MenuCheckBox = new CheckBox();

            HtmlGenericControl ULNextNode = new HtmlGenericControl("ul");

            MenuCheckBox.Text = Menu.Name;
            MenuCheckBox.ID = string.Format("MenuCheckBox{0}", Menu.MenuID);
            MenuCheckBox.AutoPostBack = true;

            MenuCheckBox.CheckedChanged += MenuCheckBox_CheckedChanged;

            LI.Controls.Add(MenuCheckBox);
            CheckBoxList.Add(MenuCheckBox);

            if (Menu.NextNodeMenu.Count > 0)
            {
                GenerateNextNode(Menu.NextNodeMenu, out ULNextNode);
                LI.Controls.Add(ULNextNode);
            }

            UL.Controls.Add(LI);
        }

        ReturnHtmlGenericControl = UL;
    }

    protected void MenuCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox CheckBox = (CheckBox)sender;
        CheckBox ParentCheckBox = null;
        Menu ThisMenu = MenuList.Where(Mu => Mu.MenuID == int.Parse(CheckBox.ID.Replace("MenuCheckBox", string.Empty))).Single();

        try { ParentCheckBox = CheckBoxList.Where(CL => CL.ID == string.Format("MenuCheckBox{0}", ThisMenu.ParentID)).Single(); }
        catch { }

        //若有子選單，則全部勾選或取消
        CheckBoxNextNodeCheck(ThisMenu, CheckBox.Checked);

        //若有父選單則勾選
        if (CheckBox.Checked)
            CheckBoxPrentCheck(ThisMenu, true);
    }

    private void CheckBoxNextNodeCheck(Menu Menu, bool IsChecked)
    {
        CheckBox MenuCheckBox = CheckBoxList.Where(CL => CL.ID == string.Format("MenuCheckBox{0}", Menu.MenuID)).Single();
        MenuCheckBox.Checked = IsChecked;

        if (Menu.NextNodeMenu.Count > 0)
            foreach (Menu NextNode in Menu.NextNodeMenu)
                CheckBoxNextNodeCheck(NextNode, IsChecked);
    }

    private void CheckBoxPrentCheck(Menu Menu, bool IsChecked)
    {
        CheckBox ParentCheckBox = null;

        if (Menu.ParentID > 0)
        {
            Menu ParentMenu = MenuList.Where(Mu => Mu.MenuID == Menu.ParentID).Single();

            ParentCheckBox = CheckBoxList.Where(CL => CL.ID == string.Format("MenuCheckBox{0}", Menu.ParentID)).Single();
            ParentCheckBox.Checked = true;

            CheckBoxPrentCheck(ParentMenu, IsChecked);
        }
    }

    public void LoadTreeViewCss()
    {
        string ScriptString = " $(function () { " +
                              "    $(\"#MenuCheckBoxTree\").treeview({ " +
                              "        collapsed: false, " +
                              "        animated: \"medium\", " +
                              "        control: \"#sidetreecontrol\", " +
                              "        persist: \"location\" " +
                              "    }); " +
                              " }); ";

        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, this.GetType(), "", ScriptString, true);
    }
}