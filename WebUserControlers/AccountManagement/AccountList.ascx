﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountList.ascx.cs" Inherits="WebUserControlers_AccountManagement_AccountList" %>
<%@ Register Src="../Popup.ascx" TagName="Popup" TagPrefix="uc1" %>
<%@ Register Src="AccountEdit.ascx" TagName="AccountEdit" TagPrefix="uc2" %>

<asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
    <ContentTemplate>
        <asp:GridView ID="AccountGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="fancytable" DataKeyNames="AccountID" OnRowDataBound="AccountGridView_RowDataBound" OnSelectedIndexChanging="AccountGridView_SelectedIndexChanging" PageSize="15" Width="500px">
            <Columns>
                <asp:BoundField DataField="LoginName" HeaderText="使用者帳號" SortExpression="LoginName" />
                <asp:BoundField DataField="Name" HeaderText="使用者姓名" SortExpression="Name" />
                <asp:TemplateField HeaderText="是否啟用" SortExpression="IsActive">
                    <ItemTemplate>
                        <asp:Label ID="IsActiveLabel" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="修改" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="SelectLinkButton" runat="server" CausesValidation="False" CommandName="Select" Text="修改"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="DeleteLinkButton" runat="server" CausesValidation="False" CommandName="Delete" OnClientClick="return confirm('您確定要刪除此帳號？')" Text="刪除"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="cleartable" HorizontalAlign="Left" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAccountList" TypeName="Account" DeleteMethod="DeleteAccount">
    <DeleteParameters>
        <asp:Parameter Name="AccountID" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <uc1:Popup ID="Popup1" runat="server">
            <ContentTemplate>
                <uc2:AccountEdit ID="AccountEdit1" runat="server" />
            </ContentTemplate>
        </uc1:Popup>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="AccountGridView" EventName="SelectedIndexChanging"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>






