﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckBoxAccountList.ascx.cs" Inherits="WebUserControlers_AccountManagement_CheckBoxAccountList" %>
<asp:CheckBoxList ID="AccountCheckBoxList" runat="server" DataSourceID="ObjectDataSource1" DataTextField="Name" DataValueField="AccountID" RepeatColumns="4" CssClass="cleartable" OnPreRender="AccountCheckBoxList_PreRender" RepeatDirection="Horizontal">
</asp:CheckBoxList>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAccountList" TypeName="Account"></asp:ObjectDataSource>