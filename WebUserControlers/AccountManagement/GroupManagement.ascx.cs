﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_GroupManagement : System.Web.UI.UserControl
{
    private static int GroupID = 0;

    private string GroupGridViewSelectedCommandArgument = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        GroupGridView.DataSourceID = "ObjectDataSource1";
    }

    protected void GroupGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView GridView = (GridView)sender;

        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            int GroupID = (int)DataBinder.Eval(e.Row.DataItem, "GroupID");
            bool IsAdministrator = (bool)DataBinder.Eval(e.Row.DataItem, "IsAdministrator");

            Label AccountsInGroupLabel = (Label)e.Row.FindControl("AccountsInGroupLabel");
            LinkButton AccountsInGroupLinkButton = (LinkButton)e.Row.FindControl("AccountsInGroupLinkButton");
            LinkButton DeleteLinkButton = (LinkButton)e.Row.FindControl("DeleteLinkButton");

            using (AccountToGroup AccountToGroup = new AccountToGroup())
            {
                AccountsInGroupLabel.Text = AccountToGroup.GetAccountToGroupByGroupID(GroupID).Count.ToString();
                AccountsInGroupLinkButton.Text = AccountToGroup.GetAccountToGroupByGroupID(GroupID).Count.ToString();
            }

            if (IsAdministrator)
                DeleteLinkButton.OnClientClick = "alert('您無法刪除系統管理員群組'); return false;";
            else
                DeleteLinkButton.OnClientClick = "return confirm('您確定要刪除該群組？');";
        }
    }

    protected void GroupGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView GridView = (GridView)sender;
        string Name = ((Label)GridView.Rows[e.NewSelectedIndex].FindControl("NameLabel")).Text;
        GroupID =  (int)GridView.DataKeys[e.NewSelectedIndex].Value;



        e.NewSelectedIndex = -1;

        switch (GroupGridViewSelectedCommandArgument)
        {
            case "Edit":
                GroupEdit1.LoadMenuAndAccount(GroupID); 

                Popup1.Title = "編輯群組";
                Popup1.Show();
                break;

            case "AccountsInGroup":
                GroupToAccountList1.GroupID = GroupID;

                Popup2.Title = string.Format("{0} 群組成員", Name);
                Popup2.Show();
                break;
        }        
    }

    protected void GroupGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
            GroupGridViewSelectedCommandArgument = e.CommandArgument.ToString();
    }
}