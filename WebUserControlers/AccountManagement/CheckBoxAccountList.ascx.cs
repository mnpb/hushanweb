﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_CheckBoxAccountList : System.Web.UI.UserControl, IAccountObject
{
    private List<Account> AccountList
    {
        get
        {
            List<Account> AccountList = new List<Account>();

            using (Account Account = new Account())
                AccountList = Account.GetAccountList();

            return AccountList;
        } 
    }

    public bool IsFirstLoad
    {
        get { return ViewState["IsFirstLoad"] == null ? false : (bool)ViewState["IsFirstLoad"]; }
        set { ViewState["IsFirstLoad"] = value; }
    }

    /// <summary>
    /// 取得被選取的帳號
    /// </summary>
    public List<Account> SelectedAccountList
    {
        get
        {
            List<Account> ReturnSelectedMenuList = new List<Account>();

            foreach (ListItem ListItem in AccountCheckBoxList.Items)
            {
                if (ListItem.Selected)
                {
                    Account SelectedAccount = AccountList.Where(AL => AL.AccountID == int.Parse(ListItem.Value)).Single();
                    ReturnSelectedMenuList.Add(SelectedAccount);
                }
            }

            return ReturnSelectedMenuList;
        }
    }

    public int GroupID
    {
        get { return ViewState["GroupID"] == null ? 0 : (int)ViewState["GroupID"]; }
        set { ViewState["GroupID"] = value; }    
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void AccountCheckBoxList_PreRender(object sender, EventArgs e)
    {
        foreach (ListItem ListItem in AccountCheckBoxList.Items)
        {
            Account ThisAccount = null;

            ThisAccount = AccountList.Where(A => A.AccountID == int.Parse(ListItem.Value)).Single();

            //if (ThisAccount.AccountToGroupList.Count > 0 && ThisAccount.AccountToGroupList[0].GroupID != this.GroupID)
            //{
            //    ListItem.Enabled = false;
            //    //ListItem.Attributes["onclick"] = "alert('該帳號已屬於其他群組；若要更改該帳號群組，請至個人資料修改選單中修改。');";
            //    ListItem.Selected = false;
            //}
            //else
            //    ListItem.Enabled = true;

            //若為第一次載入，先將所有的選項清空
            if (IsFirstLoad)
                ListItem.Selected = false;

            //若有群組編號時且第一次載入，則將該群組的成員勾選
            if (IsFirstLoad && ThisAccount.AccountToGroupList.Where(ATG => ATG.GroupID == GroupID).Count() > 0)
                ListItem.Selected = true;
        }

        IsFirstLoad = false;
    }

    public void AccountSelected(int AccountID, bool IsSelected)
    {
        ListItem ListItem = AccountCheckBoxList.Items.FindByValue(AccountID.ToString());
        ListItem.Selected = IsSelected;
    }
        
}