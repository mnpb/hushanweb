﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_GroupEdit : System.Web.UI.UserControl
{
    private int GroupID
    {
        get { return ViewState["GroupID"] == null ? 0 : (int)ViewState["GroupID"]; }
        set { ViewState["GroupID"] = value; }    
    }

    public bool IsFirstLoad
    {
        get { return ViewState["IsFirstLoad"] == null ? false : (bool)ViewState["IsFirstLoad"]; }
        set { ViewState["IsFirstLoad"] = value; }    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckBoxGroupList1.AccountObject = CheckBoxAccountList1;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    public void LoadMenuAndAccount(int LoadGroupID)
    {
        GroupID = LoadGroupID;       

        CheckBoxMenuList1.GroupID = GroupID;
        CheckBoxMenuList1.IsFirstLoad = GroupID > 0;

        CheckBoxAccountList1.GroupID = GroupID;
        CheckBoxAccountList1.IsFirstLoad = GroupID > 0;

        CheckBoxGroupList1.IsFirstLoad = GroupID > 0;

        if (GroupID > 0)
        {
            BindGroup(GroupID);
            UpdaeGoupButton.Visible = true;
            AddNewGroupButton.Visible = false;
        }
        else
        {
            AddNewGroupButton.Visible = true;
            UpdaeGoupButton.Visible = false;
        }

        IsFirstLoad = false;
    }

    private void BindGroup(int GroupID)
    {
        using (Group Group = new Group())
        {
            Group ThisGroup = Group.GetGroup(GroupID);

            NameTextBox.Text = ThisGroup.Name;
            RemarkTextBox.Text = ThisGroup.Remark;
        }
    }

    private void GroupDataCheck(out bool IsValid, out string ErrorMessage)
    {
        bool ReturnIsValid = true;
        string ReturnErrorMessage = string.Empty;

        if (ReturnIsValid)
        {
            if (string.IsNullOrEmpty(NameTextBox.Text))
            {
                ReturnIsValid = false;
                ReturnErrorMessage = "請輸入群組名稱";
            }
        }

        if (ReturnIsValid)
        {
            if (CheckBoxMenuList1.SelectedMenuList.Count == 0)
            {
                ReturnIsValid = false;
                ReturnErrorMessage = "請至少選擇一個選單權限";
            }
        }


        IsValid = ReturnIsValid;
        ErrorMessage = ReturnErrorMessage;
    }

    protected void AddNewGroupButton_Click(object sender, EventArgs e)
    {
        bool IsValid = false;
        string ErrorMessage = string.Empty;

        GroupDataCheck(out IsValid, out ErrorMessage);

        if (IsValid)
        {
            int GroupID = 0;
            List<Menu> SelectedMenuList = CheckBoxMenuList1.SelectedMenuList;
            List<Account> SelectedAccountList = CheckBoxAccountList1.SelectedAccountList;

            //新增群組
            using (Group Group = new Group())
                GroupID = Group.AddGroupReturnGroupID(NameTextBox.Text, false, RemarkTextBox.Text);

            //新增群組對應選單
            using (GroupToMenu GroupToMenu = new GroupToMenu())
                foreach (Menu SelectedMenu in SelectedMenuList)
                    GroupToMenu.AddGroupToMenu(GroupID, SelectedMenu.MenuID);

            //新增帳號對應群組
            using (AccountToGroup AccountToGroup = new AccountToGroup())
                foreach (Account SelectedAccount in SelectedAccountList)
                    AccountToGroup.AddAccountToGroup(SelectedAccount.AccountID, GroupID);

            //將資料清空
            NameTextBox.Text = string.Empty;
            RemarkTextBox.Text = string.Empty;

            CheckBoxMenuList1.GroupID = 0;
            CheckBoxMenuList1.IsFirstLoad = true;   //GroupID = 0，IsFirstLoad = true, 清空所有的選單
            CheckBoxMenuList1.LoadTreeViewCss();

            CheckBoxGroupList1.IsFirstLoad = true;    //清空GroupList
            CheckBoxAccountList1.IsFirstLoad = true;  //清空AccountList

            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('新增群組成功');</SCRIPT>");
         
            ScriptManager.RegisterClientScriptBlock(UpdaeGoupButton, UpdaeGoupButton.GetType(), "", "window.setTimeout(\"alert('新增群組成功')\", 10);", true);
        }
        else
        {
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('" + ErrorMessage + "');</SCRIPT>");
            CheckBoxMenuList1.LoadTreeViewCss();
            ScriptManager.RegisterClientScriptBlock(UpdaeGoupButton, UpdaeGoupButton.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
        }

    }

    protected void UpdaeGoupButton_Click(object sender, EventArgs e)
    {
        bool IsValid = false;
        string ErrorMessage = string.Empty;

        GroupDataCheck(out IsValid, out ErrorMessage);

        if (IsValid)
        {
            List<Menu> SelectedMenuList = CheckBoxMenuList1.SelectedMenuList;
            List<Account> SelectedAccountList = CheckBoxAccountList1.SelectedAccountList;

            //更新群組
            using (Group Group = new Group())
                Group.ModifyGroup(GroupID, NameTextBox.Text, false, RemarkTextBox.Text);

            using (GroupToMenu GroupToMenu = new GroupToMenu())
            {
                //刪除該群組對應選單
                GroupToMenu.DeleteGroupToMenuByGroupID(GroupID);

                //新增該群組對應選單新增
                foreach (Menu SelectedMenu in SelectedMenuList)
                    GroupToMenu.AddGroupToMenu(GroupID, SelectedMenu.MenuID);
            }

            using (AccountToGroup AccountToGroup = new AccountToGroup())
            {
                //刪除帳號對應該群組
                AccountToGroup.DeleteAccountToGroupByroupID(GroupID);

                //新增帳號對應該群組
                foreach (Account SelectedAccount in SelectedAccountList)
                    AccountToGroup.AddAccountToGroup(SelectedAccount.AccountID, GroupID);
            }

            ScriptManager.RegisterClientScriptBlock(UpdaeGoupButton, UpdaeGoupButton.GetType(), "", "window.setTimeout(\"alert('更新群組成功')\", 10);", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(UpdaeGoupButton, UpdaeGoupButton.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
        }
    }
} 