﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_CheckBoxDepartmentList : System.Web.UI.UserControl, IDepartmentObject
{
    public IAccountObject AccountObject { get; set; }

    public int RepeatColumns 
    {
        get { return ViewState["RepeatColumns"] == null ? 1 : (int)ViewState["RepeatColumns"]; }
        set { ViewState["RepeatColumns"] = value; }
    }

    public RepeatDirection RepeatDirection
    {
        get { return ViewState["RepeatDirection"] == null ? RepeatDirection.Vertical : (RepeatDirection)ViewState["RepeatDirection"]; }
        set { ViewState["RepeatDirection"] = value; }
    }

    private List<Department> DepartmentList
    {
        get
        {
            if (ViewState["DepartmentList"] == null)
                using (Department Department = new Department())
                    ViewState["DepartmentList"] = Department.GetDepartmentList();

            return (List<Department>)ViewState["DepartmentList"];
        }    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GenerateDepartmentTable();
    }

    private void GenerateDepartmentTable()
    {
        for (int i = 0; i < Math.Ceiling(float.Parse(DepartmentList.Count.ToString()) / RepeatColumns); i++)
        {
            TableRow TableRow = new TableRow();

            for (int j = 0; j < RepeatColumns; j++ )
                TableRow.Cells.Add(new TableCell());

            DepartmentTable.Rows.Add(TableRow);     
        }

        for (int i = 0; i < DepartmentTable.Rows.Count; i++)
        {
            for (int j = 0; j < RepeatColumns; j++)
            {
                int DepartmentListIndex = 0;

                switch (RepeatDirection)
                {
                    case RepeatDirection.Vertical :
                        DepartmentListIndex = i + j * RepeatColumns;
                        break;

                    case RepeatDirection.Horizontal:
                        DepartmentListIndex = i * RepeatColumns + j;
                        break;
                }

                if (DepartmentListIndex >= DepartmentList.Count)
                    continue;
                else
                {
                    CheckBox DepartmentCheckBox = new CheckBox();

                    object obj = Page.FindControl(string.Format("DepartmentCheckBox{0}", DepartmentList[DepartmentListIndex].DepartmentID));

                    DepartmentCheckBox.ID = string.Format("DepartmentCheckBox{0}", DepartmentList[DepartmentListIndex].DepartmentID);
                    DepartmentCheckBox.Text = DepartmentList[DepartmentListIndex].Name;

                    DepartmentCheckBox.AutoPostBack = true;
                    DepartmentCheckBox.CheckedChanged += DepartmentCheckBox_CheckedChanged;

                    DepartmentTable.Rows[i].Cells[j].Controls.Add(DepartmentCheckBox);
                }
            }
        }
    }

    protected void DepartmentCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox CheckBox = (CheckBox)sender;

        List<AccountToDepartment> AccountToDepartmentList = GetAccountToDepartmentList(short.Parse(CheckBox.ID.Replace("DepartmentCheckBox", string.Empty)));

        foreach (AccountToDepartment AccountToDepartment in AccountToDepartmentList)
            DepartmentSelected(AccountObject, AccountToDepartment.AccountID, CheckBox.Checked);    
    }

    public void DepartmentSelected(IAccountObject AccountObject, int AccountID, bool IsSelected)
    {
        AccountObject.AccountSelected(AccountID, IsSelected);                
    }

    private List<AccountToDepartment> GetAccountToDepartmentList(short DepartmentID)
    {
        List<AccountToDepartment> AccountToDepartmentList = new List<AccountToDepartment>();

        using (AccountToDepartment AccountToDepartment = new AccountToDepartment())
            AccountToDepartmentList = AccountToDepartment.GetAccountToDepartmentByDepartmentID(DepartmentID);

        return AccountToDepartmentList;
    }

}