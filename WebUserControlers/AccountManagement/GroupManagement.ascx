﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupManagement.ascx.cs" Inherits="WebUserControlers_AccountManagement_GroupManagement" %>
<%@ Register Src="../Popup.ascx" TagName="Popup" TagPrefix="uc1" %>
<%@ Register Src="GroupEdit.ascx" TagName="GroupEdit" TagPrefix="uc2" %>
<%@ Register Src="GroupToAccountList.ascx" TagName="GroupToAccountList" TagPrefix="uc3" %>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:GridView ID="GroupGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="fancytable" DataKeyNames="GroupID" OnRowDataBound="GroupGridView_RowDataBound" OnSelectedIndexChanging="GroupGridView_SelectedIndexChanging" Width="600px" OnRowCommand="GroupGridView_RowCommand">
            <Columns>
                <asp:TemplateField HeaderText="群組名稱" SortExpression="Name">
                    <ItemTemplate>
                        <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Remark" HeaderText="備註" SortExpression="Remark" />
                <asp:TemplateField HeaderText="群組人數">
                    <ItemTemplate>
                        <asp:LinkButton ID="AccountsInGroupLinkButton" runat="server" CausesValidation="False" CommandName="Select" CommandArgument="AccountsInGroup"></asp:LinkButton>
                        <asp:Label ID="AccountsInGroupLabel" runat="server" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="修改" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="SelectLinkButton" runat="server" CausesValidation="False" CommandName="Select" CommandArgument="Edit" Text="修改"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="DeleteLinkButton" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="cleartable" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetGroupList" TypeName="Group" DeleteMethod="DeleteGroup">
    <DeleteParameters>
        <asp:Parameter Name="GroupID" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <uc1:Popup ID="Popup1" runat="server">
            <ContentTemplate>
                <uc2:GroupEdit ID="GroupEdit1" runat="server" />
            </ContentTemplate>
        </uc1:Popup>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="GroupGridView" EventName="SelectedIndexChanging" />
    </Triggers>
</asp:UpdatePanel>

<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <uc1:Popup ID="Popup2" runat="server">
            <ContentTemplate>
                <uc3:GroupToAccountList ID="GroupToAccountList1" runat="server" />
            </ContentTemplate>
        </uc1:Popup>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="GroupGridView" EventName="SelectedIndexChanging" />
    </Triggers>
</asp:UpdatePanel>











