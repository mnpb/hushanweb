﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_AccountList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        AccountGridView.DataSourceID = "ObjectDataSource1";
    }

    protected void AccountGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView GridView = (GridView)sender;

        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            bool IsActive = (bool)DataBinder.Eval(e.Row.DataItem, "IsActive");

            Label IsActiveLabel = (Label)e.Row.FindControl("IsActiveLabel");

            IsActiveLabel.Text = IsActive ? "是" : "否";
        }
    }

    protected void AccountGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView GridView = (GridView)sender;

        int AccountID = (int)GridView.DataKeys[e.NewSelectedIndex].Value;
        AccountEdit1.AccountID = AccountID;

        Popup1.Title = "修改使用者資料";
        Popup1.Show();

        e.NewSelectedIndex = -1;
    }
}