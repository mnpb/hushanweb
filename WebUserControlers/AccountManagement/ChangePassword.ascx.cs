﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_ChangePassword : System.Web.UI.UserControl
{
    public int AccountID 
    {
        get { return ViewState["AccountID"] == null ? 0 : (int)ViewState["AccountID"]; }
        set { ViewState["AccountID"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        AccountFormView.DataSourceID = "ObjectDataSource1";
    }    

    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["AccountID"] = AccountID;
    }

    protected void AccountFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        FormView FormView = (FormView)sender;

        string ErrorMessage = string.Empty;

        Literal PasswordLiteral = (Literal)FormView.FindControl("PasswordLiteral");
        TextBox PasswordTextBox = (TextBox)FormView.FindControl("PasswordTextBox");
        TextBox NewPasswordTextBox = (TextBox)FormView.FindControl("NewPasswordTextBox");
        TextBox ConfirmPasswordTextBox = (TextBox)FormView.FindControl("ConfirmPasswordTextBox");

        if (string.IsNullOrEmpty(PasswordTextBox.Text))
            ErrorMessage += "請輸入舊密碼\\\\n";
        else if (PasswordLiteral.Text != PasswordTextBox.Text)
            ErrorMessage += "舊密碼輸入錯誤\\\\n";

        if (string.IsNullOrEmpty(NewPasswordTextBox.Text))
            ErrorMessage += "請輸入新密碼\\\\n";

        if (string.IsNullOrEmpty(ConfirmPasswordTextBox.Text))
            ErrorMessage += "請輸入確認密碼\\\\n";
        else if (ConfirmPasswordTextBox.Text != NewPasswordTextBox.Text)
            ErrorMessage += "新密碼和確認密碼不符\\\\n";


        e.NewValues["Password"] = NewPasswordTextBox.Text;

        e.Cancel = !string.IsNullOrEmpty(ErrorMessage);

        if (e.Cancel)
            ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
    }

    protected void AccountFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        FormView FormView = (FormView)sender;

        ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "window.setTimeout(\"alert('更新密碼成功\\\\n新密碼將於下次登入時生效'); location.href='" + ResolveUrl("~/index.aspx") + "'\", 10);", true);
    }

    protected void UpdateCancelButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/index.aspx");
    }
}