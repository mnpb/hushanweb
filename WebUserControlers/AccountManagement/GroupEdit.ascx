﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupEdit.ascx.cs" Inherits="WebUserControlers_AccountManagement_GroupEdit" %>

<%@ Register src="CheckBoxGroupList.ascx" tagname="CheckBoxGroupList" tagprefix="uc1" %>
<%@ Register Src="CheckBoxAccountList.ascx" TagName="CheckBoxAccountList" TagPrefix="uc2" %>
<%@ Register Src="CheckBoxMenuList.ascx" TagName="CheckBoxMenuList" TagPrefix="uc3" %>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td valign="top">
            <table class="fancytable" style="width: 400px">
                <tr>
                    <th style="text-align: right">群組名稱</th>
                    <td style="text-align: left">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:TextBox ID="NameTextBox" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right">備註</th>
                    <td style="text-align: left">
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:TextBox ID="RemarkTextBox" runat="server" MaxLength="255" Width="220px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right">群組網頁使用權限</th>
                    <td style="text-align: left">
                        <asp:Panel ID="CheckBoxMenuListPanel" runat="server" Height="350px" BackColor="White" ScrollBars="Auto">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <uc3:CheckBoxMenuList ID="CheckBoxMenuList1" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table class="fancytable" width="500px">
                <tr>
                    <th>群組清單</th>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Panel ID="CheckBoxDepartmentListPanel" runat="server" Height="90px" BackColor="White" ScrollBars="Auto">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <uc1:CheckBoxGroupList ID="CheckBoxGroupList1" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <th>所屬帳號清單</th>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Panel ID="CheckBoxAccountListPanel" runat="server" Height="217px" BackColor="White" ScrollBars="Auto">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <uc2:CheckBoxAccountList ID="CheckBoxAccountList1" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:Button ID="AddNewGroupButton" runat="server" OnClick="AddNewGroupButton_Click" Text="新增" />
                                <asp:Button ID="UpdaeGoupButton" runat="server" OnClick="UpdaeGoupButton_Click" Text="更新" Visible="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

