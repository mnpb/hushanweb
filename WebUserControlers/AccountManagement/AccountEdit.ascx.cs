﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_AccountEdit : System.Web.UI.UserControl
{
    public int AccountID
    {
        get { return ViewState["AccountID"] == null ? 0 : (int)ViewState["AccountID"]; }
        set { ViewState["AccountID"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (AccountID > 0)
            AccountFormView.ChangeMode(FormViewMode.Edit);
        else
            AccountFormView.ChangeMode(FormViewMode.Insert);

        AccountFormView.DataSourceID = "ObjectDataSource1";
    }

    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["AccountID"] = this.AccountID;
    }

    protected void AccountFormView_DataBound(object sender, EventArgs e)
    {
        FormView FormView = (FormView)sender;

        if (FormView.CurrentMode == FormViewMode.Edit)
        {
            string Password = (string)DataBinder.Eval(FormView.DataItem, "Password");
            bool IsActive = (bool)DataBinder.Eval(FormView.DataItem, "IsActive");
            List<AccountToGroup> AccountToGroupList = (List<AccountToGroup>)DataBinder.Eval(FormView.DataItem, "AccountToGroupList");

            TextBox PasswordTextBox = (TextBox)FormView.FindControl("PasswordTextBox");
            RadioButtonList IsActiveRadioButtonList = (RadioButtonList)FormView.FindControl("IsActiveRadioButtonList");
            CheckBoxList GroupCheckBoxList = (CheckBoxList)FormView.FindControl("GroupCheckBoxList");

            PasswordTextBox.Attributes["value"] = Password;
            IsActiveRadioButtonList.SelectedValue = IsActive.ToString();

            foreach (ListItem ListItem in GroupCheckBoxList.Items)
                ListItem.Selected = AccountToGroupList.Where(ATG => ATG.GroupID == int.Parse(ListItem.Value)).Count() > 0;
        }
    }

    protected void AccountFormView_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
        FormView FormView = (FormView)sender;
        string ErrorMessage = string.Empty;

        TextBox LoginNameTextBox = (TextBox)FormView.FindControl("LoginNameTextBox");
        TextBox NameTextBox = (TextBox)FormView.FindControl("NameTextBox");
        TextBox PasswordTextBox = (TextBox)FormView.FindControl("PasswordTextBox");
        RadioButtonList IsActiveRadioButtonList = (RadioButtonList)FormView.FindControl("IsActiveRadioButtonList");

        CheckBoxList GroupCheckBoxList = (CheckBoxList)FormView.FindControl("GroupCheckBoxList");

        //檢查帳號、使用者姓名、密碼、所屬群組是否為空值
        //檢查帳號是否有重複

        if (string.IsNullOrEmpty(LoginNameTextBox.Text))
            ErrorMessage += "請輸入帳號名稱\\\\n";
        else
            using (Account Account = new Account())
                ErrorMessage += Account.GetAccountByLoginName(LoginNameTextBox.Text) == null ? string.Empty : "該帳號已經被申請，請重新輸入帳號\\\\n";

        if (string.IsNullOrEmpty(NameTextBox.Text))
            ErrorMessage += "請輸入使用者姓名\\\\n";

        if (string.IsNullOrEmpty(PasswordTextBox.Text))
            ErrorMessage += "請輸入密碼\\\\n";

        if (string.IsNullOrEmpty(GroupCheckBoxList.SelectedValue))
            ErrorMessage += "請選擇所屬群組\\\\n";

        e.Cancel = !string.IsNullOrEmpty(ErrorMessage);

        e.Values["IsActive"] = bool.Parse(IsActiveRadioButtonList.SelectedValue);
        e.Values["IsEnable"] = true;
        e.Values["CreatedDateTime"] = DateTime.Now;
        e.Values["ModifiedDateTime"] = DateTime.Now;

        if (e.Cancel)
        {
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('" + ErrorMessage + "');</SCRIPT>");
            ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
        }
    }

    protected void AccountFormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
        FormView FormView = (FormView)sender;

        TextBox LoginNameTextBox = (TextBox)FormView.FindControl("LoginNameTextBox");
        CheckBoxList GroupCheckBoxList = (CheckBoxList)FormView.FindControl("GroupCheckBoxList");

        //取得AccountID, 新增AccountToGroup
        using (Account Account = new Account())
        {
            Account ThisAccount = Account.GetAccountByLoginName(LoginNameTextBox.Text);

            using (AccountToGroup AccountToGroup = new AccountToGroup())
                foreach (ListItem ListItem in GroupCheckBoxList.Items)
                    if (ListItem.Selected)
                        AccountToGroup.AddAccountToGroup(ThisAccount.AccountID, int.Parse(ListItem.Value));
        }

        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('新增一筆帳號成功');</SCRIPT>");
        ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "alert('新增一筆帳號成功');", true);
    }


    protected void AccountFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        FormView FormView = (FormView)sender;
        string ErrorMessage = string.Empty;

        TextBox LoginNameTextBox = (TextBox)FormView.FindControl("LoginNameTextBox");
        TextBox NameTextBox = (TextBox)FormView.FindControl("NameTextBox");
        TextBox PasswordTextBox = (TextBox)FormView.FindControl("PasswordTextBox");
        RadioButtonList IsActiveRadioButtonList = (RadioButtonList)FormView.FindControl("IsActiveRadioButtonList");

        CheckBoxList GroupCheckBoxList = (CheckBoxList)FormView.FindControl("GroupCheckBoxList");

        //檢查使用者姓名、密碼、所屬群組是否為空值
        //檢查帳號是否有重複


        if (string.IsNullOrEmpty(NameTextBox.Text))
            ErrorMessage += "請輸入使用者姓名\\\\n";

        if (string.IsNullOrEmpty(PasswordTextBox.Text))
            ErrorMessage += "請輸入密碼\\\\n";

        if (string.IsNullOrEmpty(GroupCheckBoxList.SelectedValue))
            ErrorMessage += "請選擇所屬群組\\\\n";

        e.Cancel = !string.IsNullOrEmpty(ErrorMessage);

        e.NewValues["IsActive"] = bool.Parse(IsActiveRadioButtonList.SelectedValue);
        e.NewValues["ModifiedDateTime"] = DateTime.Now;

        if (e.Cancel)
            ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
    }

    protected void AccountFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        FormView FormView = (FormView)sender;

        TextBox AccountIDTextBox = (TextBox)FormView.FindControl("AccountIDTextBox");
        CheckBoxList GroupCheckBoxList = (CheckBoxList)FormView.FindControl("GroupCheckBoxList");

        //取得AccountID, 刪除舊的AccountToGroup，新增AccountToGroup
        using (AccountToGroup AccountToGroup = new AccountToGroup())
        {
            AccountToGroup.DeleteAccountToGroupByAccountID(int.Parse(AccountIDTextBox.Text));

            foreach (ListItem ListItem in GroupCheckBoxList.Items)
                if (ListItem.Selected)
                    AccountToGroup.AddAccountToGroup(int.Parse(AccountIDTextBox.Text), int.Parse(ListItem.Value));
        }

        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('更新帳號成功');</SCRIPT>");
        ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "window.setTimeout(\"alert('更新帳號成功')\", 10);", true);
    }
}