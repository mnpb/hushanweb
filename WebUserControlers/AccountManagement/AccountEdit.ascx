﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountEdit.ascx.cs" Inherits="WebUserControlers_AccountManagement_AccountEdit" %>
<asp:FormView ID="AccountFormView" runat="server" RenderOuterTable="False" DefaultMode="Edit" OnItemInserted="AccountFormView_ItemInserted" OnItemInserting="AccountFormView_ItemInserting" OnDataBound="AccountFormView_DataBound" OnItemUpdated="AccountFormView_ItemUpdated" OnItemUpdating="AccountFormView_ItemUpdating">
    <EditItemTemplate>
        <table class="fancytable" style="width: 700px">
            <tr>
                <th style="width: 20%; text-align: right">帳號名稱</th>
                <td style="width: 25%; text-align: left">
                    <asp:Label ID="LoginNameLabel" runat="server" Text='<%# Bind("LoginName") %>' />
                </td>
                <th style="width: 55%">所屬群組</th>
            </tr>
            <tr>
                <th style="text-align: right">使用者姓名</th>
                <td style="text-align: left">
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="15" Width="100px" /></td>
                <td rowspan="7" style="vertical-align: top">
                    <asp:Panel ID="DepartmentPanel" runat="server" Height="200px" ScrollBars="Auto">
                        <asp:CheckBoxList ID="GroupCheckBoxList" runat="server" CssClass="cleartable" DataSourceID="ObjectDataSource2" DataTextField="Name" DataValueField="GroupID" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <th style="text-align: right">密碼</th>
                <td style="text-align: left">
                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' TextMode="Password" MaxLength="30" Width="120px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">Email</th>
                <td style="text-align: left">
                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' MaxLength="50" Width="170px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">是否啟用</th>
                <td style="text-align: left">
                    <asp:RadioButtonList ID="IsActiveRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Selected="True" Value="true">是</asp:ListItem>
                        <asp:ListItem Value="false">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th style="text-align: right">單位名稱</th>
                <td style="text-align: left">
                    <asp:TextBox ID="UnitTextBox" runat="server" Text='<%# Bind("Unit") %>' MaxLength="50" Width="170px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">聯絡電話</th>
                <td style="text-align: left">
                    <asp:TextBox ID="PhoneNoTextBox" runat="server" Text='<%# Bind("PhoneNo") %>' MaxLength="20" Width="100px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">地址</th>
                <td style="text-align: left">
                    <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' MaxLength="100" Width="170px" /></td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                </td>
            </tr>
        </table>

        <asp:TextBox ID="AccountIDTextBox" runat="server" Text='<%# Bind("AccountID") %>' Visible="false" />
        <asp:TextBox ID="LoginNameTextBox" runat="server" Text='<%# Bind("LoginName") %>' Visible="false" />
        <asp:CheckBox ID="IsEnableCheckBox" runat="server" Checked='<%# Bind("IsEnable") %>' Visible="false" />
        <asp:TextBox ID="CreatedDateTimeTextBox" runat="server" Text='<%# Bind("CreatedDateTime") %>' Visible="false" />
        <asp:TextBox ID="ModifiedDateTimeTextBox" runat="server" Text='<%# Bind("ModifiedDateTime") %>' Visible="false" />
    </EditItemTemplate>
    <InsertItemTemplate>
        <table class="fancytable" style="width: 700px">
            <tr>
                <th style="width: 20%; text-align: right">帳號名稱</th>
                <td style="width: 25%; text-align: left">
                    <asp:TextBox ID="LoginNameTextBox" runat="server" Text='<%# Bind("LoginName") %>' MaxLength="30" Width="120px" />
                </td>
                <th style="width: 55%">所屬群組</th>
            </tr>
            <tr>
                <th style="text-align: right">使用者姓名</th>
                <td style="text-align: left">
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="15" Width="100px" /></td>
                <td rowspan="7" style="vertical-align: top">
                    <asp:Panel ID="DepartmentPanel" runat="server" Height="200px" ScrollBars="Auto">
                        <asp:CheckBoxList ID="GroupCheckBoxList" runat="server" CssClass="cleartable" DataSourceID="ObjectDataSource2" DataTextField="Name" DataValueField="GroupID" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <th style="text-align: right">密碼</th>
                <td style="text-align: left">
                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' TextMode="Password" MaxLength="30" Width="120px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">Email</th>
                <td style="text-align: left">
                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' MaxLength="50" Width="170px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">是否啟用</th>
                <td style="text-align: left">
                    <asp:RadioButtonList ID="IsActiveRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Selected="True" Value="true">是</asp:ListItem>
                        <asp:ListItem Value="false">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th style="text-align: right">單位名稱</th>
                <td style="text-align: left">
                    <asp:TextBox ID="UnitTextBox" runat="server" Text='<%# Bind("Unit") %>' MaxLength="50" Width="170px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">聯絡電話</th>
                <td style="text-align: left">
                    <asp:TextBox ID="PhoneNoTextBox" runat="server" Text='<%# Bind("PhoneNo") %>' MaxLength="20" Width="100px" /></td>
            </tr>
            <tr>
                <th style="text-align: right">地址</th>
                <td style="text-align: left">
                    <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' MaxLength="100" Width="170px" /></td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="新增" />
                </td>
            </tr>

        </table>
    </InsertItemTemplate>
</asp:FormView>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" InsertMethod="AddAccount" SelectMethod="GetAccount" TypeName="Account" UpdateMethod="ModifyAccount" OnSelecting="ObjectDataSource1_Selecting">
    <InsertParameters>
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="LoginName" Type="String" />
        <asp:Parameter Name="Password" Type="String" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="IsActive" Type="Boolean" />
        <asp:Parameter Name="IsEnable" Type="Boolean" />
        <asp:Parameter Name="Unit" Type="String" />
        <asp:Parameter Name="PhoneNo" Type="String" />
        <asp:Parameter Name="Address" Type="String" />
        <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
        <asp:Parameter Name="ModifiedDateTime" Type="DateTime" />
    </InsertParameters>
    <SelectParameters>
        <asp:Parameter Name="AccountID" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="AccountID" Type="Int32" />
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="LoginName" Type="String" />
        <asp:Parameter Name="Password" Type="String" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="IsActive" Type="Boolean" />
        <asp:Parameter Name="IsEnable" Type="Boolean" />
        <asp:Parameter Name="Unit" Type="String" />
        <asp:Parameter Name="PhoneNo" Type="String" />
        <asp:Parameter Name="Address" Type="String" />
        <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
        <asp:Parameter Name="ModifiedDateTime" Type="DateTime" />
    </UpdateParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetGroupList" TypeName="Group" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
