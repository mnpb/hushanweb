﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.ascx.cs" Inherits="WebUserControlers_AccountManagement_ChangePassword" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
    <ContentTemplate>
        <asp:FormView ID="AccountFormView" runat="server" DefaultMode="Edit" OnItemUpdated="AccountFormView_ItemUpdated" OnItemUpdating="AccountFormView_ItemUpdating">
            <EditItemTemplate>
                <table class="fancytable" style="width: 450px">
                    <tr>
                        <th style="text-align: right; width: 30%">舊密碼</th>
                        <td style="text-align: left">
                            <asp:Literal ID="PasswordLiteral" runat="server" Text='<%# Bind("Password") %>' Visible="false"></asp:Literal>
                            <asp:TextBox ID="PasswordTextBox" runat="server" MaxLength="30" Text='<%# Bind("Password") %>' TextMode="Password" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: right">新密碼</th>
                        <td style="text-align: left">
                            <asp:TextBox ID="NewPasswordTextBox" runat="server" MaxLength="30" TextMode="Password" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: right">確認密碼</th>
                        <td style="text-align: left">
                            <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" MaxLength="30" TextMode="Password" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="確認修改" />
                            &nbsp;
                            <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" OnClick="UpdateCancelButton_Click" Text="取消" />
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="AccountIDTextBox" runat="server" Text='<%# Bind("AccountID") %>' Visible="false" />
                <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' Visible="false" />
                <asp:TextBox ID="LoginNameTextBox" runat="server" Text='<%# Bind("LoginName") %>' Visible="false" />
                <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' Visible="false" />
                <asp:CheckBox ID="IsActiveCheckBox" runat="server" Checked='<%# Bind("IsActive") %>' Visible="false" />
                <asp:CheckBox ID="IsEnableCheckBox" runat="server" Checked='<%# Bind("IsEnable") %>' Visible="false" />
                <asp:TextBox ID="UnitTextBox" runat="server" Text='<%# Bind("Unit") %>' Visible="false" />
                <asp:TextBox ID="PhoneNoTextBox" runat="server" Text='<%# Bind("PhoneNo") %>' Visible="false" />
                <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' Visible="false" />
                <asp:TextBox ID="CreatedDateTimeTextBox" runat="server" Text='<%# Bind("CreatedDateTime") %>' Visible="false" />
                <asp:TextBox ID="ModifiedDateTimeTextBox" runat="server" Text='<%# Bind("ModifiedDateTime") %>' Visible="false" />
            </EditItemTemplate>
        </asp:FormView>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAccount" TypeName="Account" UpdateMethod="ModifyAccount" OnSelecting="ObjectDataSource1_Selecting">
    <SelectParameters>
        <asp:Parameter Name="AccountID" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="AccountID" Type="Int32" />
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="LoginName" Type="String" />
        <asp:Parameter Name="Password" Type="String" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="IsActive" Type="Boolean" />
        <asp:Parameter Name="IsEnable" Type="Boolean" />
        <asp:Parameter Name="Unit" Type="String" />
        <asp:Parameter Name="PhoneNo" Type="String" />
        <asp:Parameter Name="Address" Type="String" />
        <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
        <asp:Parameter Name="ModifiedDateTime" Type="DateTime" />
    </UpdateParameters>
</asp:ObjectDataSource>

