﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControlers_AccountManagement_GroupToAccountList : System.Web.UI.UserControl
{
    public int GroupID
    {
        get { return ViewState["GroupID"] == null ? 0 : (int)ViewState["GroupID"]; }
        set { ViewState["GroupID"] = value; }    
    }

    public List<Account> AccountList
    {
        get 
        {
            if (ViewState["AccountList"] == null)
                using (Account Account = new Account())
                    ViewState["AccountList"] = Account.GetAccountList();

            return (List<Account>)ViewState["AccountList"];
        }    
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        AccountToGroupGridView.DataSourceID = "ObjectDataSource1";
    }

    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["GroupID"] = GroupID;
    }

    protected void AccountToGroupGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView GridView = (GridView)sender;

        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            int AccountID = (int)DataBinder.Eval(e.Row.DataItem, "AccountID");

            Account ThisAccount = AccountList.Where(A => A.AccountID == AccountID).Single();

            Literal AccountNameLiteral = (Literal)e.Row.FindControl("AccountNameLiteral");
            AccountNameLiteral.Text = ThisAccount.Name;
        }
    }
}