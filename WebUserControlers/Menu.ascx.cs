﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class WebUserControlers_Menu : System.Web.UI.UserControl
{
    public int AccountID
    {
        get { return ViewState["AccountID"] == null ? 0 : (int)ViewState["AccountID"]; }
        set { ViewState["AccountID"] = value; }    
    }

    private Account LoginAccount
    {
        get 
        {
            if (ViewState["Account"] == null)
                using (Account Account = new Account())
                    ViewState["Account"] = Account.GetAccount(AccountID);

            return ViewState["Account"] == null ? null : (Account)ViewState["Account"];
        }    
    }

    private List<Menu> MenuList
    {
        get 
        {
            if (ViewState["MenuList"] == null)
                using (Menu Menu = new Menu())
                    ViewState["MenuList"] = Menu.GetFirstNodeMenuList();

            return (List<Menu>)ViewState["MenuList"];
        }    
    }

    private List<GroupToMenu> GroupToMenuList 
    {
        get
        {
            if (ViewState["GroupToMenuList"] == null)
            {
                using (Group Group = new Group())
                    using (GroupToMenu GroupToMenu = new GroupToMenu())
                        ViewState["GroupToMenuList"] = GroupToMenu.GetGroupToMenuByGroupList(Group.GetGroupList(LoginAccount.AccountToGroupList));
            }

           return (List<GroupToMenu>)ViewState["GroupToMenuList"];
        }    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GenerateMenu();
    }

    private void GenerateMenu()
    {
        HtmlGenericControl MenuUL = new HtmlGenericControl("ul");

        if (MenuList.Count > 0 )             
            GenerateNextNode(MenuList, out MenuUL);

        MenuPlaceHolder.Controls.Add(MenuUL);
    }

    public void GenerateNextNode(List<Menu> MenuList, out HtmlGenericControl ReturnHtmlGenericControl)
    {
        HtmlGenericControl UL = new HtmlGenericControl("ul");        

        foreach (Menu Menu in MenuList)
        {
            HtmlGenericControl LI = new HtmlGenericControl("li");
            HyperLink HyperLink = new HyperLink();

            HtmlGenericControl ULNextNode = new HtmlGenericControl("ul");

            //若對應不到選單，則忽略過去            
            if (GroupToMenuList.Where(GTM => GTM.MenuID == Menu.MenuID).Count() == 0)
                continue;           

            HyperLink.Text = Menu.Name;
            HyperLink.NavigateUrl = Menu.NavigateUrl;
            HyperLink.Target = Menu.NavigateTarget;

            LI.Controls.Add(HyperLink);

            if (Menu.NextNodeMenu.Count > 0)
            {
                GenerateNextNode(Menu.NextNodeMenu, out ULNextNode);
                LI.Controls.Add(ULNextNode);
            }

            UL.Controls.Add(LI);
        }

        ReturnHtmlGenericControl = UL;
    }
}