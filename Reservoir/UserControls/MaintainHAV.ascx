﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainHAV.ascx.cs" Inherits="Reservoir_UserControls_MaintainHAV" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/cleartable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/AjaxCalendar.css") %>" rel="stylesheet" />

<table class="fancytable" style="width: 450px">
    <tr style="visibility:collapse">
        <th style="text-align: right">水庫站碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="ReservoirIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">水庫名稱</th>
        <td style="text-align: left">
            <asp:TextBox ID="ReservoirNameTextBox" runat="server" CssClass="textBoxStyle" Enabled="false" /></td>
    </tr>
      <tr>
        <th style="text-align: right">量測年份</th>
        <td style="text-align: left">
            <asp:TextBox ID="MeasureYearTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">資料起始日</th>
        <td style="text-align: left">
            <asp:TextBox ID="BeginDateTextBox" runat="server" Height="30px" Width="150px" CssClass="textBoxStyle"></asp:TextBox>
            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="BeginDateTextBox" Format="yyyy年MM月dd日" CssClass="cleartable cal_Theme1"></cc1:CalendarExtender>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">水位(m)</th>
        <td style="text-align: left">
            <asp:TextBox ID="WaterlevelTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">蓄水量(萬噸)</th>
        <td style="text-align: left">
            <asp:TextBox ID="VolumeTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
