﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reservoir_UserControls_MaintainHAV : System.Web.UI.UserControl
{
    #region Properties

    private string stationID;
    public string StationID
    {
        get
        {
            return stationID;
        }
        set
        {
            stationID = value;
            ShowHAVInfo(HAVInfo);
        }
    }

    public DateTime BeginDate
    {
        get;
        set;
    }

    public short MeasureYear
    {
        get;
        set;
    }

    public decimal Waterlevel
    {
        get;
        set;
    }

    private dynamic HAVInfo
    {
        get
        {
            return (from info in ReservoirController.Instance.GetHAVInfos(StationID, MeasureYear)
                    where info.Waterlevel == Waterlevel
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    private void EmptyAllControls()
    {
        foreach (Control c in this.Controls)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                drp.SelectedIndex = 0;
            }
        }
        MeasureYearTextBox.Enabled = true;
        BeginDateTextBox.Enabled = true;
    }

    /// <summary>
    /// 顯示HAV資料
    /// </summary>
    /// <param name="havInfo"></param>
    private void ShowHAVInfo(dynamic havInfo)
    {
        if (havInfo == null)
        { // 新增
            EmptyAllControls();
            ReservoirIDTextBox.Text = StationID;
            ReservoirNameTextBox.Text = ReservoirController.Instance.GetReservoirName(StationID);
            return;
        }

        try
        {
            ReservoirIDTextBox.Text = havInfo.ReservoirID;
            ReservoirNameTextBox.Text = ReservoirController.Instance.GetReservoirName(havInfo.ReservoirID);
            MeasureYearTextBox.Text = havInfo.MeasureYear.ToString();
            MeasureYearTextBox.Enabled = false; // 不可修改
            BeginDateTextBox.Text = Convert.ToDateTime(havInfo.BeginDate).ToString("yyyy年MM月dd日");
            BeginDateTextBox.Enabled = false; // 不可修改
            WaterlevelTextBox.Text = havInfo.Waterlevel.ToString();
            VolumeTextBox.Text = havInfo.Volume.ToString();
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var info = new ReservoirHAV
            {
                ReservoirID = ReservoirIDTextBox.Text,
                BeginDate = Convert.ToDateTime(BeginDateTextBox.Text),
                MeasureYear = Convert.ToInt16(MeasureYearTextBox.Text),
                Waterlevel = NumericUtility.ToDecimal(WaterlevelTextBox.Text),
                Volume = NumericUtility.ToDecimal(VolumeTextBox.Text),
            };
            if (!ReservoirController.Instance.AddHAVInfo(info))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}