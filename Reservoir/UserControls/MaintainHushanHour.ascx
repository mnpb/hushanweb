﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainHushanHour.ascx.cs" Inherits="Reservoir_UserControls_MaintainHushanHour" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/cleartable.css") %>" rel="stylesheet" />

<table class="fancytable" style="width: 600px">
<%--    <tr style="visibility:collapse">
        <th style="text-align: right">水庫站碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="ReservoirIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>--%>
    <tr>
<%--        <th style="text-align: right">水庫名稱</th>
        <td style="text-align: left">
            <asp:TextBox ID="ReservoirNameTextBox" runat="server" CssClass="textBoxStyle" Enabled="false" Text="湖山水庫" /></td>--%>
        <th style="text-align: right">時間</th>
        <td style="text-align: left" colspan="3">
            <asp:TextBox ID="InfoTimeTextBox" runat="server" CssClass="textBoxStyle" Enabled="false"/></td>
    </tr>
    <tr>
        <th style="text-align: right">水位(m)</th>
        <td style="text-align: left">
            <asp:TextBox ID="WaterlevelTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">蓄水量(萬噸)</th>
        <td style="text-align: left">
            <asp:TextBox ID="VolumeTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">總入流量(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="TotalInFlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">溢洪道(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="SpillwayFlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">第一出水工(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="OutletWork1FlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">第二出水工(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="OutletWork2FlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">生態放流量(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="EcologyFlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">總放流量(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="TotalOutFlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">第一原水管(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="OriginalPipe1FlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">公共給水(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="PublicWaterSupplyTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">湖山水庫雨量(mm)</th>
        <td style="text-align: left">
            <asp:TextBox ID="HushanRainQtyTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">取水口外水位(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="TongtouWaterIntakeLevelTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">越域引水量(cms)</th>
        <td style="text-align: left">
            <asp:TextBox ID="TongtouCrossAreaInFlowTextBox" runat="server" CssClass="textBoxStyle" /></td>
        <th style="text-align: right">桶頭堰雨量(mm)</th>
        <td style="text-align: left">
            <asp:TextBox ID="TongtouRainQtyTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>