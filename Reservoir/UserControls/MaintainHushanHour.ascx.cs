﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reservoir_UserControls_MaintainHushanHour : System.Web.UI.UserControl
{
    #region Properties

    string ReservoirFieldInfo = "Waterlevel,Volume,TotalInFlow,SpillwayFlow,OutletWork1Flow,OutletWork2Flow,EcologyFlow,TotalOutFlow,OriginalPipe1Flow,OriginalPipe2Flow,PublicWaterSupply,HushanRainQty,TongtouWaterIntakeLevel,TongtouCrossAreaInFlow,TongtouRainQty";

    //private string stationID;
    //public string StationID
    //{
    //    get
    //    {
    //        return stationID;
    //    }
    //    set
    //    {
    //        stationID = value;
    //        ShowHourInfo(HourInfo);
    //    }
    //}

    private DateTime infoTime;
    public DateTime InfoTime
    {
        get
        {
            return infoTime;
        }
        set
        {
            infoTime = value;
            ShowHourInfo(HourInfo);
        }
    }

    private HushanHour HourInfo
    {
        get
        {
            return (from info in ReservoirController.Instance.GetHushanHourInfos(InfoTime, InfoTime)
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    private void EmptyAllControls()
    {
        ControlUtility.EmptyAllControls(this.Controls);
    }

    /// <summary>
    /// 顯示水情小時資料
    /// </summary>
    /// <param name="hourInfo"></param>
    private void ShowHourInfo(HushanHour hourInfo)
    {
        if (hourInfo == null)
        { // 新增
            EmptyAllControls();
            //ReservoirIDTextBox.Text = StationID;
            //ReservoirNameTextBox.Text = ReservoirController.Instance.GetReservoirName(StationID);
            return;
        }

        try
        {
            //ReservoirIDTextBox.Text = havInfo.ReservoirID;
            //ReservoirNameTextBox.Text = "湖山水庫";
            //ReservoirController.Instance.GetReservoirName(havInfo.ReservoirID);
            InfoTimeTextBox.Text = hourInfo.InfoTime.ToString("yyyy年MM月dd日HH時");
            foreach (var field in ReservoirFieldInfo.Split(',').ToList())
            {
                TextBox textBox = this.FindControl(field + "TextBox") as TextBox;
                if (textBox != null)
                {
                    var value = hourInfo.GetType().GetProperty(field).GetValue(hourInfo);
                    if (value != null && !NumericUtility.ToDouble(value).Equals(NumericUtility.NoHydData))
                    {
                        textBox.Text = value.ToString();
                    }
                }
            }
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var hourInfo = new HushanHour
            {
                //ReservoirID = ReservoirIDTextBox.Text,
                InfoTime = Convert.ToDateTime(InfoTimeTextBox.Text),
            };
            foreach (var field in ReservoirFieldInfo.Split(',').ToList())
            {
                TextBox textBox = this.FindControl(field + "TextBox") as TextBox;
                if (textBox != null)
                {
                    hourInfo.GetType().GetProperty(field).SetValue(hourInfo, NumericUtility.ToDecimal(textBox.Text));
                }
            }

            if (!ReservoirController.Instance.AddHour(hourInfo))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}