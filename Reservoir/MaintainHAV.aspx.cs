﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reservoir_MaintainHAV : System.Web.UI.Page
{
    #region Properties

    string ReservoirID
    {
        get
        {
            return ReservoirDropDownList.SelectedItem.Value;
        }
    }

    short MeasureYear
    {
        get
        {
            return Convert.ToInt16(MeasureYearDropDownList.SelectedItem.Value);
        }
    }

    //DateTime BeginDate
    //{
    //    get
    //    {
    //        return Convert.ToDateTime(BeginDateDropDownList.SelectedItem.Value);
    //    }
    //}

    List<ReservoirHAV> ReservoirHAVInfos
    {
        get
        {
            return ReservoirController.Instance.GetHAVInfos(ReservoirID, MeasureYear);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillAllReservoirs(ReservoirDropDownList);
            FillAllMeasureYears(MeasureYearDropDownList);
            //FillAllBeginDates(BeginDateDropDownList);
            ShowHAVInfos(0);
        }
        MessageLabel.Visible = false;
    }

    private void FillAllReservoirs(DropDownList dropDownList)
    {
        try
        {
            dropDownList.DataSource = ReservoirController.Instance.GetAllReservoirs();
            dropDownList.DataTextField = "ReservoirName";
            dropDownList.DataValueField = "ReservoirID";
            dropDownList.DataBind();
        }
        catch
        {
        }
    }

    private void FillAllMeasureYears(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();
        foreach (var year in ReservoirController.Instance.GetAllMeasureYears(ReservoirID))
        {
            dropDownList.Items.Add(year.ToString());
        }
        if (dropDownList.Items.Count > 0)
        {
            dropDownList.SelectedIndex = 0;
        }
    }

    //private void FillAllBeginDates(DropDownList dropDownList)
    //{
    //    dropDownList.Items.Clear();
    //    foreach (var date in ReservoirController.Instance.GetAllBeginDates(ReservoirID))
    //    {
    //        dropDownList.Items.Add(date.ToString("yyyy年MM月dd日"));
    //    }
    //    if (dropDownList.Items.Count > 0)
    //    {
    //        dropDownList.SelectedIndex = 0;
    //    }
    //}

    private void ShowHAVInfos(int pageIndex)
    {
        StationBaseGridView.PageIndex = pageIndex;
        StationBaseGridView.DataSource = ReservoirHAVInfos;
        StationBaseGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainStationUserControl.StationID = ReservoirID;
        Popup.Title = "水庫庫容曲線資料";
        Popup.Show();
    }

    #region GridView Events

    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowHAVInfos(e.NewPageIndex);
    }

    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        MaintainStationUserControl.MeasureYear =
            Convert.ToInt16(StationBaseGridView.DataKeys[e.NewEditIndex].Values["MeasureYear"].ToString());
        MaintainStationUserControl.BeginDate =
            Convert.ToDateTime(StationBaseGridView.DataKeys[e.NewEditIndex].Values["BeginDate"].ToString());
        MaintainStationUserControl.Waterlevel =
            Convert.ToDecimal(StationBaseGridView.DataKeys[e.NewEditIndex].Values["Waterlevel"].ToString());
        MaintainStationUserControl.StationID = StationBaseGridView.DataKeys[e.NewEditIndex].Values["ReservoirID"].ToString();
        Popup.Title = "水庫庫容曲線資料";
        Popup.Show();
    }

    protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string stationID = StationBaseGridView.DataKeys[e.RowIndex].Values["ReservoirID"].ToString();
            DateTime BeginDate = Convert.ToDateTime(StationBaseGridView.DataKeys[e.RowIndex].Values["BeginDate"].ToString());
            short measureYear = Convert.ToInt16(StationBaseGridView.DataKeys[e.RowIndex].Values["MeasureYear"].ToString());
            decimal waterlevel = Convert.ToDecimal(StationBaseGridView.DataKeys[e.RowIndex].Values["Waterlevel"].ToString());
            var havInfo = new ReservoirHAV
            {
                ReservoirID = stationID,
                MeasureYear = measureYear,
                BeginDate = BeginDate,
                Waterlevel = waterlevel,
            };
            if (!ReservoirController.Instance.DeleteHAVInfo(havInfo))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowHAVInfos(StationBaseGridView.PageIndex);
        }
    }

    #endregion

    protected void ReservoirDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //FillAllBeginDates(BeginDateDropDownList);
        FillAllMeasureYears(MeasureYearDropDownList);
        ShowHAVInfos(0);
    }

    protected void MeasureYearDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHAVInfos(0);
    }

    //protected void BeginDateDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ShowHAVInfos(0);
    //}

    /// <summary>
    /// 另存檔案
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SaveFileButton_Click(object sender, EventArgs e)
    {
        StringBuilder csvStringBuilder = new StringBuilder();

        try
        {
            string reservoirName = ReservoirDropDownList.SelectedItem.Text;
            csvStringBuilder.Append("水庫名稱," + reservoirName);
            csvStringBuilder.AppendLine();
            csvStringBuilder.Append("量測年份," + MeasureYear + "年");
            csvStringBuilder.AppendLine();
            csvStringBuilder.Append("啟用日期," + ReservoirHAVInfos.First().BeginDate.ToString("yyyy年MM月dd日"));
            csvStringBuilder.AppendLine();

            csvStringBuilder.Append("水位(m),水量(萬噸)");
            csvStringBuilder.AppendLine();
            foreach (var item in ReservoirHAVInfos)
            {
                csvStringBuilder.Append(item.Waterlevel + "," + item.Volume);
                csvStringBuilder.AppendLine();
            }

            string fileName = Server.UrlPathEncode(reservoirName + ".csv");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/csv";
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write(csvStringBuilder.ToString());
            Response.End();
        }
        catch
        {
        }
    }

    /// <summary>
    /// 匯入HAV
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImportButton_Click(object sender, EventArgs e)
    {
        List<string> havInfoLines = new List<string>();
        try
        {
            if (HavFileUpload.HasFile)
            {
                var reader = new StreamReader(HavFileUpload.PostedFile.InputStream, Encoding.Default);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (!string.IsNullOrEmpty(line))
                    {
                        havInfoLines.Add(line);
                    }
                }
                string reservoirName = havInfoLines[0].Split(',')[1];
                string reservoirID = ReservoirDropDownList.Items.FindByText(reservoirName).Value;
                short measureYear = Convert.ToInt16(havInfoLines[1].Split(',')[1].Replace("年", string.Empty));
                DateTime beginDate = Convert.ToDateTime(havInfoLines[2].Split(',')[1]);
                if (ReservoirController.Instance.AddHAVInfos(reservoirID, measureYear, beginDate, havInfoLines.Skip(4).ToList()))
                {
                    MessageLabel.Text = "已匯入HAV!";
                    MessageLabel.Visible = true;
                    FillAllMeasureYears(MeasureYearDropDownList);
                    //FillAllBeginDates(BeginDateDropDownList);
                    ShowHAVInfos(0);
                }
            }
            else
            {
                MessageLabel.Text = "請選擇欲匯入之檔案!";
                MessageLabel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "匯入檔案發生錯誤! 錯誤原因: " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    protected void MaintainStationUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowHAVInfos(StationBaseGridView.PageIndex);
    }

    protected string GetReservoirName(object reservoirID)
    {
        return ReservoirController.Instance.GetReservoirName(reservoirID.ToString());
    }
}