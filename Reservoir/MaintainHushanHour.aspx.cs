﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reservoir_MaintainHushanHour : SuperPage
{
    #region Properties

    //string ReservoirID
    //{
    //    get
    //    {
    //        return ReservoirDropDownList.SelectedItem.Value;
    //    }
    //}

    DateTime InfoDate
    {
        get
        {
            return SingleDateSelectorUserControl.InfoDate;
        }
    }

    List<HushanHour> HourInfos
    {
        get
        {
            return ReservoirController.Instance.GetHushanHourInfos(InfoDate);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //FillAllReservoirs(ReservoirDropDownList);
            //FillAllMeasureYears(MeasureYearDropDownList);
            //FillAllBeginDates(BeginDateDropDownList);
            ShowHourInfos();
        }
        //MessageLabel.Visible = false;
    }

    //private void FillAllReservoirs(DropDownList dropDownList)
    //{
    //    try
    //    {
    //        dropDownList.DataSource = ReservoirController.Instance.GetAllReservoirs();
    //        dropDownList.DataTextField = "ReservoirName";
    //        dropDownList.DataValueField = "ReservoirID";
    //        dropDownList.DataBind();
    //    }
    //    catch
    //    {
    //    }
    //}

    //private void FillAllMeasureYears(DropDownList dropDownList)
    //{
    //    dropDownList.Items.Clear();
    //    foreach (var year in ReservoirController.Instance.GetAllMeasureYears(ReservoirID))
    //    {
    //        dropDownList.Items.Add(year.ToString());
    //    }
    //    if (dropDownList.Items.Count > 0)
    //    {
    //        dropDownList.SelectedIndex = 0;
    //    }
    //}

    //private void FillAllBeginDates(DropDownList dropDownList)
    //{
    //    dropDownList.Items.Clear();
    //    foreach (var date in ReservoirController.Instance.GetAllBeginDates(ReservoirID))
    //    {
    //        dropDownList.Items.Add(date.ToString("yyyy年MM月dd日"));
    //    }
    //    if (dropDownList.Items.Count > 0)
    //    {
    //        dropDownList.SelectedIndex = 0;
    //    }
    //}

    private void ShowHourInfos()
    {
        //HourInfoGridView.PageIndex = pageIndex;
        try
        {
            HourInfoGridView.DataSource = HourInfos;
            HourInfoGridView.DataBind();
        }
        catch
        {
        }
    }

    ///// <summary>
    ///// 顯示「新增資料介面」
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void AddButton_Click(object sender, EventArgs e)
    //{
    //    MaintainHushanHourUserControl.StationID = "1";
    //    Popup.Title = "湖山水庫小時資料";
    //    Popup.Show();
    //}

    #region GridView Events

    //protected void HourInfoGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    ShowHourInfos(e.NewPageIndex);
    //}

    protected void HourInfoGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        MaintainHushanHourUserControl.InfoTime = 
            Convert.ToDateTime(HourInfoGridView.DataKeys[e.NewEditIndex].Values["InfoTime"].ToString());
        Popup.Title = "湖山水庫小時資料";
        Popup.Show();
    }

    //protected void HourInfoGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    try
    //    {
    //        string stationID = StationBaseGridView.DataKeys[e.RowIndex].Values["ReservoirID"].ToString();
    //        DateTime BeginDate = Convert.ToDateTime(StationBaseGridView.DataKeys[e.RowIndex].Values["BeginDate"].ToString());
    //        short measureYear = Convert.ToInt16(StationBaseGridView.DataKeys[e.RowIndex].Values["MeasureYear"].ToString());
    //        decimal waterlevel = Convert.ToDecimal(StationBaseGridView.DataKeys[e.RowIndex].Values["Waterlevel"].ToString());
    //        var havInfo = new ReservoirHAV
    //        {
    //            ReservoirID = stationID,
    //            MeasureYear = measureYear,
    //            BeginDate = BeginDate,
    //            Waterlevel = waterlevel,
    //        };
    //        if (!ReservoirController.Instance.DeleteHAVInfo(havInfo))
    //        {
    //            IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
    //        }
    //    }
    //    finally
    //    {
    //        StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
    //        ShowHourInfos(StationBaseGridView.PageIndex);
    //    }
    //}

    #endregion

    //protected void ReservoirDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    //FillAllBeginDates(BeginDateDropDownList);
    //    FillAllMeasureYears(MeasureYearDropDownList);
    //    ShowHAVInfos(0);
    //}

    //protected void MeasureYearDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ShowHAVInfos(0);
    //}

    //protected void BeginDateDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ShowHAVInfos(0);
    //}

    ///// <summary>
    ///// 另存檔案
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void SaveFileButton_Click(object sender, EventArgs e)
    //{
    //    StringBuilder csvStringBuilder = new StringBuilder();

    //    try
    //    {
    //        string reservoirName = ReservoirDropDownList.SelectedItem.Text;
    //        csvStringBuilder.Append("水庫名稱," + reservoirName);
    //        csvStringBuilder.AppendLine();
    //        csvStringBuilder.Append("量測年份," + MeasureYear + "年");
    //        csvStringBuilder.AppendLine();
    //        csvStringBuilder.Append("啟用日期," + ReservoirHAVInfos.First().BeginDate.ToString("yyyy年MM月dd日"));
    //        csvStringBuilder.AppendLine();

    //        csvStringBuilder.Append("水位(m),水量(萬噸)");
    //        csvStringBuilder.AppendLine();
    //        foreach (var item in ReservoirHAVInfos)
    //        {
    //            csvStringBuilder.Append(item.Waterlevel + "," + item.Volume);
    //            csvStringBuilder.AppendLine();
    //        }

    //        string fileName = Server.UrlPathEncode(reservoirName + ".csv");
    //        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
    //        Response.ContentType = "application/csv";
    //        Response.ContentEncoding = System.Text.Encoding.Default;
    //        Response.Write(csvStringBuilder.ToString());
    //        Response.End();
    //    }
    //    catch
    //    {
    //    }
    //}

    ///// <summary>
    ///// 匯入HAV
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void ImportButton_Click(object sender, EventArgs e)
    //{
    //    List<string> havInfoLines = new List<string>();
    //    try
    //    {
    //        if (HavFileUpload.HasFile)
    //        {
    //            var reader = new StreamReader(HavFileUpload.PostedFile.InputStream, Encoding.Default);
    //            while (!reader.EndOfStream)
    //            {
    //                string line = reader.ReadLine();
    //                if (!string.IsNullOrEmpty(line))
    //                {
    //                    havInfoLines.Add(line);
    //                }
    //            }
    //            string reservoirName = havInfoLines[0].Split(',')[1];
    //            string reservoirID = ReservoirDropDownList.Items.FindByText(reservoirName).Value;
    //            short measureYear = Convert.ToInt16(havInfoLines[1].Split(',')[1].Replace("年", string.Empty));
    //            DateTime beginDate = Convert.ToDateTime(havInfoLines[2].Split(',')[1]);
    //            if (ReservoirController.Instance.AddHAVInfos(reservoirID, measureYear, beginDate, havInfoLines.Skip(4).ToList()))
    //            {
    //                MessageLabel.Text = "已匯入HAV!";
    //                MessageLabel.Visible = true;
    //                FillAllMeasureYears(MeasureYearDropDownList);
    //                //FillAllBeginDates(BeginDateDropDownList);
    //                ShowHAVInfos(0);
    //            }
    //        }
    //        else
    //        {
    //            MessageLabel.Text = "請選擇欲匯入之檔案!";
    //            MessageLabel.Visible = true;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        MessageLabel.Text = "匯入檔案發生錯誤! 錯誤原因: " + ex.Message;
    //        MessageLabel.Visible = true;
    //    }
    //}

    protected void MaintainHushanHourUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowHourInfos();
    }

    //protected string GetReservoirName(object reservoirID)
    //{
    //    return ReservoirController.Instance.GetReservoirName(reservoirID.ToString());
    //}
    protected void QueryButton_Click(object sender, EventArgs e)
    {
        ShowHourInfos();
    }

    protected string ToFormat(object valueObj)
    {
        var value = NumericUtility.ToDouble(valueObj);
        if (!value.Equals(NumericUtility.NoHydData))
        {
            return value.ToString();
        }
        return string.Empty;
    }
}