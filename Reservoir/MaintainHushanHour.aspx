﻿<%@ Page Title="湖山水庫營運資料維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainHushanHour.aspx.cs" Inherits="Reservoir_MaintainHushanHour" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
    <%@ Register Src="~/Reservoir/UserControls/MaintainHushanHour.ascx" TagPrefix="uc1" TagName="MaintainHushanHour" %>
    <%@ Register Src="~/WebUserControlers/DateTimes/SingleDateSelector.ascx" TagPrefix="uc1" TagName="SingleDateSelector" %>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainHushanHour runat="server" ID="MaintainHushanHourUserControl" OnCompleted="MaintainHushanHourUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            日期 : <uc1:SingleDateSelector runat="server" ID="SingleDateSelectorUserControl" />&nbsp;<asp:Button ID="QueryButton" runat="server" OnClick="QueryButton_Click" Text="查詢" CssClass="buttonStyle" />
            <%--            水庫 :
            <div class="selectStyle">
                <asp:DropDownList ID="ReservoirDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ReservoirDropDownList_SelectedIndexChanged" CssClass="selectStyle-select"></asp:DropDownList>
            </div>--%>
            <%--            <asp:Button ID="ImportButton" runat="server" OnClick="ImportButton_Click" Text="匯入HAV" CssClass="buttonStyle" />
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>--%>
            <%--<br />
            <br />
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增資料" CssClass="buttonStyle" /><br />
            <br />--%>
            <asp:GridView ID="HourInfoGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="InfoTime" EmptyDataText="沒有任何資料"
                CssClass="fancytable" OnRowEditing="HourInfoGridView_RowEditing">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <%--                    <asp:TemplateField HeaderText="站碼" Visible="false">
                        <ItemTemplate><%# Eval("ReservoirID") %></ItemTemplate>
                    </asp:TemplateField>--%>
                    <%--    <asp:TemplateField HeaderText="水庫名稱">
                        <ItemTemplate><%# GetReservoirName(Eval("ReservoirID")) %></ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="時間">
                        <ItemTemplate><%# Eval("InfoTime", "{0:yyyy年MM月dd日HH時}") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="水位<br>(m)">
                        <ItemTemplate><%# ToFormat(Eval("Waterlevel")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="蓄水量<br>(萬噸)">
                        <ItemTemplate><%# ToFormat(Eval("Volume")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="總入流量<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("TotalInFlow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="溢洪道<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("SpillwayFlow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="第一出水工<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("OutletWork1Flow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="第二出水工<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("OutletWork2Flow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="生態放流量<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("EcologyFlow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="總放流量<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("TotalOutFlow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="第一原水管<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("OriginalPipe1Flow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="公共給水<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("PublicWaterSupply")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="湖山水庫雨量<br>(mm)">
                        <ItemTemplate><%# ToFormat(Eval("HushanRainQty")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="取水口外水位<br>(m)">
                        <ItemTemplate><%# ToFormat(Eval("TongtouWaterIntakeLevel")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="越域引水量<br>(cms)">
                        <ItemTemplate><%# ToFormat(Eval("TongtouCrossAreaInFlow")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="桶頭堰雨量<br>(mm)">
                        <ItemTemplate><%# ToFormat(Eval("TongtouRainQty")) %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

