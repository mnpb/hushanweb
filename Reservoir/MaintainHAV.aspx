﻿<%@ Page Title="水庫庫容曲線維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainHAV.aspx.cs" Inherits="Reservoir_MaintainHAV" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Reservoir/UserControls/MaintainHAV.ascx" TagPrefix="uc1" TagName="MaintainStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
    <link href="../css/SelectStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainStation runat="server" ID="MaintainStationUserControl" OnCompleted="MaintainStationUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            水庫 :
            <div class="selectStyle">
                <asp:DropDownList ID="ReservoirDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ReservoirDropDownList_SelectedIndexChanged" CssClass="selectStyle-select"></asp:DropDownList>
            </div>
            ，&nbsp;量測年份 : 
               <div class="selectStyle">
                   <asp:DropDownList ID="MeasureYearDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="MeasureYearDropDownList_SelectedIndexChanged" CssClass="selectStyle-select"></asp:DropDownList>年
               </div>
            &nbsp;
        <%--    啟用日期 :
            <div class="selectStyle" style="width: 150px">
                <asp:DropDownList ID="BeginDateDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="BeginDateDropDownList_SelectedIndexChanged" CssClass="selectStyle-select">
                </asp:DropDownList>
            </div>
            &nbsp;--%>
            <asp:Button ID="SaveFileButton" runat="server" OnClick="SaveFileButton_Click" Text="另存檔案" CssClass="buttonStyle" />
            &nbsp;
            <asp:FileUpload ID="HavFileUpload" runat="server" CssClass="textBoxStyle" Height="30px" size="35" />
            &nbsp;
            <asp:Button ID="ImportButton" runat="server" OnClick="ImportButton_Click" Text="匯入HAV" CssClass="buttonStyle" />
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
            <br />
            <br />
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增資料" CssClass="buttonStyle" /><br />
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="ReservoirID,MeasureYear,BeginDate,Waterlevel" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing" OnRowDeleting="StationBaseGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站碼" Visible="false">
                        <ItemTemplate><%# Eval("ReservoirID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="水庫名稱">
                        <ItemTemplate><%# GetReservoirName(Eval("ReservoirID")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="量測年份">
                        <ItemTemplate><%# Eval("MeasureYear") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="啟用日期">
                        <ItemTemplate><%# Eval("BeginDate", "{0:yyyy年MM月dd日}") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="水位(m)">
                        <ItemTemplate><%# Eval("Waterlevel") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="蓄水量(萬噸)">
                        <ItemTemplate><%# Eval("Volume") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="SaveFileButton" />
            <asp:PostBackTrigger ControlID="ImportButton" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

