﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GetChart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // *************************************************************
        // Code to retrieve Session saved streamed Charts to a WebForm.
        // This code should be included if the WebChart 'UseStreams' property is set to True.
        // *************************************************************

        string chartName = Request.QueryString["Chart"];

        if (Session[chartName] != null)
        {
            System.IO.MemoryStream chartStream = new System.IO.MemoryStream();
            chartStream = ((System.IO.MemoryStream)Session[chartName]);
            Response.OutputStream.Write(chartStream.ToArray(), 0, (int)chartStream.Length);
            Response.AddHeader("Content-type", "image/png");
            chartStream.Close();
            Session.Remove(chartName);
        }
    }
}
