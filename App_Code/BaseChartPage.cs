﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.UI.HtmlControls;
using Steema.TeeChart.Styles;

/// <summary>
/// BaseChartPage 的摘要描述
/// </summary>
public class BaseDamChartPage : System.Web.UI.Page
{
    protected RainController rain = new RainController();
    protected HushanDayService hushanDay = new HushanDayService();

    protected DamChartManager chartManager = new DamChartManager();
    protected HtmlInputControl sDate, eDate, month;

    protected Color[] colors = new Color[] {
        Color.Red, Color.Orange, Color.Green, Color.DarkOrange, Color.Blue,
        Color.Purple, Color.Pink, Color.FromArgb(10,0,0), Color.Gray
    };
    protected PointerStyles[] pointerStyles = new PointerStyles[] {
        PointerStyles.Triangle, PointerStyles.Star, PointerStyles.Circle, PointerStyles.Diamond,PointerStyles.Cross,
        PointerStyles.Rectangle
    };

    public BaseDamChartPage()
    {
        this.Load += new EventHandler(this.Page_Load);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        sDate = Master.FindControl("sDate") as HtmlInputControl;
        eDate = Master.FindControl("eDate") as HtmlInputControl;
        month = Master.FindControl("month") as HtmlInputControl;

    }

    protected void CustomChartColumnName(Dictionary<string, string> dict, params List<DamChartData>[] list)
    {
        foreach (List<DamChartData> items in list)
        {
            foreach (var item in items)
            {
                item.AxisTitle = item.AxisTitle.Replace("Pressure_", "");//移除WaterPressure圖表的prefix
                item.AxisTitle = item.AxisTitle.Replace("Well_", "");//移除WaterPressure圖表的prefix
                if (dict.ContainsKey(item.AxisTitle))
                {
                    item.AxisTitle = dict[item.AxisTitle];
                }
            }
        }
    }

    public void CustomGridColumnName(Dictionary<string, string> dict, List<IName2NewName> gridResultMain)
    {
        if (dict == null)
        {
            dict = new Dictionary<string, string>(1);
        }
        foreach (IName2NewName item in gridResultMain)
        {
            item.ChangeColumnName2NewName = dict;
        }
    }

    protected void GetDateTime(out DateTime sdate, out DateTime edate)
    {
        if (string.IsNullOrEmpty(sDate.Value) && string.IsNullOrEmpty(eDate.Value))
        {
            if (string.IsNullOrEmpty(month.Value))
            {
                sdate = DateTime.Parse(DateTime.Now.Year + "/" + DateTime.Now.Month + "/01");
            }
            else
            {
                sdate = DateTime.Parse(month.Value);
            }
            edate = sdate.AddMonths(1).AddSeconds(-1);
        }
        else
        {
            try
            {
                sdate = DateTime.Parse(sDate.Value);
                edate = DateTime.Parse(eDate.Value);
            }
            catch
            {
                sdate = DateTime.Parse(DateTime.Now.Year + "/" + DateTime.Now.Month + "/01");
                edate = sdate.AddMonths(1).AddSeconds(-1);
            }
        }
    }
    public List<RainDay> RainDayData(DateTime sdate, DateTime edate)
    {
        var rainday = rain.GetDayInfos("1", sdate, edate);
        return rainday;
    }
    public Dictionary<DateTime, double?> SerialRainDayData(DateTime sdate, DateTime edate)
    {
        var data = RainDayData(sdate, edate);
        var ans = data.ToDictionary(x => x.InfoDate, y => (double?)y.RainQty);
        return ans;
    }
    public Dictionary<DateTime, double?> ReservoirDayData(DateTime sdate, DateTime edate)
    {
        var ans = hushanDay.GetWaterlevel(sdate, edate);
        return ans;
    }
    public Dictionary<DateTime, double?> SerialReservoirDayData(DateTime sdate, DateTime edate)
    {
        var data = ReservoirDayData(sdate, edate);
        return data;
    }
    protected virtual void ShowChart(WebChart webchart, string title, DateTime sdate, DateTime edate, List<DamChartData> datas,
        Dictionary<DateTime, double?> rain, Dictionary<DateTime, double?> reservoir, string axisTitle = "變化量(mm)")
    {
        Chart chart = chartManager.InitChart(webchart);
        chartManager.InitChartBottom(chart, sdate, edate);
        chartManager.DrawRainUp(chart, rain);
        chartManager.DrawReservoirR(chart, reservoir);
        DrawDatas(chart, datas, axisTitle);

        chart.Header.Text = title;
        chart.Axes.Bottom.SetMinMax(sdate, edate);
    }

    protected virtual void DrawDatas(Chart chart, List<DamChartData> datas, string axisTitle)
    {
        Axis axis = new Axis();
        axis.Title.Text = axisTitle;
        axis.Title.Angle = 90;
        axis.StartPosition = DamChartManager.UpPercent + 1;//y軸往下，51大概是中間的位置
        axis.EndPosition = 100;//最下面，圖框可視範選最大值為100
        axis.Automatic = true;
        chart.Axes.Custom.Add(axis);

        for (int i = 0; i < datas.Count(); i++)
        {
            var style = chartManager.GetPoints(datas[i], datas[i].AxisTitle, colors[i % colors.Length], pointerStyles[i / colors.Length % pointerStyles.Length]);
            style.CustomVertAxis = axis;
            chart.Series.Add(style);
        }
    }

}