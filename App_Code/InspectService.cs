﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// InspectService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
 [System.Web.Script.Services.ScriptService]
public class InspectService : System.Web.Services.WebService {

    public InspectService () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public object GetMessageInfos(string beginDate, string endDate)
    {
        var allInfos = InspectController.Instance.GetMessageInfos(Convert.ToDateTime(beginDate), Convert.ToDateTime(endDate));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    
}
