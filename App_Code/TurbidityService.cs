﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// TurbidityService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
 [System.Web.Script.Services.ScriptService]
public class TurbidityService : System.Web.Services.WebService {

    public TurbidityService () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod(Description = "取得所有濁度站")]
    public dynamic GetAllStations()
    {
        return TurbidityController.Instance.GetAllStations();
    }


    [WebMethod(Description = "取得濁度站小時資料")]
    public dynamic GetHourInfos(string stationID, DateTime date)
    {
        return TurbidityController.Instance.GetHourInfos(stationID, date, date.AddHours(23));
    }
    [WebMethod(Description = "取得濁度站小時資料")]
    public dynamic GetHourInfosForGrid(string stationID, DateTime date)
    {
        var allInfos = GetHourInfos(stationID, date);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得濁度站日資料")]
    public dynamic GetDayInfos(string stationID, int year, int month)
    {
        return TurbidityController.Instance.GetDayInfos(stationID, year, month);
    }
    [WebMethod(Description = "取得濁度站日資料")]
    public dynamic GetDayInfosForGrid(string stationID, int year, int month)
    {
        var allInfos = GetDayInfos(stationID, year, month);
        try
        {
            var resultObj = new
            {
                total = 1, 
                page = 1, 
                records = allInfos.Count,
                rows = allInfos, 
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }


    [WebMethod(Description = "取得濁度站月資料")]
    public dynamic GetMonthInfos(string stationID, int year)
    {
        return TurbidityController.Instance.GetMonthInfos(stationID, year);
    }
    [WebMethod(Description = "取得濁度站月資料")]
    public dynamic GetMonthInfosForGrid(string stationID, int year)
    {
        var allInfos = GetMonthInfos(stationID, year);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1, 
                records = allInfos.Count, 
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    
}
