﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

/// <summary>
/// ConfigInfo 的摘要描述
/// </summary>
public class ConfigInfo
{
    /// <summary>
    /// MS Sql資料庫連線字串
    /// </summary>
    public static string MSSqlConnectionString
    {
        get
        {
            return ConfigurationManager.ConnectionStrings["HushanConnectionString"].ConnectionString;
        }
    }

    public static string InspectConnectionString
    {
        get
        {
            return ConfigurationManager.ConnectionStrings["InspectConnectionString"].ConnectionString;
        }
    }
}