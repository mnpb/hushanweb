﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// SeepageService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
 [System.Web.Script.Services.ScriptService]
public class SeepageService : System.Web.Services.WebService {

    public SeepageService () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod(Description = "取得所有滲漏室")]
    public dynamic GetAllMeters()
    {
        return SeepageController.Instance.GetAllMeters();
    }

    [WebMethod(Description = "取得滲漏室小時資料")]
    public dynamic GetHourInfos(string meterID, DateTime date)
    {
        return SeepageController.Instance.GetHourInfos(meterID, date, date.AddHours(23));
    }
    [WebMethod(Description = "取得滲漏室小時資料")]
    public dynamic GetHourInfosForGrid(string meterID, DateTime date)
    {
        var allInfos = GetHourInfos(meterID, date);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得滲漏室日資料")]
    public dynamic GetDayInfos(string meterID, int year, int month)
    {
        return SeepageController.Instance.GetDayInfos(meterID, year, month);
    }
    [WebMethod(Description = "取得滲漏室日資料")]
    public dynamic GetDayInfosForGrid(string meterID, int year, int month)
    {
        var allInfos = GetDayInfos(meterID, year, month);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得滲漏室月資料")]
    public dynamic GetMonthInfos(string meterID, int year)
    {
        return SeepageController.Instance.GetMonthInfos(meterID, year);
    }
    [WebMethod(Description = "取得滲漏室月資料")]
    public dynamic GetMonthInfosForGrid(string meterID, int year)
    {
        var allInfos = GetMonthInfos(meterID, year);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
}
