﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// GroundWaterReportInfo 的摘要描述
/// </summary>
public class GroundWaterReportInfo
{
    public int Year
    {
        get;
        set;
    }
    public int Month
    {
        get;
        set;
    }
    public int Day
    {
        get;
        set;
    }
    public int Hour
    {
        get;
        set;
    }
    public DateTime InfoDateTime
    {
        get;
        set;
    }
    public double Waterlevel1
    {
        get;
        set;
    }
    public double Waterlevel2
    {
        get;
        set;
    }
    public double Waterlevel3
    {
        get;
        set;
    }
    public double Waterlevel4
    {
        get;
        set;
    }
}