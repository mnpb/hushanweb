﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WarnRainQtyInfo 的摘要描述
/// </summary>
public class WarnRainQtyInfo
{
    public double Warn10MinuteRainQty
    {
        get;
        set;
    }
    public double Warn1HourRainQty
    {
        get;
        set;
    }
    public double Warn3HourRainQty
    {
        get;
        set;
    }
    public double Warn6HourRainQty
    {
        get;
        set;
    }
    public double Warn12HourRainQty
    {
        get;
        set;
    }
    public double Warn24HourRainQty
    {
        get;
        set;
    }
    public double Warn1DayRainQty
    {
        get;
        set;
    }
    public double Warn2DayRainQty
    {
        get;
        set;
    }
    public double Warn3DayRainQty
    {
        get;
        set;
    }
}