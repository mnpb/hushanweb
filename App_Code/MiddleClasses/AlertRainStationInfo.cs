﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AlertRainStationInfo 的摘要描述
/// </summary>
public class AlertRainStationInfo
{
    public string StationID
    {
        get;
        set;
    }
    public string StationTwName
    {
        get;
        set;
    }
    public WarnRainQtyInfo WarnRainQtyInfo
    {
        get;
        set;
    }
    public bool Active
    {
        get;
        set;
    }
}