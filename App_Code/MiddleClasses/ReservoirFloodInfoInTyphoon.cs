﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// ReservoirFloodInfoInTyphoon 的摘要描述
/// </summary>
public class ReservoirFloodInfoInTyphoon
{
    public DateTime InfoDateTime
    {
        get;
        set;
    }
    /// <summary>
    /// 水位
    /// </summary>
    public double WaterLevel
    {
        get;
        set;
    }
    /// <summary>
    /// 有效蓄水量
    /// </summary>
    public double Volume
    {
        get;
        set;
    }
    /// <summary>
    /// 總入流量
    /// </summary>
    public double TotalInflow
    {
        get;
        set;
    }
    /// <summary>
    /// 放水量
    /// </summary>
    public double PutWaterQuantity
    {
        get;
        set;
    }
    /// <summary>
    /// 排洪量
    /// </summary>
    public double OverflowQuantity
    {
        get;
        set;
    }
    /// <summary>
    /// 累計雨量
    /// </summary>
    public double AccRainQty
    {
        get;
        set;
    }
    /// <summary>
    /// 時雨量
    /// </summary>
    public double RainQty
    {
        get;
        set;
    }
}