﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// AlertRainStationInfo 的摘要描述
/// </summary>
public class GISPointInfo
{
    /// <summary>
    /// Sql server裡的geometry
    /// </summary>
    public string Polygon { get; set; }
    public string SectNo { get; set; }
    public string LandNo { get; set; }
    
}

public static class GISPointInfoExtention
{
    ///120.34863129,23.82135189 120.34877708,23.82138758
    public static string GetSqlGeometryPolygonPoints(string polygon)
    {
        string coordinates = polygon.
                                Replace("POLYGON ((", "").
                                Replace(")", "").
                                Replace(" ", ",").
                                Replace(",,", " ");
        return coordinates;
    }
    /// "POLYGON ((120.34863129 23.82135189, 120.34877708 23.82138758, 120.34884048 23.82140796,  120.34884827 23.82128455, 120.34864415 23.82117155, 120.34863879 23.82116861, 120.34863129 23.82135189))"
    public static List<string> ToKMLJSLatLngArray(this GISPointInfo area)
    {
        List<string> ans = new List<string>();
        string sqlGeometryString = GetSqlGeometryPolygonPoints(area.Polygon);
        //，如果有多筆可能是有地中地，第一筆是最外圈，之後內圈
        string[] polygons = sqlGeometryString.Split(new string[] { "("}, StringSplitOptions.RemoveEmptyEntries);
        foreach (var linearRing in polygons)
        {
            string[] coordinates = linearRing.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (coordinates.Length > 0)
            {
                string ring = "";
                foreach (var xy in coordinates)
                {
                    try
                    {
                        string[] xyarray = xy.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        dynamic point = new System.Dynamic.ExpandoObject();
                        point.lng = double.Parse(xyarray[0]);
                        point.lat = double.Parse(xyarray[1]);
                        ring += point.lng + "," + point.lat + ",0 ";
                    }
                    catch (Exception ex)
                    {
                    }
                }
                ans.Add(ring);
            }
        }
        return ans;
    }
    public static string ToKML(this GISPointInfo area)
    {
        string coordinates = GetSqlGeometryPolygonPoints(area.Polygon);
        string ans = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
<kml xmlns=""http://earth.google.com/kml/2.0"">
  <Document>
    <Name>{0}</Name>
    <Style id=""WraDefaultStyle"">
      <IconStyle id=""TpcIconStyle"">
        <color>a1ff00ff</color>
        <scale>0.5</scale>
      </IconStyle>
      <LableStyle id=""EpaLableStyle"">
        <color>ffff0000</color>
        <scale>1.5</scale>
      </LableStyle>
      <LineStyle id=""EpaLineStyle"">
        <color>ffff0000</color>
        <width>3</width>
      </LineStyle>
      <PolyStyle id=""EpaPolyStyle"">
        <color>7f3fe4e4</color>
        <colorMode>random</colorMode>
      </PolyStyle>
    </Style>
    <Placemark>
      <name>{1}</name>
      <styleUrl>#WraDefaultStyle</styleUrl>
        <Polygon>
          <outerBoundaryIs>
            <LinearRing>
              <coordinates>{2}
              </coordinates>
            </LinearRing>
          </outerBoundaryIs>
        </Polygon>
    </Placemark>
  </Document>
</kml>
"
            , "", "", coordinates);
        return ans;
    }
}
