﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// MonitoredServerStatusInfo 的摘要描述
/// </summary>
public class MonitoredServerStatusInfo
{
    public string ServerIP { get; set; }
    public string Name { get; set; }
    public string Location { get; set; }
    public byte CPUTemperature { get; set; }
    public byte MemoryUsage { get; set; }
    public byte HD_C { get; set; }
    public byte HD_D { get; set; }
    public short Power { get; set; }
    public short Power1 { get; set; }
    public string Net1 { get; set; }
    public string Net2 { get; set; }
    public string Net3 { get; set; }
    public string Net4 { get; set; }
    public DateTime CreateDatetime { get; set; }
    public string DatetimeStamp { get; set; }
    /// <summary>
    /// 顯示前台燈號的顏色
    /// </summary>
    public MonitoredServerLevelEnum Level { get; set; }
    public MonitoredServerLevelEnum PowerStatus { get; set; }
    public string PowerStatusText { get; set; }
}
