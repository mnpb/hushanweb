﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// StationInfo 的摘要描述
/// </summary>
public class StationInfo
{
    /// <summary>
    /// 站碼
    /// </summary>
    public string StationID
    {
        get;
        set;
    }
    /// <summary>
    /// 站名
    /// </summary>
    public string StationTwName
    {
        get;
        set;
    }
    /// <summary>
    /// 縣市
    /// </summary>
    public string County
    {
        get;
        set;
    }
    /// <summary>
    /// 行政區
    /// </summary>
    public string Town
    {
        get;
        set;
    }
    /// <summary>
    /// 地址
    /// </summary>
    public string Address
    {
        get;
        set;
    }
    /// <summary>
    /// 經度
    /// </summary>
    public decimal Longitude
    {
        get;
        set;
    }
    /// <summary>
    /// 經度(度)
    /// </summary>
    public decimal LongitudeDegree
    {
        get;
        set;
    }
    /// <summary>
    /// 經度(分)
    /// </summary>
    public decimal LongitudeMinute
    {
        get;
        set;
    }
    /// <summary>
    /// 經度(秒)
    /// </summary>
    public decimal LongitudeSecond
    {
        get;
        set;
    }
    /// <summary>
    /// 經度(xx°yy'zz")
    /// </summary>
    public string LongitudeString
    {
        get;
        set;
    }
    /// <summary>
    /// 緯度
    /// </summary>
    public decimal Latitude
    {
        get;
        set;
    }
    /// <summary>
    /// 緯度(度)
    /// </summary>
    public decimal LatitudeDegree
    {
        get;
        set;
    }
    /// <summary>
    /// 緯度(分)
    /// </summary>
    public decimal LatitudeMinute
    {
        get;
        set;
    }
    /// <summary>
    /// 緯度(秒)
    /// </summary>
    public decimal LatitudeSecond
    {
        get;
        set;
    }
    /// <summary>
    /// 緯度(xx°yy'zz")
    /// </summary>
    public string LatitudeString
    {
        get;
        set;
    }

    public double WarningLine1
    {
        get;
        set;
    }
    public double WarningLine2
    {
        get;
        set;
    }
    public double WarningLine3
    {
        get;
        set;
    }
}