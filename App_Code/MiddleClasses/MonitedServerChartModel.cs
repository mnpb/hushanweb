﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// MonitedServerChartModel 的摘要描述
/// </summary>
public class MonitedServerChartModel
{
    /// <summary>
    /// 1420  1425
    /// </summary>
    public List<string> XLabels = new List<string>();
    public List<Series> Datas { get; set; }

    public void InitXLabels(DateTime startDatetime, DateTime endDatetime)
    {
        //x軸的間距
        int intervalMin = endDatetime.Subtract(startDatetime).TotalHours > 10 ? 60 : 10;
        string sdatetime, edatetime;
        sdatetime = DatetimeUtility.MinDownTo0or10String(startDatetime);
        sdatetime = DatetimeUtility.MinDownToNumString(startDatetime, intervalMin);//調到整點00分
        edatetime = DatetimeUtility.MinDownTo0or10String(endDatetime);
        do
        {
            //只捉時和分
            string hhmm = sdatetime.Substring(sdatetime.Length - 4, 4);//.Insert(2, ":");
            XLabels.Add(hhmm);

            startDatetime = startDatetime.AddMinutes(intervalMin);
            sdatetime = DatetimeUtility.MinDownToNumString(startDatetime, intervalMin);//調到整點00分
        }
        while (edatetime.CompareTo(sdatetime) >= 0);
    }

    public class Series
    {
        public Series()
        {
            Data = new List<short>();
        }

        [JsonProperty("data")]
        public List<short> Data { get; set; }
        [JsonProperty("label")]
        public string Label { get; set; }
    }
}