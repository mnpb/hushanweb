﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// RainInfo 的摘要描述
/// </summary>
public class RainInfo
{
    public string StationID
    {
        get;
        set;
    }
    public int Year
    {
        get;
        set;
    }
    public int Month
    {
        get;
        set;
    }
    public int Day
    {
        get;
        set;
    }
    public int Hour
    {
        get;
        set;
    }
    public DateTime InfoDateTime
    {
        get;
        set;
    }
    public double RainQty
    {
        get;
        set;
    }
    public double AccRainQty
    {
        get;
        set;
    }
}