﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// ReservoirOverflowReportInfo 的摘要描述
/// </summary>
public class ReservoirOverflowReportInfo
{
    public DateTime InfoDateTime
    {
        get;
        set;
    }
    public int Year
    {
        get;
        set;
    }
    public int Month
    {
        get;
        set;
    }
    public int Day
    {
        get;
        set;
    }

    /// <summary>
    /// 雨量
    /// </summary>
    public double RainQty
    {
        get;
        set;
    }

    /// <summary>
    /// 溢流起時
    /// </summary>
    public int BeginHour
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流起分
    /// </summary>
    public int BeginMinute
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流訖時
    /// </summary>
    public int EndHour
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流訖分
    /// </summary>
    public int EndMinute
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流合計時
    /// </summary>
    public int TotalHours
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流合計分
    /// </summary>
    public int TotalMinutes
    {
        get;
        set;
    }

    /// <summary>
    /// 前一日水位
    /// </summary>
    public double PredayWaterLevel
    {
        get;
        set;
    }
    /// <summary>
    /// 當日水位
    /// </summary>
    public double TodayWaterLevel
    {
        get;
        set;
    }

    /// <summary>
    /// 溢流量(流量)
    /// </summary>
    public double Overflow
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流量(水量)
    /// </summary>
    public double OverflowQuality
    {
        get;
        set;
    }

    /// <summary>
    /// 閘門開度
    /// </summary>
    public double GateOpenDegree
    {
        get;
        set;
    }

    /// <summary>
    /// 洩洪量(流量)
    /// </summary>
    public double Dischargeflow
    {
        get;
        set;
    }
    /// <summary>
    /// 洩洪量(水量)
    /// </summary>
    public double DischargeflowQuality
    {
        get;
        set;
    }

    /// <summary>
    /// 當日最大溢流水位
    /// </summary>
    public double MaxOverflowWaterLevel
    {
        get;
        set;
    }
    /// <summary>
    /// 當日最大溢流流量
    /// </summary>
    public double MaxOverflow
    {
        get;
        set;
    }
    public string OverflowCause
    {
        get;
        set;
    }
    public string Remarks
    {
        get;
        set;
    }
}