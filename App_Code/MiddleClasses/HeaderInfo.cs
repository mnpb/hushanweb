﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// HeaderInfo 的摘要描述
/// </summary>
public class HeaderInfo
{
    public string Text
    {
        get;
        set;
    }
    public int RowSpan
    {
        get;
        set;
    }
    public int ColumnSpan
    {
        get;
        set;
    }
}