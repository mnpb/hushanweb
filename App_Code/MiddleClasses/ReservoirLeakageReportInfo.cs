﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// ReservoirLeakageReportInfo 的摘要描述
/// </summary>
public class ReservoirLeakageReportInfo
{
    /// <summary>
    /// 紀錄時間
    /// </summary>
    public DateTime InfoDateTime
    {
        get;
        set;
    }
    public int Year
    {
        get;
        set;
    }
    public int Month
    {
        get;
        set;
    }
    public int Day
    {
        get;
        set;
    }
    public int Hour
    {
        get;
        set;
    }
    /// <summary>
    /// 雨量
    /// </summary>
    public double RainQty
    {
        get;
        set;
    }
    /// <summary>
    /// 水庫水位
    /// </summary>
    public double Waterlevel
    {
        get;
        set;
    }
    /// <summary>
    /// 水庫蓄水量
    /// </summary>
    public double Volume
    {
        get;
        set;
    }
    /// <summary>
    /// 量水堰水位
    /// </summary>
    public double WeirWaterlevel
    {
        get;
        set;
    }
    /// <summary>
    /// 滲漏量
    /// </summary>
    public double LeakageQuantity
    {
        get;
        set;
    }
}