﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AlertReservoirInfo 的摘要描述
/// </summary>
public class AlertReservoirInfo
{
    public string StationID
    {
        get;
        set;
    }
    public string StationTwName
    {
        get;
        set;
    }
    public double WarningLine
    {
        get;
        set;
    }
    public bool Active
    {
        get;
        set;
    }
}