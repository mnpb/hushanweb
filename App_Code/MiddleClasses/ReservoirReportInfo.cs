﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// ReservoirReportInfo 的摘要描述
/// </summary>
public class ReservoirReportInfo
{
    /// <summary>
    /// 紀錄時間
    /// </summary>
    public DateTime InfoDateTime
    {
        get;
        set;
    }
    public int Year
    {
        get;
        set;
    }
    public int Month
    {
        get;
        set;
    }
    public string Tenday
    {
        get;
        set;
    }
    public int Day
    {
        get;
        set;
    }
    public int Hour
    {
        get;
        set;
    }
    
    /// <summary>
    /// 水位
    /// </summary>
    public double WaterLevel
    {
        get;
        set;
    }
    /// <summary>
    /// 有效蓄水量
    /// </summary>
    public double Volume
    {
        get;
        set;
    }
    /// <summary>
    /// 放水量
    /// </summary>
    public double PutWaterQuality
    {
        get;
        set;
    }

    /// <summary>
    /// 雨量
    /// </summary>
    public double RainQty
    {
        get;
        set;
    }
    /// <summary>
    /// 累計雨量
    /// </summary>
    public double AccRainQty
    {
        get;
        set;
    }

    /// <summary>
    /// 溢流量(流量)
    /// </summary>
    public double Overflow
    {
        get;
        set;
    }
    /// <summary>
    /// 溢流量(水量)
    /// </summary>
    public double OverflowQuality
    {
        get;
        set;
    }

    /// <summary>
    /// 洩洪量(流量)
    /// </summary>
    public double Dischargeflow
    {
        get;
        set;
    }
    /// <summary>
    /// 洩洪量(水量)
    /// </summary>
    public double DischargeflowQuality
    {
        get;
        set;
    }

    /// <summary>
    /// 滲漏量
    /// </summary>
    public double LeakageQuality
    {
        get;
        set;
    }
}