﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// DamChartData 的摘要描述
/// </summary>
public class DamChartData : Dictionary<DateTime, double?>
{
    public string AxisTitle { get; set; }
}