﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// MonitoredServerLevelEnum 的摘要描述
/// </summary>
public enum MonitoredServerLevelEnum : short
{
    None = 0,
    Normal = 1,
    Warning = 2,
    Danger = 3,
}