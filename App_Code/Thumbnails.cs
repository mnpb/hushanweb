﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Thumbnails 的摘要描述
/// </summary>
public class Thumbnails
{
    public static Bitmap thumbnails(string srcFilename, int width, int height)
    {
        MemoryStream ms = new MemoryStream();
        if (File.Exists(srcFilename))
            throw new Exception("欲壓縮的圖檔不存在");


        byte[] srcBytes = System.IO.File.ReadAllBytes(srcFilename);
        ms.Write(srcBytes, 0, srcBytes.Length);

        Bitmap srcMap = new Bitmap(ms);
        Bitmap desMap = new Bitmap(srcMap, width, height);

        return desMap;
    }


    public static Bitmap thumbnails(Stream stream)
    {
        const int WIDTH = 800;
        const int HEIGHT = 600;

        Bitmap srcMap = new Bitmap(stream);

        int h = srcMap.Height;
        int w = srcMap.Width;

        int ss, os;// source side and objective side
        double temp1, temp2;
        //compute the picture's proportion
        temp1 = (h * 1.0D) / HEIGHT;
        temp2 = (w * 1.0D) / WIDTH;
        if (temp1 < temp2)
        {
            ss = w;
            os = WIDTH;
        }
        else
        {
            ss = h;
            os = HEIGHT;
        }

        double per = (os * 1.0D) / ss;
        if (per < 1.0D)
        {
            h = (int)(h * per);
            w = (int)(w * per);
        }

        Bitmap desMap = new Bitmap(srcMap, w, h);

        return desMap;
    }


    public static Bitmap thumbnails(byte[] bytes, int width, int height)
    {
        int WIDTH = width;
        int HEIGHT = height;
        MemoryStream ms = new MemoryStream(bytes);
        Bitmap srcMap = new Bitmap(ms);

        int h = srcMap.Height;
        int w = srcMap.Width;

        int ss, os;// source side and objective side
        double temp1, temp2;
        //compute the picture's proportion
        temp1 = (h * 1.0D) / HEIGHT;
        temp2 = (w * 1.0D) / WIDTH;
        if (temp1 < temp2)
        {
            ss = w;
            os = WIDTH;
        }
        else
        {
            ss = h;
            os = HEIGHT;
        }

        double per = (os * 1.0D) / ss;
        if (per < 1.0D)
        {
            h = (int)(h * per);
            w = (int)(w * per);
        }

        Bitmap desMap = new Bitmap(srcMap, w, h);

        return desMap;
    }
}