﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// EvaporationService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class EvaporationService : System.Web.Services.WebService
{

    public EvaporationService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod(Description = "取得所有蒸發皿測站")]
    public dynamic GetAllStations()
    {
        return EvaporationController.Instance.GetAllStations();
    }

    private dynamic GetHourInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        var hourInfos = EvaporationController.Instance.GetHourInfos(stationID, beginTime, endTime);

        List<EvaporationQtyInfo> allInfos = new List<EvaporationQtyInfo>();
        double accEvapQty = 0;
        for (DateTime dateTime = beginTime; dateTime.CompareTo(endTime) <= 0; dateTime = dateTime.AddHours(1))
        {
            var theInfo = (from item in hourInfos
                           where item.InfoTime == dateTime
                           select item).FirstOrDefault();
            double evapQty = (theInfo != null ? Convert.ToDouble(Math.Round(theInfo.EvapValue, 1)) : 0);
            accEvapQty += IISI.Util.NumericUtility.ToPositive(evapQty);
            allInfos.Add(new EvaporationQtyInfo
            {
                StationID = stationID,
                InfoTime = dateTime,
                EvaporationQty = evapQty,
                AccEvaporationQty = Math.Round(accEvapQty, 1),
            });
        }
        return allInfos;
    }

    [WebMethod(Description = "取得蒸發量小時資料")]
    public dynamic GetHourInfos(string stationID, DateTime date)
    {
        DateTime beginTime = date.AddHours(1);
        DateTime endTime = beginTime.AddHours(23);
        return GetHourInfos(stationID, beginTime, endTime);
    }

    [WebMethod(Description = "取得蒸發量小時資料")]
    public dynamic GetHourInfosForGrid(string stationID, DateTime date)
    {
        var allInfos = GetHourInfos(stationID, date);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得蒸發量日資料")]
    public dynamic GetDayInfos(string stationID, int year, int month)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        DateTime beginTime = beginDate.AddHours(1);
        DateTime endTime = endDate.AddDays(1);
        var allHourInfos = EvaporationController.Instance.GetHourInfos(stationID, beginTime, endTime);

        List<EvaporationQtyInfo> allInfos = new List<EvaporationQtyInfo>();
        double accEvapQty = 0;
        for (DateTime theBeginDate = beginDate; theBeginDate.CompareTo(endDate) <= 0; theBeginDate = theBeginDate.AddDays(1))
        {
            DateTime theBeginTime = theBeginDate.AddHours(1);
            DateTime theEndTime = theBeginTime.AddHours(23);
            double evapQty = Convert.ToDouble(Math.Round((from info in allHourInfos
                                                          where info.InfoTime >= theBeginTime && info.InfoTime <= theEndTime
                                                          select info.EvapValue).Sum(), 1));
            accEvapQty += evapQty;
            allInfos.Add(new EvaporationQtyInfo
            {
                StationID = stationID,
                InfoTime = theBeginDate,
                EvaporationQty = evapQty,
                AccEvaporationQty = Math.Round(accEvapQty, 1),
            });
        }
        return allInfos;
    }

    [WebMethod(Description = "取得蒸發量日資料")]
    public dynamic GetDayInfosForGrid(string stationID, int year, int month)
    {
        var allInfos = GetDayInfos(stationID, year, month);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得蒸發量月資料")]
    public dynamic GetMonthInfos(string stationID, int year)
    {
        DateTime beginDate = new DateTime(year, 1, 1);
        DateTime endDate = new DateTime(year, 12, 31);
        DateTime beginTime = beginDate.AddHours(1);
        DateTime endTime = endDate.AddDays(1);
        var allHourInfos = EvaporationController.Instance.GetHourInfos(stationID, beginTime, endTime);

        List<EvaporationQtyInfo> allInfos = new List<EvaporationQtyInfo>();
        double accEvapQty = 0;
        for (DateTime theBeginDate = beginDate; theBeginDate.CompareTo(endDate) <= 0; theBeginDate = theBeginDate.AddMonths(1))
        {
            DateTime theBeginTime = theBeginDate.AddHours(1);
            DateTime theEndTime = theBeginTime.AddMonths(1).AddHours(-1);
            double evapQty = Convert.ToDouble(Math.Round((from info in allHourInfos
                                                          where info.InfoTime >= theBeginTime && info.InfoTime <= theEndTime
                                                          select info.EvapValue).Sum(), 1));
            accEvapQty += evapQty;
            allInfos.Add(new EvaporationQtyInfo
            {
                StationID = stationID,
                InfoTime = theBeginDate,
                EvaporationQty = evapQty,
                AccEvaporationQty = Math.Round(accEvapQty, 1),
            });
        }
        return allInfos;
    }

    [WebMethod(Description = "取得蒸發量月資料")]
    public dynamic GetMonthInfosForGrid(string stationID, int year)
    {
        var allInfos = GetMonthInfos(stationID, year);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    public class EvaporationQtyInfo
    {
        public string StationID
        {
            get;
            set;
        }
        public DateTime InfoTime
        {
            get;
            set;
        }
        public double EvaporationQty
        {
            get;
            set;
        }
        public double AccEvaporationQty
        {
            get;
            set;
        }
    }
}
