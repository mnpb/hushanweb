﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// MessageService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class MessageService : System.Web.Services.WebService
{

    public MessageService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public dynamic GetSmsLogs(string sendTypeID, DateTime beginDate, DateTime endDate)
    {
        List<SmsLog> logs =
            MessageController.Instance.GetSmsLogs(sendTypeID, beginDate, endDate.AddDays(1).AddMilliseconds(-100));
        var result = (from info in logs
                      select new
                      {
                          Sender = info.Sender,
                          SendTime = info.SendTime,
                          ReceiverName = info.ReceiverName,
                          ReceiverPhone = info.ReceiverPhone,
                          Message = info.Message,
                      }).ToList();
        try
        {
            return new
            {
                total = 1,
                page = 1,
                records = result.Count,
                rows = result,
            };
        }
        catch
        {
            return null;
        }
    }

}
