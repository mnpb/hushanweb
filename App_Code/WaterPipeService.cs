﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// WaterPipeService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
 [System.Web.Script.Services.ScriptService]
public class WaterPipeService : System.Web.Services.WebService {

    public WaterPipeService () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod(Description = "取得所有原水管站")]
    public dynamic GetAllStations()
    {
        return WaterPipeController.Instance.GetAllStations();
    }

    [WebMethod(Description = "取得原水管小時資料")]
    public dynamic GetHourInfos(string stationID, DateTime date)
    {
        return WaterPipeController.Instance.GetHourInfos(stationID, date);
    }
    [WebMethod(Description = "取得原水管小時資料")]
    public dynamic GetHourInfosForGrid(string stationID, DateTime date)
    {
        var allInfos = GetHourInfos(stationID, date);
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得原水管日資料")]
    public dynamic GetDayInfos(string stationID, int year, int month)
    {
        return WaterPipeController.Instance.GetDayInfos(stationID, year, month);
    }
    [WebMethod(Description = "取得原水管日資料")]
    public dynamic GetDayInfosForGrid(string stationID, int year, int month)
    {
        var allInfos = GetDayInfos(stationID, year, month);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(Description = "取得水位站月資料")]
    public dynamic GetMonthInfos(string stationID, short year)
    {
        return WaterPipeController.Instance.GetMonthInfos(stationID, year);
    }
    [WebMethod(Description = "取得水位站月資料")]
    public dynamic GetMonthInfosForGrid(string stationID, short year)
    {
        var allInfos = GetMonthInfos(stationID, year);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
}
