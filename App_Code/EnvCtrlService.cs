﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// EnvCtrlService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class EnvCtrlService : System.Web.Services.WebService
{

    public EnvCtrlService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetAllEnvCtrlTypes()
    {
        return JsonConvert.SerializeObject(EnvCtrlController.Instance.GetAllEnvCtrlTypes());
    }

    [WebMethod]
    public string GetAllAnalogRealtimeInfos()
    {
        return EnvCtrlController.Instance.GetAllAnalogRealtimeInfos();
    }

    [WebMethod]
    public string GetAllDiscreteRealtimeInfos()
    {
        return EnvCtrlController.Instance.GetAllDiscreteRealtimeInfos();
    }

    [WebMethod]
    public double GetWarningValue(string tagID, string fieldName)
    {
        return EnvCtrlController.Instance.GetWarningValue(tagID, fieldName);
    }

    [WebMethod]
    public List<TemperatureHumidityInfo> GetTemperatureHumidityLatest24HourInfos(string area)
    {
        return EnvCtrlController.Instance.GetTemperatureHumidityLatest24HourInfos(area);
    }

    [WebMethod]
    public object GetSupplierEventInfos(string typeId, DateTime beginDate, DateTime endDate, string searchOper, string searchString)
    {
        List<EnvCtrlMessage> oriInfos = 
            EnvCtrlController.Instance.GetEvents(typeId, "2", beginDate, endDate.AddDays(1).AddMilliseconds(-100));
        List<EnvCtrlMessage> fileterInfos = oriInfos;
        if (!string.IsNullOrEmpty(searchString))
        {
            if (searchOper == "eq")
            { // 等於
                fileterInfos = (from item in oriInfos
                                where item.Statement == searchString
                                select item).ToList();
            }
            else if (searchOper == "cn")
            { // 包含
                fileterInfos = (from item in oriInfos
                                where item.Statement.Contains(searchString)
                                select item).ToList();
            }
        }

        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = fileterInfos.Count,
                rows = fileterInfos.ToList(),
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
}
