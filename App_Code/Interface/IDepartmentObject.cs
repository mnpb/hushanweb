﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public interface IDepartmentObject
{
    void DepartmentSelected(IAccountObject AccountObject, int AccountID, bool IsSelected);
}