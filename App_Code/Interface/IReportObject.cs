﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public interface IReportObject
{
    void ObjectDataBind(DateTime StartDateTime, DateTime EndDateTime);

    void ExportToExcel(DateTime StartDateTime, DateTime EndDateTime);

    void BatchExportToExcel(DateTime StartDateTime, DateTime EndDateTime);
}