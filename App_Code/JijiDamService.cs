﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;

/// <summary>
/// JijiService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class JijiDamService : System.Web.Services.WebService
{
    static int CommonDigit = 2;
    static int RainQtyDigit = 1;
    string JijiDamReportFieldInfos = "InfoTime;Waterlevel;Volume;TotalInFlow;SpillwayFlow;SandsluicewayFlow;FishwayFlow;TotalOutFlow;NorthFlumeFlow;NorthOutFlow;SouthFlumeFlow;SouthOutFlow;WaterPowerSupplyFlow;IrrChanghuaSupplyFlow;IrrYunlinSupplyFlow;PublicSupplyFlow;IndustrySupplyFlow;DouliouInFlow;UpstreamNaturalFlow;TaipowerOutFlow;JijiRainQty;ChuoshuiRiverRainQty;NanyuenFlow;ChinshuiRainQty;ChingshuiRiverRainQty";

    public JijiDamService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    #region 日報

    [WebMethod]
    public object GetJijiDamHourInfos(DateTime date)
    {
        return JijiDamController.Instance.GetJijiDamHourInfos(date);
    }
    [WebMethod]
    public object GetJijiDamDayReportInfos(DateTime date)
    {
        var hourInfos = JijiDamController.Instance.GetJijiDamHourInfos(date);
        var allInfos = ConvertToJijiDamReportInfo(hourInfos);
        allInfos.Add(GetJijiDamReportAverageInfo(hourInfos));
        allInfos.Add(GetJijiDamReportSumInfo(hourInfos));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    private List<object> ConvertToJijiDamReportInfo(List<JijiHour> hourInfos)
    {
        List<object> reportInfos = new List<object>();
        foreach (var hourInfo in hourInfos)
        { // 每小時資料轉成報表格式
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piSource = hourInfo.GetType().GetProperty(fieldName);
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                object objValue = piSource.GetValue(hourInfo);
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    DateTime dateTime = Convert.ToDateTime(objValue);
                    value = string.Format("{0}~{1}", dateTime.Hour, dateTime.Hour + 1);
                }
                else
                {
                    double hydValue = NumericUtility.ToHydValue(objValue);
                    if (fieldName.Contains("RainQty"))
                    {
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        value = NumericUtility.ToFormat(hydValue, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            reportInfos.Add(reportInfo);
        }
        return reportInfos;
    }
    private object GetJijiDamReportAverageInfo(List<JijiHour> hourInfos)
    {
        try
        {
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    value = "平均";
                }
                else if (fieldName.Equals("Waterlevel") || fieldName.Equals("Volume"))
                {
                    value = "-";
                }
                else
                {
                    double hydValue = hourInfos.Average(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                    if (fieldName.Contains("RainQty"))
                    {
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        value = NumericUtility.ToFormat(hydValue, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            return reportInfo;
        }
        catch
        {
            return new object();
        }
    }
    private object GetJijiDamReportSumInfo(List<JijiHour> hourInfos)
    {
        try
        {
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    value = "總計(萬噸)";
                }
                else if (fieldName.Equals("Waterlevel") || fieldName.Equals("Volume"))
                {
                    value = "-";
                }
                else
                {
                    if (fieldName.Contains("RainQty"))
                    {
                        double hydValue = hourInfos.Sum(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        double hydValue = hourInfos.Average(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                        value = NumericUtility.ToFormat(hydValue * 8.64, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            return reportInfo;
        }
        catch
        {
            return new object();
        }
    }

    #endregion

    #region 月報

    [WebMethod]
    public object GetJijiDamDayInfos(int year, int month)
    {
        return JijiDamController.Instance.GetJijiDamDayInfos(year, month);
    }
    [WebMethod]
    public object GetJijiDamMonthReportInfos(int year, int month)
    {
        var dayInfos = JijiDamController.Instance.GetJijiDamDayInfos(year, month);
        var allInfos = ConvertToJijiDamReportDayInfo(dayInfos);
        allInfos.Add(GetJijiDamReportDayAverageInfo(dayInfos));
        allInfos.Add(GetJijiDamReportDaySumInfo(dayInfos));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    private List<object> ConvertToJijiDamReportDayInfo(List<JijiHour> hourInfos)
    {
        List<object> reportInfos = new List<object>();
        foreach (var hourInfo in hourInfos)
        { // 每日資料轉成報表格式
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piSource = hourInfo.GetType().GetProperty(fieldName);
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                object objValue = piSource.GetValue(hourInfo);
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    DateTime dateTime = Convert.ToDateTime(objValue);
                    value = string.Format("{0}", dateTime.Day);
                }
                else
                {
                    double hydValue = NumericUtility.ToHydValue(objValue);
                    if (fieldName.Contains("RainQty"))
                    {
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        value = NumericUtility.ToFormat(hydValue, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            reportInfos.Add(reportInfo);
        }
        return reportInfos;
    }
    private object GetJijiDamReportDayAverageInfo(List<JijiHour> hourInfos)
    {
        try
        {
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    value = "平均";
                }
                else if (fieldName.Equals("Waterlevel") || fieldName.Equals("Volume"))
                {
                    value = "-";
                }
                else
                {
                    double hydValue = hourInfos.Average(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                    if (fieldName.Contains("RainQty"))
                    {
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        value = NumericUtility.ToFormat(hydValue, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            return reportInfo;
        }
        catch
        {
            return new object();
        }
    }
    private object GetJijiDamReportDaySumInfo(List<JijiHour> hourInfos)
    {
        try
        {
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    value = "總計(萬噸)";
                }
                else if (fieldName.Equals("Waterlevel") || fieldName.Equals("Volume"))
                {
                    value = "-";
                }
                else
                {
                    if (fieldName.Contains("RainQty"))
                    {
                        double hydValue = hourInfos.Sum(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        double hydValue = hourInfos.Average(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                        value = NumericUtility.ToFormat(hydValue * 8.64, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            return reportInfo;
        }
        catch
        {
            return new object();
        }
    }

    #endregion

    #region 年報

        [WebMethod]
    public object GetJijiDamMonthInfos(int year)
    {
        return JijiDamController.Instance.GetJijiDamMonthInfos(year);
    }
    [WebMethod]
    public object GetJijiDamYearReportInfos(int year)
    {
        var monthInfos = JijiDamController.Instance.GetJijiDamMonthInfos(year);
        var allInfos = ConvertToJijiDamReportMonthInfo(monthInfos);
        allInfos.Add(GetJijiDamReportMonthAverageInfo(monthInfos));
        allInfos.Add(GetJijiDamReportMonthSumInfo(monthInfos));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    private List<object> ConvertToJijiDamReportMonthInfo(List<JijiHour> hourInfos)
    {
        List<object> reportInfos = new List<object>();
        foreach (var hourInfo in hourInfos)
        { // 每月資料轉成報表格式
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piSource = hourInfo.GetType().GetProperty(fieldName);
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                object objValue = piSource.GetValue(hourInfo);
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    DateTime dateTime = Convert.ToDateTime(objValue);
                    value = string.Format("{0}月{1}", dateTime.Month, IISI.Util.DateUtility.Get10DayDesc(dateTime.Day / 10 + 1));
                }
                else
                {
                    double hydValue = NumericUtility.ToHydValue(objValue);
                    if (fieldName.Contains("RainQty"))
                    {
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        value = NumericUtility.ToFormat(hydValue, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            reportInfos.Add(reportInfo);
        }
        return reportInfos;

        //List<object> reportInfos = new List<object>();
        //foreach (var hourInfo in hourInfos)
        //{ // 每月資料轉成報表格式
        //    reportInfos.Add(new
        //    {
        //        TimeDescription = string.Format("{0}月{1}", hourInfo.InfoTime.Month,
        //            IISI.Util.DateUtility.Get10DayDesc(hourInfo.InfoTime.Day / 10 + 1)),
        //        Waterlevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Waterlevel), 2),
        //        Volume = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Volume), 2),
        //        VolumeRate = "98.73",
        //        TotalInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalInFlow), 2),
        //        SpillwayFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.SpillwayFlow), 2),
        //        OutletWork1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork1Flow), 2),
        //        OutletWork2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork2Flow), 2),
        //        EcologyFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.EcologyFlow), 2),
        //        TotalOutFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalOutFlow), 2),
        //        OriginalPipe1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe1Flow), 2),
        //        OriginalPipe2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe2Flow), 2),
        //        PublicWaterSupply = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.PublicWaterSupply), 2),
        //        HushanRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.HushanRainQty), 1),
        //        TongtouWaterIntakeLevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouWaterIntakeLevel), 2),
        //        TongtouCrossAreaInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouCrossAreaInFlow), 2),
        //        TongtouRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouRainQty), 1),
        //    });
        //}

        //return reportInfos;
    }
    private object GetJijiDamReportMonthAverageInfo(List<JijiHour> hourInfos)
    {
        try
        {
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    value = "平均";
                }
                else if (fieldName.Equals("Waterlevel") || fieldName.Equals("Volume"))
                {
                    value = "-";
                }
                else
                {
                    double hydValue = hourInfos.Average(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                    if (fieldName.Contains("RainQty"))
                    {
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        value = NumericUtility.ToFormat(hydValue, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            return reportInfo;
        }
        catch
        {
            return new object();
        }

        //try
        //{
        //    return new
        //    {
        //        TimeDescription = "平均",
        //        Waterlevel = "-",
        //        Volume = "-",
        //        TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)), 2),
        //        SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)), 2),
        //        OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)), 2),
        //        OutletWork2Flow = "-",
        //        EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)), 2),
        //        TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)), 2),
        //        OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)), 2),
        //        OriginalPipe2Flow = "-",
        //        PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)), 2),
        //        HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.HushanRainQty)), 1),
        //        TongtouWaterIntakeLevel = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouWaterIntakeLevel)), 2),
        //        TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)), 2),
        //        TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouRainQty)), 1),
        //    };
        //}
        //catch
        //{
        //    return new object();
        //}
    }
    private object GetJijiDamReportMonthSumInfo(List<JijiHour> hourInfos)
    {
        try
        {
            JijiReport reportInfo = new JijiReport();
            foreach (var fieldInfo in JijiDamReportFieldInfos.Split(';'))
            {
                string fieldName = fieldInfo.Split(';')[0];
                PropertyInfo piDest = reportInfo.GetType().GetProperty(fieldName);
                string value = string.Empty;
                if (fieldName.Equals("InfoTime"))
                {
                    piDest = reportInfo.GetType().GetProperty("TimeDescription");
                    value = "總計(萬噸)";
                }
                else if (fieldName.Equals("Waterlevel") || fieldName.Equals("Volume"))
                {
                    value = "-";
                }
                else
                {
                    if (fieldName.Contains("RainQty"))
                    {
                        double hydValue = hourInfos.Sum(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                        value = NumericUtility.ToFormat(hydValue, RainQtyDigit);
                    }
                    else
                    {
                        double hydValue = hourInfos.Average(x => Convert.ToDouble(x.GetType().GetProperty(fieldName).GetValue(x, null)));
                        value = NumericUtility.ToFormat(hydValue * 8.64, CommonDigit);
                    }
                }
                piDest.SetValue(reportInfo, value);
            }
            return reportInfo;
        }
        catch
        {
            return new object();
        }
        //try
        //{
        //    return new
        //    {
        //        TimeDescription = "總計(萬噸)",
        //        Waterlevel = "-",
        //        Volume = "-",
        //        TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)) * 8.64, 2),
        //        SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)) * 8.64, 2),
        //        OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)) * 8.64, 2),
        //        OutletWork2Flow = "-",
        //        EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)) * 8.64, 2),
        //        TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)) * 8.64, 2),
        //        OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)) * 8.64, 2),
        //        OriginalPipe2Flow = "-",
        //        PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)) * 8.64, 2),
        //        HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.HushanRainQty)), 1),
        //        TongtouWaterIntakeLevel = "-",
        //        TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)) * 8.64, 2),
        //        TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.TongtouRainQty)), 1),
        //    };
        //}
        //catch
        //{
        //    return new object();
        //}
    }
    #endregion
}
