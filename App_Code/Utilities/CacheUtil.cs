﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

/// <summary>
/// CacheUtil 的摘要描述
/// </summary>
public static class CacheUtil
{
    public static T GetFromCache<T>(string key, string identity, Func<T> func, int minute = 1)
    {
        var k = GetCacheKey(key, identity);
        return GetCacheValue(k, func, minute);
    }

    public static T GetCacheValue<T>(string key, Func<T> func, int minute)
    {
        var obj = HttpContext.Current.Cache.Get(key);
        //var obj = (T)HttpContext.Current.Cache[key];

        if (obj != null)
        {
            return (T)obj;
        }

        if (func != null)
        {
            obj = func();
            if (obj != null)
                HttpContext.Current.Cache.Insert(key, obj, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1));
            //HttpContext.Current.Cache[key] = obj;
        }

        return (T)obj;
    }

    private static string GetCacheKey(string key, string identity)
    {
        return
            string.Format("{0}_{1}", key, identity)
                .Replace(@":", "")
                .Replace("{", "")
                .Replace("}", "")
                .Replace(",", "")
                .Replace(@"""", "");
    }


}