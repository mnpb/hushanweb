﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ControlUtility 的摘要描述
/// </summary>
public class ControlUtility
{
	public ControlUtility()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}

    public static void FillData(dynamic control, dynamic dataSource, string dataTextField, string dataValueField)
    {
        try
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
        }
        catch
        {
        }
    }

    public static void EmptyAllControls(ControlCollection controlCollection)
    {
        foreach (Control c in controlCollection)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                if (drp.Items.Count > 0)
                {
                    drp.SelectedIndex = 0;
                }
                drp.Enabled = true;
            }
            else if (c is CheckBox)
            {
                CheckBox cb = c as CheckBox;
                cb.Checked = false;
            }
        }
    }
}