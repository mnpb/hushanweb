﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WebUtil 的摘要描述
/// </summary>
public class WebUtil
{
    public static string GetDomain
    {
        get
        {
            HttpRequest request = HttpContext.Current.Request;
            string domainName = "";
            if (request != null)
            {
                domainName = request.Url.GetLeftPart(UriPartial.Authority) + request.ApplicationPath;
            }
            return domainName;
        }
    }
}