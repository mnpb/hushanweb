﻿using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using Steema.TeeChart.Drawing;

/// <summary>
/// DamChartManager 的摘要描述
/// </summary>
public class DamChartManager
{
    public static int UpPercent = 30;
    public DamChartManager()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }
    public Chart InitChart(WebChart webchart)
    {
        Chart chart = webchart.Chart;
        chart.Series.Clear();
        chart.Axes.Custom.Clear();
        chart.Chart.Aspect.View3D = false;
        chart.Series.RemoveAllSeries();
        chart.Legend.Font = new Steema.TeeChart.Drawing.ChartFont() { Size = 12 };
        chart.Legend.Alignment = LegendAlignments.Bottom;
        chart.Legend.FontSeriesColor = true;//legend的文字顏色

        chart.Header.Font = new Steema.TeeChart.Drawing.ChartFont() { Size = 14 };
        return chart;
    }

    public void InitChartBottom(Chart chart, DateTime sdate, DateTime edate)
    {
        chart.Axes.Bottom.Labels.DateTimeFormat = "M/dd";
        chart.Axes.Bottom.Labels.Angle = 60;
        chart.Axes.Bottom.SetMinMax(sdate, edate);
        chart.Axes.Bottom.Increment = Utils.DateTimeStep[(int)DateTimeSteps.OneDay];
    }

    public void DrawRainUp(Chart chart, Dictionary<DateTime, double?> rainday)
    {
        if (rainday == null || rainday.Count == 0)
        {
            UpPercent = 0;
            return;
        }
        else
        {
            UpPercent = 30;
        }
        Bar bar = GetRainBar(rainday, "雨量");
        chart.Series.Add(bar);

        Axis axis = new Axis();
        axis.Title.Text = "雨量(mm)";
        axis.Title.Angle = 90;
        chart.Axes.Custom.Add(axis);

        // 左邊刻度Y軸
        chart.Axes.Left.Inverted = true;
        chart.Axes.Left.StartPosition = 0;
        chart.Axes.Left.EndPosition = UpPercent;
        chart.Axes.Left.Title.Text = axis.Title.Text;
    }
    public void DrawReservoirR(Chart chart, Dictionary<DateTime, double?> reservoir)
    {
        if (reservoir == null || reservoir.Count == 0)
        {
            return;
        }
        Line line = GetLine(reservoir, "水庫水位(m)", Color.Aqua);
        line.CustomVertAxis = chart.Axes.Right;
        line.CustomVertAxis.Grid.Visible = false;
        line.CustomVertAxis.StartPosition = UpPercent + 1;
        line.CustomVertAxis.EndPosition = 100;
        line.CustomVertAxis.Title.Angle = 90;
        line.CustomVertAxis.Title.Text = "水庫水位(m)";
        chart.Series.Add(line);
    }

    /// <summary>
    /// 產生雨量柱狀圖
    /// </summary>
    /// <param name="allInfos"></param>
    /// <returns></returns>
    public Bar GetRainBar(Dictionary<DateTime, double?> data, string title)
    {
        Bar bar = new Bar();
        bar.BarWidthPercent = 10;
        bar.Color = Color.Blue;
        bar.Marks.Visible = false;
        foreach (var kv in data)
        {
            bar.Add(kv.Key, kv.Value ?? 0);
        }
        bar.Title = title;
        return bar;
    }

    public Line GetLine(Dictionary<DateTime, double?> data, string title, Color color, PointerStyles pointer = PointerStyles.Triangle)
    {
        Line line = new Line();
        line.LinePen.Width = 4;
        line.Pointer.Visible = true;
        line.Pointer.Style = pointer;
        line.Color = color;
        foreach (var kv in data)
        {
            line.Add(kv.Key, kv.Value ?? 0);
        }
        line.Title = title;
        return line;
    }
    public Points GetPoints(Dictionary<DateTime, double?> data, string title, Color color, PointerStyles pointer = PointerStyles.Triangle)
    {
        Points Style = new Points();
        Style.LinePen.Width = 4;
        Style.Color = color;
        Style.Pointer.Visible = true;
        Style.Pointer.Style = pointer;
        foreach (var kv in data)
        {
            if (kv.Value != null)
                Style.Add(kv.Key, kv.Value);
        }
        Style.Title = title;
        return Style;
    }

}