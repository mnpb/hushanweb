﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// IconUtility 的摘要描述
/// </summary>
public class IconUtility
{
    #region Properties

    /// <summary>
    /// 綠燈
    /// </summary>
    public static string GreenStatusIcon = "~/images/Icons/green.ico";

    /// <summary>
    /// 紅燈
    /// </summary>
    static string RedStatusIcon = "~/Images/Icons/red.ico";

    #endregion

    #region Methods

    public IconUtility()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }

    /// <summary>
    /// 取得燈號Icon
    /// </summary>
    /// <param name="status">0/1</param>
    /// <returns></returns>
    public static string GetLightIcon(bool status)
    {
        switch (status)
        {
            case true:
                return GreenStatusIcon;
            case false:
            default:
                return RedStatusIcon;
        }
    }

    public static string GetActiveDescription(bool active)
    {
        if (active)
        {
            return "啟用";
        }
        return "停用";
    }

    #endregion
}