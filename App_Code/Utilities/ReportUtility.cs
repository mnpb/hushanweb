﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ReportUtility 的摘要描述
/// </summary>
public class ReportUtility
{
    public static void ExportFile(Page page, GridView gridView)
    {
        try
        {
            string fileName = page.Header.Title + ".xls";
            page.Response.ClearContent();
            page.Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>");
            page.Response.AddHeader("content-disposition", "attachment;filename=" + page.Server.UrlEncode(fileName));
            page.Response.ContentType = "application/excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter HtmlWrite = new HtmlTextWriter(sw);

            gridView.RenderControl(HtmlWrite);

            page.Response.Write(sw.ToString());
            //page.Response.Write(
            //    string.Format("<table><tr><td style='text-align:center;font-weight:bold' colspan={0}>{1}</td></tr></table>{2}",
            //    gridView.Columns.Count, page.Header.Title, sw.ToString()));
            page.Response.End();
        }
        catch (Exception)
        {
        }
    }
}