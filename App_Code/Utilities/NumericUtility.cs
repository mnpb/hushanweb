﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// NumericUtility 的摘要描述
/// </summary>
public class NumericUtility
{
    public static double NoHydData = IISI.Util.NumericUtility.NoHydData;

    public static decimal ToDecimal(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return -998;
        }
        decimal result;
        if (decimal.TryParse(value.ToString(), out result))
        {
            return result;
        }
        return -998;
    }

    public static decimal ToWarningLine(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return 9999;
        }
        decimal result;
        if (decimal.TryParse(value.ToString(), out result))
        {
            return result;
        }
        return 9999;
    }

    public static double ToDouble(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return -998;
        }
        double result;
        if (double.TryParse(value.ToString(), out result))
        {
            return result;
        }
        return -998;
    }

    public static string ToFormat(double value, int digit)
    {
        return IISI.Util.NumericUtility.ToFormat(value, digit);
    }
    public static double ToHydValue(object valueObject)
    {
        return IISI.Util.NumericUtility.ToHydValue(valueObject);
    }
}