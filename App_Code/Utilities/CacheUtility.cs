﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CacheUtility 的摘要描述
/// </summary>
public class CacheUtility
{
    #region Methods

    public static void AddToCache(string cacheKey, object value, DateTime absoluteExpiration)
    {
        RemoveFromCache(cacheKey);
        try
        {
            HttpContext.Current.Cache.Insert(
                cacheKey,
                value,
                null,
                absoluteExpiration,
                System.Web.Caching.Cache.NoSlidingExpiration);
        }
        catch
        {
        }
    }

    private static void RemoveFromCache(string cacheKey)
    {
        HttpContext.Current.Cache.Remove(cacheKey);
    }

    public static object GetFromCache(string cacheKey)
    {
        if (HttpContext.Current.Cache[cacheKey] != null)
        {
            return HttpContext.Current.Cache[cacheKey];
        }
        return null;
    }

    #endregion
}