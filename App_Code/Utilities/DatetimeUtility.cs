﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// NumericUtility 的摘要描述
/// </summary>
public class DatetimeUtility
{
    public static string MinDownToNumString(DateTime value,int min)
    {
        string ans = value.ToString("yyyyMMddHH") + (value.Minute - (value.Minute % min)).ToString("00");
        return ans;
    }
    public static DateTime MinDownToNum(DateTime value, int min)
    {
        string ans = value.ToString("yyyy/MM/dd HH:") + (value.Minute - (value.Minute % min)).ToString("00");
        return DateTime.Parse(ans);
    }

    public static string MinDownTo0or10String(DateTime value)
    {
        string ans = value.ToString("yyyyMMddHH") + (value.Minute - (value.Minute % 10)).ToString("00");
        return ans;
    }
    public static DateTime MinDownTo0or10(DateTime value)
    {
        string ans = value.ToString("yyyy/MM/dd HH:") + (value.Minute - (value.Minute % 10)).ToString("00");
        return DateTime.Parse(ans);
    }

}