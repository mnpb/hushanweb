﻿using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using Steema.TeeChart.Drawing;
using System.Data.OleDb;
using System.Data;
using Models;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// DamChartManager 的摘要描述
/// </summary>
public class DamUploadData
{
    const string connMdb_ = @"Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Registry Path=;Jet OLEDB:Database Locking Mode=0;Jet OLEDB:Database Password=;Password=;Jet OLEDB:Engine Type=4;Jet OLEDB:Global Bulk Transactions=1;Provider='Microsoft.Jet.OLEDB.4.0';Jet OLEDB:System database=;Jet OLEDB:SFP=False;Extended Properties=;Mode=Share Deny None;Jet OLEDB:New Database Password=;Jet OLEDB:Create System Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;User ID=Admin;Jet OLEDB:Encrypt Database=False;Data Source=";  //32位元用的
    string connSql = ConfigurationManager.ConnectionStrings["HushanConnectionString"].ConnectionString;
    string[] WhiteList = {
        "DamMonitorBlindDrain","DamMonitorSurface","DamMonitorHorizontalPipe", "DamMonitorMeasureWater",
        "DamMonitorHorizontalChange_MainChange", "DamMonitorHorizontalChange_MainSChange", "DamMonitorHorizontalChange_SouthChange", "DamMonitorHorizontalChange_SouthSChange", "DamMonitorHorizontalChange_SubChange", "DamMonitorHorizontalChange_SubSChange",
        "DamMonitorSI01", "DamMonitorSI02", "DamMonitorSI03", "DamMonitorSI04", "DamMonitorSI05", "DamMonitorSI06", "DamMonitorSI07", "DamMonitorSI08", "DamMonitorSI09", "DamMonitorSI10",
        "DamMonitorSI11", "DamMonitorSI12", "DamMonitorSI13", "DamMonitorSI14", "DamMonitorSI15", "DamMonitorSI16", "DamMonitorSI17", "DamMonitorSI18", "DamMonitorSI19", "DamMonitorSI20",
        "DamMonitorSI21", "DamMonitorSI22", "DamMonitorSI23", "DamMonitorSI24", "DamMonitorSI25", "DamMonitorSI26", "DamMonitorSI27", "DamMonitorSI28", "DamMonitorSI29",
        "DamMonitorI1D1", "DamMonitorI1D2", "DamMonitorI1D3",
        "DamMonitorMeasureWaterObserveHalfAuto", "DamMonitorMeasureWaterObserveManual", "DamMonitorWaterMeasureMain", "DamMonitorWaterMeasureSouth", "DamMonitorWaterMeasureSub"  };
    public DamUploadData()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }

    public List<string> GetTableList(string mdbPath)
    {
        string ConnStr = connMdb_ + mdbPath;

        OleDbConnection cnnSrcMDB = new OleDbConnection();
        cnnSrcMDB.ConnectionString = ConnStr;
        cnnSrcMDB.Open();

        DataTable userTables = cnnSrcMDB.GetSchema("Tables");
        List<string> ans = new List<string>(128);
        foreach (DataRow row in userTables.Rows)
        {

            if (row["TABLE_TYPE"].ToString() == "TABLE" && WhiteList.Contains(row["TABLE_NAME"].ToString()))
            {
                ans.Add(row["TABLE_NAME"].ToString());
            }
        }
        cnnSrcMDB.Close();

        return ans;
    }


    public class ImportResult
    {
        public string TableName { get; set; }
        public bool IsOk { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return TableName + (IsOk ? "匯入成功" : "匯入失敗：") + Message;
        }
    }

    public List<ImportResult> ImportTables(string mdbPath, List<string> importTables, bool isDelete)
    {
        List<ImportResult> ans = new List<ImportResult>();
        try
        {
            foreach (string tableName in importTables)
            {
                ImportResult ir = new ImportResult();
                ir.TableName = tableName;
                try
                {
                    if (isDelete)
                    {
                        Delete(tableName);
                    }
                    InsertToSql(mdbPath, tableName);
                    ir.IsOk = true;
                }
                catch (Exception ex)
                {
                    ir.IsOk = false;
                    if (ex.Message.Contains("重複的索引鍵值是"))
                    {
                        DateTime date;
                        string datestring = ex.Message.Substring(
                            ex.Message.IndexOf("重複的索引鍵值是 (") + "重複的索引鍵值是 (".Length,
                            ex.Message.LastIndexOf(")") - ex.Message.IndexOf("重複的索引鍵值是 (") - "重複的索引鍵值是 (".Length);
                        if (DateTime.TryParse(datestring, out date))
                        {
                            ir.Message = "已存在相同資料：" + date.ToString("yyyy/MM/dd");
                        }
                        else
                        {
                            ir.Message = "已存在相同資料：" +
                                ex.Message.Substring(
                                ex.Message.IndexOf("重複的索引鍵值是"),
                                ex.Message.LastIndexOf(")") - ex.Message.IndexOf("重複的索引鍵值是"));
                        }

                    }
                    else
                    {
                        ir.Message = ex.Message;
                    }
                }
                ans.Add(ir);
            }

        }
        catch (Exception ex)
        {

        }
        return ans;
    }

    private void InsertToSql(string mdbPath, string tableName)
    {
        string connMdb = connMdb_ + mdbPath;
        using (var sourceConnection = new OleDbConnection(connMdb))
        {
            sourceConnection.Open();

            var commandSourceData = new OleDbCommand("SELECT * FROM " + tableName, sourceConnection);
            var reader = commandSourceData.ExecuteReader();

            using (var destinationConnection = new SqlConnection(connSql))
            {
                destinationConnection.Open();

                using (var bulkCopy = new SqlBulkCopy(destinationConnection))
                {
                    bulkCopy.DestinationTableName = "dbo." + tableName;

                    try
                    {
                        bulkCopy.WriteToServer(reader);
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
        }
    }

    private void Delete(string tableName)
    {
        using (var sourceConnection = new SqlConnection(connSql))
        {
            sourceConnection.Open();

            var commandSourceData = new SqlCommand("delete from " + tableName, sourceConnection);
            var ans = commandSourceData.ExecuteNonQuery();
        }
    }
}