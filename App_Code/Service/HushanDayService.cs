﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

public class HushanDayService
{
    public Dictionary<DateTime, double?> GetWaterlevel(DateTime sdate, DateTime edate)
    {
        //1/1   → 1/1 23:59:59
        edate = edate.AddDays(1).AddSeconds(-1);

        using (HushanEntities db = new HushanEntities())
        {
            var ans = (from hd in db.HushanDay
                       where hd.InfoDate >= sdate && hd.InfoDate <= edate
                       select new 
                       {
                           InfoDate = hd.InfoDate,
                           Waterlevel = hd.Waterlevel
                       }).ToDictionary(k => k.InfoDate, v => (double?)v.Waterlevel);

            return ans;
        }
    }

}

