﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// 對外統一的介面，用object當參數
/// </summary>
public interface IDam
{
    IEnumerable<object> GetData(DateTime sdate, DateTime edate);
    List<DamChartData> GetObjectDatas(object datas, string[] serialColumns);
    string[] SerialColumnsA { get; }
    string[] SerialColumnsB { get; }
}
public class DamSlopeFactory
{
    private static Dictionary<string, IDam> modelFactory = new Dictionary<string, IDam> {
        { "I1D1", new DamMonitorService<DamMonitorI1D1>() },
        { "I1D2", new DamMonitorService<DamMonitorI1D2>() },
        { "I1D3", new DamMonitorService<DamMonitorI1D3>() },
        { "SI01", new DamMonitorService<DamMonitorSI01>() },
        { "SI02", new DamMonitorService<DamMonitorSI02>() },
        { "SI03", new DamMonitorService<DamMonitorSI03>() },
        { "SI04", new DamMonitorService<DamMonitorSI04>() },
        { "SI05", new DamMonitorService<DamMonitorSI05>() },
        { "SI06", new DamMonitorService<DamMonitorSI06>() },
        { "SI07", new DamMonitorService<DamMonitorSI07>() },
        { "SI08", new DamMonitorService<DamMonitorSI08>() },
        { "SI09", new DamMonitorService<DamMonitorSI09>() },
        { "SI10", new DamMonitorService<DamMonitorSI10>() },
        { "SI11", new DamMonitorService<DamMonitorSI11>() },
        { "SI12", new DamMonitorService<DamMonitorSI12>() },
        { "SI13", new DamMonitorService<DamMonitorSI13>() },
        { "SI14", new DamMonitorService<DamMonitorSI14>() },
        { "SI15", new DamMonitorService<DamMonitorSI15>() },
        { "SI16", new DamMonitorService<DamMonitorSI16>() },
        { "SI17", new DamMonitorService<DamMonitorSI17>() },
        { "SI18", new DamMonitorService<DamMonitorSI18>() },
        { "SI19", new DamMonitorService<DamMonitorSI19>() },
        { "SI20", new DamMonitorService<DamMonitorSI20>() },
        { "SI21", new DamMonitorService<DamMonitorSI21>() },
        { "SI22", new DamMonitorService<DamMonitorSI22>() },
        { "SI23", new DamMonitorService<DamMonitorSI23>() },
        { "SI24", new DamMonitorService<DamMonitorSI24>() },
        { "SI25", new DamMonitorService<DamMonitorSI25>() },
        { "SI26", new DamMonitorService<DamMonitorSI26>() },
        { "SI27", new DamMonitorService<DamMonitorSI27>() },
        { "SI28", new DamMonitorService<DamMonitorSI28>() },
        { "SI29", new DamMonitorService<DamMonitorSI29>() },
    };
    public static IDam Create(string selectedValue)
    {
        return modelFactory[selectedValue];
    }

}
