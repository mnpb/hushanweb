﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;



public class DamMonitorService<T> : IDam where T : AbstractDamMonitor
{
    public string[] SerialColumnsA
    {
        get
        {
            AbstractDamSlopeMonitor obj = (AbstractDamSlopeMonitor)Activator.CreateInstance(typeof(T));
            return obj.SerialColumnsA;
        }
    }

    public string[] SerialColumnsB
    {
        get
        {
            AbstractDamSlopeMonitor obj = (AbstractDamSlopeMonitor)Activator.CreateInstance(typeof(T));
            return obj.SerialColumnsB;
        }
    }

    public List<T> GetData(DateTime sdate, DateTime edate)
    {
        //1/1   → 1/1 23:59:59
        edate = edate.AddDays(1).AddSeconds(-1);

        using (Entities db = new Entities())
        {
            System.Data.Entity.Core.Objects.ObjectQuery<T> query = ToObjectQuery(db);
            var ans = query
                .Where(dm => dm.InfoDate >= sdate && dm.InfoDate <= edate)
                .OrderBy(dm => dm.InfoDate)
                .ToList();

            return ans;
        }
    }

    private static System.Data.Entity.Core.Objects.ObjectQuery<T> ToObjectQuery(Entities db)
    {
        var key = typeof(T).Name;
        var adapter = (IObjectContextAdapter)db;
        var objectContext = adapter.ObjectContext;
        // 1. we need the container for the conceptual model
        var container = objectContext.MetadataWorkspace.GetEntityContainer(
            objectContext.DefaultContainerName, System.Data.Entity.Core.Metadata.Edm.DataSpace.CSpace);
        // 2. we need the name given to the element set in that conceptual model
        var name = container.BaseEntitySets.Where((s) => s.ElementType.Name.Equals(key)).FirstOrDefault().Name;
        // 3. finally, we can create a basic query for this set
        var query = objectContext.CreateQuery<T>("[" + name + "]");
        return query;
    }

    /// <summary>
    /// 同一張資料表的資料有分類，只捉取serialcolumns裡的資料
    /// </summary>
    /// <param name="datas"></param>
    /// <param name="serialColumns"></param>
    /// <returns></returns>
    public List<DamChartData> GetObjectDatas(List<T> datas, string[] serialColumns)
    {
        List<DamChartData> list = new List<DamChartData>(32);
        foreach (string column in serialColumns)
        {
            DamChartData dict = new DamChartData();
            PropertyInfo prop = (typeof(T)).GetProperty(column);
            PropertyInfo propInfoDate = (typeof(T)).GetProperty("InfoDate");
            foreach (T item in datas)
            {
                try
                {
                    DateTime date = (DateTime)propInfoDate.GetValue(item);
                    double? value = (double?)prop.GetValue(item);
                    dict.Add(date, value);
                }
                catch (NullReferenceException ex)
                {
                    throw new NullReferenceException("GetObjectData:" + column);
                }
            }
            dict.AxisTitle = column;
            list.Add(dict);
        }

        return list;
    }

    public List<DamChartData> GetObjectDatas(object datas, string[] serialColumns)
    {
        return GetObjectDatas((List<T>)datas, serialColumns);
    }

    IEnumerable<object> IDam.GetData(DateTime sdate, DateTime edate)
    {
        return GetData(sdate, edate);
    }

}

