﻿using Models.Earthquake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// EarthquakeController 的摘要描述
/// </summary>
public class EarthquakeController
{
    #region Properties

    public static EarthquakeController Instance
    {
        get
        {
            return new EarthquakeController();
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有地震測站基本資料
    /// </summary>
    /// <returns></returns>
    public List<POINT> GetAllStations()
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                return (from station in db.POINT
                        orderby station.POINT_ID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<POINT>();
        }
    }

    /// <summary>
    /// 新增地震測站基本資料
    /// </summary>
    /// <param name="newStation"></param>
    /// <returns></returns>
    public bool AddStation(POINT newStation)
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                var oldInfo = (from info in db.POINT
                               where info.POINT_ID == newStation.POINT_ID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有資料，只需更新
                    oldInfo.NAME = newStation.NAME;
                    oldInfo.Longitude = newStation.Longitude;
                    oldInfo.Latitude = newStation.Latitude;
                }
                else
                { // 原無資料，需新增
                    db.POINT.Add(newStation);
                }

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除地震測站基本資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <returns></returns>
    public bool DeleteStation(int stationID)
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                var oldInfo = (from info in db.POINT
                               where info.POINT_ID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有此測站
                    // 刪除
                    db.POINT.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #region 狀態資料

    public POINT GetPointInfo(int pointID)
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                return (from item in db.POINT
                        where item.POINT_ID == pointID
                        select item).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    public STATE_UNIT GetStateUnit(int siteID)
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                return (from item in db.STATE_UNIT
                        where item.SITE_ID == siteID
                        select item).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    public STATE_POINT GetStatePoint(int pointID)
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                return (from item in db.STATE_POINT
                        where item.POINT_ID == pointID
                        select item).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    public STATE_INTENSITY GetStateIntensity(int intID)
    {
        try
        {
            using (EarthquakeEntities db = new EarthquakeEntities())
            {
                return (from item in db.STATE_INTENSITY
                        where item.INT_ID == intID
                        select item).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    #endregion

    #endregion
}