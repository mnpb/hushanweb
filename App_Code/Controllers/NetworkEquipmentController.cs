﻿using Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

/// <summary>
/// NetworkEquipmentController 的摘要描述
/// </summary>
public class NetworkEquipmentController
{
    #region Properties

    public static NetworkEquipmentController Instance
    {
        get
        {
            return new NetworkEquipmentController();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 取得所有網路設備
    /// </summary>
    /// <returns></returns>
    public List<NetworkEquipmentBase> GetAllNetworkEquipments()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from equipment in db.NetworkEquipmentBase
                        orderby equipment.CategoryID, equipment.IP
                        select equipment).ToList();
            }
        }
        catch
        {
            return new List<NetworkEquipmentBase>();
        }
    }

    public bool AddNetworkEquipment(NetworkEquipmentBase equipment)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.NetworkEquipmentBase
                               where info.IP == equipment.IP
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 修改
                    oldInfo.Name = equipment.Name;
                    oldInfo.CategoryID = equipment.CategoryID;
                    oldInfo.Location = equipment.Location;
                    oldInfo.Account = equipment.Account;
                    oldInfo.Password = equipment.Password;
                    oldInfo.Note = equipment.Note;
                }
                else
                { // 新增
                    db.NetworkEquipmentBase.Add(equipment);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
        }
        return false;
    }

    public bool DeleteNetworkEquipment(string equipmentIP)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.NetworkEquipmentBase
                               where info.IP == equipmentIP
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.NetworkEquipmentBase.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    /// <summary>
    /// 取得網路設備即時狀態
    /// </summary>
    /// <param name="equipmentCategoryID">網路設備類型編號</param>
    /// <returns></returns>
    public dynamic GetNetworkEquipmentRealtimeStatus(byte equipmentCategoryID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.NetworkEquipmentBase
                        join realStatus in db.NetworkEquipmentRealtimeStatus
                            on station.IP equals realStatus.IP
                        join category in db.NetworkEquipmentCategory on station.CategoryID equals category.ID
                        where category.ID == equipmentCategoryID
                        orderby station.IP
                        select new
                        {
                            IP = station.IP,
                            Name = station.Name,
                            Location = station.Location,
                            CategoryID = category.ID,
                            CategoryName = category.Name,
                            Note = station.Note,
                            Status = realStatus.Status,
                        }).ToList();
            }
        }
        catch
        {
            return new object();
        }
    }

    public dynamic GetNetworkEquipmentHistoryStatus(DateTime queryDate, byte equipmentCategoryID)
    {
        DateTime beginTime = queryDate.Date;
        DateTime endTime = beginTime.AddDays(1).AddMilliseconds(-1);
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.NetworkEquipmentBase
                        join status in db.NetworkEquipmentStatus on station.IP equals status.IP
                        join category in db.NetworkEquipmentCategory on station.CategoryID equals category.ID
                        where category.ID == equipmentCategoryID && status.InfoTime >= beginTime && status.InfoTime <= endTime
                        orderby station.IP, status.InfoTime
                        select new
                        {
                            IP = station.IP,
                            Name = station.Name,
                            Location = station.Location,
                            CategoryID = category.ID,
                            CategoryName = category.Name,
                            Note = station.Note,
                            StatusTime = status.InfoTime,
                            Status = status.Status,
                        }).ToList();
            }
        }
        catch
        {
            return new object();
        }
    }

    #endregion
}