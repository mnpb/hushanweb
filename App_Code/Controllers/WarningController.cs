﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WarningController 的摘要描述
/// </summary>
public class WarningController
{
    #region Properties

    private static WarningController instance = new WarningController();
    public static WarningController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有警報站基本資料
    /// </summary>
    /// <returns></returns>
    public List<WarningStation> GetAllStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.WarningStation
                        orderby station.StationID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<WarningStation>();
        }
    }

    /// <summary>
    /// 新增警報站基本資料
    /// </summary>
    /// <param name="newStation"></param>
    /// <returns></returns>
    public bool AddStation(WarningStation newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WarningStation
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有資料，只需更新
                    oldInfo.StationName = newStation.StationName;
                    oldInfo.Longitude = newStation.Longitude;
                    oldInfo.Latitude = newStation.Latitude;
                }
                else
                { // 原無資料，需新增
                    db.WarningStation.Add(newStation);
                }

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除警報站基本資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <returns></returns>
    public bool DeleteStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WarningStation
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有此測站
                    // 刪除
                    db.WarningStation.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #region 警戒相關

    public List<AlertWarningStation> GetAllAlertStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.AlertWarningStation
                        orderby info.StationID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<AlertWarningStation>();
        }
    }

    public bool AddAlertStation(AlertWarningStation newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.AlertWarningStation
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.ACError = newStation.ACError;
                    oldInfo.ACNormal = newStation.ACNormal;
                    oldInfo.DCError = newStation.DCError;
                    oldInfo.DCNormal = newStation.DCNormal;
                    oldInfo.DoorOpen = newStation.DoorOpen;
                    oldInfo.DoorClose = newStation.DoorClose;
                    oldInfo.AmplifierError = newStation.AmplifierError;
                    oldInfo.AmplifierNormal = newStation.AmplifierNormal;
                    oldInfo.TrumpetError = newStation.TrumpetError;
                    oldInfo.TrumpetNormal = newStation.TrumpetNormal;
                    oldInfo.Active = newStation.Active;
                }
                else
                {
                    db.AlertWarningStation.Add(newStation);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteAlertStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.AlertWarningStation
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.AlertWarningStation.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #endregion
}