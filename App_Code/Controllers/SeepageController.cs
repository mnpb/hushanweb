﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// SeepageController 的摘要描述
/// </summary>
public class SeepageController
{
    #region Properties

    private static SeepageController instance = new SeepageController();
    public static SeepageController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有滲漏室基本資料
    /// </summary>
    /// <returns></returns>
    public List<SeepageBase> GetAllMeters()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.SeepageBase
                        orderby station.DamLocationID, station.MeterID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<SeepageBase>();
        }
    }

    #endregion

    #region 水情資料

    /// <summary>
    /// 取得小時資料
    /// </summary>
    /// <param name="meterID"></param>
    /// <param name="beginTime"></param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public List<SeepageHour> GetHourInfos(string meterID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SeepageHour
                        where info.MeterID == meterID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SeepageHour>();
        }
    }

    /// <summary>
    /// 取得日資料
    /// </summary>
    /// <param name="meterID"></param>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<SeepageDay> GetDayInfos(string meterID, int year, int month)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SeepageDay
                        where info.MeterID == meterID && info.InfoDate >= beginDate && info.InfoDate <= endDate
                        orderby info.InfoDate
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SeepageDay>();
        }
    }

    /// <summary>
    /// 取得月資料
    /// </summary>
    /// <param name="meterID"></param>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<SeepageMonth> GetMonthInfos(string meterID, int year)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SeepageMonth
                        where info.MeterID == meterID && info.InfoYear == year
                        orderby info.InfoMonth
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SeepageMonth>();
        }
    }

    #endregion

    #endregion
}