﻿using Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// InspectController 的摘要描述
/// </summary>
public class InspectController
{
    #region Properties

    public static InspectController Instance
    {
        get
        {
            return new InspectController();
        }
    }

    #endregion

    #region Methods

    private DataTable GetFilterMessage(DateTime beginTime, DateTime endTime)
    {
        try
        {
            string commandText = string.Format(
                "SELECT * FROM message WHERE (create_date >= @BeginDate AND create_date <= @EndDate AND create_time >= '00時00分00秒'  AND create_time <= '23時59分59秒') AND message_type={0} AND (message_content like {1} OR message_content like {2} OR  message_content like {3} OR message_content like {4})",
                "@MessageType", "@Filter1", "@Filter2", "@Filter3", "@Filter4");
            using (MySqlConnection con = new MySqlConnection(ConfigInfo.InspectConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(commandText))
                {
                    cmd.Parameters.AddWithValue("@BeginDate", beginTime.Date.ToString("yyyy/MM/dd"));
                    cmd.Parameters.AddWithValue("@EndDate", endTime.Date.ToString("yyyy/MM/dd"));
                    cmd.Parameters.AddWithValue("@MessageType", "巡更訊息");
                    cmd.Parameters.AddWithValue("@Filter1", "%遲到%");
                    cmd.Parameters.AddWithValue("@Filter2", "%感應了%");
                    cmd.Parameters.AddWithValue("@Filter3", "%早到%");
                    cmd.Parameters.AddWithValue("@Filter4", "%已完成巡查任務%");
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }
        catch
        {
            return null;
        }
    }

    public dynamic GetMessageInfos(DateTime beginDate, DateTime endDate)
    {
        DateTime beginTime = beginDate.Date;
        DateTime endTime = endDate.AddDays(1).AddMilliseconds(-1);

        DataTable dataTable = GetFilterMessage(beginTime, endTime);
        if (dataTable != null)
        {
            try
            {
                return dataTable.AsEnumerable().Select(row => new
                  {
                      MessageType = row.Field<string>("message_type"),
                      InfoTime = Convert.ToDateTime(row.Field<string>("create_date") + " " + row.Field<string>("create_time")),
                      MessageContent = row.Field<string>("message_content"),
                  }).ToList();
            }
            catch
            {
            }
        }
        return new object();
    }

    public List<OnSiteLandProfile> GetAllOnSiteLandProfiles()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from item in db.OnSiteLandProfile
                        orderby item.ProfileTime descending
                        select item).ToList();
            }
        }
        catch
        {
            return new List<OnSiteLandProfile>();
        }
    }

    /// <summary>
    /// 更新地籍資料
    /// </summary>
    /// <param name="newProfile"></param>
    /// <returns></returns>
    public bool UpdateOnSiteLandProfile(OnSiteLandProfile newProfile)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.OnSiteLandProfile
                               where info.ProfileID == newProfile.ProfileID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有資料，只需更新
                    oldInfo.Staff = newProfile.Staff;
                    oldInfo.Longitude = newProfile.Longitude;
                    oldInfo.Latitude = newProfile.Latitude;
                    oldInfo.TWD97X = newProfile.TWD97X;
                    oldInfo.TWD97Y = newProfile.TWD97Y;
                    oldInfo.Description = newProfile.Description;
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    public List<OnSiteLandImage> GetOnSiteLandImages(int profileID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from item in db.OnSiteLandImage
                        where item.ProfileID == profileID
                        orderby item.ImageTime
                        select item).ToList();
            }
        }
        catch
        {
            return new List<OnSiteLandImage>();
        }
    }

    public OnSiteLandImage GetOnSiteLandImage(int imageID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from item in db.OnSiteLandImage
                        where item.ImageID == imageID
                        select item).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    public bool AddImage(int profileID, DateTime dateTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                db.OnSiteLandImage.Add(new OnSiteLandImage
                {
                    ProfileID = profileID,
                    ImageTime = dateTime,
                    ImagePath = dateTime.ToString("yyyyMMddHHmmss") + ".JPG",
                });
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteImage(int imageID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from item in db.OnSiteLandImage
                               where item.ImageID == imageID
                               select item).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.OnSiteLandImage.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion
}