﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// ReservoirController 的摘要描述
/// </summary>
public class ReservoirController
{
    #region Properties

    private static ReservoirController instance = new ReservoirController();
    public static ReservoirController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    public List<ReservoirBase> GetAllReservoirs()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.ReservoirBase
                        orderby info.ReservoirID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<ReservoirBase>();
        }
    }

    public string GetReservoirName(string reservoirID)
    {
        try
        {
            return (from info in GetAllReservoirs()
                    where info.ReservoirID == reservoirID
                    select info.ReservoirName).FirstOrDefault();
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion

    #region 水情資料

    public List<HushanHour> GetHushanHourInfos(DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.HushanHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<HushanHour>();
        }
    }

    /// <summary>
    /// 取得水庫小時資訊
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public List<HushanHour> GetHushanHourInfos(DateTime date)
    {
        DateTime beginTime = date;
        DateTime endTime = date.AddDays(1).AddHours(-1);
        return GetHushanHourInfos(beginTime, endTime);
    }

    public bool AddHour(HushanHour hourInfo)
    {
        try
        {
            using(HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from item in db.HushanHour
                               where item.InfoTime == hourInfo.InfoTime
                               select item).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.HushanHour.Remove(oldInfo);
                }
                db.HushanHour.Add(hourInfo);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 取得水庫日資訊
    /// </summary>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<HushanHour> GetHushanDayInfos(int year, int month)
    {
        DateTime beginTime = new DateTime(year, month, 1);
        DateTime endTime = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.HushanHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime && info.InfoTime.Hour == 0
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<HushanHour>();
        }
    }

    /// <summary>
    /// 取得水庫月資訊
    /// </summary>
    /// <param name="year"></param>
    /// <returns></returns>
    public List<HushanHour> GetHushanMonthInfos(int year)
    {
        DateTime beginTime = new DateTime(year, 1, 1);
        DateTime endTime = new DateTime(year, 12, 31);
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.HushanHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime
                        && info.InfoTime.Day % 10 == 1 && info.InfoTime.Hour == 0 && info.InfoTime.Day <= 21
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<HushanHour>();
        }
    }

    /// <summary>
    /// 取得水庫歷年資訊
    /// </summary>
    /// <returns></returns>
    public List<HushanHour> GetHushanAllYearInfos()
    {
        DateTime beginTime = new DateTime(2016, 1, 1);
        DateTime endTime = new DateTime(DateTime.Today.Year, 12, 31);
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.HushanHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime
                        && info.InfoTime.Month == 1 && info.InfoTime.Day == 1 && info.InfoTime.Hour == 0
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<HushanHour>();
        }
    }

    #endregion

    #region HAV

    /// <summary>
    /// 取得HAV資訊
    /// </summary>
    /// <param name="reservoirID"></param>
    /// <param name="measureYear"></param>
    /// <returns></returns>
    public List<ReservoirHAV> GetHAVInfos(string reservoirID, short measureYear)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.ReservoirHAV
                        where info.ReservoirID == reservoirID && info.MeasureYear == measureYear
                        orderby info.Waterlevel
                        select info).ToList();
            }
        }
        catch
        {
            return new List<ReservoirHAV>();
        }
    }

    ///// <summary>
    ///// 取得某水庫之所有啟用日期
    ///// </summary>
    ///// <param name="reservoirID"></param>
    ///// <returns></returns>
    //public List<DateTime> GetAllBeginDates(string reservoirID)
    //{
    //    try
    //    {
    //        using (HushanEntities db = new HushanEntities())
    //        {
    //            return (from info in db.ReservoirHAV
    //                    where info.ReservoirID == reservoirID
    //                    orderby info.BeginDate
    //                    select info.BeginDate).Distinct().ToList();
    //        }
    //    }
    //    catch
    //    {
    //        return new List<DateTime>();
    //    }
    //}

    public List<short> GetAllMeasureYears(string reservoirID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.ReservoirHAV
                        where info.ReservoirID == reservoirID
                        orderby info.MeasureYear
                        select info.MeasureYear).Distinct().ToList();
            }
        }
        catch
        {
            return new List<short>();
        }
    }

    public bool AddHAVInfo(ReservoirHAV reservoirHAV)
    {
        try
        {
            string reservoirID = reservoirHAV.ReservoirID;
            DateTime beginDate = reservoirHAV.BeginDate;
            short measureYear = reservoirHAV.MeasureYear;
            decimal waterlevel = reservoirHAV.Waterlevel;
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.ReservoirHAV
                               where info.ReservoirID == reservoirID && info.MeasureYear == measureYear &&
                               info.BeginDate == beginDate && info.Waterlevel == waterlevel
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.ReservoirHAV.Remove(oldInfo);
                }

                db.ReservoirHAV.Add(reservoirHAV);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool AddHAVInfos(string reservoirID, short measureYear, DateTime beginDate, List<string> havInfos)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                // 刪除舊資料
                var oldInfos = (from info in db.ReservoirHAV
                                where info.ReservoirID == reservoirID && info.MeasureYear == measureYear && info.BeginDate == beginDate
                                select info).ToList();
                if (oldInfos.Count > 0)
                {
                    db.ReservoirHAV.RemoveRange(oldInfos);
                    db.SaveChanges();
                }

                // 新增
                List<ReservoirHAV> addedInfos = new List<ReservoirHAV>();
                foreach (var item in havInfos)
                {
                    addedInfos.Add(new ReservoirHAV
                        {
                            ReservoirID = reservoirID,
                            BeginDate = beginDate,
                            MeasureYear = measureYear,
                            Waterlevel = Convert.ToDecimal(item.Split(',')[0]),
                            Volume = Convert.ToDecimal(item.Split(',')[1]),
                        });
                }
                db.ReservoirHAV.AddRange(addedInfos);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
        }
        return false;
    }

    public bool DeleteHAVInfo(ReservoirHAV reservoirHAV)
    {
        try
        {
            string reservoirID = reservoirHAV.ReservoirID;
            DateTime beginDate = reservoirHAV.BeginDate;
            short measureYear = reservoirHAV.MeasureYear;
            decimal waterlevel = reservoirHAV.Waterlevel;
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.ReservoirHAV
                               where info.ReservoirID == reservoirID && info.MeasureYear == measureYear &&
                               info.BeginDate == beginDate && info.Waterlevel == waterlevel
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.ReservoirHAV.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #endregion
}