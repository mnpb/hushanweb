﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AccountController 的摘要描述
/// </summary>
public class AccountController
{
    #region Properties

    private static AccountController instance = new AccountController();
    public static AccountController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    public string GetAccountName()
    {
        if (HttpContext.Current.Session["AccountName"] == null)
        { // 無session
            return string.Empty;
        }
        return HttpContext.Current.Session["AccountName"].ToString();
    }

    #endregion

}