﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// EvaporationController 的摘要描述
/// </summary>
public class EvaporationController
{
    #region Properties

    private static EvaporationController instance = new EvaporationController();
    public static EvaporationController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有蒸發皿基本資料
    /// </summary>
    /// <returns></returns>
    public List<EvaporationBase> GetAllStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.EvaporationBase
                        orderby station.StationID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<EvaporationBase>();
        }
    }

    #endregion

    #region 水情資料

    /// <summary>
    /// 取得小時資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="beginTime"></param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public List<EvaporationHour> GetHourInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.EvaporationHour
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<EvaporationHour>();
        }
    }

    #endregion

    #endregion
}