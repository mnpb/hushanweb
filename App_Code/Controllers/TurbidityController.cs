﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// TurbidityController 的摘要描述
/// </summary>
public class TurbidityController
{
    #region Properties

    private static TurbidityController instance = new TurbidityController();
    public static TurbidityController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有濁度測站基本資料
    /// </summary>
    /// <returns></returns>
    public List<TurbidityStationBase> GetAllStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.TurbidityStationBase
                        orderby station.StationID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<TurbidityStationBase>();
        }
    }

    #endregion

    #region 水情資料

    /// <summary>
    /// 取得小時資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="beginTime"></param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public List<TurbidityHour> GetHourInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.TurbidityHour
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<TurbidityHour>();
        }
    }

    /// <summary>
    /// 取得日資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<TurbidityDay> GetDayInfos(string stationID, int year, int month)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.TurbidityDay
                        where info.StationID == stationID && info.InfoDate >= beginDate && info.InfoDate <= endDate
                        orderby info.InfoDate
                        select info).ToList();
            }
        }
        catch
        {
            return new List<TurbidityDay>();
        }
    }

    /// <summary>
    /// 取得月資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<TurbidityMonth> GetMonthInfos(string stationID, int year)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.TurbidityMonth
                        where info.StationID == stationID && info.InfoYear == year
                        orderby info.InfoMonth
                        select info).ToList();
            }
        }
        catch
        {
            return new List<TurbidityMonth>();
        }
    }

    #endregion

    #endregion
}