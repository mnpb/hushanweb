﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WaterPipeController 的摘要描述
/// </summary>
public class WaterPipeController
{
    #region Properties

    private static WaterPipeController instance = new WaterPipeController();
    public static WaterPipeController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有原水管站基本資料
    /// </summary>
    /// <returns></returns>
    public List<WaterPipeStationBase> GetAllStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.WaterPipeStationBase
                        orderby station.StationID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<WaterPipeStationBase>();
        }
    }

    /// <summary>
    /// 新增原水管站基本資料
    /// </summary>
    /// <param name="newStation"></param>
    /// <returns></returns>
    public bool AddStation(WaterPipeStationBase newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterPipeStationBase
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有資料，只需更新
                    oldInfo.StationName = newStation.StationName;
                    oldInfo.Address = newStation.Address;
                    oldInfo.Longitude = newStation.Longitude;
                    oldInfo.Latitude = newStation.Latitude;
                    oldInfo.BasinID = newStation.BasinID;
                    oldInfo.OrganizationID = newStation.OrganizationID;
                }
                else
                { // 原無資料，需新增
                    db.WaterPipeStationBase.Add(newStation);
                }

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除原水管站基本資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <returns></returns>
    public bool DeleteStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterPipeStationBase
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有此測站
                    // 刪除
                    db.WaterPipeStationBase.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #region 水情資料

    public List<WaterPipeTenMinute> GetTenMinuteInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterPipeTenMinute
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterPipeTenMinute>();
        }
    }

    public bool AddTenMinuteInfo(WaterPipeTenMinute tenMinuteInfo)
    {
        try
        {
            string stationID = tenMinuteInfo.StationID;
            DateTime infoTime = tenMinuteInfo.InfoTime;
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterPipeTenMinute
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.Flow = tenMinuteInfo.Flow;
                }
                else
                {
                    db.WaterPipeTenMinute.Add(tenMinuteInfo);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
        }
        return false;
    }

    public bool DeleteTenMinuteInfo(string stationID, DateTime infoTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterPipeTenMinute
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.WaterPipeTenMinute.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    public List<WaterPipeHour> GetHourInfos(string stationID, DateTime date)
    {
        DateTime beginTime = date;
        DateTime endTime = beginTime.AddHours(23);
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterPipeHour
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterPipeHour>();
        }
    }

    public List<WaterPipeDay> GetDayInfos(string stationID, int year, int month)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterPipeDay
                        where info.StationID == stationID && info.InfoDate >= beginDate && info.InfoDate <= endDate
                        orderby info.InfoDate
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterPipeDay>();
        }
    }

    public List<WaterPipeMonth> GetMonthInfos(string stationID, short year)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterPipeMonth
                        where info.StationID == stationID && info.InfoYear == year
                        orderby info.InfoMonth
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterPipeMonth>();
        }
    }

    #endregion

    #endregion
}