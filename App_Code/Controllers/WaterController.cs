﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WaterController 的摘要描述
/// </summary>
public class WaterController
{
    #region Properties

    private static WaterController instance = new WaterController();
    public static WaterController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有水位站基本資料
    /// </summary>
    /// <returns></returns>
    public List<WaterStationBase> GetAllStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.WaterStationBase
                        orderby station.StationID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<WaterStationBase>();
        }
    }

    /// <summary>
    /// 新增水位站基本資料
    /// </summary>
    /// <param name="newStation"></param>
    /// <returns></returns>
    public bool AddStation(WaterStationBase newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterStationBase
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有資料，只需更新
                    oldInfo.StationName = newStation.StationName;
                    oldInfo.Address = newStation.Address;
                    oldInfo.Longitude = newStation.Longitude;
                    oldInfo.Latitude = newStation.Latitude;
                    oldInfo.BasinID = newStation.BasinID;
                    oldInfo.OrganizationID = newStation.OrganizationID;
                }
                else
                { // 原無資料，需新增
                    db.WaterStationBase.Add(newStation);
                }

                //var warnOldInfo = (from info in db.Alarm_Hydrograph
                //                   where info.STATIONID == newStation.StationID
                //                   select info).FirstOrDefault();
                //if (warnOldInfo != null)
                //{ // 已有資料，只需更新
                //    warnOldInfo.OVER_VALUE = Convert.ToDecimal(newStation.WarningLine1);
                //    warnOldInfo.ASSING_VALUE = Convert.ToDecimal(newStation.WarningLine2);
                //    warnOldInfo.DEFAULT_VALUE = Convert.ToDecimal(newStation.WarningLine3);
                //}

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除水位站基本資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <returns></returns>
    public bool DeleteStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterStationBase
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有此測站
                    // 刪除
                    db.WaterStationBase.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #region 水情資料

    public List<WaterTenMinute> GetTenMinuteInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterTenMinute
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterTenMinute>();
        }
    }

    public bool AddTenMinuteInfo(WaterTenMinute tenMinuteInfo)
    {
        try
        {
            string stationID = tenMinuteInfo.StationID;
            DateTime infoTime = tenMinuteInfo.InfoTime;
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterTenMinute
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.Waterlevel = tenMinuteInfo.Waterlevel;
                    oldInfo.Flow = tenMinuteInfo.Flow;
                }
                else
                {
                    db.WaterTenMinute.Add(tenMinuteInfo);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
        }
        return false;
    }

    public bool DeleteTenMinuteInfo(string stationID, DateTime infoTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterTenMinute
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.WaterTenMinute.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    /// <summary>
    /// 取得小時資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="beginTime"></param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public List<WaterHour> GetHourInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterHour
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterHour>();
        }
    }

    public bool AddHourInfos(WaterHour hourInfo)
    {
        try
        {
            string stationID = hourInfo.StationID;
            DateTime infoTime = hourInfo.InfoTime;
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterHour
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.Waterlevel = hourInfo.Waterlevel;
                    oldInfo.Flow = hourInfo.Flow;
                }
                else
                {
                    db.WaterHour.Add(hourInfo);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 取得日資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<WaterDay> GetDayInfos(string stationID, int year, int month)
    {
        DateTime beginTime = new DateTime(year, month, 1);
        DateTime endTime = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterDay
                        where info.StationID == stationID && info.InfoDate >= beginTime && info.InfoDate <= endTime
                        orderby info.InfoDate
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterDay>();
        }
    }

    /// <summary>
    /// 取得月資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<WaterMonth> GetMonthInfos(string stationID, int year)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterMonth
                        where info.StationID == stationID && info.InfoYear == year
                        orderby info.InfoMonth
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterMonth>();
        }
    }

    //#region 10分鐘資料

    ///// <summary>
    ///// 取得河川水位即時10分鐘資訊
    ///// </summary>
    ///// <param name="stationID"></param>
    ///// <returns></returns>
    //public wsWater.WaterInfo GetRealtime10MinuteInfo(short stationID)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.GetRealtime10MinuteInfo(stationID);
    //    }
    //    catch
    //    {
    //        return null;
    //    }
    //}

    ///// <summary>
    ///// 取得河川水位10分鐘資料
    ///// </summary>
    ///// <param name="stationID">測站ID</param>
    ///// <param name="dateTime">時間</param>
    ///// <returns></returns>
    //public wsWater.WaterInfo Get10MinuteInfo(short stationID, DateTime dateTime)
    //{
    //    try
    //    {
    //        return (from info in Get10MinuteInfos(stationID, dateTime, dateTime)
    //                select info).FirstOrDefault();
    //    }
    //    catch
    //    {
    //        return null;
    //    }
    //}

    ///// <summary>
    ///// 取得河川水位10分鐘資料
    ///// </summary>
    ///// <param name="stationID">測站ID</param>
    ///// <param name="beginTime">開始時間</param>
    ///// <param name="endTime">結束時間</param>
    ///// <returns></returns>
    //public List<wsWater.WaterInfo> Get10MinuteInfos(short stationID, DateTime beginTime, DateTime endTime)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.Get10MinuteInfos(stationID, beginTime, endTime).ToList();
    //    }
    //    catch
    //    {
    //        return new List<wsWater.WaterInfo>();
    //    }
    //}

    ///// <summary>
    ///// 修改河川10分鐘資料
    ///// </summary>
    ///// <param name="waterInfo"></param>
    ///// <returns></returns>
    //public bool Modify10MinuteInfo(wsWater.WaterInfo waterInfo)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.Modify10MinuteInfo(waterInfo);
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}

    //#endregion

    ///// <summary>
    ///// 取得小時水位資料
    ///// </summary>
    ///// <param name="stationID"></param>
    ///// <param name="date"></param>
    ///// <returns></returns>
    //public List<wsWater.WaterInfo> GetHourInfos(short stationID, DateTime date)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.GetHourInfos(stationID, date).ToList();
    //    }
    //    catch
    //    {
    //        return new List<wsWater.WaterInfo>();
    //    }
    //}

    ///// <summary>
    ///// 取得日水位資料
    ///// </summary>
    ///// <param name="stationID"></param>
    ///// <param name="year"></param>
    ///// <param name="month"></param>
    ///// <returns></returns>
    //public List<wsWater.WaterInfo> GetDayInfos(short stationID, int year, int month)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.GetDayInfos(stationID, year, month).ToList();
    //    }
    //    catch
    //    {
    //        return new List<wsWater.WaterInfo>();
    //    }
    //}

    ///// <summary>
    ///// 取得旬水位資料
    ///// </summary>
    ///// <param name="stationID"></param>
    ///// <param name="year"></param>
    ///// <param name="month"></param>
    ///// <returns></returns>
    //public List<wsWater.WaterInfo> GetTendayInfos(short stationID, int year)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.GetTendayInfos(stationID, year).ToList();
    //    }
    //    catch
    //    {
    //        return new List<wsWater.WaterInfo>();
    //    }
    //}

    ///// <summary>
    ///// 取得月水位資料
    ///// </summary>
    ///// <param name="stationID"></param>
    ///// <param name="year"></param>
    ///// <param name="month"></param>
    ///// <returns></returns>
    //public List<wsWater.WaterInfo> GetMonthInfos(short stationID, int year)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.GetMonthInfos(stationID, year).ToList();
    //    }
    //    catch
    //    {
    //        return new List<wsWater.WaterInfo>();
    //    }
    //}

    //#endregion

    //#region 警戒資訊

    //#region Public

    ///// <summary>
    ///// 取得所有警戒水位站
    ///// </summary>
    ///// <returns></returns>
    //public List<wsWater.AlertWaterStationInfo> GetAllAlertStationInfos()
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.GetAllAlertStationInfos().ToList();
    //    }
    //    catch
    //    {
    //        return new List<wsWater.AlertWaterStationInfo>();
    //    }
    //}

    ///// <summary>
    ///// 取得警戒水位站
    ///// </summary>
    ///// <param name="stationID">測站ID</param>
    ///// <returns></returns>
    //public wsWater.AlertWaterStationInfo GetAlertStationInfo(short stationID)
    //{
    //    try
    //    {
    //        return (from info in GetAllAlertStationInfos()
    //                where info.StationID == stationID
    //                select info).FirstOrDefault();
    //    }
    //    catch
    //    {
    //        return null;
    //    }
    //}

    ///// <summary>
    ///// 新增警戒水位站
    ///// </summary>
    ///// <param name="baseInfo"></param>
    ///// <returns></returns>
    //public bool AddAlertStationInfo(wsWater.AlertWaterStationInfo baseInfo)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.AddAlertStationInfo(baseInfo);
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}

    ///// <summary>
    ///// 刪除警戒水位站
    ///// </summary>
    ///// <param name="stationID"></param>
    ///// <returns></returns>
    //public bool DeleteAlertStationInfo(short stationID)
    //{
    //    try
    //    {
    //        wsWater.WaterService ws = new wsWater.WaterService();
    //        return ws.DeleteAlertStationInfo(stationID);
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}

    //#endregion

    #endregion

    #region 率定曲線

    public List<WaterStationCurve> GetCurves(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterStationCurve
                        where info.StationID == stationID
                        orderby info.StartDate descending
                        select info).ToList();
            }
        }
        catch
        {
            return new List<WaterStationCurve>();
        }
    }

    public WaterStationCurve GetCurve(string stationID, DateTime startDate)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.WaterStationCurve
                        where info.StationID == stationID && info.StartDate == startDate
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return new WaterStationCurve();
        }
    }

    public bool AddCurve(WaterStationCurve curve)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterStationCurve
                               where info.StationID == curve.StationID && info.StartDate == curve.StartDate
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.Parameter1 = curve.Parameter1;
                    oldInfo.Parameter2 = curve.Parameter2;
                    oldInfo.Parameter3 = curve.Parameter3;
                    oldInfo.LowLevel = curve.LowLevel;
                    oldInfo.HighLevel = curve.HighLevel;
                }
                else
                {
                    db.WaterStationCurve.Add(curve);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteCurve(string stationID, DateTime startDate)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.WaterStationCurve
                               where info.StationID == stationID && info.StartDate == startDate
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.WaterStationCurve.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #region 警戒相關

    public List<AlertWaterStation> GetAllAlertStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.AlertWaterStation
                        orderby info.StationID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<AlertWaterStation>();
        }
    }

    public bool AddAlertStation(AlertWaterStation newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.AlertWaterStation
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.WarningLevel1 = newStation.WarningLevel1;
                    oldInfo.WarningLevel2 = newStation.WarningLevel2;
                    oldInfo.WarningLevel3 = newStation.WarningLevel3;
                    oldInfo.Active = newStation.Active;
                }
                else
                {
                    db.AlertWaterStation.Add(newStation);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteAlertStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.AlertWaterStation
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.AlertWaterStation.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #endregion
}