﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// NetworkEquipmentCategoryController 的摘要描述
/// </summary>
public class NetworkEquipmentCategoryController
{
    #region Properties

    public static NetworkEquipmentCategoryController Instance
    {
        get
        {
            return new NetworkEquipmentCategoryController();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 取得所有網路設備分類
    /// </summary>
    /// <returns></returns>
    public List<NetworkEquipmentCategory> GetAllCategories()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.NetworkEquipmentCategory
                        orderby info.ID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<NetworkEquipmentCategory>();
        }
    }

    public string GetCategoryName(byte categoryID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.NetworkEquipmentCategory
                        where info.ID == categoryID
                        select info.Name).FirstOrDefault();
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion
}