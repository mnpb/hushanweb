﻿using Models.Choshui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// OpenContractController 的摘要描述
/// </summary>
public class OpenContractController
{
    #region Properties

    private static OpenContractController instance = new OpenContractController();
    public static OpenContractController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    public List<OpenContract> GetOpenContract(string stationID)
    {
        try
        {
            if (string.IsNullOrEmpty(stationID))
            { // 全部
                using (ChoshuiEntities db = new ChoshuiEntities())
                {
                    return (from item in db.OpenContract
                            orderby item.StationID
                            select item).ToList();
                }
            }
            else
            { // 單一
                int theStationID = Convert.ToInt32(stationID);
                using (ChoshuiEntities db = new ChoshuiEntities())
                {
                    return (from item in db.OpenContract
                            where item.StationID == theStationID
                            select item).ToList();
                }
            }
        }
        catch
        {
            return new List<OpenContract>();
        }
    }

    /// <summary>
    /// 新增開口合約
    /// </summary>
    /// <param name="openContract"></param>
    /// <returns></returns>
    public bool AddOpenContract(OpenContract openContract)
    {
        try
        {
            using (ChoshuiEntities db = new ChoshuiEntities())
            {
                var oldInfo = (from info in db.OpenContract
                               where info.StationID == openContract.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 修改
                    oldInfo.StationName = openContract.StationName;
                    oldInfo.RiverSection = openContract.RiverSection;
                    oldInfo.StandbyPosition = openContract.StandbyPosition;
                    oldInfo.Tools = openContract.Tools;
                    oldInfo.Longitude = openContract.Longitude;
                    oldInfo.Latitude = openContract.Latitude;
                }
                else
                { // 新增
                    db.OpenContract.Add(new OpenContract
                    {
                        StationName = openContract.StationName,
                        RiverSection = openContract.RiverSection,
                        StandbyPosition = openContract.StandbyPosition,
                        Tools = openContract.Tools,
                        Longitude = openContract.Longitude,
                        Latitude = openContract.Latitude,
                    });
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除開口合約
    /// </summary>
    /// <param name="stationID"></param>
    /// <returns></returns>
    public bool DeleteOpenContract(int stationID)
    {
        try
        {
            using (ChoshuiEntities db = new ChoshuiEntities())
            {
                var oldInfo = (from info in db.OpenContract
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.OpenContract.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion
}