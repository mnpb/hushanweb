﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// TownController 的摘要描述
/// </summary>
public class TownController
{
    #region Properties

    public static TownController Instance
    {
        get
        {
            return new TownController();
        }
    }

    #endregion

    #region Methods

    public List<Town> GetAllTowns()
    {
        string cacheKey = CacheKeyInfo.TownCacheKey;

        var result = CacheUtility.GetFromCache(cacheKey);
        if (result != null)
        { // 從cache抓取
            return (result as List<Town>);
        }

        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var allInfos = (from info in db.Town
                                orderby info.TownID
                                select info).ToList();
                CacheUtility.AddToCache(cacheKey, allInfos, DateTime.UtcNow.AddYears(1)); // 有效期限1年
                return allInfos;
            }
        }
        catch
        {
            return new List<Town>();
        }
    }

    public List<Town> GetTowns(string countyID)
    {
        try
        {
            return (from info in GetAllTowns()
                    where info.CountyID == countyID
                    orderby info.TownID
                    select info).ToList();
        }
        catch
        {
            return new List<Town>();
        }
    }

    public string GetTownName(string townID)
    {
        try
        {
            return (from info in GetAllTowns()
                    where info.TownID == townID
                    select info.TownName).FirstOrDefault();
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion
}