﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

/// <summary>
/// EnvCtrlController 的摘要描述
/// </summary>
public class EnvCtrlController
{
    #region Properties

    public static EnvCtrlController Instance
    {
        get
        {
            return new EnvCtrlController();
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    public List<EnvCtrlType> GetAllEnvCtrlTypes()
    {
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                return (from info in db.EnvCtrlType
                        orderby info.TypeID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<EnvCtrlType>();
        }
    }

    public List<Tag> GetTags(List<string> typeIDs)
    {
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                return (from info in db.Tag
                        where typeIDs.Contains(info.TypeID)
                        select info).ToList();
            }
        }
        catch
        {
            return new List<Tag>();
        }
    }

    #endregion

    #region 即時資訊

    public string GetAllAnalogRealtimeInfos()
    {
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var infos = (from info in db.AnalogRealtimeInfo
                             select info).ToList();
                return JsonConvert.SerializeObject(infos);
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    public string GetAllDiscreteRealtimeInfos()
    {
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var infos = (from info in db.DiscreteRealtimeInfo
                             select info).ToList();
                return JsonConvert.SerializeObject(infos);
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion

    #region 溫溼度

    public List<TemperatureHumidityInfo> GetTemperatureHumidityLatest24HourInfos(string area)
    {
        DateTime now = DateTime.Now;
        DateTime endTime = now.Date.AddHours(now.Hour).AddHours(-1);
        if (now.Minute > 10)
        {
            endTime = now.Date.AddHours(now.Hour);
        }
        DateTime beginTime = endTime.AddHours(-23);

        string tempTagID = area + "_Temp";
        string humidityTagID = area + "_RH";
        List<string> tags = new List<string> { tempTagID, humidityTagID };
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var allInfos = (from info in db.AnalogHourInfo
                                where info.DataTime >= beginTime && info.DataTime <= endTime
                                orderby info.DataTime
                                select info).ToList();
                List<TemperatureHumidityInfo> result = new List<TemperatureHumidityInfo>();
                for (DateTime dateTime = beginTime; dateTime.CompareTo(endTime) <= 0; dateTime = dateTime.AddHours(1))
                {
                    var query = (from info in allInfos
                                 where tags.Contains(info.TagID) && info.DataTime == dateTime
                                 select info).ToList();
                    double temp = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == tempTagID)
                        .Select(x => x.Value).FirstOrDefault());
                    double humidity = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == humidityTagID)
                        .Select(x => x.Value).FirstOrDefault());
                    result.Add(new TemperatureHumidityInfo
                    {
                        InfoTime = dateTime,
                        Temperature = Math.Round(temp, 2),
                        Humidity = Math.Round(humidity, 2),
                    });
                }
                return result;
            }
        }
        catch
        {
            return new List<TemperatureHumidityInfo>();
        }
    }

    #endregion

    #region 電力

    private List<string> GetPowerTags(string dataClass)
    {
        List<string> tags = new List<string>();
        tags.Add(string.Format("HS{0}C_PM_VRS", dataClass));
        tags.Add(string.Format("HS{0}C_PM_VST", dataClass));
        tags.Add(string.Format("HS{0}C_PM_VTR", dataClass));
        tags.Add(string.Format("HS{0}C_PM_AR", dataClass));
        tags.Add(string.Format("HS{0}C_PM_AS", dataClass));
        tags.Add(string.Format("HS{0}C_PM_AT", dataClass));
        tags.Add(string.Format("HS{0}C_PM_KW", dataClass));
        tags.Add(string.Format("HS{0}C_PM_PF", dataClass));
        return tags;
    }

    /// <summary>
    /// 取得電力日報資料
    /// </summary>
    /// <param name="date"></param>
    /// <param name="dataClass"></param>
    /// <returns></returns>
    public List<PowerReportInfo> GetPowerDayReport(DateTime date, string dataClass)
    {
        DateTime beginTime = date;
        DateTime endTime = beginTime.AddDays(1).AddHours(-1);
        List<string> tags = GetPowerTags(dataClass);

        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var allInfos = (from info in db.AnalogHourInfo
                                where info.DataTime >= beginTime && info.DataTime <= endTime
                                orderby info.DataTime
                                select info).ToList();
                List<PowerReportInfo> result = new List<PowerReportInfo>();
                for (DateTime dateTime = beginTime; dateTime.CompareTo(endTime) <= 0; dateTime = dateTime.AddHours(1))
                {
                    var query = (from info in allInfos
                                 where tags.Contains(info.TagID) && info.DataTime == dateTime
                                 select info).ToList();
                    double vab = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VRS")
                        .Select(x => x.Value).FirstOrDefault());
                    double vbc = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VST")
                        .Select(x => x.Value).FirstOrDefault());
                    double vca = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VTR")
                        .Select(x => x.Value).FirstOrDefault());
                    double ia = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AR")
                        .Select(x => x.Value).FirstOrDefault());
                    double ib = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AS")
                        .Select(x => x.Value).FirstOrDefault());
                    double ic = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AT")
                        .Select(x => x.Value).FirstOrDefault());
                    double kw = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_KW")
                        .Select(x => x.Value).FirstOrDefault());
                    double factor = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_PF")
                        .Select(x => x.Value).FirstOrDefault());

                    result.Add(new PowerReportInfo
                    {
                        Year = dateTime.Year,
                        Month = dateTime.Month,
                        Day = dateTime.Day,
                        Hour = dateTime.Hour,
                        Vab = vab,
                        Vbc = vbc,
                        Vca = vca,
                        Ia = ia,
                        Ib = ib,
                        Ic = ic,
                        Kw = kw,
                        Factor = factor,
                    });
                }
                return result;
            }
        }
        catch
        {
            return new List<PowerReportInfo>();
        }
    }

    /// <summary>
    /// 取得電力月報資料
    /// </summary>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <param name="dataClass"></param>
    /// <returns></returns>
    public List<PowerReportInfo> GetPowerMonthReport(int year, int month, string dataClass)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        List<string> tags = GetPowerTags(dataClass);
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var allInfos = (from info in db.AnalogDayInfo
                                where info.DataTime >= beginDate && info.DataTime <= endDate
                                orderby info.DataTime
                                select info).ToList();
                List<PowerReportInfo> result = new List<PowerReportInfo>();
                for (DateTime date = beginDate; date.CompareTo(endDate) <= 0; date = date.AddDays(1))
                {
                    var query = (from info in allInfos
                                 where tags.Contains(info.TagID) && info.DataTime == date
                                 select info).ToList();

                    double vab = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VRS")
                      .Select(x => x.Value).FirstOrDefault());
                    double vbc = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VST")
                        .Select(x => x.Value).FirstOrDefault());
                    double vca = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VTR")
                        .Select(x => x.Value).FirstOrDefault());
                    double ia = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AR")
                        .Select(x => x.Value).FirstOrDefault());
                    double ib = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AS")
                        .Select(x => x.Value).FirstOrDefault());
                    double ic = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AT")
                        .Select(x => x.Value).FirstOrDefault());
                    double kw = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_KW")
                        .Select(x => x.Value).FirstOrDefault());
                    double factor = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_PF")
                        .Select(x => x.Value).FirstOrDefault());

                    result.Add(new PowerReportInfo
                    {
                        Year = date.Year,
                        Month = date.Month,
                        Day = date.Day,
                        Vab = vab,
                        Vbc = vbc,
                        Vca = vca,
                        Ia = ia,
                        Ib = ib,
                        Ic = ic,
                        Kw = kw,
                        Factor = factor,
                    });
                }
                return result;
            }
        }
        catch
        {
            return new List<PowerReportInfo>();
        }
    }

    /// <summary>
    /// 取得電力年報資料
    /// </summary>
    /// <param name="year"></param>
    /// <param name="dataClass"></param>
    /// <returns></returns>
    public List<PowerReportInfo> GetPowerYearReport(int year, string dataClass)
    {
        List<string> tags = GetPowerTags(dataClass);
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var allInfos = (from info in db.AnalogMonthInfo
                                where info.Year == year
                                orderby info.Month
                                select info).ToList();
                List<PowerReportInfo> result = new List<PowerReportInfo>();
                for (int month = 1; month <= 12; month++)
                {
                    var query = (from info in allInfos
                                 where tags.Contains(info.TagID) && info.Month == month
                                 select info).ToList();
                    double vab = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VRS")
                       .Select(x => x.Value).FirstOrDefault());
                    double vbc = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VST")
                        .Select(x => x.Value).FirstOrDefault());
                    double vca = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_VTR")
                        .Select(x => x.Value).FirstOrDefault());
                    double ia = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AR")
                        .Select(x => x.Value).FirstOrDefault());
                    double ib = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AS")
                        .Select(x => x.Value).FirstOrDefault());
                    double ic = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_AT")
                        .Select(x => x.Value).FirstOrDefault());
                    double kw = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_KW")
                        .Select(x => x.Value).FirstOrDefault());
                    double factor = IISI.Util.NumericUtility.ToHydValue(query.Where(x => x.TagID == "HS" + dataClass + "C_PM_PF")
                        .Select(x => x.Value).FirstOrDefault());

                    result.Add(new PowerReportInfo
                    {
                        Year = year,
                        Month = month,
                        Vab = vab,
                        Vbc = vbc,
                        Vca = vca,
                        Ia = ia,
                        Ib = ib,
                        Ic = ic,
                        Kw = kw,
                        Factor = factor,
                    });
                }
                return result;
            }
        }
        catch
        {
            return new List<PowerReportInfo>();
        }
    }

    #endregion

    #region 告警

    public double GetWarningValue(string tagID, string fieldName)
    {
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                var info = (from item in db.Alarm
                            where item.TagID == tagID
                            select item).FirstOrDefault();
                PropertyInfo pi = info.GetType().GetProperty(fieldName);
                Object value = pi.GetValue(info, null);
                if (value != null)
                {
                    return Convert.ToDouble(value);
                }
            }
        }
        catch
        {
        }
        return NumericUtility.NoHydData;
    }

    #endregion

    #region 告警事件

    public List<EnvCtrlMessage> GetEvents(string typeID, string sourceID, DateTime beginDate, DateTime endDate)
    {
        List<string> allTypes = new List<string>();
        if (typeID.Equals("all"))
        {
            allTypes.AddRange(EnvCtrlController.Instance.GetAllEnvCtrlTypes().Select(x => x.TypeID).ToList());
        }
        else
        {
            allTypes.Add(typeID);
        }
        List<Tag> allTags = GetTags(allTypes);
        try
        {
            using (HushanEnvCtrlEntities db = new HushanEnvCtrlEntities())
            {
                return (from message in db.Message.AsEnumerable()
                        where message.SourceID == sourceID
                        && message.DateTime >= beginDate && message.DateTime <= endDate
                        && allTags.Select(x => x.TagID).Contains(message.TagID)
                        join tag in db.Tag on message.TagID equals tag.TagID
                        join type in db.EnvCtrlType on tag.TypeID equals type.TypeID
                        orderby message.DateTime
                        select new EnvCtrlMessage
                        {
                            InfoTime = message.DateTime,
                            Statement = message.Statement,
                            TagID = message.TagID,
                            TypeID = type.TypeID,
                            TypeName = type.TypeName,
                        }).ToList();
            }
        }
        catch
        {
            return new List<EnvCtrlMessage>();
        }
    }

    #endregion

    #endregion
}