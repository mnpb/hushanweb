﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CountyController 的摘要描述
/// </summary>
public class CountyController
{
    #region Properties

    public static CountyController Instance
    {
        get
        {
            return new CountyController();
        }
    }

    #endregion

    #region Methods

    public List<County> GetAllCounties()
    {
        string cacheKey = CacheKeyInfo.CountyCacheKey;

        var result = CacheUtility.GetFromCache(cacheKey);
        if (result != null)
        { // 從cache抓取
            return (result as List<County>);
        }

        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var allInfos = (from info in db.County
                                orderby info.CountyID
                                select info).ToList();
                CacheUtility.AddToCache(cacheKey, allInfos, DateTime.UtcNow.AddYears(1)); // 有效期限1年
                return allInfos;
            }
        }
        catch
        {
            return new List<County>();
        }
    }

    public string GetCountyName(string countyID)
    {
        try
        {
            return (from info in GetAllCounties()
                    where info.CountyID == countyID
                    select info.CountyName).FirstOrDefault();
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion
}