﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// MessageController 的摘要描述
/// </summary>
public class MessageController
{
    #region Properties

    public static MessageController Instance
    {
        get
        {
            return new MessageController();
        }
    }

    #endregion

    #region Methods

    public List<AutoSmsConditionCompoundInfo> GetAllAutoSmsConditionCompoundInfos()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var result = (from condition in db.AutoSmsCondition
                              join type in db.AutoSmsType on condition.AutoSmsTypeID equals type.AutoSmsTypeID
                              orderby type.AutoSmsTypeID, condition.AutoSmsConditionID
                              select new AutoSmsConditionCompoundInfo
                              {
                                  AutoSmsConditionID = condition.AutoSmsConditionID,
                                  AutoSmsConditionName = condition.Name,
                                  AutoSmsTypeID = type.AutoSmsTypeID,
                                  AutoSmsTypeName = type.Name,
                                  IsActive = condition.IsActive,
                              }).ToList();
                foreach (var item in result)
                {
                    item.SmsGroups = GetSmsGroupsByCondition(item.AutoSmsConditionID);
                }
                return result;
            }
        }
        catch
        {
            return new List<AutoSmsConditionCompoundInfo>();
        }
    }

    public AutoSmsConditionCompoundInfo GetAutoSmsConditionCompoundInfo(int autoSmsConditionID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var result = (from condition in db.AutoSmsCondition
                              where condition.AutoSmsConditionID == autoSmsConditionID
                              join type in db.AutoSmsType on condition.AutoSmsTypeID equals type.AutoSmsTypeID
                              select new AutoSmsConditionCompoundInfo
                              {
                                  AutoSmsConditionID = condition.AutoSmsConditionID,
                                  AutoSmsConditionName = condition.Name,
                                  AutoSmsTypeID = type.AutoSmsTypeID,
                                  AutoSmsTypeName = type.Name,
                                  IsActive = condition.IsActive,
                              }).FirstOrDefault();
                result.SmsGroups = GetSmsGroupsByCondition(autoSmsConditionID);
                return result;
            }
        }
        catch
        {
            return null;
        }
    }

    public bool SetAutoSmsConditionCompoundInfo(AutoSmsConditionCompoundInfo newInfo)
    {
        try
        {
            int autoSmsConditionID = newInfo.AutoSmsConditionID;
            using (HushanEntities db = new HushanEntities())
            {
                #region 更新「啟用與否?」

                var query1 = (from info in db.AutoSmsCondition
                              where info.AutoSmsConditionID == autoSmsConditionID
                              select info).FirstOrDefault();
                if (query1 != null)
                { // 更新
                    query1.IsActive = newInfo.IsActive;
                }

                #endregion

                #region 更新「通報群組」

                var query2 = (from info in db.AutoSmsConditionToSmsGroup
                              where info.AutoSmsConditionID == autoSmsConditionID
                              select info).ToList();
                if (query2 != null)
                { // 已有，先刪除所有群組
                    foreach (var item in query2)
                    {
                        db.AutoSmsConditionToSmsGroup.Remove(item);
                    }
                }
                // 然後一次新增所有群組
                foreach (var group in newInfo.SmsGroups)
                {
                    db.AutoSmsConditionToSmsGroup.Add(new AutoSmsConditionToSmsGroup
                    {
                        AutoSmsConditionID = autoSmsConditionID,
                        SmsGroupID = group.SmsGroupID,
                    });
                }

                #endregion

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    private List<SmsGroup> GetSmsGroupsByCondition(int autoSmsConditionID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from condition in db.AutoSmsCondition
                        where condition.AutoSmsConditionID == autoSmsConditionID
                        join mappingSmsGroup in db.AutoSmsConditionToSmsGroup on condition.AutoSmsConditionID equals mappingSmsGroup.AutoSmsConditionID
                        join smsGroup in db.SmsGroup on mappingSmsGroup.SmsGroupID equals smsGroup.SmsGroupID
                        select smsGroup).ToList();
            }
        }
        catch
        {
            return new List<SmsGroup>();
        }
    }

    public List<SmsGroup> GetAllSmsGroups()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SmsGroup
                        orderby info.SmsGroupID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SmsGroup>();
        }
    }

    public bool AddSmsGroup(SmsGroup smsGroup)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.SmsGroup
                               where info.SmsGroupID == smsGroup.SmsGroupID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.Name = smsGroup.Name;
                    oldInfo.IsEnable = true;
                }
                else
                {
                    db.SmsGroup.Add(smsGroup);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeletetSmsGroup(short smsGroupID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldMappingMembers = (from info in db.SmsGroupToMember
                                         where info.SmsGroupID == smsGroupID
                                         select info).ToList();
                foreach (var item in oldMappingMembers)
                {
                    db.SmsGroupToMember.Remove(item);
                }

                var oldInfo = (from info in db.SmsGroup
                               where info.SmsGroupID == smsGroupID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.SmsGroup.Remove(oldInfo);
                }

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public List<SmsMember> GetAllSmsMembers()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SmsMember
                        orderby info.SmsMemberID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SmsMember>();
        }
    }

    public bool AddSmsMember(SmsMember smsMember)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.SmsMember
                               where info.SmsMemberID == smsMember.SmsMemberID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.Name = smsMember.Name;
                    oldInfo.MobileNumber = smsMember.MobileNumber;
                }
                else
                {
                    db.SmsMember.Add(smsMember);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeletetSmsMember(int smsMemberID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldMappingMembers = (from info in db.SmsGroupToMember
                                         where info.SmsMemberID == smsMemberID
                                         select info).ToList();
                foreach (var item in oldMappingMembers)
                {
                    db.SmsGroupToMember.Remove(item);
                }

                var oldInfo = (from info in db.SmsMember
                               where info.SmsMemberID == smsMemberID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.SmsMember.Remove(oldInfo);
                }

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public List<SmsMember> GetSmsMembersByGroup(List<SmsGroup> smsGroups)
    {
        try
        {
            var groupIDs = smsGroups.Select(x => x.SmsGroupID).ToList();
            using (HushanEntities db = new HushanEntities())
            {
                return (from member in db.SmsMember
                        join mapping in db.SmsGroupToMember on member.SmsMemberID equals mapping.SmsMemberID
                        where groupIDs.Contains(mapping.SmsGroupID)
                        select member).Distinct().ToList();
            }
        }
        catch
        {
            return new List<SmsMember>();
        }
    }

    public bool SetMemberToGroup(short groupID, List<int> memberIDs)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.SmsGroupToMember
                               where info.SmsGroupID == groupID
                               select info).ToList();
                foreach (var info in oldInfo)
                {
                    db.SmsGroupToMember.Remove(info);
                }
                foreach (var memberID in memberIDs)
                {
                    db.SmsGroupToMember.Add(new SmsGroupToMember
                    {
                        SmsGroupID = groupID,
                        SmsMemberID = memberID,
                    });
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public List<SmsLog> GetSmsLogs(string sendTypeID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            bool sendType = sendTypeID.Equals("1") ? true : false; // 1:手動，0:自動
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SmsLog
                        where info.IsManualSend == sendType && info.SendTime >= beginTime && info.SendTime <= endTime
                        orderby info.SendTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SmsLog>();
        }
    }

    public string GetSmsGroupsDescription(object o)
    {
        StringBuilder description = new StringBuilder();
        try
        {
            if ((o as List<SmsGroup>) != null)
            {
                foreach (var item in (o as List<SmsGroup>))
                {
                    description.Append(item.Name + "。");
                }
            }
            return description.ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    #region 範本

    public List<SmsContentsTemplate> GetAllSmsContentsTemplates()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SmsContentsTemplate
                        orderby info.SmsContentsTemplateID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<SmsContentsTemplate>();
        }
    }

    public SmsContentsTemplate GetSmsContentsTemplate(short templateID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.SmsContentsTemplate
                        where info.SmsContentsTemplateID == templateID
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    public bool AddSmsContentsTemplate(string name, string contentsTemplate)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                db.SmsContentsTemplate.Add(new SmsContentsTemplate
                {
                    Name = name,
                    ContentsTemplate = contentsTemplate,
                });
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region 傳送簡訊

    public void SendSms(string sender, SmsMember receiver, string smsContent, ref string returnMessage)
    {
        try
        {
            wsSmsService.SmsService ws = new wsSmsService.SmsService();
            ws.SendSms(
                sender,
                new wsSmsService.SmsMember
                {
                    Name = receiver.Name,
                    MobileNumber = receiver.MobileNumber,
                },
                smsContent,
                ref  returnMessage
                );
        }
        catch
        {
        }
    }

    #endregion

    #endregion
}