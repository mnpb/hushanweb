﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// FloodEventController 的摘要描述
/// </summary>
public class FloodEventController
{
    #region Properties

    public static FloodEventController Instance
    {
        get
        {
            return new FloodEventController();
        }
    }

    #endregion

    #region Methods

    public List<FloodEvent> GetAllFloodEvents()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.FloodEvent
                        orderby info.StartTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<FloodEvent>();
        }
    }

    public FloodEvent GetFloodEvent(string eventID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.FloodEvent
                        where info.ID == eventID
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return new FloodEvent();
        }
    }

    public bool AddEvent(FloodEvent floodEvent)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.FloodEvent
                               where info.ID == floodEvent.ID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.FloodEvent.Remove(oldInfo);
                    db.SaveChanges();
                }

                db.FloodEvent.Add(floodEvent);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteEvent(string eventID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.FloodEvent
                               where info.ID == eventID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.FloodEvent.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion
}