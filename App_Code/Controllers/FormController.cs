﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// FormController 的摘要描述
/// </summary>
public class FormController
{
    #region Properties

    public static FormController Instance
    {
        get
        {
            return new FormController();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 新增「鯉魚潭水庫管制區進入作業申請單」
    /// </summary>
    /// <param name="formArea"></param>
    /// <returns></returns>
    public bool AddAreaForm(FormArea formArea)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                db.FormArea.Add(formArea);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 取得「鯉魚潭水庫管制區進入作業申請單」
    /// </summary>
    /// <param name="areaID"></param>
    /// <returns></returns>
    public FormArea GetAreaForm(int areaID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.FormArea
                        where info.AreaID == areaID
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    /// <summary>
    /// 刪除「鯉魚潭水庫管制區進入作業申請單」
    /// </summary>
    /// <param name="areaID"></param>
    /// <returns></returns>
    public bool DeleteAreaForm(int areaID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var query = (from info in db.FormArea
                             where info.AreaID == areaID
                             select info).FirstOrDefault();
                if (query != null)
                {
                    db.FormArea.Remove(query);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    /// <summary>
    /// 新增「鯉魚潭水庫管理中心－閘門啟閉申請單」
    /// </summary>
    /// <param name="formGate"></param>
    /// <returns></returns>
    public bool AddGateApplyForm(FormGate formGate)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                db.FormGate.Add(formGate);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除「鯉魚潭水庫管理中心－閘門啟閉申請單」
    /// </summary>
    /// <param name="gateApplyID"></param>
    /// <returns></returns>
    public bool DeleteGateApplyForm(int gateApplyID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var query = (from info in db.FormGate
                             where info.GateApplyID == gateApplyID
                             select info).FirstOrDefault();
                if (query != null)
                {
                    db.FormGate.Remove(query);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    public FormGate GetGateApplyForm(int gateApplyID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.FormGate
                        where info.GateApplyID == gateApplyID
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    /// <summary>
    /// 新增「鯉魚潭水庫出水工閘門操作紀錄表」
    /// </summary>
    /// <param name="formGateOperate"></param>
    /// <returns></returns>
    public bool AddGateOpraForm(FormGateOperate formGateOperate)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                db.FormGateOperate.Add(formGateOperate);
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除「鯉魚潭水庫出水工閘門操作紀錄表」
    /// </summary>
    /// <param name="gateOpraID"></param>
    /// <returns></returns>
    public bool DeleteGateOpraForm(int gateOpraID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var query = (from info in db.FormGateOperate
                             where info.GateOpraID == gateOpraID
                             select info).FirstOrDefault();
                if (query != null)
                {
                    db.FormGateOperate.Remove(query);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    /// <summary>
    /// 取得「鯉魚潭水庫出水工閘門操作紀錄表」
    /// </summary>
    /// <param name="gateOpraID"></param>
    /// <returns></returns>
    public FormGateOperate GetGateOpraForm(int gateOpraID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.FormGateOperate
                        where info.GateOpraID == gateOpraID
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return null;
        }
    }

    #endregion
}