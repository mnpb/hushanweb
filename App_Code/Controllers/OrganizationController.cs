﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// OrganizationController 的摘要描述
/// </summary>
public class OrganizationController
{
    #region Properties

    public static OrganizationController Instance
    {
        get
        {
            return new OrganizationController();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 取得所有機關
    /// </summary>
    /// <returns></returns>
    public List<Organization> GetAllOrganizations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.Organization
                        orderby info.OrganizationID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<Organization>();
        }
    }

    public string GetOrganizationName(string organizationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.Organization
                        where info.OrganizationID == organizationID
                        select info.OrganizationName).FirstOrDefault();
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion
}