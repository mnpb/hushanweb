﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// BasinController 的摘要描述
/// </summary>
public class BasinController
{
    #region Properties

    public static BasinController Instance
    {
        get
        {
            return new BasinController();
        }
    }

    #endregion

    #region Methods

    public List<Basin> GetAllBasins()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.Basin
                        orderby info.BasinID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<Basin>();
        }
    }

    public string GetBasinName(string basinID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.Basin
                        where info.BasinID == basinID
                        select info.BasinName).FirstOrDefault();
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion
}