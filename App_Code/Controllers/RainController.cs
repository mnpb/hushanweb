﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

/// <summary>
/// RainController 的摘要描述
/// </summary>
public class RainController
{
    #region Properties

    private static RainController instance = new RainController();
    public static RainController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    #region 基本資料

    /// <summary>
    /// 取得所有雨量站基本資料
    /// </summary>
    /// <returns></returns>
    public List<RainStationBase> GetAllStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from station in db.RainStationBase
                        orderby station.StationID
                        select station).ToList();
            }
        }
        catch
        {
            return new List<RainStationBase>();
        }
    }

    /// <summary>
    /// 新增雨量站基本資料
    /// </summary>
    /// <param name="newStation"></param>
    /// <returns></returns>
    public bool AddStation(RainStationBase newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.RainStationBase
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有資料，只需更新
                    oldInfo.StationName = newStation.StationName;
                    oldInfo.Address = newStation.Address;
                    oldInfo.Longitude = newStation.Longitude;
                    oldInfo.Latitude = newStation.Latitude;
                    oldInfo.CountyID = newStation.CountyID;
                    oldInfo.TownID = newStation.TownID;
                    oldInfo.OrganizationID = newStation.OrganizationID;
                }
                else
                { // 原無資料，需新增
                    db.RainStationBase.Add(newStation);
                }

                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 刪除雨量站基本資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <returns></returns>
    public bool DeleteStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.RainStationBase
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                { // 已有此測站
                    // 刪除
                    db.RainStationBase.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #region 水情資料

    public List<RainTenMinute> GetTenMinuteInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.RainTenMinute
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<RainTenMinute>();
        }
    }

    public bool AddTenMinuteInfo(RainTenMinute tenMinuteInfo)
    {
        try
        {
            string stationID = tenMinuteInfo.StationID;
            DateTime infoTime = tenMinuteInfo.InfoTime;
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.RainTenMinute
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.RainQty = tenMinuteInfo.RainQty;
                }
                else
                {
                    db.RainTenMinute.Add(tenMinuteInfo);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
        }
        return false;
    }

    public bool DeleteTenMinuteInfo(string stationID, DateTime infoTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.RainTenMinute
                               where info.StationID == stationID && info.InfoTime == infoTime
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.RainTenMinute.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    /// <summary>
    /// 取得小時資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="beginTime"></param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public List<RainHour> GetHourInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.RainHour
                        where info.StationID == stationID && info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<RainHour>();
        }
    }

    /// <summary>
    /// 取得日資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="beginDate"></param>
    /// <param name="endDate"></param>
    /// <returns></returns>
    public List<RainDay> GetDayInfos(string stationID, DateTime beginDate, DateTime endDate)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.RainDay
                        where info.StationID == stationID && info.InfoDate >= beginDate && info.InfoDate <= endDate
                        orderby info.InfoDate
                        select info).ToList();
            }
        }
        catch
        {
            return new List<RainDay>();
        }
    }

    /// <summary>
    /// 取得月資料
    /// </summary>
    /// <param name="stationID"></param>
    /// <param name="beginDate"></param>
    /// <param name="endDate"></param>
    /// <returns></returns>
    public List<RainMonth> GetMonthInfos(string stationID, int year)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.RainMonth
                        where info.StationID == stationID && info.InfoYear == year
                        orderby info.InfoMonth
                        select info).ToList();
            }
        }
        catch
        {
            return new List<RainMonth>();
        }
    }

    #endregion

    #region 警戒相關

    public List<AlertRainStation> GetAllAlertStations()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.AlertRainStation
                        orderby info.StationID
                        select info).ToList();
            }
        }
        catch
        {
            return new List<AlertRainStation>();
        }
    }

    public bool AddAlertStation(AlertRainStation newStation)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.AlertRainStation
                               where info.StationID == newStation.StationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    oldInfo.M10 = newStation.M10;
                    oldInfo.H1 = newStation.H1;
                    oldInfo.H3 = newStation.H3;
                    oldInfo.H6 = newStation.H6;
                    oldInfo.H12 = newStation.H12;
                    oldInfo.H24 = newStation.H24;
                    oldInfo.H48 = newStation.H48;
                    oldInfo.H72 = newStation.H72;
                    oldInfo.D1 = newStation.D1;
                    oldInfo.D2 = newStation.D2;
                    oldInfo.D3 = newStation.D3;
                    oldInfo.Active = newStation.Active;
                }
                else
                {
                    db.AlertRainStation.Add(newStation);
                }
                db.SaveChanges();
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteAlertStation(string stationID)
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                var oldInfo = (from info in db.AlertRainStation
                               where info.StationID == stationID
                               select info).FirstOrDefault();
                if (oldInfo != null)
                {
                    db.AlertRainStation.Remove(oldInfo);
                    db.SaveChanges();
                    return true;
                }
            }
        }
        catch
        {
        }
        return false;
    }

    #endregion

    #endregion
}