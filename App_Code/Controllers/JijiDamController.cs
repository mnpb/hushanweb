﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// JijiDamController 的摘要描述
/// </summary>
public class JijiDamController
{
    #region Properties

    private static JijiDamController instance = new JijiDamController();
    public static JijiDamController Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 取得水庫小時資訊
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public List<JijiHour> GetJijiDamHourInfos(DateTime date)
    {
        DateTime beginTime = date;
        DateTime endTime = date.AddDays(1).AddHours(-1);
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.JijiHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<JijiHour>();
        }
    }

    /// <summary>
    /// 取得水庫日資訊
    /// </summary>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <returns></returns>
    public List<JijiHour> GetJijiDamDayInfos(int year, int month)
    {
        DateTime beginTime = new DateTime(year, month, 1);
        DateTime endTime = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.JijiHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime && info.InfoTime.Hour == 0
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<JijiHour>();
        }
    }

    /// <summary>
    /// 取得水庫月資訊
    /// </summary>
    /// <param name="year"></param>
    /// <returns></returns>
    public List<JijiHour> GetJijiDamMonthInfos(int year)
    {
        DateTime beginTime = new DateTime(year, 1, 1);
        DateTime endTime = new DateTime(year, 12, 31);
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.JijiHour
                        where info.InfoTime >= beginTime && info.InfoTime <= endTime
                        && info.InfoTime.Day % 10 == 1 && info.InfoTime.Hour == 0 && info.InfoTime.Day <= 21
                        orderby info.InfoTime
                        select info).ToList();
            }
        }
        catch
        {
            return new List<JijiHour>();
        }
    }

    #endregion
}