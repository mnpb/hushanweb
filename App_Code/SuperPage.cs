﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class SuperPage : System.Web.UI.Page
{
    private List<Menu> MenuList
    {
        get
        {
            if (ViewState["MenuList"] == null)
                using (Menu Menu = new Menu())
                    ViewState["MenuList"] = Menu.GetMenuList();

            return (List<Menu>)ViewState["MenuList"];
        }
    }

    public Account LoginAccount
    {
        get 
        {
            if (ViewState["LoginAccount"] == null)
                using (Account Account = new Account())
                    ViewState["LoginAccount"] = Account.GetAccount((int)Session["AccountID"]);

            return (Account)ViewState["LoginAccount"];
        }    
    }

    protected void Page_PreInit(object sender, EventArgs e) 
    {
        // Session["AccountID"] = 1;      //開發用，佈署時要把它註解掉

        //若未處於登錄狀態直接導到首頁
        if (Session["AccountID"] == null)
            Response.Redirect("~/index.aspx");
        else  //檢查登錄人員是否有在權限選單內
        {
            if (!IsPostBack)   //第一次登入頁面時才檢查
            {
                List<GroupToMenu> LoginGroupToMenuList = new List<GroupToMenu>();

                //取得目前選單
                Menu ThisMenu = MenuList.Find(delegate(Menu Mu) { return ResolveUrl(Mu.NavigateUrl) == Request.RawUrl; });

                //取得登入人員擁有的選單權限
                using (Group Group = new Group())
                    using (GroupToMenu GroupToMenu = new GroupToMenu())
                        LoginGroupToMenuList = GroupToMenu.GetGroupToMenuByGroupList(Group.GetGroupList(LoginAccount.AccountToGroupList));

                //若目前的選單權限沒有包含目前選單，則導到首頁
                if (LoginGroupToMenuList.Find(delegate(GroupToMenu GTM) { return GTM.MenuID == ThisMenu.MenuID; }) == null)
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('您沒有權限登入此頁'); location.href='" + ResolveUrl("~/index.aspx") + "'</SCRIPT>");
            }
        }
    }
}