﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// WaterService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class WaterService : System.Web.Services.WebService
{

    public WaterService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod(Description = "取得所有水位站")]
    public dynamic GetAllStations()
    {
        return WaterController.Instance.GetAllStations();
    }


    [WebMethod(Description = "取得水位站小時資料")]
    public dynamic GetHourInfos(string stationID, DateTime date)
    {
        return WaterController.Instance.GetHourInfos(stationID, date, date.AddHours(23));
    }
    [WebMethod(Description = "取得水位站小時資料")]
    public dynamic GetHourInfosForGrid(string stationID, DateTime date)
    {
        var allInfos = GetHourInfos(stationID, date);
        try
        {
            var resultObj = new
            {
                total = 1, 
                page = 1, 
                records = allInfos.Count, 
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    //[WebMethod(Description = "編輯水位站小時資料")]
    //public void EditHourInfos(string stationID, DateTime date)
    //{
    //    int a = 0;
    //}


    [WebMethod(Description = "取得水位站日資料")]
    public dynamic GetDayInfos(string stationID, int year, int month)
    {
        return WaterController.Instance.GetDayInfos(stationID, year, month);
    }
    [WebMethod(Description = "取得水位站日資料")]
    public dynamic GetDayInfosForGrid(string stationID, int year, int month)
    {
        var allInfos = WaterController.Instance.GetDayInfos(stationID, year, month);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos.ToList(), // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }


    [WebMethod(Description = "取得水位站月資料")]
    public dynamic GetMonthInfos(string stationID, int year)
    {
        return WaterController.Instance.GetMonthInfos(stationID, year);
    }
    [WebMethod(Description = "取得水位站月資料")]
    public dynamic GetMonthInfosForGrid(string stationID, int year)
    {
        var allInfos = WaterController.Instance.GetMonthInfos(stationID, year);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos.ToList(), // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
}
