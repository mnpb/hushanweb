﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Service 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class CompositeRealtimeService : System.Web.Services.WebService
{
    #region Properties

    string HushanReservoirFieldInfo = "水位,DamLevel,M;有效蓄容量,DamVolume,萬噸;蓄水率,DamVolumeRate,%;總入流量,TotalInFlow,CMS;總放流量,TotalOutFlow,CMS";

    string HushanRainFieldInfo = "湖山水庫,HushanDayRainQty,mm;蒸發量,DamEvaporation,mm";

    string MenanRiverFieldInfo = "梅南橋,MenanRiverLevel,M;石榴班橋,ShihliubanRiverLevel,M";

    string HushanOutletWorksFieldInfo = "緊急排水閘門,OutletWorks1MainFlow,CMS;河道噴流閥,OutletWorks1BypassFlow,CMS;第二(預留),OutletWorks2Flow,CMS";
    //string HushanOutletWorksFieldInfo = "緊急排水閘門,OutletWorks1MainGate,CM,OutletWorks1MainFlow,CMS;河道噴流閥,OutletWorks1BypassGate,%,OutletWorks1BypassFlow,CMS;第二(預留),OutletWorks2Gate,CM,OutletWorks2Flow,CMS";

    string HushanSpillwayFieldInfo = "溢洪道溢洪量,SpillwayOverFlow,CMS";

    string JiJiWeirFieldInfo = "水位,Waterlevel,M;有效庫容,Volume,萬噸;入流量,InFlow,CMS;總放流量,TotalOutFlow,CMS;南北岸取水量,SouthNorthSupplyFlow,CMS;公共給水,PublicSupplyFlow,CMS;工業用水,IndustrialSupplyFlow,CMS;農業用水,IrrigationSupplyFlow,CMS";

    string DownstreamWaterPipeFieldInfo = "濁度,DownstreamWaterPipeTurbidity,NTU;總供水量,DownstreamWaterPipeTotalSupplyFlow,CMS;湖山淨水場,HushanPureWaterPlantFlow,CMS;林內淨水廠,LinneiPureWaterPlantFlow,CMS;林內前處理場,LinneiPreprocessPlantFlow,CMS";

    string TongtouWeirFieldInfo = "取水口外水位,TongtouWeirLevel,M;桶頭堰濁度,TongtouWeirTurbidity,NTU;巴歇爾量水堰流量,TongtouWeirParshallFlumeFlow,CMS;排砂道放水量,TongtouWeirSandSluicewayFlow,CMS;溢洪道溢流量,TongtouWeirSpillwayFlow,CMS;魚道放流量,TongtouWeirFishwayFlow,CMS;董安圳放水量,TongtouWeirDongAnChunFlow,CMS";

    string TongtouWeirRainFieldInfo = "桶頭堰上游平均,TongtouWeirUpstreamAvgDayRainQty,mm;豐山,FengshanDayRainQty,mm;草嶺,CaolingDayRainQty,mm;桶頭,TongtouDayRainQty,mm;桶頭堰,TongtouWeirDayRainQty,mm";

    string ChingshuiRiverFieldInfo = "清水溪橋,ChingshuiRiverLevel,M,ChingshuiRiverFlow,CMS;瑞草橋,RuicaoLevel,M,RuicaoFlow,CMS;新行正橋,NewShinzhenRiverLevel,M,NewShinzhenRiverFlow,CMS";

    #endregion

    #region Methods

    public CompositeRealtimeService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public dynamic GetAllRealtimeInfos()
    {
        try
        {
            using (HushanEntities db = new HushanEntities())
            {
                return (from info in db.CompositeRealtimeInfo
                        select info).FirstOrDefault();
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    [WebMethod]
    public string GetHushanReservoir(dynamic realtimeData)
    {
        return GenGeneralTable(realtimeData, HushanReservoirFieldInfo, "table");
    }

    [WebMethod]
    public string GetHushanRain(dynamic realtimeData)
    {
        return GenGeneralTable(realtimeData, HushanRainFieldInfo, "table");
    }

    [WebMethod]
    public string GetMenanRiver(dynamic realtimeData)
    {
        return GenRiverTable(realtimeData, MenanRiverFieldInfo);
    }

    [WebMethod]
    public string GetDownstreamWaterPipe(dynamic realtimeData)
    {
        return GenGeneralTable(realtimeData, DownstreamWaterPipeFieldInfo, "table");
    }

    [WebMethod]
    public string GetHushanOutletWorks(dynamic realtimeData)
    {
        //return GenOutletWorksTable(realtimeData, HushanOutletWorksFieldInfo);
        return GenGeneralTable(realtimeData, "溢洪道,1;,1;,1", HushanSpillwayFieldInfo, "table table-bordered") + 
            GenGeneralTable(realtimeData, "出水工,1;,1;,1", HushanOutletWorksFieldInfo, "table table-bordered");
    }

    [WebMethod]
    public string GetTongtouWeir(dynamic realtimeData)
    {
        return GenGeneralTable(realtimeData, TongtouWeirFieldInfo, "table");
    }

    [WebMethod]
    public string GetTongtouWeirRain(dynamic realtimeData)
    {
        return GenGeneralTable(realtimeData, TongtouWeirRainFieldInfo, "table");
    }

    [WebMethod]
    public string GetChingshuiRiver(dynamic realtimeData)
    {
        return GenGeneralTable(realtimeData, ",1;水位,2;流量,2", ChingshuiRiverFieldInfo, "table table-bordered");
    }

    private string GenGeneralTable(dynamic realtimeData, string fieldInfo, string tableCss)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div class=table-responsive>");
        sb.Append(string.Format("<table class='{0}'>", tableCss));
        dynamic realInfo = JsonConvert.DeserializeObject<CompositeRealtimeInfo>(realtimeData);
        foreach (var field in fieldInfo.Split(';'))
        {
            sb.Append("<tr>");
            string[] oneFiled = field.Split(',');
            for (int idx = 0; idx < oneFiled.Length; idx++)
            {
                string fieldName = oneFiled[idx];
                string value = fieldName;
                if (idx.Equals(1))
                {
                    if (realInfo != null)
                    {
                        var pi = realInfo.GetType().GetProperty(fieldName);
                        if (pi != null)
                        {
                            try
                            {
                                value = pi.GetValue(realInfo, null).ToString();
                                if (fieldName.Equals("CapVolDamPrecent"))
                                {
                                    value = (pi.GetValue(realInfo, null) * 100).ToString();
                                }
                            }
                            catch
                            {
                                value = "-";
                            }
                            value = "<font color='#fff64c'>" + value + "</font>";
                        }
                    }
                }
                sb.Append(string.Format("<td>{0}</td>", value));
            }
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        sb.Append("</div>");
        return sb.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="realtimeData"></param>
    /// <param name="fieldInfo"></param>
    /// <returns></returns>
    private string GenGeneralTable(dynamic realtimeData, string headerFieldInfo, string fieldInfo, string tableCss)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div class=table-responsive>");
        sb.Append(string.Format("<table class='{0}'>", tableCss));

        // 標題
        sb.Append("<tr>");
        foreach (var fieldItem in headerFieldInfo.Split(';').ToList())
        {
            sb.Append(string.Format("<td colspan={1}>{0}</td>", fieldItem.Split(',')[0], fieldItem.Split(',')[1]));
        }
        //sb.Append(string.Format("<td>{0}</td><td colspan=2>{1}</td><td colspan=2>{2}</td>",
        //    headerFieldInfo.Split(',')[0], headerFieldInfo.Split(',')[1], headerFieldInfo.Split(',')[2]));
        sb.Append("</tr>");

        dynamic realInfo = JsonConvert.DeserializeObject<CompositeRealtimeInfo>(realtimeData);
        foreach (var field in fieldInfo.Split(';'))
        {
            sb.Append("<tr>");
            string[] oneFiled = field.Split(',');
            for (int idx = 0; idx < oneFiled.Length; idx++)
            {
                string fieldName = oneFiled[idx];
                string value = fieldName;
                if (idx.Equals(1) || idx.Equals(3))
                {
                    if (realInfo != null)
                    {
                        var pi = realInfo.GetType().GetProperty(fieldName);
                        if (pi != null)
                        {
                            try
                            {
                                value = pi.GetValue(realInfo, null).ToString();
                            }
                            catch
                            {
                                value = "-";
                            }
                            value = "<font color='#fff64c'>" + value + "</font>";
                        }
                    }
                }
                sb.Append(string.Format("<td>{0}</td>", value));
            }
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        sb.Append("</div>");
        return sb.ToString();
    }

    /// <summary>
    /// 河道資訊
    /// </summary>
    /// <param name="realtimeData"></param>
    /// <param name="fieldInfo"></param>
    /// <returns></returns>
    private string GenRiverTable(dynamic realtimeData, string fieldInfo)
    {
        return GenGeneralTable(realtimeData, fieldInfo, "table");
    }

    ///// <summary>
    ///// 出水工閘閥室
    ///// </summary>
    ///// <param name="realtimeData"></param>
    ///// <param name="fieldInfo"></param>
    ///// <param name="type"></param>
    ///// <returns></returns>
    //private string GenOutletWorksTable(dynamic realtimeData, string fieldInfo)
    //{
    //    return GenGeneralTable(realtimeData, fieldInfo, "table table-bordered");
    //    //return GenGeneralTable(realtimeData, ",開度,流量", fieldInfo);
    //}

    private dynamic GetJiJiWeirRealtimeInfo()
    {
        try
        {
            wsWracbJiJi.Jiji ws = new wsWracbJiJi.Jiji();
            DataSet dataSet = ws.GetJijiLastHrHydrology();
            return dataSet.Tables[0].AsEnumerable().Select(dataRow => new
            {
                Waterlevel = dataRow.Field<double>("WL"),
                Volume = dataRow.Field<double>("VOL"),
                InFlow = dataRow.Field<double>("_IF"),
                TotalOutFlow = dataRow.Field<double>("TotalOutFlow"),
                SouthNorthSupplyFlow = dataRow.Field<double>("NorthFlow") + dataRow.Field<double>("SouthFlow"),
                PublicSupplyFlow = dataRow.Field<double>("PUB_SUPPLY"),
                IndustrialSupplyFlow = dataRow.Field<double>("INDUSTRY_SUPPLY"),
                IrrigationSupplyFlow = dataRow.Field<double>("CHHUA_IRR_AREA") + dataRow.Field<double>("YL_IRR_AREA"),
            }).FirstOrDefault();
        }
        catch
        {
            return string.Empty;
        }
    }

    [WebMethod]
    public string GetJiJiWeir(dynamic realtimeData)
    {
        return GenJiJiWeirTable(GetJiJiWeirRealtimeInfo(), JiJiWeirFieldInfo);
    }
    private string GenJiJiWeirTable(dynamic realtimeData, string fieldInfo)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div class=table-responsive>");
        sb.Append("<table class='table'>"); //  table-bordered
        dynamic realInfo = realtimeData;
        foreach (var field in fieldInfo.Split(';'))
        {
            sb.Append("<tr>");
            string[] oneFiled = field.Split(',');
            for (int idx = 0; idx < oneFiled.Length; idx++)
            {
                string fieldName = oneFiled[idx];
                string value = fieldName;
                if (idx.Equals(1))
                {
                    if (realInfo != null)
                    {
                        var pi = realInfo.GetType().GetProperty(fieldName);
                        if (pi != null)
                        {
                            try
                            {
                                value = pi.GetValue(realInfo, null).ToString();
                            }
                            catch
                            {
                                value = "-";
                            }
                            value = "<font color='#fff64c'>" + value + "</font>";
                        }
                    }
                }
                sb.Append(string.Format("<td>{0}</td>", value));
            }
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        sb.Append("</div>");
        return sb.ToString();
    }

    #endregion
}
