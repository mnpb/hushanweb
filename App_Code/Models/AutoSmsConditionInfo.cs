﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AutoSmsConditionInfo 的摘要描述
/// </summary>
public class AutoSmsConditionCompoundInfo
{
    public int AutoSmsConditionID
    {
        get;
        set;
    }
    public string AutoSmsConditionName
    {
        get;
        set;
    }
    public short AutoSmsTypeID
    {
        get;
        set;
    }
    public string AutoSmsTypeName
    {
        get;
        set;
    }
    public bool IsActive
    {
        get;
        set;
    }
    public List<SmsGroup> SmsGroups
    {
        get;
        set;
    }
}