﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// EnvCtrlMessage 的摘要描述
/// </summary>
public class EnvCtrlMessage
{
    public DateTime InfoTime
    {
        get;
        set;
    }
    public string TypeID
    {
        get;
        set;
    }
    public string TypeName
    {
        get;
        set;
    }
    public string Statement
    {
        get;
        set;
    }
    public string TagID
    {
        get;
        set;
    }
}