﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AbstractDamMonitor 的摘要描述
/// </summary>
public abstract class AbstractDamMonitor
{
    public AbstractDamMonitor()
    {
    }
    public DateTime InfoDate { get; set; }
}
public abstract class AbstractDamSlopeMonitor : AbstractDamMonitor
{
    public abstract string[] SerialColumnsA { get; }
    public abstract string[] SerialColumnsB { get; }
}

namespace Models
{
    public partial class RainDay : AbstractDamMonitor
    {
    }
    public partial class DamMonitorHorizontalChange_MainChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsL
        {
            get { return new[] { "SG01D1", "SG02D1", "SG03D1", "SG04D1", "SG05D1", "SG06D1" }; }
        }

        public static string[] SerialColumnsR
        {
            get { return new[] { "SG07D1", "SG08D1", "SG09D1", "SG10D1", "SG11D1", "SG12D1" }; }
        }
    }
    public partial class DamMonitorHorizontalChange_MainSChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsL
        {
            get { return new[] { "SG01D1", "SG02D1", "SG03D1", "SG04D1", "SG05D1", "SG06D1" }; }
        }

        public static string[] SerialColumnsR
        {
            get { return new[] { "SG07D1", "SG08D1", "SG09D1", "SG10D1", "SG11D1", "SG12D1" }; }
        }
    }
    public partial class DamMonitorHorizontalChange_SubChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsL
        {
            get { return new[] { "SG01D2", "SG02D2", "SG03D2", "SG04D2", "SG05D2", "SG06D2" }; }
        }

        public static string[] SerialColumnsR
        {
            get { return new[] { "SG07D2", "SG08D2", "SG09D2", "SG10D2", "SG11D2", "SG12D2" }; }
        }
    }
    public partial class DamMonitorHorizontalChange_SubSChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsL
        {
            get { return new[] { "SG01D2", "SG02D2", "SG03D2", "SG04D2", "SG05D2", "SG06D2" }; }
        }

        public static string[] SerialColumnsR
        {
            get { return new[] { "SG07D2", "SG08D2", "SG09D2", "SG10D2", "SG11D2", "SG12D2" }; }
        }
    }
    public partial class DamMonitorHorizontalChange_SouthChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsL
        {
            get { return new[] { "SG01D3", "SG02D3", "SG03D3", "SG04D3", "SG05D3", "SG06D3" }; }
        }

        public static string[] SerialColumnsR
        {
            get { return new[] { "SG07D3", "SG08D3", "SG09D3", "SG10D3", "SG11D3", "SG12D3" }; }
        }
    }
    public partial class DamMonitorHorizontalChange_SouthSChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsL
        {
            get { return new[] { "SG01D3", "SG02D3", "SG03D3", "SG04D3", "SG05D3", "SG06D3" }; }
        }

        public static string[] SerialColumnsR
        {
            get { return new[] { "SG07D3", "SG08D3", "SG09D3", "SG10D3", "SG11D3", "SG12D3" }; }
        }
    }
    public partial class DamMonitorHorizontalChange_SouthSChange : AbstractDamMonitor
    {
        public static string[] SerialColumnsMain
        {
            get { return new[] { "SP1D1", "SP2D1", "SP3D1" }; }
        }
        public static string[] SerialColumnsSub
        {
            get { return new[] { "SP1D2", "SP1D2", "SP1D2" }; }
        }
        public static string[] SerialColumnsSouth
        {
            get { return new[] { "SP1D3", "SP2D3", "SP3D3" }; }
        }
        public static string[] SerialColumnsWater
        {
            get { return new[] { "water" }; }
        }
    }
    public partial class DamMonitorBlindDrain : AbstractDamMonitor
    {
        public static string[] SerialColumnsMain
        {
            get { return new[] { "TD1D1", "TD2D1", "TD3D1", "TD4D1" }; }
        }
        public static string[] SerialColumnsSouth
        {
            get { return new[] { "TD1D3", "TD2D3", "TD3D3", "TD4D3" }; }
        }
    }
    public partial class DamMonitorHorizontalPipe : AbstractDamMonitor
    {
        public static string[] SerialColumns
        {
            get { return new[] { "DH01", "DH02", "DH03" }; }
        }
    }
    public partial class DamMonitorMeasureWater : AbstractDamMonitor
    {
        public static string[] SerialColumnsMain
        {
            get { return new[] { "SP1D1", "SP2D1", "SP3D1" }; }
        }
        public static string[] SerialColumnsSub
        {
            get { return new[] { "SP1D2", "SP2D2", "SP3D2" }; }
        }
        public static string[] SerialColumnsSouth
        {
            get { return new[] { "SP1D3", "SP2D3", "SP3D3" }; }
        }
        public static string[] SerialColumnsWater
        {
            get { return new[] { "Water" }; }
        }
    }
    public partial class DamMonitorHorizontalPipe2 : AbstractDamMonitor
    {
        public static string[] SerialColumns
        {
            get { return new[] { "DH01", "DH02", "DH03" }; }
        }
    }
    public partial class DamMonitorSurface : AbstractDamMonitor
    {
        public static string[] SerialColumns
        {
            get { return new[] { "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "E10", "E11", "E12", "E13", "E14", "E15", "E16", "E17", "E18", "E19", "E20", "E21", "E22", "E23", "E24" }; }
        }
    }
    public partial class DamMonitor : AbstractDamMonitor
    {
        public static string[] SerialColumns
        {
            get { return new[] { "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "E10", "E11", "E12", "E13", "E14", "E15", "E16", "E17", "E18", "E19", "E20", "E21", "E22", "E23", "E24" }; }
        }
    }

    public partial class DamMonitorWaterMeasureMain : AbstractDamMonitor
    {
        public static string[] SerialColumnsBasic
        {
            get { return new[] { "F5D1A", "F6D1A", "F7D1A", "F8D1A","Heart", "Shell" }; }
        }
        public static string[] SerialColumnsHeart
        {
            get { return new[] { "P52D1A", "P53D1A", "P72D1A", "P73D1A", "P75D1A", "P83D1A", "P92D1A", "P05D1A", "Heart", "Shell" }; }
        }
        public static string[] SerialColumnsFilter
        {
            get { return new[] { "P41D1A", "P42D1A", "P43D1A", "P54D1A", "P74D1A", "Heart", "Shell" }; }
        }

        public static string[] SerialColumnsBasicPressure
        {
            get { return new[] { "Pressure_F5D1A", "Pressure_F6D1A", "Pressure_F7D1A", "Pressure_F8D1A", "Pressure_Heart" }; }
        }
        public static string[] SerialColumnsHeartPressure
        {
            get { return new[] { "Pressure_P52D1A", "Pressure_P53D1A", "Pressure_P72D1A", "Pressure_P73D1A", "Pressure_P75D1A", "Pressure_P83D1A", "Pressure_P92D1A", "Pressure_P05D1A", "Pressure_Heart"}; }
        }
        public static string[] SerialColumnsFilterPressure
        {
            get { return new[] { "Pressure_P41D1A", "Pressure_P42D1A", "Pressure_P43D1A", "Pressure_P54D1A", "Pressure_P74D1A", "Pressure_Heart" }; }
        }
    }

    public partial class DamMonitorWaterMeasureSub : AbstractDamMonitor
    {
        public static string[] SerialColumnsBasic
        {
            get { return new[] { "F5D2A", "F6D2A",  "Heart", "Shell" }; }
        }
        public static string[] SerialColumnsHeart
        {
            get { return new[] { "P62D2A", "P63D2A", "P84D2A", "P85D2A", "P86D2A", "P03D2A", "P02D2A", "Heart", "Shell" }; }
        }
        public static string[] SerialColumnsFilter
        {
            get { return new[] { "P51D2A", "P52D2A", "P53D2A", "Heart", "Shell" }; }
        }

        public static string[] SerialColumnsBasicPressure
        {
            get { return new[] { "Pressure_F5D2A", "Pressure_F6D2A", "Pressure_Heart" }; }
        }
        public static string[] SerialColumnsHeartPressure
        {
            get { return new[] { "Pressure_P62D2A", "Pressure_P63D2A", "Pressure_P84D2A", "Pressure_P85D2A", "Pressure_P86D2A", "Pressure_P03D2A", "Pressure_P02D2A", "Pressure_Heart" }; }
        }
        public static string[] SerialColumnsFilterPressure
        {
            get { return new[] { "Pressure_P51D2A", "Pressure_P52D2A", "Pressure_P53D2A", "Pressure_Heart" }; }
        }
    }
    public partial class DamMonitorWaterMeasureSouth : AbstractDamMonitor
    {
        public static string[] SerialColumnsBasic
        {
            get { return new[] { "F6D3A", "F7D3A", "F8D3A", "F9D3A", "Heart", "Shell" }; }
        }
        public static string[] SerialColumnsHeart
        {
            get { return new[] { "P54D3A", "P55D3A", "P74D3A", "P75D3A", "P77D3A", "P93D3A", "P93D3AN", "P94D3A", "P05D3A", "Heart", "Shell" }; }
        }
        public static string[] SerialColumnsFilter
        {
            get { return new[] { "P41D3A", "P42D3A", "P43D3A", "P56D3A", "P76D3A", "Heart", "Shell" }; }
        }

        public static string[] SerialColumnsBasicPressure
        {
            get { return new[] { "Pressure_F6D3A", "Pressure_F7D3A", "Pressure_F8D3A", "Pressure_F9D3A", "Pressure_Heart" }; }
        }
        public static string[] SerialColumnsHeartPressure
        {
            get { return new[] { "Pressure_P54D3A", "Pressure_P55D3A", "Pressure_P74D3A", "Pressure_P75D3A", "Pressure_P77D3A", "Pressure_P93D3A", "Pressure_P93D3AN", "Pressure_P94D3A", "Pressure_P05D3A", "Pressure_Heart" }; }
        }
        public static string[] SerialColumnsFilterPressure
        {
            get { return new[] { "Pressure_P41D3A", "Pressure_P42D3A", "Pressure_P43D3A", "Pressure_P56D3A", "Pressure_P76D3A", "Pressure_Heart" }; }
        }
    }
    public partial class DamMonitorMeasureWaterObserveHalfAuto : AbstractDamMonitor
    {
        public static string[] ST1_3
        {
            get { return new[] { "ST01", "ST02", "ST03" }; }
        }
        public static string[] ST4_6
        {
            get { return new[] { "ST04", "ST05", "ST06" }; }
        }
        public static string[] ST7_9
        {
            get { return new[] { "ST07", "ST08", "ST09" }; }
        }
        public static string[] ST10_12
        {
            get { return new[] { "ST10", "ST11", "ST12" }; }
        }
        public static string[] ST13_15
        {
            get { return new[] { "ST13", "ST12", "ST15" }; }
        }
    }
    public partial class DamMonitorMeasureWaterObserveManual : AbstractDamMonitor
    {
        public static string[] Half
        {
            get { return new[] { "W1", "W2", "W3", "W4", "W5", "W6", "W7", "W8", "W9", "W10", "W11", "W12", "W13", "W14", "W15", "W16", "W17", "W18", "W19", "W20", "W21", "W22", "W23", "W24", "W25", "W26", "W27", "W28", "W29", "W30", "W31", "W32", "W33" }; }
        }
        public static string[] W1_3
        {
            get { return new[] { "W1", "W2", "W3" }; }
        }
        public static string[] W4_9
        {
            get { return new[] { "W4", "W5", "W6", "W7", "W8", "W9" }; }
        }
        public static string[] W6_10
        {
            get { return new[] { "W6", "W7", "W8", "W9", "W10" }; }
        }
        public static string[] W11_14
        {
            get { return new[] { "W11", "W12", "W13", "W14" }; }
        }
        public static string[] W15_16
        {
            get { return new[] { "W15", "W16" }; }
        }
        public static string[] W18_21
        {
            get { return new[] { "W18", "W19", "W20", "W21" }; }
        }
        public static string[] W22_24
        {
            get { return new[] { "W22", "W23", "W24" }; }
        }
        public static string[] W26a
        {
            get { return new[] { "W26" }; }
        }
        public static string[] W27_28
        {
            get { return new[] { "W27", "W28" }; }
        }
        public static string[] W29_30
        {
            get { return new[] { "W29", "W30" }; }
        }
        public static string[] W26b
        {
            get { return new[] { "W26", "W29", "W30" }; }
        }
        public static string[] W31_33
        {
            get { return new[] { "W31", "W32", "W33" }; }
        }
    }














    public partial class DamMonitorSI29 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get { return new[] { "A238_8", "A238_3", "A237_8", "A237_3", "A236_8", "A236_3", "A235_8", "A235_3", "A234_8", "A234_3", "A233_8", "A233_3", "A232_8", "A232_3", "A231_8", "A231_3", "A230_8", "A230_3", "A229_8", "A229_3", "A228_8", "A228_3", "A227_8", "A227_3", "A226_8", "A226_3", "A225_8", "A225_3", "A224_8", "A224_3", "A223_8", "A223_3", "A222_8", "A222_3", "A221_8", "A221_3", "A220_8", "A220_3", "A219_8", "A219_3", "A218_8", "A218_3", "A217_8", "A217_3", "A216_8", "A216_3", "A215_8", "A215_3", "A214_8", "A214_3", "A213_8", "B238_8", "B238_3", "B237_8", "B237_3", "B236_8", "B236_3", "B235_8", "B235_3", "B234_8", "B234_3", "B233_8", "B233_3", "B232_8", "B232_3", "B231_8", "B231_3", "B230_8", "B230_3", "B229_8", "B229_3", "B228_8", "B228_3", "B227_8", "B227_3", "B226_8", "B226_3", "B225_8", "B225_3", "B224_8", "B224_3", "B223_8", "B223_3", "B222_8", "B222_3", "B221_8", "B221_3", "B220_8", "B220_3", "B219_8", "B219_3", "B218_8", "B218_3", "B217_8", "B217_3", "B216_8", "B216_3", "B215_8", "B215_3", "B214_8", "B214_3", "B213_8" }; }
        }
        public override string[] SerialColumnsB
        {
            get { return new[] { "B238_8", "B238_3", "B237_8", "B237_3", "B236_8", "B236_3", "B235_8", "B235_3", "B234_8", "B234_3", "B233_8", "B233_3", "B232_8", "B232_3", "B231_8", "B231_3", "B230_8", "B230_3", "B229_8", "B229_3", "B228_8", "B228_3", "B227_8", "B227_3", "B226_8", "B226_3", "B225_8", "B225_3", "B224_8", "B224_3", "B223_8", "B223_3", "B222_8", "B222_3", "B221_8", "B221_3", "B220_8", "B220_3", "B219_8", "B219_3", "B218_8", "B218_3", "B217_8", "B217_3", "B216_8", "B216_3", "B215_8", "B215_3", "B214_8", "B214_3", "B213_8", "B238_8", "B238_3", "B237_8", "B237_3", "B236_8", "B236_3", "B235_8", "B235_3", "B234_8", "B234_3", "B233_8", "B233_3", "B232_8", "B232_3", "B231_8", "B231_3", "B230_8", "B230_3", "B229_8", "B229_3", "B228_8", "B228_3", "B227_8", "B227_3", "B226_8", "B226_3", "B225_8", "B225_3", "B224_8", "B224_3", "B223_8", "B223_3", "B222_8", "B222_3", "B221_8", "B221_3", "B220_8", "B220_3", "B219_8", "B219_3", "B218_8", "B218_3", "B217_8", "B217_3", "B216_8", "B216_3", "B215_8", "B215_3", "B214_8", "B214_3", "B213_8" }; }
        }
    }
    public partial class DamMonitorI1D1 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A194_8", "A194_3", "A193_8", "A193_3", "A192_8", "A192_3", "A191_8", "A191_3", "A190_8", "A190_3", "A189_8", "A189_3", "A188_8", "A188_3", "A187_8", "A187_3", "A186_8", "A186_3", "A185_8", "A185_3", "A184_8", "A184_3", "A183_8", "A183_3", "A182_8", "A182_3", "A181_8", "A181_3", "A180_8", "A180_3", "A179_8", "A179_3", "A178_8", "A178_3", "A177_8", "A177_3", "A176_8", "A176_3", "A175_8", "A175_3", "A174_8", "A174_3", "A173_8", "A173_3", "A172_8", "A172_3", "A171_8", "A171_3", "A170_8", "A170_3", "A169_8", "A169_3", "A168_8", "A168_3", "A167_8", "A167_3", "A166_8", "A166_3", "A165_8", "A165_3", "A164_8", "A164_3", "A163_8", "A163_3", "A162_8", "A162_3", "A161_8", "A161_3", "A160_8", "A160_3", "A159_8", "A159_3", "A158_8", "A158_3", "A157_8", "A157_3", "A156_8", "A156_3", "A155_8", "A155_3", "A154_8", "A154_3", "A153_8", "A153_3", "A152_8", "A152_3", "A151_8", "A151_3", "A150_8", "A150_3", "A149_8", "A149_3", "A148_8", "A148_3", "A147_8", "A147_3", "A146_8", "A146_3", "A145_8", "A145_3", "A144_8", "A144_3", "A143_8", "A143_3", "A142_8", "A142_3", "A141_8", "A141_3", "A140_8", "A140_3", "A139_8", "A139_3", "A138_8", "A138_3", "A137_8", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B194_8", "B194_3", "B193_8", "B193_3", "B192_8", "B192_3", "B191_8", "B191_3", "B190_8", "B190_3", "B189_8", "B189_3", "B188_8", "B188_3", "B187_8", "B187_3", "B186_8", "B186_3", "B185_8", "B185_3", "B184_8", "B184_3", "B183_8", "B183_3", "B182_8", "B182_3", "B181_8", "B181_3", "B180_8", "B180_3", "B179_8", "B179_3", "B178_8", "B178_3", "B177_8", "B177_3", "B176_8", "B176_3", "B175_8", "B175_3", "B174_8", "B174_3", "B173_8", "B173_3", "B172_8", "B172_3", "B171_8", "B171_3", "B170_8", "B170_3", "B169_8", "B169_3", "B168_8", "B168_3", "B167_8", "B167_3", "B166_8", "B166_3", "B165_8", "B165_3", "B164_8", "B164_3", "B163_8", "B163_3", "B162_8", "B162_3", "B161_8", "B161_3", "B160_8", "B160_3", "B159_8", "B159_3", "B158_8", "B158_3", "B157_8", "B157_3", "B156_8", "B156_3", "B155_8", "B155_3", "B154_8", "B154_3", "B153_8", "B153_3", "B152_8", "B152_3", "B151_8", "B151_3", "B150_8", "B150_3", "B149_8", "B149_3", "B148_8", "B148_3", "B147_8", "B147_3", "B146_8", "B146_3", "B145_8", "B145_3", "B144_8", "B144_3", "B143_8", "B143_3", "B142_8", "B142_3", "B141_8", "B141_3", "B140_8", "B140_3", "B139_8", "B139_3", "B138_8", "B138_3", "B137_8", };
            }
        }
    }
    public partial class DamMonitorI1D2 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A195_3", "A194_8", "A194_3", "A193_8", "A193_3", "A192_8", "A192_3", "A191_8", "A191_3", "A190_8", "A190_3", "A189_8", "A189_3", "A188_8", "A188_3", "A187_8", "A187_3", "A186_8", "A186_3", "A185_8", "A185_3", "A184_8", "A184_3", "A183_8", "A183_3", "A182_8", "A182_3", "A181_8", "A181_3", "A180_8", "A180_3", "A179_8", "A179_3", "A178_8", "A178_3", "A177_8", "A177_3", "A176_8", "A176_3", "A175_8", "A175_3", "A174_8", "A174_3", "A173_8", "A173_3", "A172_8", "A172_3", "A171_8", "A171_3", "A170_8", "A170_3", "A169_8", "A169_3", "A168_8", "A168_3", "A167_8", "A167_3", "A166_8", "A166_3", "A165_8", "A165_3", "A164_8", "A164_3", "A163_8", "A163_3", "A162_8", "A162_3", "A161_8", "A161_3", "A160_8", "A160_3", "A159_8", "A159_3", "A158_8", "A158_3", "A157_8", "A157_3", "A156_8", "A156_3", "A155_8", "A155_3", "A154_8", "A154_3", "A153_8", "A153_3", "A152_8", "A152_3", "A151_8", "A151_3", "A150_8", "A150_3", "A149_8", "A149_3", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B195_3", "B194_8", "B194_3", "B193_8", "B193_3", "B192_8", "B192_3", "B191_8", "B191_3", "B190_8", "B190_3", "B189_8", "B189_3", "B188_8", "B188_3", "B187_8", "B187_3", "B186_8", "B186_3", "B185_8", "B185_3", "B184_8", "B184_3", "B183_8", "B183_3", "B182_8", "B182_3", "B181_8", "B181_3", "B180_8", "B180_3", "B179_8", "B179_3", "B178_8", "B178_3", "B177_8", "B177_3", "B176_8", "B176_3", "B175_8", "B175_3", "B174_8", "B174_3", "B173_8", "B173_3", "B172_8", "B172_3", "B171_8", "B171_3", "B170_8", "B170_3", "B169_8", "B169_3", "B168_8", "B168_3", "B167_8", "B167_3", "B166_8", "B166_3", "B165_8", "B165_3", "B164_8", "B164_3", "B163_8", "B163_3", "B162_8", "B162_3", "B161_8", "B161_3", "B160_8", "B160_3", "B159_8", "B159_3", "B158_8", "B158_3", "B157_8", "B157_3", "B156_8", "B156_3", "B155_8", "B155_3", "B154_8", "B154_3", "B153_8", "B153_3", "B152_8", "B152_3", "B151_8", "B151_3", "B150_8", "B150_3", "B149_8", "B149_3", };
            }
        }
    }
    public partial class DamMonitorI1D3 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A194_8", "A194_3", "A193_8", "A193_3", "A192_8", "A192_3", "A191_8", "A191_3", "A190_8", "A190_3", "A189_8", "A189_3", "A188_8", "A188_3", "A187_8", "A187_3", "A186_8", "A186_3", "A185_8", "A185_3", "A184_8", "A184_3", "A183_8", "A183_3", "A182_8", "A182_3", "A181_8", "A181_3", "A180_8", "A180_3", "A179_8", "A179_3", "A178_8", "A178_3", "A177_8", "A177_3", "A176_8", "A176_3", "A175_8", "A175_3", "A174_8", "A174_3", "A173_8", "A173_3", "A172_8", "A172_3", "A171_8", "A171_3", "A170_8", "A170_3", "A169_8", "A169_3", "A168_8", "A168_3", "A167_8", "A167_3", "A166_8", "A166_3", "A165_8", "A165_3", "A164_8", "A164_3", "A163_8", "A163_3", "A162_8", "A162_3", "A161_8", "A161_3", "A160_8", "A160_3", "A159_8", "A159_3", "A158_8", "A158_3", "A157_8", "A157_3", "A156_8", "A156_3", "A155_8", "A155_3", "A154_8", "A154_3", "A153_8", "A153_3", "A152_8", "A152_3", "A151_8", "A151_3", "A150_8", "A150_3", "A149_8", "A149_3", "A148_8", "A148_3", "A147_8", "A147_3", "A146_8", "A146_3", "A145_8", "A145_3", "A144_8", "A144_3", "A143_8", "A143_3", "A142_8", "A142_3", "A141_8", "A141_3", "A140_8", "A140_3", "A139_8", "A139_3", "A138_8", "A138_3", "A137_8", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B194_8", "B194_3", "B193_8", "B193_3", "B192_8", "B192_3", "B191_8", "B191_3", "B190_8", "B190_3", "B189_8", "B189_3", "B188_8", "B188_3", "B187_8", "B187_3", "B186_8", "B186_3", "B185_8", "B185_3", "B184_8", "B184_3", "B183_8", "B183_3", "B182_8", "B182_3", "B181_8", "B181_3", "B180_8", "B180_3", "B179_8", "B179_3", "B178_8", "B178_3", "B177_8", "B177_3", "B176_8", "B176_3", "B175_8", "B175_3", "B174_8", "B174_3", "B173_8", "B173_3", "B172_8", "B172_3", "B171_8", "B171_3", "B170_8", "B170_3", "B169_8", "B169_3", "B168_8", "B168_3", "B167_8", "B167_3", "B166_8", "B166_3", "B165_8", "B165_3", "B164_8", "B164_3", "B163_8", "B163_3", "B162_8", "B162_3", "B161_8", "B161_3", "B160_8", "B160_3", "B159_8", "B159_3", "B158_8", "B158_3", "B157_8", "B157_3", "B156_8", "B156_3", "B155_8", "B155_3", "B154_8", "B154_3", "B153_8", "B153_3", "B152_8", "B152_3", "B151_8", "B151_3", "B150_8", "B150_3", "B149_8", "B149_3", "B148_8", "B148_3", "B147_8", "B147_3", "B146_8", "B146_3", "B145_8", "B145_3", "B144_8", "B144_3", "B143_8", "B143_3", "B142_8", "B142_3", "B141_8", "B141_3", "B140_8", "B140_3", "B139_8", "B139_3", "B138_8", "B138_3", "B137_8", };
            }
        }
    }
    public partial class DamMonitorSI01 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", "A141_5", "A141", "A140_5", "A140", "A139_5", "A139", "A138_5", "A138", "A137_5", "A137", "A136_5", "A136", "A135_5", "A135", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", "B141_5", "B141", "B140_5", "B140", "B139_5", "B139", "B138_5", "B138", "B137_5", "B137", "B136_5", "B136", "B135_5", "B135", };
            }
        }
    }
    public partial class DamMonitorSI02 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", "A141_5", "A141", "A140_5", "A140", "A139_5", "A139", "A138_5", "A138", "A137_5", "A137", "A136_5", "A136", "A135_5", "A135", "A134_5", "A134", "A133_5", "A133", "A132_5", "A132", "A131_5", "A131", "A130_5", "A130", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", "B141_5", "B141", "B140_5", "B140", "B139_5", "B139", "B138_5", "B138", "B137_5", "B137", "B136_5", "B136", "B135_5", "B135", "B134_5", "B134", "B133_5", "B133", "B132_5", "B132", "B131_5", "B131", "B130_5", "B130", };
            }
        }
    }
    public partial class DamMonitorSI03 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", "A141_5", "A141", "A140_5", "A140", "A139_5", "A139", "A138_5", "A138", "A137_5", "A137", "A136_5", "A136", "A135_5", "A135", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", "B141_5", "B141", "B140_5", "B140", "B139_5", "B139", "B138_5", "B138", "B137_5", "B137", "B136_5", "B136", "B135_5", "B135", };
            }
        }
    }
    public partial class DamMonitorSI04 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A195", "A194_5", "A194", "A193_5", "A193", "A192_5", "A192", "A191_5", "A191", "A190_5", "A190", "A189_5", "A189", "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B195", "B194_5", "B194", "B193_5", "B193", "B192_5", "B192", "B191_5", "B191", "B190_5", "B190", "B189_5", "B189", "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", };
            }
        }
    }
    public partial class DamMonitorSI05 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", "A141_5", "A141", "A140_5", "A140", "A139_5", "A139", "A138_5", "A138", "A137_5", "A137", "A136_5", "A136", "A135_5", "A135", "A134_5", "A134", "A133_5", "A133", "A132_5", "A132", "A131_5", "A131", "A130_5", "A130", "A129_5", "A129", "A128_5", "A128", "A127_5", "A127", "A126_5", "A126", "A125_5", "A125", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", "B141_5", "B141", "B140_5", "B140", "B139_5", "B139", "B138_5", "B138", "B137_5", "B137", "B136_5", "B136", "B135_5", "B135", "B134_5", "B134", "B133_5", "B133", "B132_5", "B132", "B131_5", "B131", "B130_5", "B130", "B129_5", "B129", "B128_5", "B128", "B127_5", "B127", "B126_5", "B126", "B125_5", "B125", };
            }
        }
    }
    public partial class DamMonitorSI06 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", "A141_5", "A141", "A140_5", "A140", "A139_5", "A139", "A138_5", "A138", "A137_5", "A137", "A136_5", "A136", "A135_5", "A135", "A134_5", "A134", "A133_5", "A133", "A132_5", "A132", "A131_5", "A131", "A130_5", "A130", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", "B141_5", "B141", "B140_5", "B140", "B139_5", "B139", "B138_5", "B138", "B137_5", "B137", "B136_5", "B136", "B135_5", "B135", "B134_5", "B134", "B133_5", "B133", "B132_5", "B132", "B131_5", "B131", "B130_5", "B130", };
            }
        }
    }
    public partial class DamMonitorSI07 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", };
            }
        }
    }
    public partial class DamMonitorSI08 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A195", "A194_5", "A194", "A193_5", "A193", "A192_5", "A192", "A191_5", "A191", "A190_5", "A190", "A189_5", "A189", "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B195", "B194_5", "B194", "B193_5", "B193", "B192_5", "B192", "B191_5", "B191", "B190_5", "B190", "B189_5", "B189", "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", };
            }
        }
    }
    public partial class DamMonitorSI09 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", "A141_5", "A141", "A140_5", "A140", "A139_5", "A139", "A138_5", "A138", "A137_5", "A137", "A136_5", "A136", "A135_5", "A135", "A134_5", "A134", "A133_5", "A133", "A132_5", "A132", "A131_5", "A131", "A130_5", "A130", "A129_5", "A129", "A128_5", "A128", "A127_5", "A127", "A126_5", "A126", "A125_5", "A125", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", "B141_5", "B141", "B140_5", "B140", "B139_5", "B139", "B138_5", "B138", "B137_5", "B137", "B136_5", "B136", "B135_5", "B135", "B134_5", "B134", "B133_5", "B133", "B132_5", "B132", "B131_5", "B131", "B130_5", "B130", "B129_5", "B129", "B128_5", "B128", "B127_5", "B127", "B126_5", "B126", "B125_5", "B125", };
            }
        }
    }
    public partial class DamMonitorSI10 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", };
            }
        }
    }
    public partial class DamMonitorSI11 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A213", "A212_5", "A212", "A211_5", "A211", "A210_5", "A210", "A209_5", "A209", "A208_5", "A208", "A207_5", "A207", "A206_5", "A206", "A205_5", "A205", "A204_5", "A204", "A203_5", "A203", "A202_5", "A202", "A201_5", "A201", "A200_5", "A200", "A199_5", "A199", "A198_5", "A198", "A197_5", "A197", "A196_5", "A196", "A195_5", "A195", "A194_5", "A194", "A193_5", "A193", "A192_5", "A192", "A191_5", "A191", "A190_5", "A190", "A189_5", "A189", "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B213", "B212_5", "B212", "B211_5", "B211", "B210_5", "B210", "B209_5", "B209", "B208_5", "B208", "B207_5", "B207", "B206_5", "B206", "B205_5", "B205", "B204_5", "B204", "B203_5", "B203", "B202_5", "B202", "B201_5", "B201", "B200_5", "B200", "B199_5", "B199", "B198_5", "B198", "B197_5", "B197", "B196_5", "B196", "B195_5", "B195", "B194_5", "B194", "B193_5", "B193", "B192_5", "B192", "B191_5", "B191", "B190_5", "B190", "B189_5", "B189", "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", };
            }
        }
    }
    public partial class DamMonitorSI12 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A197", "A196_5", "A196", "A195_5", "A195", "A194_5", "A194", "A193_5", "A193", "A192_5", "A192", "A191_5", "A191", "A190_5", "A190", "A189_5", "A189", "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B197", "B196_5", "B196", "B195_5", "B195", "B194_5", "B194", "B193_5", "B193", "B192_5", "B192", "B191_5", "B191", "B190_5", "B190", "B189_5", "B189", "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", };
            }
        }
    }
    public partial class DamMonitorSI13 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A212", "A211_5", "A211", "A210_5", "A210", "A209_5", "A209", "A208_5", "A208", "A207_5", "A207", "A206_5", "A206", "A205_5", "A205", "A204_5", "A204", "A203_5", "A203", "A202_5", "A202", "A201_5", "A201", "A200_5", "A200", "A199_5", "A199", "A198_5", "A198", "A197_5", "A197", "A196_5", "A196", "A195_5", "A195", "A194_5", "A194", "A193_5", "A193", "A192_5", "A192", "A191_5", "A191", "A190_5", "A190", "A189_5", "A189", "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B212", "B211_5", "B211", "B210_5", "B210", "B209_5", "B209", "B208_5", "B208", "B207_5", "B207", "B206_5", "B206", "B205_5", "B205", "B204_5", "B204", "B203_5", "B203", "B202_5", "B202", "B201_5", "B201", "B200_5", "B200", "B199_5", "B199", "B198_5", "B198", "B197_5", "B197", "B196_5", "B196", "B195_5", "B195", "B194_5", "B194", "B193_5", "B193", "B192_5", "B192", "B191_5", "B191", "B190_5", "B190", "B189_5", "B189", "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", };
            }
        }
    }
    public partial class DamMonitorSI14 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A191", "A190_5", "A190", "A189_5", "A189", "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B191", "B190_5", "B190", "B189_5", "B189", "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", };
            }
        }
    }
    public partial class DamMonitorSI15 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A157_4", "A156_9", "A156_4", "A155_9", "A155_4", "A154_9", "A154_4", "A153_9", "A153_4", "A152_9", "A152_4", "A151_9", "A151_4", "A150_9", "A150_4", "A149_9", "A149_4", "A148_9", "A148_4", "A147_9", "A147_4", "A146_9", "A146_4", "A145_9", "A145_4", "A144_9", "A144_4", "A143_9", "A143_4", "A142_9", "A142_4", "A141_9", "A141_4", "A140_9", "A140_4", "A139_9", "A139_4", "A138_9", "A138_4", "A137_9", "A137_4", "A136_9", "A136_4", "A135_9", "A135_4", "A134_9", "A134_4", "A133_9", "A133_4", "A132_9", "A132_4", "A131_9", "A131_4", "A130_9", "A130_4", "A129_9", "A129_4", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B157_4", "B156_9", "B156_4", "B155_9", "B155_4", "B154_9", "B154_4", "B153_9", "B153_4", "B152_9", "B152_4", "B151_9", "B151_4", "B150_9", "B150_4", "B149_9", "B149_4", "B148_9", "B148_4", "B147_9", "B147_4", "B146_9", "B146_4", "B145_9", "B145_4", "B144_9", "B144_4", "B143_9", "B143_4", "B142_9", "B142_4", "B141_9", "B141_4", "B140_9", "B140_4", "B139_9", "B139_4", "B138_9", "B138_4", "B137_9", "B137_4", "B136_9", "B136_4", "B135_9", "B135_4", "B134_9", "B134_4", "B133_9", "B133_4", "B132_9", "B132_4", "B131_9", "B131_4", "B130_9", "B130_4", "B129_9", "B129_4", };
            }
        }
    }
    public partial class DamMonitorSI16 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A188_5", "A188", "A187_5", "A187", "A186_5", "A186", "A185_5", "A185", "A184_5", "A184", "A183_5", "A183", "A182_5", "A182", "A181_5", "A181", "A180_5", "A180", "A179_5", "A179", "A178_5", "A178", "A177_5", "A177", "A176_5", "A176", "A175_5", "A175", "A174_5", "A174", "A173_5", "A173", "A172_5", "A172", "A171_5", "A171", "A170_5", "A170", "A169_5", "A169", "A168_5", "A168", "A167_5", "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B188_5", "B188", "B187_5", "B187", "B186_5", "B186", "B185_5", "B185", "B184_5", "B184", "B183_5", "B183", "B182_5", "B182", "B181_5", "B181", "B180_5", "B180", "B179_5", "B179", "B178_5", "B178", "B177_5", "B177", "B176_5", "B176", "B175_5", "B175", "B174_5", "B174", "B173_5", "B173", "B172_5", "B172", "B171_5", "B171", "B170_5", "B170", "B169_5", "B169", "B168_5", "B168", "B167_5", "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", };
            }
        }
    }
    public partial class DamMonitorSI17 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A214_9", "A214_4", "A213_9", "A213_4", "A212_9", "A212_4", "A211_9", "A211_4", "A210_9", "A210_4", "A209_9", "A209_4", "A208_9", "A208_4", "A207_9", "A207_4", "A206_9", "A206_4", "A205_9", "A205_4", "A204_9", "A204_4", "A203_9", "A203_4", "A202_9", "A202_4", "A201_9", "A201_4", "A200_9", "A200_4", "A199_9", "A199_4", "A198_9", "A198_4", "A197_9", "A197_4", "A196_9", "A196_4", "A195_9", "A195_4", "A194_9", "A194_4", "A193_9", "A193_4", "A192_9", "A192_4", "A191_9", "A191_4", "A190_9", "A190_4", "A189_9", "A189_4", "A188_9", "A188_4", "A187_9", "A187_4", "A186_9", "A186_4", "A185_9", "A185_4", "A184_9", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B214_9", "B214_4", "B213_9", "B213_4", "B212_9", "B212_4", "B211_9", "B211_4", "B210_9", "B210_4", "B209_9", "B209_4", "B208_9", "B208_4", "B207_9", "B207_4", "B206_9", "B206_4", "B205_9", "B205_4", "B204_9", "B204_4", "B203_9", "B203_4", "B202_9", "B202_4", "B201_9", "B201_4", "B200_9", "B200_4", "B199_9", "B199_4", "B198_9", "B198_4", "B197_9", "B197_4", "B196_9", "B196_4", "B195_9", "B195_4", "B194_9", "B194_4", "B193_9", "B193_4", "B192_9", "B192_4", "B191_9", "B191_4", "B190_9", "B190_4", "B189_9", "B189_4", "B188_9", "B188_4", "B187_9", "B187_4", "B186_9", "B186_4", "B185_9", "B185_4", "B184_9", };
            }
        }
    }
    public partial class DamMonitorSI18 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A218_7", "A218_2", "A217_7", "A217_2", "A216_7", "A216_2", "A215_7", "A215_2", "A214_7", "A214_2", "A213_7", "A213_2", "A212_7", "A212_2", "A211_7", "A211_2", "A210_7", "A210_2", "A209_7", "A209_2", "A208_7", "A208_2", "A207_7", "A207_2", "A206_7", "A206_2", "A205_7", "A205_2", "A204_7", "A204_2", "A203_7", "A203_2", "A202_7", "A202_2", "A201_7", "A201_2", "A200_7", "A200_2", "A199_7", "A199_2", "A198_7", "A198_2", "A197_7", "A197_2", "A196_7", "A196_2", "A195_7", "A195_2", "A194_7", "A194_2", "A193_7", "A193_2", "A192_7", "A192_2", "A191_7", "A191_2", "A190_7", "A190_2", "A189_7", "A189_2", "A188_7", "A188_2", "A187_7", "A187_2", "A186_7", "A186_2", "A185_7", "A185_2", "A184_7", "A184_2", "A183_7", "A183_2", "A182_7", "A182_2", "A181_7", "A181_2", "A180_7", "A180_2", "A179_7", "A179_2", "A178_7", "A178_2", "A177_7", "A177_2", "A176_7", "A176_2", "A175_7", "A175_2", "A174_7", "A174_2", "A173_7", "A173_2", "A172_7", "A172_2", "A171_7", "A171_2", "A170_7", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B218_7", "B218_2", "B217_7", "B217_2", "B216_7", "B216_2", "B215_7", "B215_2", "B214_7", "B214_2", "B213_7", "B213_2", "B212_7", "B212_2", "B211_7", "B211_2", "B210_7", "B210_2", "B209_7", "B209_2", "B208_7", "B208_2", "B207_7", "B207_2", "B206_7", "B206_2", "B205_7", "B205_2", "B204_7", "B204_2", "B203_7", "B203_2", "B202_7", "B202_2", "B201_7", "B201_2", "B200_7", "B200_2", "B199_7", "B199_2", "B198_7", "B198_2", "B197_7", "B197_2", "B196_7", "B196_2", "B195_7", "B195_2", "B194_7", "B194_2", "B193_7", "B193_2", "B192_7", "B192_2", "B191_7", "B191_2", "B190_7", "B190_2", "B189_7", "B189_2", "B188_7", "B188_2", "B187_7", "B187_2", "B186_7", "B186_2", "B185_7", "B185_2", "B184_7", "B184_2", "B183_7", "B183_2", "B182_7", "B182_2", "B181_7", "B181_2", "B180_7", "B180_2", "B179_7", "B179_2", "B178_7", "B178_2", "B177_7", "B177_2", "B176_7", "B176_2", "B175_7", "B175_2", "B174_7", "B174_2", "B173_7", "B173_2", "B172_7", "B172_2", "B171_7", "B171_2", "B170_7", };
            }
        }
    }
    public partial class DamMonitorSI19 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A190_8", "A190_3", "A189_8", "A189_3", "A188_8", "A188_3", "A187_8", "A187_3", "A186_8", "A186_3", "A185_8", "A185_3", "A184_8", "A184_3", "A183_8", "A183_3", "A182_8", "A182_3", "A181_8", "A181_3", "A180_8", "A180_3", "A179_8", "A179_3", "A178_8", "A178_3", "A177_8", "A177_3", "A176_8", "A176_3", "A175_8", "A175_3", "A174_8", "A174_3", "A173_8", "A173_3", "A172_8", "A172_3", "A171_8", "A171_3", "A170_8", "A170_3", "A169_8", "A169_3", "A168_8", "A168_3", "A167_8", "A167_3", "A166_8", "A166_3", "A165_8", "A165_3", "A164_8", "A164_3", "A163_8", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B190_8", "B190_3", "B189_8", "B189_3", "B188_8", "B188_3", "B187_8", "B187_3", "B186_8", "B186_3", "B185_8", "B185_3", "B184_8", "B184_3", "B183_8", "B183_3", "B182_8", "B182_3", "B181_8", "B181_3", "B180_8", "B180_3", "B179_8", "B179_3", "B178_8", "B178_3", "B177_8", "B177_3", "B176_8", "B176_3", "B175_8", "B175_3", "B174_8", "B174_3", "B173_8", "B173_3", "B172_8", "B172_3", "B171_8", "B171_3", "B170_8", "B170_3", "B169_8", "B169_3", "B168_8", "B168_3", "B167_8", "B167_3", "B166_8", "B166_3", "B165_8", "B165_3", "B164_8", "B164_3", "B163_8", };
            }
        }
    }
    public partial class DamMonitorSI20 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A175_9", "A175_4", "A174_9", "A174_4", "A173_9", "A173_4", "A172_9", "A172_4", "A171_9", "A171_4", "A170_9", "A170_4", "A169_9", "A169_4", "A168_9", "A168_4", "A167_9", "A167_4", "A166_9", "A166_4", "A165_9", "A165_4", "A164_9", "A164_4", "A163_9", "A163_4", "A162_9", "A162_4", "A161_9", "A161_4", "A160_9", "A160_4", "A159_9", "A159_4", "A158_9", "A158_4", "A157_9", "A157_4", "A156_9", "A156_4", "A155_9", "A155_4", "A154_9", "A154_4", "A153_9", "A153_4", "A152_9", "A152_4", "A151_9", "A151_4", "A150_9", "A150_4", "A149_9", "A149_4", "A148_9", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B175_9", "B175_4", "B174_9", "B174_4", "B173_9", "B173_4", "B172_9", "B172_4", "B171_9", "B171_4", "B170_9", "B170_4", "B169_9", "B169_4", "B168_9", "B168_4", "B167_9", "B167_4", "B166_9", "B166_4", "B165_9", "B165_4", "B164_9", "B164_4", "B163_9", "B163_4", "B162_9", "B162_4", "B161_9", "B161_4", "B160_9", "B160_4", "B159_9", "B159_4", "B158_9", "B158_4", "B157_9", "B157_4", "B156_9", "B156_4", "B155_9", "B155_4", "B154_9", "B154_4", "B153_9", "B153_4", "B152_9", "B152_4", "B151_9", "B151_4", "B150_9", "B150_4", "B149_9", "B149_4", "B148_9", };
            }
        }
    }
    public partial class DamMonitorSI21 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A150_7", "A150_2", "A149_7", "A149_2", "A148_7", "A148_2", "A147_7", "A147_2", "A146_7", "A146_2", "A145_7", "A145_2", "A144_7", "A144_2", "A143_7", "A143_2", "A142_7", "A142_2", "A141_7", "A141_2", "A140_7", "A140_2", "A139_7", "A139_2", "A138_7", "A138_2", "A137_7", "A137_2", "A136_7", "A136_2", "A135_7", "A135_2", "A134_7", "A134_2", "A133_7", "A133_2", "A132_7", "A132_2", "A131_7", "A131_2", "A130_7", "A130_2", "A129_7", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B150_7", "B150_2", "B149_7", "B149_2", "B148_7", "B148_2", "B147_7", "B147_2", "B146_7", "B146_2", "B145_7", "B145_2", "B144_7", "B144_2", "B143_7", "B143_2", "B142_7", "B142_2", "B141_7", "B141_2", "B140_7", "B140_2", "B139_7", "B139_2", "B138_7", "B138_2", "B137_7", "B137_2", "B136_7", "B136_2", "B135_7", "B135_2", "B134_7", "B134_2", "B133_7", "B133_2", "B132_7", "B132_2", "B131_7", "B131_2", "B130_7", "B130_2", "B129_7", };
            }
        }
    }
    public partial class DamMonitorSI22 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A167", "A166_5", "A166", "A165_5", "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B167", "B166_5", "B166", "B165_5", "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", };
            }
        }
    }
    public partial class DamMonitorSI23 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A210_9", "A210_4", "A209_9", "A209_4", "A208_9", "A208_4", "A207_9", "A207_4", "A206_9", "A206_4", "A205_9", "A205_4", "A204_9", "A204_4", "A203_9", "A203_4", "A202_9", "A202_4", "A201_9", "A201_4", "A200_9", "A200_4", "A199_9", "A199_4", "A198_9", "A198_4", "A197_9", "A197_4", "A196_9", "A196_4", "A195_9", "A195_4", "A194_9", "A194_4", "A193_9", "A193_4", "A192_9", "A192_4", "A191_9", "A191_4", "A190_9", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B210_9", "B210_4", "B209_9", "B209_4", "B208_9", "B208_4", "B207_9", "B207_4", "B206_9", "B206_4", "B205_9", "B205_4", "B204_9", "B204_4", "B203_9", "B203_4", "B202_9", "B202_4", "B201_9", "B201_4", "B200_9", "B200_4", "B199_9", "B199_4", "B198_9", "B198_4", "B197_9", "B197_4", "B196_9", "B196_4", "B195_9", "B195_4", "B194_9", "B194_4", "B193_9", "B193_4", "B192_9", "B192_4", "B191_9", "B191_4", "B190_9", };
            }
        }
    }
    public partial class DamMonitorSI24 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A177_7", "A177_2", "A176_7", "A176_2", "A175_7", "A175_2", "A174_7", "A174_2", "A173_7", "A173_2", "A172_7", "A172_2", "A171_7", "A171_2", "A170_7", "A170_2", "A169_7", "A169_2", "A168_7", "A168_2", "A167_7", "A167_2", "A166_7", "A166_2", "A165_7", "A165_2", "A164_7", "A164_2", "A163_7", "A163_2", "A162_7", "A162_2", "A161_7", "A161_2", "A160_7", "A160_2", "A159_7", "A159_2", "A158_7", "A158_2", "A157_7", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B177_7", "B177_2", "B176_7", "B176_2", "B175_7", "B175_2", "B174_7", "B174_2", "B173_7", "B173_2", "B172_7", "B172_2", "B171_7", "B171_2", "B170_7", "B170_2", "B169_7", "B169_2", "B168_7", "B168_2", "B167_7", "B167_2", "B166_7", "B166_2", "B165_7", "B165_2", "B164_7", "B164_2", "B163_7", "B163_2", "B162_7", "B162_2", "B161_7", "B161_2", "B160_7", "B160_2", "B159_7", "B159_2", "B158_7", "B158_2", "B157_7", };
            }
        }
    }
    public partial class DamMonitorSI25 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A180_3", "A179_8", "A179_3", "A178_8", "A178_3", "A177_8", "A177_3", "A176_8", "A176_3", "A175_8", "A175_3", "A174_8", "A174_3", "A173_8", "A173_3", "A172_8", "A172_3", "A171_8", "A171_3", "A170_8", "A170_3", "A169_8", "A169_3", "A168_8", "A168_3", "A167_8", "A167_3", "A166_8", "A166_3", "A165_8", "A165_3", "A164_8", "A164_3", "A163_8", "A163_3", "A162_8", "A162_3", "A161_8", "A161_3", "A160_8", "A160_3", "A159_8", "A159_3", "A158_8", "A158_3", "A157_8", "A157_3", "A156_8", "A156_3", "A155_8", "A155_3", "A154_8", "A154_3", "A153_8", "A153_3", "A152_8", "A152_3", "A151_8", "A151_3", "A150_8", "A150_3", "A149_8", "A149_3", "A148_8", "A148_3", "A147_8", "A147_3", "A146_8", "A146_3", "A145_8", "A145_3", "A144_8", "A144_3", "A143_8", "A143_3", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B180_3", "B179_8", "B179_3", "B178_8", "B178_3", "B177_8", "B177_3", "B176_8", "B176_3", "B175_8", "B175_3", "B174_8", "B174_3", "B173_8", "B173_3", "B172_8", "B172_3", "B171_8", "B171_3", "B170_8", "B170_3", "B169_8", "B169_3", "B168_8", "B168_3", "B167_8", "B167_3", "B166_8", "B166_3", "B165_8", "B165_3", "B164_8", "B164_3", "B163_8", "B163_3", "B162_8", "B162_3", "B161_8", "B161_3", "B160_8", "B160_3", "B159_8", "B159_3", "B158_8", "B158_3", "B157_8", "B157_3", "B156_8", "B156_3", "B155_8", "B155_3", "B154_8", "B154_3", "B153_8", "B153_3", "B152_8", "B152_3", "B151_8", "B151_3", "B150_8", "B150_3", "B149_8", "B149_3", "B148_8", "B148_3", "B147_8", "B147_3", "B146_8", "B146_3", "B145_8", "B145_3", "B144_8", "B144_3", "B143_8", "B143_3", };
            }
        }
    }
    public partial class DamMonitorSI26 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A165", "A164_5", "A164", "A163_5", "A163", "A162_5", "A162", "A161_5", "A161", "A160_5", "A160", "A159_5", "A159", "A158_5", "A158", "A157_5", "A157", "A156_5", "A156", "A155_5", "A155", "A154_5", "A154", "A153_5", "A153", "A152_5", "A152", "A151_5", "A151", "A150_5", "A150", "A149_5", "A149", "A148_5", "A148", "A147_5", "A147", "A146_5", "A146", "A145_5", "A145", "A144_5", "A144", "A143_5", "A143", "A142_5", "A142", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B165", "B164_5", "B164", "B163_5", "B163", "B162_5", "B162", "B161_5", "B161", "B160_5", "B160", "B159_5", "B159", "B158_5", "B158", "B157_5", "B157", "B156_5", "B156", "B155_5", "B155", "B154_5", "B154", "B153_5", "B153", "B152_5", "B152", "B151_5", "B151", "B150_5", "B150", "B149_5", "B149", "B148_5", "B148", "B147_5", "B147", "B146_5", "B146", "B145_5", "B145", "B144_5", "B144", "B143_5", "B143", "B142_5", "B142", };
            }
        }
    }
    public partial class DamMonitorSI27 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A224", "A223_5", "A223", "A222_5", "A222", "A221_5", "A221", "A220_5", "A220", "A219_5", "A219", "A218_5", "A218", "A217_5", "A217", "A216_5", "A216", "A215_5", "A215", "A214_5", "A214", "A213_5", "A213", "A212_5", "A212", "A211_5", "A211", "A210_5", "A210", "A209_5", "A209", "A208_5", "A208", "A207_5", "A207", "A206_5", "A206", "A205_5", "A205", "A204_5", "A204", "A203_5", "A203", "A202_5", "A202", "A201_5", "A201", "A200_5", "A200", "A199_5", "A199", "A198_5", "A198", "A197_5", "A197", "A196_5", "A196", "A195_5", "A195", "A194_5", "A194", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B224", "B223_5", "B223", "B222_5", "B222", "B221_5", "B221", "B220_5", "B220", "B219_5", "B219", "B218_5", "B218", "B217_5", "B217", "B216_5", "B216", "B215_5", "B215", "B214_5", "B214", "B213_5", "B213", "B212_5", "B212", "B211_5", "B211", "B210_5", "B210", "B209_5", "B209", "B208_5", "B208", "B207_5", "B207", "B206_5", "B206", "B205_5", "B205", "B204_5", "B204", "B203_5", "B203", "B202_5", "B202", "B201_5", "B201", "B200_5", "B200", "B199_5", "B199", "B198_5", "B198", "B197_5", "B197", "B196_5", "B196", "B195_5", "B195", "B194_5", "B194", };
            }
        }
    }
    public partial class DamMonitorSI28 : AbstractDamSlopeMonitor
    {
        public override string[] SerialColumnsA
        {
            get
            {
                return new[] { "A237", "A236_5", "A236", "A235_5", "A235", "A234_5", "A234", "A233_5", "A233", "A232_5", "A232", "A231_5", "A231", "A230_5", "A230", "A229_5", "A229", "A228_5", "A228", "A227_5", "A227", "A226_5", "A226", "A225_5", "A225", "A224_5", "A224", "A223_5", "A223", "A222_5", "A222", "A221_5", "A221", "A220_5", "A220", "A219_5", "A219", "A218_5", "A218", "A217_5", "A217", "A216_5", "A216", "A215_5", "A215", "A214_5", "A214", "A213_5", "A213", "A212_5", "A212", };
            }
        }
        public override string[] SerialColumnsB
        {
            get
            {
                return new[] { "B237", "B236_5", "B236", "B235_5", "B235", "B234_5", "B234", "B233_5", "B233", "B232_5", "B232", "B231_5", "B231", "B230_5", "B230", "B229_5", "B229", "B228_5", "B228", "B227_5", "B227", "B226_5", "B226", "B225_5", "B225", "B224_5", "B224", "B223_5", "B223", "B222_5", "B222", "B221_5", "B221", "B220_5", "B220", "B219_5", "B219", "B218_5", "B218", "B217_5", "B217", "B216_5", "B216", "B215_5", "B215", "B214_5", "B214", "B213_5", "B213", "B212_5", "B212", };
            }
        }
    }
}

