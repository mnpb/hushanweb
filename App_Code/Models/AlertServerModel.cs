﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AlertServerModel 的摘要描述
/// </summary>
public class AlertServerModel
{
    public short CPUTemp { get; set; }
    public short CPUUsage { get; set; }
    public short MemUsage { get; set; }
    public short HDUsage { get; set; }


}