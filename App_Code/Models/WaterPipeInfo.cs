﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WaterPipeInfo 的摘要描述
/// </summary>
public class WaterPipeInfo
{
    public string StationID
    {
        get;
        set;
    }
    public DateTime InfoTime
    {
        get;
        set;
    }
    public decimal Flow
    {
        get;
        set;
    }
}