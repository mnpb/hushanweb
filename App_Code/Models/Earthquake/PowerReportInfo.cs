﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// PowerReportInfo 的摘要描述
/// </summary>
public class PowerReportInfo
{
    public int Year
    {
        get;
        set;
    }
    public int Month
    {
        get;
        set;
    }
    public int Day
    {
        get;
        set;
    }
    public int Hour
    {
        get;
        set;
    }
    public double Vab
    {
        get;
        set;
    }
    public double Vbc
    {
        get;
        set;
    }
    public double Vca
    {
        get;
        set;
    }
    public double Ia
    {
        get;
        set;
    }
    public double Ib
    {
        get;
        set;
    }
    public double Ic
    {
        get;
        set;
    }
    public double Kw
    {
        get;
        set;
    }
    public double Factor
    {
        get;
        set;
    }
}