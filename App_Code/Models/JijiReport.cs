﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// JijiReport 的摘要描述
/// </summary>
public class JijiReport
{
    public string TimeDescription
    {
        get;
        set;
    }
    public string Waterlevel
    {
        get;
        set;
    }
    public string Volume
    {
        get;
        set;
    }
    public string TotalInFlow
    {
        get;
        set;
    }
    public string SpillwayFlow
    {
        get;
        set;
    }
    public string SandsluicewayFlow
    {
        get;
        set;
    }
    public string FishwayFlow
    {
        get;
        set;
    }
    public string TotalOutFlow
    {
        get;
        set;
    }
    public string NorthFlumeFlow
    {
        get;
        set;
    }
    public string NorthOutFlow
    {
        get;
        set;
    }
    public string SouthFlumeFlow
    {
        get;
        set;
    }
    public string SouthOutFlow
    {
        get;
        set;
    }
    public string WaterPowerSupplyFlow
    {
        get;
        set;
    }
    public string IrrChanghuaSupplyFlow
    {
        get;
        set;
    }
    public string IrrYunlinSupplyFlow
    {
        get;
        set;
    }
    public string PublicSupplyFlow
    {
        get;
        set;
    }
    public string IndustrySupplyFlow
    {
        get;
        set;
    }
    public string DouliouInFlow
    {
        get;
        set;
    }
    public string UpstreamNaturalFlow
    {
        get;
        set;
    }
    public string TaipowerOutFlow
    {
        get;
        set;
    }
    public string JijiRainQty
    {
        get;
        set;
    }
    public string ChuoshuiRiverRainQty
    {
        get;
        set;
    }
    public string NanyuenFlow
    {
        get;
        set;
    }
    public string ChinshuiRainQty
    {
        get;
        set;
    }
    public string ChingshuiRiverRainQty
    {
        get;
        set;
    }
}