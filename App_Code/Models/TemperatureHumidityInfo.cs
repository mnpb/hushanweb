﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// TemperatureHumidityInfo 的摘要描述
/// </summary>
public class TemperatureHumidityInfo
{
    public DateTime InfoTime
    {
        get;
        set;
    }
    public double Temperature
    {
        get;
        set;
    }
    public double Humidity
    {
        get;
        set;
    }
}