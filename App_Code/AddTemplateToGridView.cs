﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// AddTemplateToGridView 的摘要描述
/// </summary>
public class AddTemplateToGridView : ITemplate
{
    ListItemType _type;
    string _colName;

    public AddTemplateToGridView(ListItemType type, string colname)
    {
        _type = type;

        _colName = colname;

    }
    void ITemplate.InstantiateIn(System.Web.UI.Control container)
    {

        switch (_type)
        {
            case ListItemType.Item:

                Label lbl = new Label();
                lbl.DataBinding += new EventHandler(ht_DataBinding);
                container.Controls.Add(lbl);

                break;
        }

    }

    void ht_DataBinding(object sender, EventArgs e)
    {
        Label lbl = (Label)sender;
        GridViewRow container = (GridViewRow)lbl.NamingContainer;
        object dataValue = DataBinder.Eval(container.DataItem, _colName);
        if (dataValue.Equals(IISI.Util.NumericUtility.NoHydData))
        {
            lbl.Text = "--";
        }
        else
        {
            if (_colName.Contains("TEMP") || _colName.Contains("RH") ||
                _colName.Equals("Vab") || _colName.Equals("Vbc") || _colName.Equals("Vca") ||
                _colName.Equals("Ia") || _colName.Equals("Ib") || _colName.Equals("Ic") || _colName.Equals("Kw") || _colName.Equals("Factor"))
            {
                lbl.Text = IISI.Util.NumericUtility.ToFormat(Convert.ToDouble(dataValue), 2);
            }
            else
            {
                lbl.Text = dataValue.ToString();
            }
        }
    }
}