﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// ReservoirService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class ReservoirService : System.Web.Services.WebService
{

    public ReservoirService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public object GetHushanHourInfos(DateTime date)
    {
        return ReservoirController.Instance.GetHushanHourInfos(date);
    }
    [WebMethod]
    public object GetHushanDayReportInfos(DateTime date)
    {
        var hourInfos = ReservoirController.Instance.GetHushanHourInfos(date);
        var allInfos = ConvertToHushanReportInfo(hourInfos);
        allInfos.Add(GetHushanReportAverageInfo(hourInfos));
        allInfos.Add(GetHushanReportSumInfo(hourInfos));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    private List<object> ConvertToHushanReportInfo(List<HushanHour> hourInfos)
    {
        List<object> reportInfos = new List<object>();
        foreach (var hourInfo in hourInfos)
        { // 每小時資料轉成報表格式
            reportInfos.Add(new
            {
                TimeDescription = string.Format("{0}~{1}", hourInfo.InfoTime.Hour, hourInfo.InfoTime.Hour + 1),
                Waterlevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Waterlevel), 2),
                Volume = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Volume), 2),
                VolumeRate = "98.73",
                TotalInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalInFlow), 2),
                SpillwayFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.SpillwayFlow), 2),
                OutletWork1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork1Flow), 2),
                OutletWork2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork2Flow), 2),
                EcologyFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.EcologyFlow), 2),
                TotalOutFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalOutFlow), 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe1Flow), 2),
                OriginalPipe2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe2Flow), 2),
                PublicWaterSupply = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.PublicWaterSupply), 2),
                HushanRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.HushanRainQty), 1),
                TongtouWaterIntakeLevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouWaterIntakeLevel), 2),
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouCrossAreaInFlow), 2),
                TongtouRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouRainQty), 1),
            });
        }

        return reportInfos;
    }
    private object GetHushanReportAverageInfo(List<HushanHour> hourInfos)
    {
        try
        {
            return new
            {
                TimeDescription = "平均",
                Waterlevel = "-",
                Volume = "-",
                VolumeRate = "-",
                TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)), 2),
                SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)), 2),
                OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)), 2),
                OutletWork2Flow = "-",
                EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)), 2),
                TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)), 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)), 2),
                OriginalPipe2Flow = "-",
                PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)), 2),
                HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.HushanRainQty)), 1),
                TongtouWaterIntakeLevel = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouWaterIntakeLevel)), 2),
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)), 2),
                TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouRainQty)), 1),
            };
        }
        catch
        {
            return new object();
        }
    }
    private object GetHushanReportSumInfo(List<HushanHour> hourInfos)
    {
        try
        {
            return new
            {
                TimeDescription = "總計(萬噸)",
                Waterlevel = "-",
                Volume = "-",
                VolumeRate = "-",
                TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)) * 8.64, 2),
                SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)) * 8.64, 2),
                OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)) * 8.64, 2),
                OutletWork2Flow = "-",
                EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)) * 8.64, 2),
                TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)) * 8.64, 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)) * 8.64, 2),
                OriginalPipe2Flow = "-",
                PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)) * 8.64, 2),
                HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.HushanRainQty)), 1),
                TongtouWaterIntakeLevel = "-",
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)) * 8.64, 2),
                TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.TongtouRainQty)), 1),
            };
        }
        catch
        {
            return new object();
        }
    }

    [WebMethod]
    public object GetHushanDayInfos(int year, int month)
    {
        return ReservoirController.Instance.GetHushanDayInfos(year, month);
    }
    [WebMethod]
    public object GetHushanMonthReportInfos(int year, int month)
    {
        var dayInfos = ReservoirController.Instance.GetHushanDayInfos(year, month);
        var allInfos = ConvertToHushanReportDayInfo(dayInfos);
        allInfos.Add(GetHushanReportDayAverageInfo(dayInfos));
        allInfos.Add(GetHushanReportDaySumInfo(dayInfos));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    private List<object> ConvertToHushanReportDayInfo(List<HushanHour> hourInfos)
    {
        List<object> reportInfos = new List<object>();
        foreach (var hourInfo in hourInfos)
        { // 每日資料轉成報表格式
            reportInfos.Add(new
            {
                TimeDescription = hourInfo.InfoTime.Day,
                Waterlevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Waterlevel), 2),
                Volume = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Volume), 2),
                VolumeRate = "98.73",
                //Waterlevel24 = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Waterlevel), 2),
                //Volume24 = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Volume), 2),
                //VolumeRate24 = "98.73",
                TotalInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalInFlow), 2),
                SpillwayFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.SpillwayFlow), 2),
                OutletWork1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork1Flow), 2),
                OutletWork2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork2Flow), 2),
                EcologyFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.EcologyFlow), 2),
                TotalOutFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalOutFlow), 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe1Flow), 2),
                OriginalPipe2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe2Flow), 2),
                PublicWaterSupply = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.PublicWaterSupply), 2),
                HushanRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.HushanRainQty), 1),
                TongtouWaterIntakeLevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouWaterIntakeLevel), 2),
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouCrossAreaInFlow), 2),
                TongtouRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouRainQty), 1),
            });
        }

        return reportInfos;
    }
    private object GetHushanReportDayAverageInfo(List<HushanHour> hourInfos)
    {
        try
        {
            return new
            {
                TimeDescription = "平均",
                Waterlevel = "-",
                Volume = "-",
                VolumeRate = "-",
                //Waterlevel24 = "-",
                //Volume24 = "-",
                //VolumeRate24 = "-",
                TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)), 2),
                SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)), 2),
                OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)), 2),
                OutletWork2Flow = "-",
                EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)), 2),
                TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)), 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)), 2),
                OriginalPipe2Flow = "-",
                PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)), 2),
                HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.HushanRainQty)), 1),
                TongtouWaterIntakeLevel = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouWaterIntakeLevel)), 2),
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)), 2),
                TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouRainQty)), 1),
            };
        }
        catch
        {
            return new object();
        }
    }
    private object GetHushanReportDaySumInfo(List<HushanHour> hourInfos)
    {
        try
        {
            return new
            {
                TimeDescription = "總計(萬噸)",
                Waterlevel = "-",
                Volume = "-",
                VolumeRate = "-",
                //Waterlevel24 = "-",
                //Volume24 = "-",
                //VolumeRate24 = "-",
                TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)) * 8.64, 2),
                SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)) * 8.64, 2),
                OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)) * 8.64, 2),
                OutletWork2Flow = "-",
                EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)) * 8.64, 2),
                TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)) * 8.64, 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)) * 8.64, 2),
                OriginalPipe2Flow = "-",
                PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)) * 8.64, 2),
                HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.HushanRainQty)), 1),
                TongtouWaterIntakeLevel = "-",
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)) * 8.64, 2),
                TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.TongtouRainQty)), 1),
            };
        }
        catch
        {
            return new object();
        }
    }

    [WebMethod]
    public object GetHushanMonthInfos(int year)
    {
        return ReservoirController.Instance.GetHushanMonthInfos(year);
    }
    [WebMethod]
    public object GetHushanYearReportInfos(int year)
    {
        var monthInfos = ReservoirController.Instance.GetHushanMonthInfos(year);
        var allInfos = ConvertToHushanReportMonthInfo(monthInfos);
        allInfos.Add(GetHushanReportMonthAverageInfo(monthInfos));
        allInfos.Add(GetHushanReportMonthSumInfo(monthInfos));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
    private List<object> ConvertToHushanReportMonthInfo(List<HushanHour> hourInfos)
    {
        List<object> reportInfos = new List<object>();
        foreach (var hourInfo in hourInfos)
        { // 每月資料轉成報表格式
            reportInfos.Add(new
            {
                TimeDescription = string.Format("{0}月{1}", hourInfo.InfoTime.Month,
                    IISI.Util.DateUtility.Get10DayDesc(hourInfo.InfoTime.Day / 10 + 1)),
                Waterlevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Waterlevel), 2),
                Volume = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.Volume), 2),
                VolumeRate = "98.73",
                TotalInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalInFlow), 2),
                SpillwayFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.SpillwayFlow), 2),
                OutletWork1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork1Flow), 2),
                OutletWork2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OutletWork2Flow), 2),
                EcologyFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.EcologyFlow), 2),
                TotalOutFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TotalOutFlow), 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe1Flow), 2),
                OriginalPipe2Flow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.OriginalPipe2Flow), 2),
                PublicWaterSupply = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.PublicWaterSupply), 2),
                HushanRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.HushanRainQty), 1),
                TongtouWaterIntakeLevel = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouWaterIntakeLevel), 2),
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouCrossAreaInFlow), 2),
                TongtouRainQty = NumericUtility.ToFormat(NumericUtility.ToHydValue(hourInfo.TongtouRainQty), 1),
            });
        }

        return reportInfos;
    }
    private object GetHushanReportMonthAverageInfo(List<HushanHour> hourInfos)
    {
        try
        {
            return new
            {
                TimeDescription = "平均",
                Waterlevel = "-",
                Volume = "-",
                VolumeRate = "-",
                TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)), 2),
                SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)), 2),
                OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)), 2),
                OutletWork2Flow = "-",
                EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)), 2),
                TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)), 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)), 2),
                OriginalPipe2Flow = "-",
                PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)), 2),
                HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.HushanRainQty)), 1),
                TongtouWaterIntakeLevel = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouWaterIntakeLevel)), 2),
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)), 2),
                TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouRainQty)), 1),
            };
        }
        catch
        {
            return new object();
        }
    }
    private object GetHushanReportMonthSumInfo(List<HushanHour> hourInfos)
    {
        try
        {
            return new
            {
                TimeDescription = "總計(萬噸)",
                Waterlevel = "-",
                Volume = "-",
                VolumeRate = "-",
                TotalInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalInFlow)) * 8.64, 2),
                SpillwayFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.SpillwayFlow)) * 8.64, 2),
                OutletWork1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OutletWork1Flow)) * 8.64, 2),
                OutletWork2Flow = "-",
                EcologyFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.EcologyFlow)) * 8.64, 2),
                TotalOutFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TotalOutFlow)) * 8.64, 2),
                OriginalPipe1Flow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.OriginalPipe1Flow)) * 8.64, 2),
                OriginalPipe2Flow = "-",
                PublicWaterSupply = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.PublicWaterSupply)) * 8.64, 2),
                HushanRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.HushanRainQty)), 1),
                TongtouWaterIntakeLevel = "-",
                TongtouCrossAreaInFlow = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Average(x => x.TongtouCrossAreaInFlow)) * 8.64, 2),
                TongtouRainQty = NumericUtility.ToFormat(Convert.ToDouble(hourInfos.Sum(x => x.TongtouRainQty)), 1),
            };
        }
        catch
        {
            return new object();
        }
    }
}
