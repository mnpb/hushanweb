﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// RainService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class RainService : System.Web.Services.WebService
{

    public RainService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod(Description = "取得所有雨量站")]
    public dynamic GetAllStations()
    {
        return RainController.Instance.GetAllStations();
    }

    private dynamic GetHourInfos(string stationID, DateTime beginTime, DateTime endTime)
    {
        var hourInfos = RainController.Instance.GetHourInfos(stationID, beginTime, endTime);

        List<RainQtyInfo> allInfos = new List<RainQtyInfo>();
        double accRainQty = 0;
        for (DateTime dateTime = beginTime; dateTime.CompareTo(endTime) <= 0; dateTime = dateTime.AddHours(1))
        {
            var theInfo = (from item in hourInfos
                           where item.InfoTime == dateTime
                           select item).FirstOrDefault();
            double rainQty = Convert.ToDouble(theInfo != null ? theInfo.RainQty : 0);
            accRainQty += IISI.Util.NumericUtility.ToPositive(rainQty);
            allInfos.Add(new RainQtyInfo
            {
                StationID = stationID,
                InfoTime = dateTime,
                RainQty = rainQty,
                AccRainQty = accRainQty,
            });
        }
        return allInfos;
    }

    [WebMethod(Description = "取得雨量站小時資料")]
    public dynamic GetHourInfos(string stationID, DateTime date)
    {
        DateTime beginTime = date.AddHours(1);
        DateTime endTime = beginTime.AddHours(23);
        return GetHourInfos(stationID, beginTime, endTime);
    }

    [WebMethod(Description = "取得雨量站小時資料")]
    public dynamic GetHourInfosForGrid(string stationID, DateTime date)
    {
        DateTime beginTime = date.AddHours(1);
        DateTime endTime = beginTime.AddHours(23);
        var allInfos = GetHourInfos(stationID, beginTime, endTime);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    private dynamic GetDayInfos(string stationID, DateTime beginDate, DateTime endDate)
    {
        var dayInfos = RainController.Instance.GetDayInfos(stationID, beginDate, endDate);

        List<RainQtyInfo> allInfos = new List<RainQtyInfo>();
        double accRainQty = 0;
        for (DateTime date = beginDate; date.CompareTo(endDate) <= 0; date = date.AddDays(1))
        {
            var theInfo = (from item in dayInfos
                           where item.InfoDate == date
                           select item).FirstOrDefault();
            double rainQty = Convert.ToDouble(theInfo != null ? theInfo.RainQty : 0);
            accRainQty += IISI.Util.NumericUtility.ToPositive(rainQty);
            allInfos.Add(new RainQtyInfo
            {
                StationID = stationID,
                InfoTime = date,
                RainQty = rainQty,
                AccRainQty = accRainQty,
            });
        }
        return allInfos;
    }

    [WebMethod(Description = "取得雨量站日資料")]
    public dynamic GetDayInfos(string stationID, int year, int month)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        return GetDayInfos(stationID, beginDate, endDate);
    }

    [WebMethod(Description = "取得雨量站日資料")]
    public dynamic GetDayInfosForGrid(string stationID, int year, int month)
    {
        DateTime beginDate = new DateTime(year, month, 1);
        DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
        var allInfos = GetDayInfos(stationID, beginDate, endDate);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    private dynamic _GetMonthInfos(string stationID, int year)
    {
        var monthInfos = RainController.Instance.GetMonthInfos(stationID, year);

        List<RainQtyInfo> allInfos = new List<RainQtyInfo>();
        double accRainQty = 0;
        for (int month = 1; month <= 12; month++)
        {
            var theInfo = (from item in monthInfos
                           where item.InfoMonth == month
                           select item).FirstOrDefault();
            double rainQty = Convert.ToDouble(theInfo != null ? theInfo.RainQty : 0);
            accRainQty += IISI.Util.NumericUtility.ToPositive(rainQty);
            allInfos.Add(new RainQtyInfo
            {
                StationID = stationID,
                InfoTime = new DateTime(year, month, 1),
                RainQty = rainQty,
                AccRainQty = accRainQty,
            });
        }
        return allInfos;
    }

    [WebMethod(Description = "取得雨量站月資料")]
    public dynamic GetMonthInfos(string stationID, int year)
    {
        return _GetMonthInfos(stationID, year);
    }

    [WebMethod(Description = "取得雨量站月資料")]
    public dynamic GetMonthInfosForGrid(string stationID, int year)
    {
        var allInfos = _GetMonthInfos(stationID, year);
        try
        {
            var resultObj = new
            {
                total = 1, // 总页数
                page = 1, // 由于客户端是loadonce的，page也可以不指定
                records = allInfos.Count, // 总记录数
                rows = allInfos, // 数据
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
}

public class RainQtyInfo
{
    public string StationID
    {
        get;
        set;
    }
    public DateTime InfoTime
    {
        get;
        set;
    }
    public double RainQty
    {
        get;
        set;
    }
    public double AccRainQty
    {
        get;
        set;
    }
}
