﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// NetworkEquipmentService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
 [System.Web.Script.Services.ScriptService]
public class NetworkEquipmentService : System.Web.Services.WebService {

    public NetworkEquipmentService () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetAllNetworkEquipmentCategories()
    {
        return JsonConvert.SerializeObject(NetworkEquipmentCategoryController.Instance.GetAllCategories());
    }

    [WebMethod]
    public object GetNetworkEquipmentRealtimeStatus(string equipmentCategoryID)
    {
        var allInfos = NetworkEquipmentController.Instance.GetNetworkEquipmentRealtimeStatus(Convert.ToByte(equipmentCategoryID));
        try
        {
            var resultObj = new
            {
                total = 1, 
                page = 1, 
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }

    [WebMethod]
    public object GetNetworkEquipmentHistoryStatus(string queryDate, string equipmentCategoryID)
    {
        var allInfos = NetworkEquipmentController.Instance.GetNetworkEquipmentHistoryStatus(
            Convert.ToDateTime(queryDate), Convert.ToByte(equipmentCategoryID));
        try
        {
            var resultObj = new
            {
                total = 1,
                page = 1,
                records = allInfos.Count,
                rows = allInfos,
            };
            return resultObj;
        }
        catch
        {
            return null;
        }
    }
}
