﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 子群組
/// </summary>
[Serializable]
[DataObject]
public class Department : IDisposable
{
	#region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion
    
    #region 建構子
    public Department()
    {

    }
    #endregion

    #region 解構子
    ~Department()
    {
        Dispose(false);
    }
    
    /// <summary>
    /// 釋放Department資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放Department資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion

    #region 類別成員

    /// <summary>
    /// 子群組編號
    /// </summary>
    public short DepartmentID { get; set; }

    /// <summary>
    /// 子群組名稱
    /// </summary>
    public string Name { get; set; }

    #endregion

    #region 類別方法

    /// <summary>
    /// 取得所有的子群組
    /// </summary>
    /// <returns></returns>
    public List<Department> GetDepartmentList()
    {
        List<Department> DepartmentList = null;

        string SqlStatements = "SELECT * FROM [Department] ORDER BY [DepartmentID]; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    DepartmentList = DataConvert.FillDataListGeneric<Department>(DataReader);
        }

        return DepartmentList; 
    }

    /// <summary>
    /// 取得一筆子群組
    /// </summary>
    /// <param name="DepartmentID">子群組編號</param>
    /// <returns></returns>
    public Department GetDepartment(short DepartmentID)
    {
        Department ReturnDepartment = null;

        string SqlStatements = "SELECT * FROM [Department] WHERE [DepartmentID] = @DepartmentID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@DepartmentID", DepartmentID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ReturnDepartment = DataConvert.FillDataListGeneric<Department>(DataReader)[0];
        }

        return ReturnDepartment;     
    }

    #endregion
}