﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 選單對應群組
/// </summary>
[Serializable]
[DataObject]
public class MonitoredServer : BaseDAL, IDisposable
{
    #region 建構子
    public MonitoredServer()
    {
    }
    #endregion

    #region 解構子
    ~MonitoredServer()
    {
        Dispose(false);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion


    /// <summary>
    /// 
    /// </summary>
    /// <param name="IPs">傳入的ip會用;隔開</param>
    /// <param name="ip"></param>
    /// <returns></returns>
    public bool IsExist(string IPs, out string ip)
    {
        string SqlStatements = @"SELECT IP  FROM [Hushan].[dbo].[MonitoredServerList]";

        DataTable dt = new System.Data.DataTable();
        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            dt = DbAdapter.ExcuteDataTable(SqlStatements);
        }
        foreach (DataRow dr in dt.Rows)
        {
            string whiteip = dr[0].ToString();
            if (IPs.Split(new char[] { ';' }).Contains(whiteip))
            {
                ip = whiteip;
                return true;
            }
        }

        ip = "";
        return false;
    }


    public bool InsertStatus(string ServerIP, short? CPUTemperature, short? MemoryUsage, short? HD_C, short? HD_D,
        short? Power, short? Power1, double? net1, double? net2, double? net3, double? net4)
    {
        DateTime now = DateTime.Now;
        string SqlStatements = @"INSERT INTO MonitoredServerStatus (ServerIP,CPUTemperature,MemoryUsage,HD_C,HD_D,Power,Power1,net1,net2,net3,net4,DatetimeStamp,CreateDatetime) 
                                values (@ServerIP,@CPUTemperature,@MemoryUsage,@HD_C,@HD_D,@Power,@Power1,@net1,@net2,@net3,@net4,@DatetimeStamp,@CreateDatetime)";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@ServerIP", ServerIP));
        DbAParameterList.Add(new DbAdapterParameter("@CPUTemperature", CPUTemperature));
        DbAParameterList.Add(new DbAdapterParameter("@MemoryUsage", MemoryUsage));
        DbAParameterList.Add(new DbAdapterParameter("@HD_C", HD_C));
        DbAParameterList.Add(new DbAdapterParameter("@HD_D", HD_D));
        DbAParameterList.Add(new DbAdapterParameter("@Power", Power));
        DbAParameterList.Add(new DbAdapterParameter("@Power1", Power1));
        DbAParameterList.Add(new DbAdapterParameter("@net1", net1));
        DbAParameterList.Add(new DbAdapterParameter("@net2", net2));
        DbAParameterList.Add(new DbAdapterParameter("@net3", net3));
        DbAParameterList.Add(new DbAdapterParameter("@net4", net4));
        DbAParameterList.Add(new DbAdapterParameter("@DatetimeStamp", DatetimeUtility.MinDownTo0or10String(now)));
        DbAParameterList.Add(new DbAdapterParameter("@CreateDatetime", now));

        int count = 0;
        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            count = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray());
        }
        if (count > 0)
            return true;
        return false;
    }


    public List<MonitoredServerStatusInfo> GetServerStatus(DateTime beginDatetime, DateTime endtime)
    {
        string SqlStatements = @"
    SELECT  
    ServerIP,CPUTemperature,MemoryUsage,DatetimeStamp,CreateDatetime,list.Name,Location,HD_C,HD_D,Power,Power1,net1,net2,net3,net4
    FROM MonitoredServerList list
	left join [dbo].[NetworkEquipmentBase] base on list.IP = base.IP
    LEFT JOIN MonitoredServerStatus s on base.IP = s.ServerIP
    where createdatetime between @begintime and @endtime

";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@begintime", DatetimeUtility.MinDownTo0or10(beginDatetime)));
        DbAParameterList.Add(new DbAdapterParameter("@endtime", endtime.ToString("yyyy/MM/dd HH:mm:ss")));

        var ans = new List<MonitoredServerStatusInfo>();
        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ans = DataConvert.FillDataListGeneric<MonitoredServerStatusInfo>(DataReader);
        }
        return ans;
    }
}