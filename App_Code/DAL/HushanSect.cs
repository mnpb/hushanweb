﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 選單對應群組
/// </summary>
[Serializable]
[DataObject]
public class HushanSect : BaseDAL, IDisposable
{
    #region 建構子
    public HushanSect()
    {
    }
    #endregion

    #region 解構子
    ~HushanSect()
    {
        Dispose(false);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion



    public List<GISPointInfo> AreasContainPoint(string x, string y)
    {
        string SqlStatements = @"
DECLARE @g geometry;
SET @g = geometry::STGeomFromText('POINT (' + @x + ' ' + @y + ')', 0);
SELECT [Polygon].ToString() as polygon,[Sect] as [Sectno],landno
FROM [HushanSect] 
where [Polygon].STContains(@g)=1
";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@x", x));
        DbAParameterList.Add(new DbAdapterParameter("@y", y));


        var ans = new List<GISPointInfo>();
        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ans = DataConvert.FillDataListGeneric<GISPointInfo>(DataReader);
        }

        return ans;


        //SqlConnection conn = new SqlConnection(base.ConnectionString);
        //SqlCommand comm = new SqlCommand(SqlStatements, conn);
        //conn.Open();
        //SqlDataAdapter d = new SqlDataAdapter(comm);
        //DataTable dt = new DataTable();
        //d.Fill(dt);


        //Utilities util = new Utilities();
        //geDocument doc = util.SqlGeogCommandToKmlDoc("SQL Server Spatial Sample Doc",
        //        ConnectionString,
        //        SqlStatements);
        //geKML kml = new geKML(doc);
        //string path = HttpContext.Current.Server.MapPath("a.kml");
        //File.WriteAllBytes(path, kml.ToKML());
        //ProcessStartInfo psInfNotepad = new ProcessStartInfo("notepad.exe", path);
        //Process.Start(psInfNotepad);


        //SqlGeography geog = SqlGeography.Parse(dt.Rows[0][0].ToString());
        //string s = geog.AsKML("Name", "Description");
        //Console.WriteLine(s);

        //var ans = new List<GISPointInfo>();
    }
}