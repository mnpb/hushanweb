﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 選單
/// </summary>
[Serializable]
[DataObject]
public class Menu : IDisposable
{
    #region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion

    #region 建構子
    public Menu()
    {

    }
    #endregion

    #region 解構子
    ~Menu()
    {
        Dispose(false);
    }
    
    /// <summary>
    /// 釋放Menu資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放Menu資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion

    #region 類別成員

    /// <summary>
    /// 選單編號
    /// </summary>
    public int MenuID { get; set; }

    /// <summary>
    /// 選單名稱
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 對應的URL
    /// </summary>
    public string NavigateUrl { get; set; }

    /// <summary>
    /// 開啟選單時對應的網頁Target
    /// </summary>
    public string NavigateTarget { get; set; }

    /// <summary>
    /// 上一層選單編號
    /// </summary>
    public int ParentID { get; set; }

    /// <summary>
    /// 選單顯示順序
    /// </summary>
    public int Sequence { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    public string Remark { get; set; }

    /// <summary>
    /// 下一層選單
    /// </summary>
    public List<Menu> NextNodeMenu
    {
        get { return this.GetNextNodeMenuList(this.MenuID); }    
    }
    

    #endregion

    #region 類別方法

    /// <summary>
    /// 取得一筆選單
    /// </summary>
    /// <param name="MenuID">選單編號</param>
    /// <returns></returns>
    public Menu GetMenu(int MenuID)
    {
        Menu ReturnMenut = null;

        string SqlStatements = "SELECT * FROM [Menu] WHERE [MenuID] = @MenuID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@MenuID", MenuID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ReturnMenut = DataConvert.FillDataListGeneric<Menu>(DataReader)[0];
        }

        return ReturnMenut;
    }

    /// <summary>
    /// 取得所有的選單
    /// </summary>
    /// <returns></returns>
    public List<Menu> GetMenuList()
    {
        List<Menu> MenuList = new List<Menu>();

        string SqlStatements = "SELECT * FROM [Menu] ORDER BY [Sequence]; ";

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements))
                if (DataReader.HasRows)
                    MenuList = DataConvert.FillDataListGeneric<Menu>(DataReader);
        }

        return MenuList;
    }

    /// <summary>
    /// 取得第一層所有的選單
    /// </summary>
    /// <returns></returns>
    public List<Menu> GetFirstNodeMenuList()
    {
        List<Menu> MenuList = new List<Menu>();

        string SqlStatements = "SELECT * FROM [Menu] WHERE [ParentID] = 0 ORDER BY [Sequence]; ";

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements))
                if (DataReader.HasRows)
                    MenuList = DataConvert.FillDataListGeneric<Menu>(DataReader);
        }

        return MenuList;
    }

     /// <summary>
    /// 依照選單編號取得下一層選單
    /// </summary>
    /// <param name="MenuID">選單編號</param>
    /// <returns></returns>
    private List<Menu> GetNextNodeMenuList(int MenuID)
    {
        List<Menu> MenuList = new List<Menu>();

        string SqlStatements = "SELECT * FROM [Menu] WHERE [ParentID] = @MenuID ORDER BY [Sequence]; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@MenuID", MenuID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    MenuList = DataConvert.FillDataListGeneric<Menu>(DataReader);
        }

        return MenuList;
    }

    #endregion 
    public override string ToString()
    {
        return this.Name;
    }
}