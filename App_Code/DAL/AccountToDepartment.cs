﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 帳號對應子群組
/// </summary>
[Serializable]
[DataObject]
public class AccountToDepartment : IDisposable
{
	#region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion
    
    #region 建構子
    public AccountToDepartment()
    {
        _Department = new Department();
    }
    #endregion

    #region 解構子
    ~AccountToDepartment()
    {
        Dispose(false);
    }
    
    /// <summary>
    /// 釋放AccountToDepartment資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放AccountToDepartment資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
        _Department.Dispose();
    }
    #endregion

    #region 類別成員

    private Department _Department;

    /// <summary>
    /// 帳號編號
    /// </summary>
    public int AccountID { get; set; }

    /// <summary>
    /// 子群組編號
    /// </summary>
    public short DepartmentID { get; set; }

    /// <summary>
    /// 對應之部門
    /// </summary>
    public Department Department
    {
        get { return _Department.GetDepartment(this.DepartmentID); }
    }

    #endregion

    #region 類別方法

    /// <summary>
    /// 新增帳號對應子群組
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <param name="DepartmentID">子群組編號</param>
    /// <returns></returns>
    public int AddAccountToDepartment(int AccountID, short DepartmentID)
    {
        int ReturnInt = 0;

        string SqlStatements = " INSERT INTO [AccountToDepartment]([AccountID],[DepartmentID]) " +
                               "			                VALUES(@AccountID, @DepartmentID); ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));
        DbAParameterList.Add(new DbAdapterParameter("@DepartmentID", DepartmentID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;    
    }

    /// <summary>
    /// 依子群組編號刪除對應之帳號
    /// </summary>
    /// <param name="AccountID"></param>
    /// <returns></returns>
    public int DeleteAccountToDepartmentByAccountID(int AccountID)
    {
        int ReturnInt = 0;

        string SqlStatements = " DELETE FROM [AccountToDepartment] WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 依帳號編號取得帳號對應子群組
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <returns></returns>
    public AccountToDepartment GetAccountToDepartmentByAccountID(int AccountID)
    {
        AccountToDepartment ReturnAccountToDepartment = null;

        string SqlStatements = "SELECT * FROM [AccountToDepartment] WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ReturnAccountToDepartment = DataConvert.FillDataListGeneric<AccountToDepartment>(DataReader)[0];
        }
        return ReturnAccountToDepartment;
    }

    /// <summary>
    /// 依子群組編號取得對應之帳號
    /// </summary>
    /// <param name="DepartmentID">子群組編號</param>
    /// <returns></returns>
    public List<AccountToDepartment> GetAccountToDepartmentByDepartmentID(short DepartmentID)
    {
        List<AccountToDepartment> AccountToDepartmentList = new List<AccountToDepartment>();

        string SqlStatements = "SELECT * FROM [AccountToDepartment] WHERE [DepartmentID] = @DepartmentID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@DepartmentID", DepartmentID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountToDepartmentList = DataConvert.FillDataListGeneric<AccountToDepartment>(DataReader);
        }
        return AccountToDepartmentList;
    }    

    #endregion
}