﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 登錄記錄
/// </summary>
[Serializable]
[DataObject]
public class AccountLogin : IDisposable
{
    #region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion

    #region 建構子
    public AccountLogin()
    {

    }
    #endregion

    #region 解構子
    ~AccountLogin()
    {
        Dispose(false);
    }

    /// <summary>
    /// 釋放AccountLogin資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放AccountLogin資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion

    #region 類別成員

    /// <summary>
    /// 登錄記錄編號
    /// </summary>
    public long LoginID { get; set; }

    /// <summary>
    /// 帳號編號
    /// </summary>
    public int AccountID { get; set; }

    /// <summary>
    /// 1. 登入  2. 登出
    /// </summary>
    public byte Aaction { get; set; }

    /// <summary>
    /// 登錄IP
    /// </summary>
    public string LoginIP { get; set; }

    /// <summary>
    /// 登錄時間
    /// </summary>
    public DateTime ActionDateTime { get; set; }

    #endregion

    #region 類別方法

    /// <summary>
    /// 依帳號編號取得登錄記錄
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <returns></returns>
    public List<AccountLogin> GetAccountLoginList(int AccountID)
    {
        List<AccountLogin> AccountLoginList = new List<AccountLogin>();

        string SqlStatements = "SELECT * FROM [AccountLogin] WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountLoginList = DataConvert.FillDataListGeneric<AccountLogin>(DataReader);
        }

        return AccountLoginList;   
    }

    /// <summary>
    /// 取得最新100筆登錄記錄
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <returns></returns>
    public List<AccountLogin> GetLatest100AccountLoginList(int AccountID)
    {
        List<AccountLogin> AccountLoginList = new List<AccountLogin>();

        string SqlStatements = "SELECT TOP 100 * FROM [AccountLogin] WHERE [AccountID] = @AccountID ORDER BY [ActionDateTime] DESC; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountLoginList = DataConvert.FillDataListGeneric<AccountLogin>(DataReader);
        }

        return AccountLoginList;
    }

    /// <summary>
    /// 新增一筆
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <param name="Aaction">1. 登入  2. 登出</param>
    /// <param name="LoginIP">登錄IP</param>
    /// <param name="ActionDateTime">登錄時間</param>
    /// <returns></returns>
    public int AddAccountLogin(int AccountID, byte Aaction, string LoginIP, DateTime ActionDateTime)
    {
        int ReturnInt = 0;

        string SqlStatements = " INSERT INTO [AccountLogin]([AccountID],[Aaction],[LoginIP],[ActionDateTime]) " +
                               "	                 VALUES(@AccountID, @Aaction, @LoginIP, @ActionDateTime); ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));
        DbAParameterList.Add(new DbAdapterParameter("@Aaction", Aaction));
        DbAParameterList.Add(new DbAdapterParameter("@LoginIP", string.IsNullOrEmpty(LoginIP) ? "" : LoginIP));
        DbAParameterList.Add(new DbAdapterParameter("@ActionDateTime", ActionDateTime));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }
        return ReturnInt;    
    }

    #endregion
}