﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 帳號對應群組
/// </summary>
[Serializable]
[DataObject]
public class AccountToGroup : IDisposable
{
    #region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion

    #region 建構子
    public AccountToGroup()
    {

    }
    #endregion

    #region 解構子
    ~AccountToGroup()
    {
        Dispose(false);
    }
    
    /// <summary>
    /// 釋放AccountToGroup資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放AccountToGroup資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion

    #region 類別成員

    /// <summary>
    /// 帳號編號
    /// </summary>
    public int AccountID { get; set; }

    /// <summary>
    /// 群組編號
    /// </summary>
    public int GroupID { get; set; }

    #endregion

    #region 類別方法

    /// <summary>
    /// 取得所有的群組
    /// </summary>
    /// <returns></returns>
    public List<AccountToGroup> GetAccountToGroup()
    {
        List<AccountToGroup> AccountToGroupList = new List<AccountToGroup>();

        string SqlStatements = "SELECT * FROM [AccountToGroup]; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountToGroupList = DataConvert.FillDataListGeneric<AccountToGroup>(DataReader);
        }

        return AccountToGroupList;
    }

    /// <summary>
    /// 依照帳號取得帳號對應群組
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <returns></returns>
    public List<AccountToGroup> GetAccountToGroupByAccountID(int AccountID)
    {
        List<AccountToGroup> AccountToGroupList = new List<AccountToGroup>();

        string SqlStatements = "SELECT * FROM [AccountToGroup] WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountToGroupList = DataConvert.FillDataListGeneric<AccountToGroup>(DataReader);
        }

        return AccountToGroupList;
    }

    /// <summary>
    /// 依群組取得帳號對應群組
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public List<AccountToGroup> GetAccountToGroupByGroupID(int GroupID)
    {
        List<AccountToGroup> AccountToGroupList = new List<AccountToGroup>();

        string SqlStatements = "SELECT * FROM [AccountToGroup] WHERE [GroupID] = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountToGroupList = DataConvert.FillDataListGeneric<AccountToGroup>(DataReader);
        }

        return AccountToGroupList;
    }

    /// <summary>
    /// 新增一筆帳號對應群組
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public int AddAccountToGroup(int AccountID, int GroupID)
    {
        int ReturnInt = 0;

        string SqlStatements = " INSERT INTO [AccountToGroup]([AccountID],[GroupID]) " +
                               "                       VALUES(@AccountID, @GroupID); ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 依群組刪除帳號對應群組
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public int DeleteAccountToGroupByroupID(int GroupID)
    {
        int ReturnInt = 0;

        string SqlStatements = " DELETE FROM [AccountToGroup] WHERE [GroupID] = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 依帳號編號刪除帳號對應群組
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <returns></returns>
    public int DeleteAccountToGroupByAccountID(int AccountID)
    {
        int ReturnInt = 0;

        string SqlStatements = " DELETE FROM [AccountToGroup] WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    #endregion
}