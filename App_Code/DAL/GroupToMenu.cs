﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 選單對應群組
/// </summary>
[Serializable]
[DataObject]
public class GroupToMenu : IDisposable
{
    #region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion

    #region 建構子
    public GroupToMenu()
    {
        _Menu = new Menu();
    }
    #endregion

    #region 解構子
    ~GroupToMenu()
    {
        Dispose(false);
    }
    
    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.

        _Menu.Dispose();
    }
    #endregion

    #region 類別成員

    private Menu _Menu;

    /// <summary>
    /// 選單編號
    /// </summary>
    public int MenuID { get; set; }

    /// <summary>
    /// 群組編號
    /// </summary>
    public int GroupID { get; set; }

    /// <summary>
    /// 對應之選單
    /// </summary>
    public Menu Menu
    {
        get { return _Menu.GetMenu(this.MenuID); }    
    }

    #endregion

    #region 類別方法

    /// <summary>
    /// 依據群組編號取得對應之選單
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public List<GroupToMenu> GetGroupToMenuByGroupID(int GroupID)
    {
        List<GroupToMenu> GroupToMenuList = new List<GroupToMenu>();

        string SqlStatements = "SELECT * FROM [GroupToMenu] WHERE [GroupID] = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    GroupToMenuList = DataConvert.FillDataListGeneric<GroupToMenu>(DataReader);
        }
        return GroupToMenuList;       
    }

    /// <summary>
    /// 依據群組清單取得對應之所有的選單
    /// </summary>
    /// <param name="GroupList">群組列表</param>
    /// <returns></returns>
    public List<GroupToMenu> GetGroupToMenuByGroupList(List<Group> GroupList)
    {
        string GroupListString = string.Empty;

        List<GroupToMenu> GroupToMenuList = new List<GroupToMenu>();
        string SqlStatements = string.Empty;

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();

        foreach (Group Group in GroupList)
            GroupListString += string.Format("{0:000},", Group.GroupID);

        if (GroupListString.Length > 0)
            GroupListString = GroupListString.Remove(GroupListString.Length - 1, 1);   //去除最後的逗號 

        SqlStatements = "SELECT * FROM [GroupToMenu] WHERE CHARINDEX(REPLACE(STR([GroupID], 3,0), ' ', '0'), @GroupListString) > 0; ";
        DbAParameterList.Add(new DbAdapterParameter("@GroupListString", GroupListString));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    GroupToMenuList = DataConvert.FillDataListGeneric<GroupToMenu>(DataReader);
        }

        return GroupToMenuList;
    }

    /// <summary>
    /// 新增一筆群組對應之選單
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <param name="MenuID">選單編號</param>
    /// <returns></returns>
    public int AddGroupToMenu(int GroupID, int MenuID)
    {
        int ReturnInt = 0;

        string SqlStatements = " INSERT INTO [GroupToMenu]([MenuID],[GroupID]) " +
                               "                   VALUES (@MenuID, @GroupID); ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));
        DbAParameterList.Add(new DbAdapterParameter("@MenuID", MenuID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 依群組編號刪除對應之選單
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public int DeleteGroupToMenuByGroupID(int GroupID)
    {
        int ReturnInt = 0;

        string SqlStatements = " DELETE FROM [GroupToMenu] WHERE [GroupID] = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 依選單編號刪除對應之選單
    /// </summary>
    /// <param name="MenuID">選單編號</param>
    /// <returns></returns>
    public int DeleteGroupToMenuByMenuID(int MenuID)
    {
        int ReturnInt = 0;

        string SqlStatements = " DELETE FROM [GroupToMenu] WHERE [MenuID] = @MenuID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    #endregion
}