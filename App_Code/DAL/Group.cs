﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 群組
/// </summary>
[Serializable]
[DataObject]
public class Group : IDisposable
{
    #region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion

    #region 建構子
    public Group()
    {
        _AccountToGroup = new AccountToGroup();
        _GroupToMenu = new GroupToMenu();
    }
    #endregion

    #region 解構子
    ~Group()
    {
        Dispose(false);
    }

    /// <summary>
    /// 釋放Group資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放Group資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
        _GroupToMenu.Dispose();
        _AccountToGroup.Dispose();
    }
    #endregion

    #region 類別成員

    private AccountToGroup _AccountToGroup;
    private GroupToMenu _GroupToMenu;

    /// <summary>
    /// 群組編號
    /// </summary>
    public int GroupID { get; set; }

    /// <summary>
    /// 群組名稱
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 是否為管理群組
    /// </summary>
    public bool IsAdministrator { get; set; }

    /// <summary>
    /// 備註
    /// </summary>
    public string Remark { get; set; }

    /// <summary>
    /// 群組對應選單
    /// </summary>
    public List<GroupToMenu> GroupToMenuList
    {
        get { return _GroupToMenu.GetGroupToMenuByGroupID(this.GroupID); }    
    }

    #endregion

    #region 類別方法

    /// <summary>
    /// 取得一筆群組
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public Group GetGroup(int GroupID)
    {
        Group ReturnGroup = null;

        string SqlStatements = "SELECT * FROM [Group] WHERE [GroupID] = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ReturnGroup = DataConvert.FillDataListGeneric<Group>(DataReader)[0];
        }

        return ReturnGroup;    
    }

    /// <summary>
    /// 依據帳號對應群組取得群組清單
    /// </summary>
    /// <param name="AccountToGroupList">帳號對應群組</param>
    /// <returns></returns>
    public List<Group> GetGroupList(List<AccountToGroup> AccountToGroupList)
    {
        List<Group> GroupList = new List<Group>();

        string AccountToGroupString = string.Empty;
        string SqlStatements = string.Empty;

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();

        foreach (AccountToGroup AccountToGroup in AccountToGroupList)
            AccountToGroupString += string.Format("{0:000},", AccountToGroup.GroupID);

        if (AccountToGroupString.Length > 0)
            AccountToGroupString = AccountToGroupString.Remove(AccountToGroupString.Length - 1, 1);   //去除最後的逗號 

        SqlStatements = "SELECT * FROM [Group] WHERE CHARINDEX(REPLACE(STR([GroupID], 3,0), ' ', '0'), @AccountToGroupString) > 0; ";
        DbAParameterList.Add(new DbAdapterParameter("@AccountToGroupString", AccountToGroupString));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    GroupList = DataConvert.FillDataListGeneric<Group>(DataReader);
        }

        return GroupList;    
    }


    /// <summary>
    /// 取得所有的群組
    /// </summary>
    /// <returns></returns>
    public List<Group> GetGroupList()
    {
        List<Group> GroupList = new List<Group>();

        string SqlStatements = "SELECT * FROM [Group]; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    GroupList = DataConvert.FillDataListGeneric<Group>(DataReader);
        }

        return GroupList; 
    }

    /// <summary>
    /// 新增一筆群組
    /// </summary>
    /// <param name="Name">群組名稱</param>
    /// <param name="IsAdministrator">是否為管理群組</param>
    /// <param name="Remark">備註</param>
    /// <returns></returns>
    public int AddGroup(string Name, bool IsAdministrator, string Remark)
    {
        int ReturnInt = 0;

        string SqlStatements = " INSERT INTO [Group]([Name],[IsAdministrator],[Remark]) " +
                               "             VALUES (@Name, @IsAdministrator, @Remark); ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@Name", Name));
        DbAParameterList.Add(new DbAdapterParameter("@IsAdministrator", IsAdministrator));
        DbAParameterList.Add(new DbAdapterParameter("@Remark", string.IsNullOrEmpty(Remark) ? "" : Remark));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;      
    }
    
    /// <summary>
    /// 新增一筆群組並回傳該群組編號
    /// </summary>
    /// <param name="Name">群組名稱</param>
    /// <param name="IsAdministrator">是否為管理群組</param>
    /// <param name="Remark">備註</param>
    /// <returns></returns>
    public int AddGroupReturnGroupID(string Name, bool IsAdministrator, string Remark)
    {
        int GroupID = 0;

        string SqlStatements = " INSERT INTO [Group]([Name],[IsAdministrator],[Remark]) VALUES (@Name, @IsAdministrator, @Remark); SELECT MAX(GroupID) FROM [Group]; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@Name", Name));
        DbAParameterList.Add(new DbAdapterParameter("@IsAdministrator", IsAdministrator));
        DbAParameterList.Add(new DbAdapterParameter("@Remark", string.IsNullOrEmpty(Remark) ? "" : Remark));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    while (DataReader.Read())
                        GroupID = (int)DataReader[0];
        }

        return GroupID;
    }

    /// <summary>
    /// 修改一筆群組
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <param name="Name">群組名稱</param>
    /// <param name="IsAdministrator">是否為管理群組</param>
    /// <param name="Remark">備註</param>
    /// <returns></returns>
    public int ModifyGroup(int GroupID, string Name, bool IsAdministrator, string Remark)
    {
        int ReturnInt = 0;

        string SqlStatements = " UPDATE [Group] " +
                               "    SET [Name] = @Name " +
                               "       ,[IsAdministrator] = @IsAdministrator " + 
                               "       ,[Remark] = @Remark " + 
                               "  WHERE GroupID = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@Name", Name));
        DbAParameterList.Add(new DbAdapterParameter("@IsAdministrator", IsAdministrator));
        DbAParameterList.Add(new DbAdapterParameter("@Remark", string.IsNullOrEmpty(Remark) ? "" : Remark));
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try { DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 刪除一筆群組
    /// </summary>
    /// <param name="GroupID">群組編號</param>
    /// <returns></returns>
    public int DeleteGroup(int GroupID)
    {
        int ReturnInt = 0;

        string SqlStatements = " DELETE FROM [Group] WHERE [GroupID] = @GroupID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@GroupID", GroupID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try 
            {
                _AccountToGroup.DeleteAccountToGroupByroupID(GroupID);
                _GroupToMenu.DeleteGroupToMenuByGroupID(GroupID);
                ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray()); 
            }
            catch (Exception e) { throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message); }
        }

        return ReturnInt;    
    }

    #endregion
}