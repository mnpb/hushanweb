﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 選單對應群組
/// </summary>
[Serializable]
[DataObject]
public class AlertServerDAL : BaseDAL, IDisposable
{
    #region 建構子
    public AlertServerDAL()
    {
    }
    ~AlertServerDAL()
    {
        Dispose(false);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放GroupToMenu資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
    }
    #endregion


    public AlertServerModel Get()
    {
        DateTime now = DateTime.Now;
        string SqlStatements = @"select * from alertserver";

        AlertServerModel model = new AlertServerModel();
        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements))
                if (DataReader.HasRows)
                {
                    var record = DataConvert.FillDataListGeneric<AlertServerModel>(DataReader);
                    model = record[0];
                }
        }
        return model;
    }


    public bool Update(short CPUTemp, short MemUsage, short HDUsage)
    {
        DateTime now = DateTime.Now;
        string SqlStatements = @"update alertserver
                                 set CPUTemp=@CPUTemp,MemUsage=@MemUsage,HDUsage=@HDUsage";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@CPUTemp", CPUTemp));
        DbAParameterList.Add(new DbAdapterParameter("@MemUsage", MemUsage));
        DbAParameterList.Add(new DbAdapterParameter("@HDUsage", HDUsage));

        int count = 0;
        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            count = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray());
        }
        if (count > 0)
            return true;
        return false;
    }


}