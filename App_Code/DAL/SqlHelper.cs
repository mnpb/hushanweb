﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Diagnostics;

/// <summary>
/// SqlHelper 的摘要描述
/// </summary>
public class SqlHelper
{
	public SqlHelper()
    {
	}

    public static DataSet ExecuteDataSet(SqlCommand sqlCmd, string dbConn)
    {
        DataSet ds = new DataSet();
        using (SqlConnection sqlConn = new SqlConnection())
        {
            sqlConn.ConnectionString = dbConn;
            sqlCmd.Connection = sqlConn;
            SqlDataAdapter sqlAdapter = new SqlDataAdapter();
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                sqlAdapter.SelectCommand = sqlCmd;
                sqlAdapter.Fill(ds);
            }
            catch
            {
            }
            finally
            {
                sqlAdapter.Dispose();
                if (sqlConn.State == ConnectionState.Open) sqlConn.Close();
            }

        }
        return ds;
    }

    public static DataTable ExecuteDataTable(SqlCommand sqlCmd, string dbConn)
    {
        DataTable dt = new DataTable();
        using (SqlConnection sqlConn = new SqlConnection())
        {
            sqlConn.ConnectionString = dbConn;
            sqlCmd.Connection = sqlConn;
            SqlDataAdapter sqlAdapter = new SqlDataAdapter();
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                sqlAdapter.SelectCommand = sqlCmd;
                sqlAdapter.Fill(dt);
            }
            catch
            {
            }
            finally
            {
                sqlAdapter.Dispose();
                if (sqlConn.State == ConnectionState.Open) sqlConn.Close();
            }

        }
        return dt;
    }
    public static int ExecuteNonQuery(SqlCommand sqlCmd, string dbConn)
    {
        int affectedRows = 0;
        using (SqlConnection sqlConn = new SqlConnection())
        {
            sqlConn.ConnectionString = dbConn;
            sqlCmd.Connection = sqlConn;
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                affectedRows = sqlCmd.ExecuteNonQuery();
            }
            catch
            {
            }
            finally
            {
                if (sqlConn.State == ConnectionState.Open) sqlConn.Close();
            }

        }
        return affectedRows;
    }
    public static object ExecuteScalar(SqlCommand sqlCmd, string dbConn)
    {
        object value = null;
        using (SqlConnection sqlConn = new SqlConnection())
        {
            sqlConn.ConnectionString = dbConn;
            sqlCmd.Connection = sqlConn;
            try
            {
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                value = sqlCmd.ExecuteScalar();
            }
            catch
            {
            }
            finally
            {
                if (sqlConn.State == ConnectionState.Open) sqlConn.Close();
            }
        }
        return value;
    }
    private static string GetCommandText(SqlCommand cmd)
    {
        string cmdText = cmd.CommandText;
        for (int i = 0; i < cmd.Parameters.Count; i++)
        {
            string pName = cmd.Parameters[i].ParameterName;
            string pValue = cmd.Parameters[i].Value.ToString();
            cmdText = cmdText.Replace(pName, pValue);
        }
        return cmdText;
    }
}