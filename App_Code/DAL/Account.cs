﻿using IISI.DLL.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// 帳號
/// </summary>
[Serializable]
[DataObject]
public class Account : IDisposable
{
    #region 資料庫連線字串
    private string ConnectionString = ConfigInfo.MSSqlConnectionString;
    #endregion

    #region 建構子
    public Account()
    {
        _AccountLogin = new AccountLogin();
        _AccountToGroup = new AccountToGroup();
        _AccountToDepartment = new AccountToDepartment();
    }
    #endregion

    #region 解構子
    ~Account()
    {
        Dispose(false);
    }

    /// <summary>
    /// 釋放Account資源
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
    }

    /// <summary>
    /// 釋放Account資源
    /// </summary>
    /// <param name="disposing">是否要使用 GC 釋放資源</param>
    public void Dispose(bool disposing)
    {
        // If disposing equals true, dispose all managed 
        // and unmanaged resources.
        if (disposing)
            GC.SuppressFinalize(this);

        // Release unmanaged resources. If disposing is false, 
        // only the following code is executed.
        _AccountLogin.Dispose();
        _AccountToGroup.Dispose();
        _AccountToDepartment.Dispose();
    }
    #endregion

    #region 類別成員

    private AccountLogin _AccountLogin;
    private AccountToGroup _AccountToGroup;
    private AccountToDepartment _AccountToDepartment;

    /// <summary>
    /// 帳號編號
    /// </summary>
    public int AccountID
    {
        get;
        set;
    }

    /// <summary>
    /// 姓名
    /// </summary>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// 登入帳號
    /// </summary>
    public string LoginName
    {
        get;
        set;
    }

    /// <summary>
    /// 登入密碼
    /// </summary>
    public string Password
    {
        get;
        set;
    }

    /// <summary>
    /// Email
    /// </summary>
    public string Email
    {
        get;
        set;
    }

    /// <summary>
    /// 是否啟用
    /// </summary>
    public bool IsActive
    {
        get;
        set;
    }

    /// <summary>
    /// 是否刪除
    /// </summary>
    public bool IsEnable
    {
        get;
        set;
    }

    /// <summary>
    /// 單位名稱
    /// </summary>
    public string Unit
    {
        get;
        set;
    }

    /// <summary>
    /// 聯絡電話
    /// </summary>
    public string PhoneNo
    {
        get;
        set;
    }

    /// <summary>
    /// 地址
    /// </summary>
    public string Address
    {
        get;
        set;
    }

    /// <summary>
    /// 建立時間
    /// </summary>
    public DateTime CreatedDateTime
    {
        get;
        set;
    }

    /// <summary>
    /// 修改時間
    /// </summary>
    public DateTime ModifiedDateTime
    {
        get;
        set;
    }

    /// <summary>
    /// 登錄記錄
    /// </summary>
    public List<AccountLogin> AccountLoginList
    {
        get
        {
            return _AccountLogin.GetLatest100AccountLoginList(this.AccountID);
        }
    }

    /// <summary>
    /// 帳號對應群組
    /// </summary>
    public List<AccountToGroup> AccountToGroupList
    {
        get
        {
            return _AccountToGroup.GetAccountToGroupByAccountID(this.AccountID);
        }
    }

    //<summary>
    //帳號對應子群組
    //</summary>
    public AccountToDepartment AccountToDepartment
    {
        get
        {
            return _AccountToDepartment.GetAccountToDepartmentByAccountID(this.AccountID);
        }
    }

    #endregion

    #region 類別方法

    /// <summary>
    /// 修改一筆帳號
    /// </summary>
    /// <param name="Name">姓名</param>
    /// <param name="LoginName">登入帳號</param>
    /// <param name="Password">登入密碼</param>
    /// <param name="Email">Email</param>
    /// <param name="IsActive">是否啟用</param>
    /// <param name="IsEnable">是否刪除</param>
    /// <param name="Uint">單位名稱</param>
    /// <param name="PhoneNo">聯絡電話</param>
    /// <param name="Address">地址</param>
    /// <param name="CreatedDateTime">建立時間</param>
    /// <param name="ModifiedDateTime">修改時間</param>
    /// <returns></returns>
    public int AddAccount(string Name, string LoginName, string Password, string Email, bool IsActive, bool IsEnable, string Unit, string PhoneNo, string Address, DateTime CreatedDateTime, DateTime ModifiedDateTime)
    {
        int ReturnInt = 0;

        string SqlStatements = " INSERT INTO [Account]([Name],[LoginName],[Password],[Email],[IsActive],[IsEnable],[Unit],[PhoneNo],[Address],[CreatedDateTime],[ModifiedDateTime]) " +
                               "                VALUES(@Name, @LoginName, @Password, @Email, @IsActive, @IsEnable, @Unit, @PhoneNo, @Address, @CreatedDateTime, @ModifiedDateTime); ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@Name", Name));
        DbAParameterList.Add(new DbAdapterParameter("@LoginName", LoginName));
        DbAParameterList.Add(new DbAdapterParameter("@Password", Password));
        DbAParameterList.Add(new DbAdapterParameter("@Email", string.IsNullOrEmpty(Email) ? "" : Email));
        DbAParameterList.Add(new DbAdapterParameter("@IsActive", IsActive));
        DbAParameterList.Add(new DbAdapterParameter("@IsEnable", IsEnable));
        DbAParameterList.Add(new DbAdapterParameter("@Unit", string.IsNullOrEmpty(Unit) ? "" : Unit));
        DbAParameterList.Add(new DbAdapterParameter("@PhoneNo", string.IsNullOrEmpty(PhoneNo) ? "" : PhoneNo));
        DbAParameterList.Add(new DbAdapterParameter("@Address", string.IsNullOrEmpty(Address) ? "" : Address));
        DbAParameterList.Add(new DbAdapterParameter("@CreatedDateTime", CreatedDateTime));
        DbAParameterList.Add(new DbAdapterParameter("@ModifiedDateTime", ModifiedDateTime));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try
            {
                ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray());
            }
            catch (Exception e)
            {
                throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message);
            }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 修改一筆帳號
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <param name="Name">姓名</param>
    /// <param name="LoginName">登入帳號</param>
    /// <param name="Password">登入密碼</param>
    /// <param name="Email">Email</param>
    /// <param name="IsActive">是否啟用</param>
    /// <param name="IsEnable">是否刪除</param>
    /// <param name="Uint">單位名稱</param>
    /// <param name="PhoneNo">聯絡電話</param>
    /// <param name="Address">地址</param>
    /// <param name="CreatedDateTime">建立時間</param>
    /// <param name="ModifiedDateTime">修改時間</param>
    /// <returns></returns>
    public int ModifyAccount(int AccountID, string Name, string LoginName, string Password, string Email, bool IsActive, bool IsEnable, string Unit, string PhoneNo, string Address, DateTime CreatedDateTime, DateTime ModifiedDateTime)
    {
        int ReturnInt = 0;

        string SqlStatements = " UPDATE [Account] " +
                               "    SET [Name] = @Name " +
                               "       ,[LoginName] = @LoginName " +
                               "       ,[Password] = @Password " +
                               "       ,[Email] = @Email " +
                               "       ,[IsActive] = @IsActive " +
                               "       ,[IsEnable] = @IsEnable " +
                               "       ,[Unit] = @Unit " +
                               "       ,[PhoneNo] = @PhoneNo " +
                               "       ,[Address] = @Address " +
                               "       ,[CreatedDateTime] = @CreatedDateTime " +
                               "       ,[ModifiedDateTime] = @ModifiedDateTime " +
                               "  WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@Name", Name));
        DbAParameterList.Add(new DbAdapterParameter("@LoginName", LoginName));
        DbAParameterList.Add(new DbAdapterParameter("@Password", Password));
        DbAParameterList.Add(new DbAdapterParameter("@Email", string.IsNullOrEmpty(Email) ? "" : Email));
        DbAParameterList.Add(new DbAdapterParameter("@IsActive", IsActive));
        DbAParameterList.Add(new DbAdapterParameter("@IsEnable", IsEnable));
        DbAParameterList.Add(new DbAdapterParameter("@Unit", string.IsNullOrEmpty(Unit) ? "" : Unit));
        DbAParameterList.Add(new DbAdapterParameter("@PhoneNo", string.IsNullOrEmpty(PhoneNo) ? "" : PhoneNo));
        DbAParameterList.Add(new DbAdapterParameter("@Address", string.IsNullOrEmpty(Address) ? "" : Address));
        DbAParameterList.Add(new DbAdapterParameter("@CreatedDateTime", CreatedDateTime));
        DbAParameterList.Add(new DbAdapterParameter("@ModifiedDateTime", ModifiedDateTime));
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            try
            {
                ReturnInt = DbAdapter.ExecuteNonQuery(SqlStatements, DbAParameterList.ToArray());
            }
            catch (Exception e)
            {
                throw new Exception(SqlStatements + "所產生的錯誤訊息如下：" + e.Message);
            }
        }

        return ReturnInt;
    }

    /// <summary>
    /// 刪除一筆帳號(藉著IsEnable來隱藏資料，達到畫面上刪除的目的)
    /// </summary>
    /// <param name="AccountID"></param>
    /// <returns></returns>
    public int DeleteAccount(int AccountID)
    {
        Account ThisAccount = GetAccount(AccountID);

        _AccountToGroup.DeleteAccountToGroupByAccountID(AccountID);

        return ModifyAccount(AccountID, ThisAccount.Name, ThisAccount.LoginName, ThisAccount.Password, ThisAccount.Email, ThisAccount.IsActive, false, ThisAccount.Unit, ThisAccount.PhoneNo, ThisAccount.Address, ThisAccount.CreatedDateTime, DateTime.Now);
    }

    /// <summary>
    /// 取得所有的帳號
    /// </summary>
    /// <returns></returns>
    public List<Account> GetAccountList()
    {
        List<Account> AccountList = new List<Account>();

        string SqlStatements = "SELECT * FROM [Account] WHERE [IsEnable] = 1 ORDER BY [AccountID]; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    AccountList = DataConvert.FillDataListGeneric<Account>(DataReader);
        }

        return AccountList;
    }

    /// <summary>
    /// 取得一筆帳號
    /// </summary>
    /// <param name="AccountID">帳號編號</param>
    /// <returns></returns>
    public Account GetAccount(int AccountID)
    {
        Account ReturnAccount = null;

        string SqlStatements = "SELECT * FROM [Account] WHERE [AccountID] = @AccountID; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@AccountID", AccountID));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
            {
                if (DataReader.HasRows)
                    ReturnAccount = DataConvert.FillDataListGeneric<Account>(DataReader)[0];
            }
        }

        return ReturnAccount;
    }

    /// <summary>
    /// 由登錄帳號取得一筆帳號
    /// </summary>
    /// <param name="LoginName">登錄帳號</param>
    /// <returns></returns>
    public Account GetAccountByLoginName(string LoginName)
    {
        Account ReturnAccount = null;

        string SqlStatements = "SELECT * FROM [Account] WHERE [LoginName] = @LoginName; ";

        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@LoginName", LoginName));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ReturnAccount = DataConvert.FillDataListGeneric<Account>(DataReader)[0];
        }

        return ReturnAccount;
    }

    /// <summary>
    /// 由登錄帳號及登錄密碼取得一筆帳號
    /// </summary>
    /// <param name="LoginName">登入帳號</param>
    /// <param name="Password">登入密碼</param>
    /// <returns></returns>
    public Account GetAccountByLogin(string LoginName, string Password)
    {
        Account ReturnAccount = null;

        string SqlStatements = "SELECT * FROM [Account] WHERE [LoginName] = @LoginName AND [Password] = @Password AND [IsEnable] = @IsEnable; ";


        List<DbAdapterParameter> DbAParameterList = new List<DbAdapterParameter>();
        DbAParameterList.Add(new DbAdapterParameter("@LoginName", LoginName));
        DbAParameterList.Add(new DbAdapterParameter("@Password", Password));
        DbAParameterList.Add(new DbAdapterParameter("@IsEnable", true));

        using (DbAdapter DbAdapter = new DbAdapter(this.ConnectionString))
        {
            using (DbDataReader DataReader = DbAdapter.ExcuteReader(SqlStatements, DbAParameterList.ToArray()))
                if (DataReader.HasRows)
                    ReturnAccount = DataConvert.FillDataListGeneric<Account>(DataReader)[0];
        }

        return ReturnAccount;
    }

    #endregion
}