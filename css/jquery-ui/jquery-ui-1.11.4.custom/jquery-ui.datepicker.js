﻿function initDatePicker(id) {
    $("#" + id).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy年mm月dd日',
        dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
        monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
    });
    $("#" + id).datepicker('setDate', new Date());
}