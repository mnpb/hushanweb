﻿<%@ Page Title="網路設備基本資料維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainStation.aspx.cs" Inherits="NetworkEquipment_MaintainStation" %>

<%@ Register Src="~/WebUserControlers/Meters/SearchStation.ascx" TagPrefix="uc1" TagName="SearchStation" %>
<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/NetworkEquipment/UserControls/MaintainStation.ascx" TagPrefix="uc1" TagName="MaintainStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainStation runat="server" ID="MaintainStationUserControl" OnCompleted="MaintainStationUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <uc1:SearchStation runat="server" ID="SearchStationUserControl" OnFilterStation="SearchStationUserControl_FilterStation" OnClearFilter="SearchStationUserControl_ClearFilter" />
            <br />
            <br />
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增測站" CssClass="buttonStyle" /><br />
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="IP" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing" OnRowDeleting="StationBaseGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="設備IP">
                        <ItemTemplate><%# Eval("IP") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="設備名稱">
                        <ItemTemplate><%# Eval("Name") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="設備分類">
                        <ItemTemplate><%# GetCategoryName(Eval("CategoryID").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="設備帳號">
                        <ItemTemplate><%# Eval("Account") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="設備密碼">
                        <ItemTemplate><%# Eval("Password") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="設備位置">
                        <ItemTemplate><%# Eval("Location") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="備註">
                        <ItemTemplate><%# Eval("Note") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

