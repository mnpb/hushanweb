﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainStation.ascx.cs" Inherits="NetworkEquipment_UserControls_MaintainStation" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable" style="width: 450px">
    <tr>
        <th style="text-align: right">設備IP</th>
        <td style="text-align: left">
            <asp:TextBox ID="EquipmentIPTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">設備名稱</th>
        <td style="text-align: left">
            <asp:TextBox ID="EquipmentNameTextBox" runat="server" CssClass="textBoxStyle" Width="200" /></td>
    </tr>
    <tr>
        <th style="text-align: right">設備分類</th>
        <td style="text-align: left">
            <div class="selectStyle" style=" width:250px">
                <asp:DropDownList ID="EquipmentCategoryDropDownList" runat="server" CssClass="selectStyle-select" ></asp:DropDownList>
            </div>
    </tr>
    <tr>
        <th style="text-align: right">設備帳號</th>
        <td style="text-align: left">
            <asp:TextBox ID="AccountTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">設備密碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="PasswordTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">設備位置</th>
        <td style="text-align: left">
            <asp:TextBox ID="LocationTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">備註</th>
        <td style="text-align: left">
            <asp:TextBox ID="NoteTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>