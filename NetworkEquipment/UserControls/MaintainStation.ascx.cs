﻿using Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NetworkEquipment_UserControls_MaintainStation : System.Web.UI.UserControl
{
    #region Properties

    private string equipmentIP;
    public string EquipmentIP
    {
        get
        {
            return equipmentIP;
        }
        set
        {
            equipmentIP = value;
            ShowEquipmentBaseInfo(EquipmentBaseInfo);
        }
    }

    private NetworkEquipmentBase EquipmentBaseInfo
    {
        get
        {
            return (from info in NetworkEquipmentController.Instance.GetAllNetworkEquipments()
                    where info.IP == EquipmentIP
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillAllCategories(EquipmentCategoryDropDownList);
        }
    }

    /// <summary>
    /// 填入所有分類
    /// </summary>
    /// <param name="dropDownList"></param>
    private void FillAllCategories(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();

        try
        {
            dropDownList.DataSource = NetworkEquipmentCategoryController.Instance.GetAllCategories();
            dropDownList.DataTextField = "Name";
            dropDownList.DataValueField = "ID";
            dropDownList.DataBind();
        }
        catch
        {
        }

        if (dropDownList.Items.Count > 0)
        {
            dropDownList.SelectedIndex = 0;
        }
    }

    private void EmptyAllControls()
    {
        foreach (Control c in this.Controls)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                drp.SelectedIndex = 0;
            }
        }
        EquipmentIPTextBox.Enabled = true;
    }

    /// <summary>
    /// 顯示基本資料
    /// </summary>
    /// <param name="stationBaseInfo"></param>
    private void ShowEquipmentBaseInfo(NetworkEquipmentBase equipmentBaseInfo)
    {
        string emptyString = string.Empty;
        if (equipmentBaseInfo == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            EquipmentIPTextBox.Text = equipmentBaseInfo.IP;
            EquipmentIPTextBox.Enabled = false; // 不可修改
            EquipmentNameTextBox.Text = equipmentBaseInfo.Name;
            EquipmentCategoryDropDownList.SelectedIndex =
                EquipmentCategoryDropDownList.Items.IndexOf(EquipmentCategoryDropDownList.Items.FindByValue(equipmentBaseInfo.CategoryID.ToString()));
            AccountTextBox.Text = equipmentBaseInfo.Account;
            PasswordTextBox.Text = equipmentBaseInfo.Password;
            LocationTextBox.Text = equipmentBaseInfo.Location;
            NoteTextBox.Text = equipmentBaseInfo.Note;
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        var newEquipment = new NetworkEquipmentBase
        {
            IP = EquipmentIPTextBox.Text,
            Name = EquipmentNameTextBox.Text,
            CategoryID = Convert.ToByte(EquipmentCategoryDropDownList.SelectedItem.Value),
            Account = AccountTextBox.Text,
            Password = PasswordTextBox.Text,
            Location = LocationTextBox.Text,
            Note = NoteTextBox.Text,
        };
        if (!NetworkEquipmentController.Instance.AddNetworkEquipment(newEquipment))
        {
            MessageLabel.Text = "新增/更新失敗!";
            MessageLabel.Visible = true;
        }
        else
        { // 成功
            MessageLabel.Text = string.Empty;
            MessageLabel.Visible = false;
            if (Completed != null)
            {
                Completed(this, null);
            }
        }
    }

    #endregion
}