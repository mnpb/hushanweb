﻿using Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NetworkEquipment_MaintainStation : System.Web.UI.Page
{
    #region Properties

    string FilterEquipmentName
    {
        get
        {
            return SearchStationUserControl.FilterStationName;
        }
    }

    List<NetworkEquipmentBase> EquipmentBaseInfos
    {
        get
        {
            var allInfos = NetworkEquipmentController.Instance.GetAllNetworkEquipments();
            if (string.IsNullOrEmpty(FilterEquipmentName))
            {
                return allInfos;
            }
            else
            {
                return (from info in allInfos
                        where info.Name.Contains(FilterEquipmentName)
                        select info).ToList();
            }
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowEquipmentBaseInfos();
        }
    }
    private void ShowEquipmentBaseInfos()
    {
        BindData(0);
    }
    private void BindData(int pageIndex)
    {
        StationBaseGridView.DataSource = EquipmentBaseInfos;
        StationBaseGridView.PageIndex = pageIndex;
        StationBaseGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增基本資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainStationUserControl.EquipmentIP = string.Empty;
        Popup.Title = "網路設備基本資料";
        Popup.Show();
    }

    #region GridView Events
    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindData(e.NewPageIndex);
    }

    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string equipmentIP = StationBaseGridView.DataKeys[e.NewEditIndex].Values["IP"].ToString();
        MaintainStationUserControl.EquipmentIP = equipmentIP;
        Popup.Title = "網路設備基本資料";
        Popup.Show();
    }

    protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string equipmentIP = StationBaseGridView.DataKeys[e.RowIndex].Values["IP"].ToString();
            if (!NetworkEquipmentController.Instance.DeleteNetworkEquipment(equipmentIP))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            BindData(0);
        }
    }

    #endregion

    protected void SearchStationUserControl_FilterStation(object sender, EventArgs e)
    {
        BindData(0);
    }
    protected void SearchStationUserControl_ClearFilter(object sender, EventArgs e)
    {
        BindData(0);
    }
    protected void MaintainStationUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        BindData(0);
    }

    protected string GetCategoryName(object categoryID)
    {
        return NetworkEquipmentCategoryController.Instance.GetCategoryName(Convert.ToByte(categoryID));
    }
}