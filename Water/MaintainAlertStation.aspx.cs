﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Water_MaintainAlertStation : SuperPage
{
    #region Properties

    dynamic AlertStations
    {
        get
        {
            return (from alertInfo in WaterController.Instance.GetAllAlertStations()
                    join baseInfo in WaterController.Instance.GetAllStations()
                    on alertInfo.StationID equals baseInfo.StationID
                    select new
                    {
                        StationID = alertInfo.StationID,
                        StationName = baseInfo.StationName,
                        WarningLevel1 = alertInfo.WarningLevel1,
                        WarningLevel2 = alertInfo.WarningLevel2,
                        WarningLevel3 = alertInfo.WarningLevel3,
                        Active = alertInfo.Active,
                    }).ToList();
        }
    }

    string PopupTitle = "警戒水位站資料";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowStationBaseInfos();
        }
    }
    private void ShowStationBaseInfos()
    {
        BindData(0);
    }
    private void BindData(int pageIndex)
    {
        StationBaseGridView.DataSource = AlertStations;
        StationBaseGridView.PageIndex = pageIndex;
        StationBaseGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增測站資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainAlertStationUserControl.StationID = string.Empty;
        Popup.Title = PopupTitle;
        Popup.Show();
    }

    #region GridView Events

    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindData(e.NewPageIndex);
    }
    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            string stationID = StationBaseGridView.DataKeys[e.NewEditIndex].Values["StationID"].ToString();
            MaintainAlertStationUserControl.StationID = stationID;
            Popup.Title = PopupTitle;
            Popup.Show();
        }
        catch
        {
        }
    }
    protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string stationID = StationBaseGridView.DataKeys[e.RowIndex].Values["StationID"].ToString();
            if (!WaterController.Instance.DeleteAlertStation(stationID))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            BindData(StationBaseGridView.PageIndex);
        }
    }

    #endregion

    #region UserControl Events

    protected void MaintainAlertStationUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        BindData(StationBaseGridView.PageIndex);
    }

    #endregion

    #region Utilities

    protected string GetActiveDescription(string active)
    {
        try
        {
            bool activeBoolean = Convert.ToBoolean(active);
            string image = string.Format("<img src={0} style=vertical-align:middle />", ResolveUrl(IconUtility.GetLightIcon(activeBoolean)));
            return string.Format("{0}({1})", image, IconUtility.GetActiveDescription(activeBoolean));
        }
        catch
        {
            return string.Empty;
        }
    }

    protected string ShowWarningValue(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            return string.Empty;
        }
        if (Convert.ToDouble(value).Equals(IISI.Util.NumericUtility.DefaultHydData))
        {
            return string.Empty;
        }
        return value.ToString();
    }

    #endregion
}