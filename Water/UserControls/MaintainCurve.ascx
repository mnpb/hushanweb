﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainCurve.ascx.cs" Inherits="Water_UserControls_MaintainCurve" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<script src="<%=ResolveUrl("~/ThirdPartyComponents/My97DatePicker/WdatePicker.js") %>"></script>

<%--<script type="text/javascript" src='<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js") %>'></script>--%>

<%--<script src="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js") %>"></script>
<link href="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css") %>" rel="stylesheet" />
<script src="<%=ResolveUrl("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.datepicker.js") %>"></script>--%>

<%--<script src="../css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link href="../css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css" rel="stylesheet" />
<link href="../css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css" rel="stylesheet" />
<script src="../css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.datepicker.js"></script>--%>

<%--<script type="text/javascript">
    function initDatePicker(id) {
        $("#" + id).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy年mm月dd日',
            dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
            monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
        });
        $("#" + id).datepicker('setDate', new Date());
    }
</script>--%>

<table class="fancytable" style="width: 450px">
    <tr style="display: none;">
        <th style="text-align: right">站碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="StationIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <%--  <tr>
        <th style="text-align: right">站名</th>
        <td style="text-align: left">
            <asp:TextBox ID="StationNameTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>--%>
    <tr>
        <th style="text-align: right">啟用日期</th>
        <td style="text-align: left">
            <input id="StartDateTextBox" class="Wdate"
                onfocus="WdatePicker({dateFmt:'yyyy/MM/dd'})" style="width: 150px;"
                type="text" runat="server" />
            <%--<asp:TextBox ID="StartDateTextBox" runat="server" CssClass="textBoxStyle" Width="200" />--%>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">參數1</th>
        <td style="text-align: left">
            <asp:TextBox ID="Parameter1TextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">參數2</th>
        <td style="text-align: left">
            <asp:TextBox ID="Parameter2TextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">參數3</th>
        <td style="text-align: left">
            <asp:TextBox ID="Parameter3TextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">最低水位(m)</th>
        <td style="text-align: left">
            <asp:TextBox ID="LowLevelTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">最高水位(m)</th>
        <td style="text-align: left">
            <asp:TextBox ID="HighLevelTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
     <tr>
        <th style="text-align: right">公式</th>
        <td style="text-align: left"> <label id="FormulaLabel" style="font-size: larger; color: green">Q=參數1(H-參數2)<sup>參數3</sup></label>
         </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
