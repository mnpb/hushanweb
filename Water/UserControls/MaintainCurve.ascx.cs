﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Water_UserControls_MaintainCurve : System.Web.UI.UserControl
{
    #region Properties

    private string stationID;
    public string StationID
    {
        get
        {
            return stationID;
        }
        set
        {
            stationID = value;
            StationIDTextBox.Text = stationID;
        }
    }

    private DateTime startDate;
    public DateTime StartDate
    {
        get
        {
            return startDate;
        }
        set
        {
            startDate = value;
            ShowCurveInfo(CurveInfo);
        }
    }

    private dynamic CurveInfo
    {
        get
        {
            return WaterController.Instance.GetCurve(StationID, StartDate);
        }
    }

    #endregion

    #region Methods

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    LoadCssFile("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css");
        //    LoadCssFile("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css");
        //    LoadJavaScriptFile("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.min.js");
        //    //LoadJavaScriptFile("~/css/jquery-ui/jquery-ui-1.11.4.custom/jquery-ui.datepicker.js");
        //    ScriptManager.RegisterClientScriptBlock(
        //        this,
        //        this.GetType(), 
        //        new Guid().ToString(),
        //        "$('#ContentPlaceHolder1_Popup_ctl00_MaintainCurveUserControl_StartDateTextBox').datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy年mm月dd日',dayNamesMin: ['日','一', '二', '三', '四', '五', '六'], monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月, '十二月']});" +
        //        "$('#ContentPlaceHolder1_Popup_ctl00_MaintainCurveUserControl_StartDateTextBox').datepicker('setDate', new Date());",
        //        //"initDatePicker('<%=StartDateTextBox.ClientID%>');", 
        //        true);
        //}
    }

    //private void LoadJavaScriptFile(string filePath)
    //{
    //    LiteralControl jsResource = new LiteralControl();
    //    jsResource.Text = "<script type='text/javascript' src='" + filePath + "' ></script>";
    //    Page.Header.Controls.Add(jsResource);
    //}
    //private void LoadCssFile(string href)
    //{
    //    HtmlLink stylesLink = new HtmlLink();
    //    stylesLink.Attributes["rel"] = "stylesheet";
    //    stylesLink.Attributes["type"] = "text/css";
    //    stylesLink.Href = href;
    //    Page.Header.Controls.Add(stylesLink);
    //}

    private void EmptyAllControls()
    {
        foreach (Control c in this.Controls)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                drp.SelectedIndex = 0;
            }

        }
        //StationIDTextBox.Enabled = true;
        StartDateTextBox.Attributes.Remove("disabled");
        StartDateTextBox.Value = string.Empty;
        MessageLabel.Visible = false;
        StationIDTextBox.Text = StationID;
    }

    /// <summary>
    /// 顯示基本資料
    /// </summary>
    /// <param name="curveInfo"></param>
    private void ShowCurveInfo(dynamic curveInfo)
    {
        if (curveInfo == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            StationIDTextBox.Text = StationID;
            //StationIDTextBox.Enabled = false; // 不可修改
            StartDateTextBox.Value = curveInfo.StartDate.ToString("yyyy年MM月dd日");
            StartDateTextBox.Attributes.Add("disabled", "true");
            Parameter1TextBox.Text = curveInfo.Parameter1.ToString();
            Parameter2TextBox.Text = curveInfo.Parameter2.ToString();
            Parameter3TextBox.Text = curveInfo.Parameter3.ToString();
            LowLevelTextBox.Text = curveInfo.LowLevel.ToString();
            HighLevelTextBox.Text = curveInfo.HighLevel.ToString();
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newCurve = new WaterStationCurve
            {
                StationID = StationIDTextBox.Text,
                StartDate = Convert.ToDateTime(StartDateTextBox.Value),
                Parameter1 = string.IsNullOrEmpty(Parameter1TextBox.Text) ? 0 : Convert.ToDouble(Parameter1TextBox.Text),
                Parameter2 = string.IsNullOrEmpty(Parameter2TextBox.Text) ? 0 : Convert.ToDouble(Parameter2TextBox.Text),
                Parameter3 = string.IsNullOrEmpty(Parameter3TextBox.Text) ? 0 : Convert.ToDouble(Parameter3TextBox.Text),
                LowLevel = string.IsNullOrEmpty(LowLevelTextBox.Text) ? 0 : Convert.ToDecimal(LowLevelTextBox.Text),
                HighLevel = string.IsNullOrEmpty(HighLevelTextBox.Text) ? 0 : Convert.ToDecimal(HighLevelTextBox.Text),
            };
            if (!WaterController.Instance.AddCurve(newCurve))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}