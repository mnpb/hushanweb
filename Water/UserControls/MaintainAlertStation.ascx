﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainAlertStation.ascx.cs" Inherits="Water_UserControls_MaintainAlertStation" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable">
    <tr>
        <th style="text-align: right">測站</th>
        <td style="text-align: left">
            <div class="selectStyle">
                <asp:DropDownList ID="StationDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">一級警戒水位</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="WarningLevel1TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">二級警戒水位</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="WarningLevel2TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">三級警戒水位</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="WarningLevel3TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">啟動與否?</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="ActiveCheckBox" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="text-align: center" colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
