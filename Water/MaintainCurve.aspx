﻿<%@ Page Title="水位率定曲線維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainCurve.aspx.cs" Inherits="Water_MaintainCurve" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Water/UserControls/MaintainCurve.ascx" TagPrefix="uc1" TagName="MaintainCurve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
    <link href="../css/SelectStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainCurve runat="server" ID="MaintainCurveUserControl" OnCompleted="MaintainCurveUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
             水位流量站 : <div class="selectStyle"><asp:DropDownList ID="StationDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList></div>
                  &nbsp;<asp:Button ID="QueryButton" runat="server" Text="查詢" CssClass="buttonStyle" OnClick="QueryButton_Click" />
                     <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增曲線" CssClass="buttonStyle" />
                <label id="FormulaLabel" style="font-size: larger; color: green">(Q=參數1(H-參數2)<sup>參數3</sup>)</label><br />
            <br />
            <asp:GridView ID="InfoGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="StationID,StartDate" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="InfoGridView_PageIndexChanging" OnRowEditing="InfoGridView_RowEditing" OnRowDeleting="InfoGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                  <%--  <asp:TemplateField HeaderText="站碼">
                        <ItemTemplate><%# Eval("StationID") %></ItemTemplate>
                    </asp:TemplateField>--%>
                 <%--   <asp:TemplateField HeaderText="站名">
                        <ItemTemplate><%# Eval("StationName") %></ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="啟用日期">
                        <ItemTemplate><%# Eval("StartDate", "{0:yyyy年MM月dd日}") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="參數1">
                        <ItemTemplate><%# Eval("Parameter1") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="參數2">
                        <ItemTemplate><%# Eval("Parameter2") %></ItemTemplate>
                    </asp:TemplateField> 
                      <asp:TemplateField HeaderText="參數3">
                        <ItemTemplate><%# Eval("Parameter3") %></ItemTemplate>
                    </asp:TemplateField> 
                      <asp:TemplateField HeaderText="最低水位(m)">
                        <ItemTemplate><%# Eval("LowLevel") %></ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="最高水位(m)">
                        <ItemTemplate><%# Eval("HighLevel") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>      
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

