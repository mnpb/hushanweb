﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Water_MaintainCurve : SuperPage
{
    #region Properties

    string StationID
    {
        get
        {
            try
            {
                return StationDropDownList.SelectedItem.Value;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
    string StationName
    {
        get
        {
            try
            {
                return StationDropDownList.SelectedItem.Text;
            }
            catch
            {
                return string.Empty;
            }
        }
    }

    dynamic CurveInfos
    {
        get
        {
            return WaterController.Instance.GetCurves(StationID);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillStations();
            ShowCurveInfos(0);
        }
    }
    private void FillStations()
    {
        try
        {
            StationDropDownList.DataSource = WaterController.Instance.GetAllStations();
            StationDropDownList.DataTextField = "StationName";
            StationDropDownList.DataValueField = "StationID";
            StationDropDownList.DataBind();
        }
        catch
        {
        }
    }
    private void ShowCurveInfos(int pageIndex)
    {
        InfoGridView.DataSource = CurveInfos;
        InfoGridView.PageIndex = pageIndex;
        InfoGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainCurveUserControl.StationID = StationID;
        MaintainCurveUserControl.StartDate = DateTime.MinValue;
        Popup.Title = StationName +  " 率定曲線資料";
        Popup.Show();
    }

    #region GridView Events
    protected void InfoGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowCurveInfos(e.NewPageIndex);
    }

    protected void InfoGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string stationID = InfoGridView.DataKeys[e.NewEditIndex].Values["StationID"].ToString();
        DateTime startDate = Convert.ToDateTime(InfoGridView.DataKeys[e.NewEditIndex].Values["StartDate"].ToString());
        MaintainCurveUserControl.StationID = stationID;
        MaintainCurveUserControl.StartDate = startDate;
        Popup.Title = StationName + " 率定曲線資料";
        Popup.Show();
    }

    protected void InfoGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string stationID = InfoGridView.DataKeys[e.RowIndex].Values["StationID"].ToString();
            DateTime startDate = Convert.ToDateTime(InfoGridView.DataKeys[e.RowIndex].Values["StartDate"].ToString());
            if (!WaterController.Instance.DeleteCurve(stationID, startDate))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            InfoGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowCurveInfos(InfoGridView.PageIndex);
        }
    }

    #endregion

    protected void MaintainCurveUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowCurveInfos(InfoGridView.PageIndex);
    }

    protected void QueryButton_Click(object sender, EventArgs e)
    {
        ShowCurveInfos(0);
    }
}