﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainTenMinute.ascx.cs" Inherits="Rain_UserControls_MaintainTenMinute" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable" style="width: 450px">
    <tr>
        <th style="text-align: right">站碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="StationIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">站名</th>
        <td style="text-align: left">
            <asp:TextBox ID="StationNameTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">資料時間</th>
        <td style="text-align: left">
            <asp:TextBox ID="InfoTimeTextBox" runat="server" CssClass="textBoxStyle" Width="200" /></td>
    </tr>
    <tr>
        <th style="text-align: right">雨量</th>
        <td style="text-align: left">
            <asp:TextBox ID="RainQtyTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
