﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Rain_UserControls_MaintainStation : System.Web.UI.UserControl
{
    #region Properties

    private string stationID;
    public string StationID
    {
        get
        {
            return stationID;
        }
        set
        {
            stationID = value;
            ShowStationBaseInfo(RainStationBaseInfo);
        }
    }

    private dynamic RainStationBaseInfo
    {
        get
        {
            return (from info in RainController.Instance.GetAllStations()
                    where info.StationID == StationID
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillAllOrganizations(OrganizationDropDownList);
            FillAllCounties(CountyDropDownList);
            FillTowns(TownDropDownList);
        }
    }

    /// <summary>
    /// 填入所有機關
    /// </summary>
    /// <param name="dropDownList"></param>
    private void FillAllOrganizations(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();

        try
        {
            dropDownList.DataSource = OrganizationController.Instance.GetAllOrganizations();
            dropDownList.DataTextField = "OrganizationName";
            dropDownList.DataValueField = "OrganizationID";
            dropDownList.DataBind();
        }
        catch
        {
        }

        if (dropDownList.Items.Count > 0)
        {
            dropDownList.SelectedIndex = 0;
        }
    }

    private void FillAllCounties(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();

        try
        {
            dropDownList.DataSource = CountyController.Instance.GetAllCounties();
            dropDownList.DataTextField = "CountyName";
            dropDownList.DataValueField = "CountyID";
            dropDownList.DataBind();
        }
        catch
        {
        }

        if (dropDownList.Items.Count > 0)
        {
            dropDownList.SelectedIndex = 0;
        }
    }

    private void FillTowns(DropDownList dropDownList)
    {
        dropDownList.Items.Clear();

        try
        {
            string countyID = CountyDropDownList.SelectedItem.Value;
            dropDownList.DataSource = TownController.Instance.GetTowns(countyID);
            dropDownList.DataTextField = "TownName";
            dropDownList.DataValueField = "TownID";
            dropDownList.DataBind();
        }
        catch
        {
        }

        if (dropDownList.Items.Count > 0)
        {
            dropDownList.SelectedIndex = 0;
        }
    }

    private void EmptyAllControls()
    {
        foreach (Control c in this.Controls)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                drp.SelectedIndex = 0;
            }
        }
        StationIDTextBox.Enabled = true;
        FillTowns(TownDropDownList);
    }

    /// <summary>
    /// 顯示基本資料
    /// </summary>
    /// <param name="stationBaseInfo"></param>
    private void ShowStationBaseInfo(dynamic stationBaseInfo)
    {
        if (stationBaseInfo == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            StationIDTextBox.Text = stationBaseInfo.StationID;
            StationIDTextBox.Enabled = false; // 不可修改
            StationNameTextBox.Text = stationBaseInfo.StationName;
            AddresssTextBox.Text = string.IsNullOrEmpty(stationBaseInfo.Address) ? string.Empty : stationBaseInfo.Address;
            LongitudeTextBox.Text = stationBaseInfo.Longitude == null ? string.Empty : stationBaseInfo.Longitude.ToString();
            LatitudeTextBox.Text = stationBaseInfo.Latitude == null ? string.Empty : stationBaseInfo.Latitude.ToString();
            OrganizationDropDownList.SelectedIndex =
                OrganizationDropDownList.Items.IndexOf(OrganizationDropDownList.Items.FindByValue(stationBaseInfo.OrganizationID));
            CountyDropDownList.SelectedIndex =
               CountyDropDownList.Items.IndexOf(CountyDropDownList.Items.FindByValue(stationBaseInfo.CountyID));
            FillTowns(TownDropDownList);
            TownDropDownList.SelectedIndex =
                TownDropDownList.Items.IndexOf(TownDropDownList.Items.FindByValue(stationBaseInfo.TownID));
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newStation = new RainStationBase
            {
                StationID = StationIDTextBox.Text,
                StationName = StationNameTextBox.Text,
                Address = AddresssTextBox.Text,
                OrganizationID = OrganizationDropDownList.SelectedItem.Value,
                CountyID = CountyDropDownList.SelectedItem.Value,
                TownID = TownDropDownList.SelectedItem.Value,
                Longitude = string.IsNullOrEmpty(LongitudeTextBox.Text) ? (Decimal?)null : Convert.ToDecimal(LongitudeTextBox.Text),
                Latitude = string.IsNullOrEmpty(LatitudeTextBox.Text) ? (Decimal?)null : Convert.ToDecimal(LatitudeTextBox.Text),
            };
            if (string.IsNullOrEmpty(newStation.StationName) || !RainController.Instance.AddStation(newStation))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    /// <summary>
    /// 選擇縣市
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CountyDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTowns(TownDropDownList);
    }

    #endregion
}