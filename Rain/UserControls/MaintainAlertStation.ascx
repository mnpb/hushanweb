﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainAlertStation.ascx.cs" Inherits="Rain_UserControls_MaintainAlertStation" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable">
    <tr>
        <th style="text-align: right">測站</th>
        <td style="text-align: left" colspan="5">
            <div class="selectStyle" style="width:200px">
                <asp:DropDownList ID="StationDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">10分鐘</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="M10TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">1小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H1TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">3小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H3TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">6小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H6TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">12小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H12TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">24小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H24TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">48小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H48TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">72小時</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="H72TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">1日</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="D1TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">2日</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="D2TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
        <th style="text-align: right">3日</th>
        <td style="text-align: left">
            <asp:TextBox runat="server" ID="D3TextBox" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">啟動與否?</th>
        <td style="text-align: left" colspan="5">
            <asp:CheckBox runat="server" ID="ActiveCheckBox" />
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="text-align: center" colspan="6">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
