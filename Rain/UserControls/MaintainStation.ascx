﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainStation.ascx.cs" Inherits="Rain_UserControls_MaintainStation" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable" style="width: 450px">
    <tr>
        <th style="text-align: right">站碼</th>
        <td style="text-align: left">
            <asp:TextBox ID="StationIDTextBox" runat="server" CssClass="textBoxStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">站名</th>
        <td style="text-align: left">
            <asp:TextBox ID="StationNameTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">地址</th>
        <td style="text-align: left">
            <asp:TextBox ID="AddresssTextBox" runat="server" CssClass="textBoxStyle" Width="200" /></td>
    </tr>
    <tr>
        <th style="text-align: right">所屬機關</th>
        <td style="text-align: left">
            <div class="selectStyle" style="width: 150px">
                <asp:DropDownList ID="OrganizationDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">所在縣市</th>
        <td style="text-align: left">
            <div class="selectStyle">
                <asp:DropDownList ID="CountyDropDownList" runat="server" CssClass="selectStyle-select" OnSelectedIndexChanged="CountyDropDownList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">所在鄉鎮</th>
        <td style="text-align: left">
            <div class="selectStyle">
                <asp:DropDownList ID="TownDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <th style="text-align: right">經度</th>
        <td style="text-align: left">
            <asp:TextBox ID="LongitudeTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <th style="text-align: right">緯度</th>
        <td style="text-align: left">
            <asp:TextBox ID="LatitudeTextBox" runat="server" CssClass="textBoxStyle" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>
