﻿<%@ Page Title="警戒雨量站設定" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainAlertStation.aspx.cs" Inherits="Rain_MaintainAlertStation" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Rain/UserControls/MaintainAlertStation.ascx" TagPrefix="uc1" TagName="MaintainAlertStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainAlertStation runat="server" ID="MaintainAlertStationUserControl" OnCompleted="MaintainAlertStationUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增警戒站" CssClass="buttonStyle" />
            <br />
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="StationID" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing"
                OnRowDeleting="StationBaseGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此警戒站?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站碼" Visible="false">
                        <ItemTemplate><%# Eval("StationID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站名">
                        <ItemTemplate><%# Eval("StationName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="10分鐘">
                        <ItemTemplate><%# ShowWarningValue(Eval("M10")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="1小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H1")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="3小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H3")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="6小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H6")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="12小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H12")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="24小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H24")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="48小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H48")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="72小時">
                        <ItemTemplate><%# ShowWarningValue(Eval("H72")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="1日">
                        <ItemTemplate><%# ShowWarningValue(Eval("D1")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="2日">
                        <ItemTemplate><%# ShowWarningValue(Eval("D2")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="3日">
                        <ItemTemplate><%# ShowWarningValue(Eval("D3")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="啟動與否?">
                        <ItemTemplate><%# GetActiveDescription(Eval("Active").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

