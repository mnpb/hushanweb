﻿$(document).ready(function () {
    //input加上date-default-firstdaythismonth→預設當月第一天
    if ($('input[data-month-default-thismonth]').val() == "") {
        $('input[data-month-default-thismonth]').val(getThisMonthForMonth());
    }

    //加上date-datetimepicker→變成月曆
    $('*[data-datetimepicker]').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy/mm/dd',
        dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
        monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
    });

})


function getThisMonthForMonth() {
    var myDate = new Date();
    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    if (month < 10) {
        var day = year + "-0" + month;
    }
    else
        var day = year + "-" + month;
    return day;
}
function getFirstDayThisMonth(splitString) {
    var myDate = new Date();
    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    if (month < 10) {
        var day = year + splitString + "0" + month + splitString + "01";
    }
    else
        var day = year + splitString + month + splitString + "01";
    return day;
}
function getLastDayThisMonth(splitString) {
    var myDate = new Date();
    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    myDate = new Date(year, month, 0);
    if (month < 10) {
        var lastDay = year + splitString + "0" + month + splitString + myDate.getDate();
    }
    else
        var lastDay = year + splitString + month + splitString + myDate.getDate();
    return (lastDay);
}