﻿function formatStatusIcon(cellvalue, options, rowObject) {
    var temp = "";
    if (cellvalue === true) {
        temp = "<img src='../images/Icons/circle-green.png'>";
    }
    else {
        temp = "<img src='../images/Icons/circle-red.png'>";
    }
    return temp;
}