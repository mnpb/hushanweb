﻿// http://www.chartjs.org/docs/
(function($) {
    $.fn.extend({
        MoniteredServerHistoryChart: function(labels, datasets, options) {
            return this.each(function() {
                MoniteredServerHistoryChart(this, labels, datasets, options);
            });
        }
    });

    MoniteredServerHistoryChart = function(target, labels, datasets, options) {

        labels = labels;
        //|| ["1640", "1645", "1650", "1655", "1700", "1705", "1710", "1715", "1720", "1725", "1730", "1735", "1740", "1745", "1750", "1755", "1800", "1805", "1810", "1815", "1820", "1825", "1830", "1835", "1840", "1845", "1850", "1855", "1900", "1905", "1910", "1915", "1920", "1925", "1930", "1935"] || [];
        ;
        datasets = datasets;
        // || [{ "data": [50, 45, 54, 55, 77, 89, 99, 100, 96, 96, 76, 38, 35, 36, 36, 35, 34, 36, 40, 35, 31, 39, 37, 39, 40, 36, 34, 34, 38, 38, 31, 32, 31, 33, 32, 30], "label": "水文滲漏及氣象監控伺服器" }, 
        // { "data": [32, 37, 32, 36, 35, 38, 34, 39, 33, 32, 39, 33, 34, 37, 39, 31, 32, 33, 35, 33, 40, 35, 36, 39, 31, 34, 38, 34, 39, 35, 33, 34, 38, 30, 40, 33], "label": "GIS應用伺服器" }, 
        // { "data": [39, 34, 37, 34, 31, 39, 31, 34, 32, 31, 35, 37, 40, 39, 33, 35, 37, 34, 40, 31, 33, 39, 35, 30, 34, 34, 30, 34, 36, 34, 39, 30, 32, 35, 32, 32], "label": "DC伺服器" }, 
        // { "data": [35, 39, 33, 35, 36, 32, 30, 33, 36, 37, 31, 31, 34, 39, 32, 31, 31, 34, 33, 35, 32, 40, 31, 31, 31, 36, 34, 38, 39, 40, 40, 40, 34, 30, 38, 36], "label": "地震監控應用伺服器" }, { "data": [31, 31, 39, 38, 33, 32, 38, 39, 37, 34, 30, 39, 39, 37, 33, 31, 34, 35, 38, 33, 31, 36, 37, 32, 35, 38, 33, 32, 35, 36, 36, 35, 38, 31, 31, 38], "label": "環控監控伺服器" }, { "data": [33, 31, 33, 32, 31, 32, 40, 32, 30, 39, 38, 35, 39, 38, 37, 37, 38, 36, 40, 38, 31, 39, 35, 38, 36, 40, 36, 37, 34, 30, 34, 34, 37, 36, 39, 30], "label": "CCTV中央NVR監控伺服器1" }, { "data": [34, 39, 36, 37, 31, 32, 37, 30, 32, 40, 34, 37, 32, 35, 33, 33, 32, 33, 40, 38, 37, 32, 35, 33, 39, 37, 36, 37, 36, 33, 34, 33, 31, 32, 31, 39], "label": "CCTV中央NVR監控伺服器2" }, { "data": [32, 31, 38, 33, 40, 40, 30, 30, 40, 30, 33, 35, 37, 37, 40, 34, 31, 36, 33, 32, 37, 33, 36, 33, 37, 31, 31, 35, 40, 35, 38, 35, 35, 36, 35, 36], "label": "通訊監控伺服器" }, { "data": [36, 33, 33, 30, 33, 33, 37, 33, 39, 30, 33, 30, 35, 40, 39, 35, 37, 38, 36, 32, 38, 35, 38, 32, 31, 35, 40, 34, 32, 36, 37, 33, 35, 37, 39, 38], "label": "資料庫伺服器(含HBA)" }, { "data": [34, 32, 32, 38, 36, 30, 33, 34, 36, 37, 37, 40, 33, 32, 35, 30, 33, 40, 30, 37, 36, 39, 39, 32, 40, 34, 35, 31, 37, 33, 38, 31, 35, 38, 33, 34], "label": "網頁伺服器(含HBA)" }];


        //處理參數
        var settings = {
            yLabel: "",
            xLabel: "",
            title: ""
        };
        $.extend(settings, options);


        Chart.defaults.global.animationSteps = 50;
        Chart.defaults.global.tooltipYPadding = 16;
        Chart.defaults.global.tooltipCornerRadius = 0;
        Chart.defaults.global.tooltipTitleFontStyle = "normal";
        Chart.defaults.global.tooltipFillColor = "rgba(0,160,0,0.8)";
        Chart.defaults.global.animationEasing = "easeOutBounce";
        Chart.defaults.global.responsive = true;
        Chart.defaults.global.scaleLineColor = "black";
        Chart.defaults.global.scaleFontSize = 14;
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontFamily = "微軟正黑體";

        var colors1 = ["rgba(222,0,0,1)", "rgba(222,123,0,1)", "rgba(242,213,43,1)", "rgba(0,255,0,1)", "rgba(20,20,222,1)", "rgba(247,182,194,1)", "rgba(222,214,255,1)", "rgba(1,2,0,1)", "rgba(151,187,205,1)", "rgba(51,187,205,1)"];
        var colors0 = ["rgba(222,0,0,0)", "rgba(222,123,0,0)", "rgba(242,213,43,0)", "rgba(0,255,0,0)", "rgba(20,20,222,0)", "rgba(247,182,194,0)", "rgba(222,214,255,0)", "rgba(1,2,0,0)", "rgba(151,187,205,0)", "rgba(51,187,205,0)"];

        $.each(datasets, function(i, v) {
            var index = i % colors1.length;
            this["backgroundColor"] = colors0[index];
            this["borderColor"] = colors1[index];
            this["borderJoinStyle"] = 'miter',
                this["pointHoverRadius"] = '8',
                this["pointBackgroundColor"] = colors1[index];
        })
        var lineChartData = {
            labels: labels,
            datasets: datasets
        };


        var optionsChart = {
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0, // minimum will be 0, unless there is a lower value.
                        suggestedMax: 60, // minimum will be 0, unless there is a lower value.
                        // OR //
                        beginAtZero: true // minimum value will be 0.
                    },
                    scaleLabel: {
                        display: true,
                        labelString: settings.yLabel
                    }
                }],
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: settings.xLabel
                    }
                }]
            },
            title: {
                display: true,
                text: settings.title,
                fontSize: 24
            },
            legend: {
                fontSize: 6,
                labels: {
                    boxWidth: 16,
                    padding: 28,
                    fontColor: "#000"

                }
            }
        };

        //Draw
        var canvasCPU = target.getContext("2d");
        var myLineChart = new Chart(canvasCPU, {
            type: 'line',
            data: lineChartData,
            options: optionsChart
        });

    }


})(jQuery);
