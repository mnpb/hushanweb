﻿<%@ Page Title="表面侵蝕觀測" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true"
    CodeFile="Surface.aspx.cs" Inherits="Report_DamMonitor_Surface" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderQuery" runat="Server">
    <style>
        .checkBoxList td {
            width: 50px; /* or percent value: 25% */
        }
    </style>
    <div>
        查詢欄位：<asp:CheckBoxList runat="server" CssClass="checkBoxList" Style="display: inline;text-align:left;" ID="checkBoxList" RepeatColumns="6" Width="400">
        </asp:CheckBoxList>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-main">表面侵蝕觀測點</a></li>
            <li><a href="#tabs-chart-main">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-main">
            <uc1:GridResult runat="server" ID="GridResultMain" Unit="cm" />
        </div>
        <div id="tabs-chart-main" class="center-block">
            <tchart:WebChart ID="WebChartMain" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" style="margin:auto;" />
            <br />
        </div>
    </div>

</asp:Content>
