﻿
<%@ Page Title="上傳" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UploadData.aspx.cs" Inherits="Report_DamMonitor_UploadData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 26px;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <div>
            <br />
            右方範例mdb資料庫下載後，用ACCESS軟體開啟，將資料輸入在對應的資料表後再上傳。
            <a href="db.mdb"><img alt="" class="auto-style1" src="../../images/Icons/download.png" />下載範例mdb資料庫</a>
            <br />
            <br />
            <br />
            請選擇mdb資料庫上傳：<asp:FileUpload ID="FileUpload1" runat="server"/>
            <asp:Button ID="ButtonUpload" runat="server" Text="上傳" OnClick="ButtonUpload_Click" />
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>

            <div id="import" runat="server" visible="false">
                <asp:HiddenField ID="HiddenFieldFileName" runat="server" />
                <hr />
                <h3>請勾選選要匯入的資料表（避免匯入時間太長，一次請勿選取超過十個）</h3>
                <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="4"></asp:CheckBoxList>
                <hr />
                <asp:CheckBox ID="CheckBoxIsDelete" name="CheckBoxIsDelete" runat="server" />
                <label for="ContentPlaceHolder1_CheckBoxIsDelete">是否先刪除舊資料</label>
                <br />
                <asp:Button ID="ButtonImport" runat="server" Text="匯入檔案" OnClick="ButtonImport_Click" />
                <br />
                <asp:Label ID="Label_result" runat="server" Text=""></asp:Label>
            </div>
        </div>

</asp:Content>
