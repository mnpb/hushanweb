﻿<%@ Page Title="測傾儀" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="MeasureSlope.aspx.cs" Inherits="Report_DamMonitor_MeasureSlope" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderQuery" runat="Server">
    <div style="display: inline-block; margin: 0 0 0 20px;" class="title">資料來源：</div><asp:DropDownList ID="DropDownListSource" runat="server" class="form-control" Width="77px">
        <asp:ListItem Value="I1D1">I1D1</asp:ListItem>
        <asp:ListItem Value="I1D2">I1D2</asp:ListItem>
        <asp:ListItem Value="I1D3">I1D3</asp:ListItem>
        <asp:ListItem Value="SI01">SI01</asp:ListItem>
        <asp:ListItem Value="SI02">SI02</asp:ListItem>
        <asp:ListItem Value="SI03">SI03</asp:ListItem>
        <asp:ListItem Value="SI04">SI04</asp:ListItem>
        <asp:ListItem Value="SI05">SI05</asp:ListItem>
        <asp:ListItem Value="SI06">SI06</asp:ListItem>
        <asp:ListItem Value="SI07">SI07</asp:ListItem>
        <asp:ListItem Value="SI08">SI08</asp:ListItem>
        <asp:ListItem Value="SI09">SI09</asp:ListItem>
        <asp:ListItem Value="SI10">SI10</asp:ListItem>
        <asp:ListItem Value="SI11">SI11</asp:ListItem>
        <asp:ListItem Value="SI12">SI12</asp:ListItem>
        <asp:ListItem Value="SI13">SI13</asp:ListItem>
        <asp:ListItem Value="SI14">SI14</asp:ListItem>
        <asp:ListItem Value="SI15">SI15</asp:ListItem>
        <asp:ListItem Value="SI16">SI16</asp:ListItem>
        <asp:ListItem Value="SI17">SI17</asp:ListItem>
        <asp:ListItem Value="SI18">SI18</asp:ListItem>
        <asp:ListItem Value="SI19">SI19</asp:ListItem>
        <asp:ListItem Value="SI20">SI20</asp:ListItem>
        <asp:ListItem Value="SI21">SI21</asp:ListItem>
        <asp:ListItem Value="SI22">SI22</asp:ListItem>
        <asp:ListItem Value="SI23">SI23</asp:ListItem>
        <asp:ListItem Value="SI24">SI24</asp:ListItem>
        <asp:ListItem Value="SI25">SI25</asp:ListItem>
        <asp:ListItem Value="SI26">SI26</asp:ListItem>
        <asp:ListItem Value="SI27">SI27</asp:ListItem>
        <asp:ListItem Value="SI28">SI28</asp:ListItem>
        <asp:ListItem Value="SI29">SI29</asp:ListItem>
    </asp:DropDownList>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-a">A累計位移量</a></li>
            <li><a href="#tabs-grid-b">B累計位移量</a></li>
            <li><a href="#tabs-chart">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-a">
            <uc1:GridResult runat="server" ID="GridResultA" Unit="mm" />
        </div>
        <div id="tabs-grid-b">
            <uc1:GridResult runat="server" ID="GridResultB" Unit="mm" />
        </div>
        <div id="tabs-chart">
            <tchart:WebChart ID="WebChartA" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="900px" PictureFormat="JPEG" />
            <tchart:WebChart ID="WebChartB" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="900px" PictureFormat="JPEG" />
        </div>
    </div>

</asp:Content>
