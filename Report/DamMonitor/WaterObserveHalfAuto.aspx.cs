﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_WaterObserveHalfAuto : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        string[] selectColumns = null;
        switch (DropDownListSource.SelectedValue)
        {
            case "ST1_3":
                selectColumns = DamMonitorMeasureWaterObserveHalfAuto.ST1_3;
                break;
            case "ST4_6":
                selectColumns = DamMonitorMeasureWaterObserveHalfAuto.ST4_6;
                break;
            case "ST7_9":
                selectColumns = DamMonitorMeasureWaterObserveHalfAuto.ST7_9;
                break;
            case "ST10_12":
                selectColumns = DamMonitorMeasureWaterObserveHalfAuto.ST10_12;
                break;
            case "ST13_15":
                selectColumns = DamMonitorMeasureWaterObserveHalfAuto.ST13_15;
                break;
            default:
                break;
        }
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);

        DamMonitorService<DamMonitorMeasureWaterObserveHalfAuto> service = new DamMonitorService<DamMonitorMeasureWaterObserveHalfAuto>();
        var dataMain = service.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultMain.IsShowExcelButton = true;

            GridResultMain.GridViewShowColumns = selectColumns;
            GridResultMain.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            List<DamChartData> listMain = service.GetObjectDatas(dataMain, selectColumns);
            ShowChart(WebChartMain, "", sdate, edate, listMain, rain, reservoir, "水位高程(m)");
        }
        else
        {
            GridResultMain.IsShowExcelButton = false;
        }
    }

    protected override void DrawDatas(Chart chart, List<DamChartData> datas, string axisTitle)
    {
        Axis axis = new Axis();
        axis.Title.Text = axisTitle;
        axis.Title.Angle = 90;
        axis.StartPosition = DamChartManager.UpPercent + 1;//y軸往下，51大概是中間的位置
        axis.EndPosition = 100;//最下面，圖框可視範選最大值為100
        axis.Automatic = true;
        chart.Axes.Custom.Add(axis);

        for (int i = 0; i < datas.Count(); i++)
        {
            var style = chartManager.GetLine(datas[i], datas[i].AxisTitle, colors[i % colors.Length]);
            style.CustomVertAxis = axis;
            chart.Series.Add(style);
        }
    }
}
