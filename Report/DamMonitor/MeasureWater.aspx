﻿<%@ Page Title="量水堰" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="MeasureWater.aspx.cs" Inherits="Report_DamMonitor_MeasureWater" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-main">主壩滲流量</a></li>
            <li><a href="#tabs-grid-sub">副壩滲流量</a></li>
            <li><a href="#tabs-grid-south">南壩滲流量</a></li>
            <li><a href="#tabs-grid-water">排水廊滲流量</a></li>
            <li><a href="#tabs-chart-main">主壩歷線圖</a></li>
            <li><a href="#tabs-chart-sub">副壩歷線圖</a></li>
            <li><a href="#tabs-chart-south">南壩歷線圖</a></li>
            <li><a href="#tabs-chart-water">排水廊歷線圖</a></li>
        </ul>
        <div id="tabs-grid-main">
            <uc1:GridResult runat="server" ID="GridResultMain" Unit="L/min" />
        </div>
        <div id="tabs-grid-sub">
            <uc1:GridResult runat="server" ID="GridResultSub" Unit="L/min" />
        </div>
        <div id="tabs-grid-south">
            <uc1:GridResult runat="server" ID="GridResultSouth" Unit="L/min" />
        </div>
        <div id="tabs-grid-water">
            <uc1:GridResult runat="server" ID="GridResultWater" Unit="L/min" />
        </div>
        <div id="tabs-chart-main">
            <tchart:WebChart ID="WebChartMain" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
        </div>
        <div id="tabs-chart-sub">
            <tchart:WebChart ID="WebChartSub" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
        </div>
        <div id="tabs-chart-south">
            <tchart:WebChart ID="WebChartSouth" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
        </div>
        <div id="tabs-chart-water">
            <tchart:WebChart ID="WebChartWater" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
        </div>
    </div>

</asp:Content>
