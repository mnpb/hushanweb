﻿<%@ Page Title="盲溝出口" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="BlindDrain.aspx.cs" Inherits="Report_DamMonitor_BlindDrain" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-main">壩滲流量</a></li>
            <li><a href="#tabs-chart-main">排水廊歷線圖</a></li>
        </ul>
        <div id="tabs-grid-main">
            <uc1:GridResult runat="server" ID="GridResultMain" Unit="L/min" />
        </div>
        <div id="tabs-chart-main">
            <tchart:WebChart ID="WebChartMain" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
            <br/>
            <tchart:WebChart ID="WebChartSouth" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
        </div>
    </div>

</asp:Content>
