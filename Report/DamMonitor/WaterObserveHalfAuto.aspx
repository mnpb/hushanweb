﻿<%@ Page Title="水位觀測_半自動" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="WaterObserveHalfAuto.aspx.cs" Inherits="Report_DamMonitor_WaterObserveHalfAuto" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderQuery" runat="Server">
    <style>
        .checkBoxList td {
            width: 80px;
        }
    </style>
    <div style="display: inline-block; margin: 0 0 0 20px;" class="title">查詢欄位：</div>
    <asp:DropDownList ID="DropDownListSource" runat="server" class="form-control" Width="90px">
        <asp:ListItem Value="ST1_3">ST1-3</asp:ListItem>
        <asp:ListItem Value="ST4_6">ST4-6</asp:ListItem>
        <asp:ListItem Value="ST7_9">ST7-9</asp:ListItem>
        <asp:ListItem Value="ST10_12">ST10-12</asp:ListItem>
        <asp:ListItem Value="ST13_15">ST13-15</asp:ListItem>
    </asp:DropDownList>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-main">開口式水壓計</a></li>
            <li><a href="#tabs-chart-main">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-main">
            <uc1:GridResult runat="server" ID="GridResultMain" Unit="m" />
        </div>
        <div id="tabs-chart-main" class="center-block">
            <tchart:WebChart ID="WebChartMain" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" Style="margin: auto;" />
            <br />
        </div>
    </div>

</asp:Content>
