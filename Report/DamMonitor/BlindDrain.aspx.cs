﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_BlindDrain : BaseDamChartPage, IDamMasterPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorBlindDrain> service = new DamMonitorService<DamMonitorBlindDrain>();
        var dataMain = service.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultMain.IsShowExcelButton = true;

            List<DamChartData> listMain = service.GetObjectDatas(dataMain, DamMonitorBlindDrain.SerialColumnsMain);
            List<DamChartData> listSouth = service.GetObjectDatas(dataMain, DamMonitorBlindDrain.SerialColumnsSouth);

            GridResultMain.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartMain, "湖山主壩下游雜填區濕潤處盲溝出水量變化圖", sdate, edate, listMain, rain, reservoir, "滲流量 (L/min)");
            ShowChart(WebChartSouth, "湖南壩下游雜填區濕潤處盲溝出水量變化圖", sdate, edate, listSouth, rain, reservoir, "滲流量 (L/min)");
        }
        else
        {
            GridResultMain.IsShowExcelButton = false;
        }
    }

}
