﻿<%@ Page Title="水壓計_副壩" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="WaterPressureSub.aspx.cs" Inherits="Report_DamMonitor_WaterPressureSub" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-pressure">副壩</a></li>
            <li><a href="#tabs-grid-k">副壩(壓力)</a></li>
            <li><a href="#tabs-chart-main">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-pressure">
            <uc1:GridResult runat="server" ID="GridResultPressure" Unit="m" />
        </div>
        <div id="tabs-grid-k">
            <uc1:GridResult runat="server" ID="GridResultK" Unit="kg/cm2" />
        </div>
        <div id="tabs-chart-main">
            <table>
                <tr>
                    <td>
                        <tchart:WebChart ID="WebChartTotalBasic" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                            AutoPostback="False" Width="700px" Height="500px" PictureFormat="JPEG" />
                    </td>
                    <td>
                        <tchart:WebChart ID="WebChartChangeBasicPressure" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                            AutoPostback="False" Width="700px" Height="500px" PictureFormat="JPEG" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <tchart:WebChart ID="WebChartTotalHeart" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                            AutoPostback="False" Width="700px" Height="500px" PictureFormat="JPEG" />
                    </td>
                    <td>
                        <tchart:WebChart ID="WebChartChangeHeartPressure" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                            AutoPostback="False" Width="700px" Height="500px" PictureFormat="JPEG" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <tchart:WebChart ID="WebChartTotalFilter" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                            AutoPostback="False" Width="700px" Height="500px" PictureFormat="JPEG" />
                    </td>
                    <td>
                        <tchart:WebChart ID="WebChartChangeFilterPressure" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                            AutoPostback="False" Width="700px" Height="500px" PictureFormat="JPEG" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>

