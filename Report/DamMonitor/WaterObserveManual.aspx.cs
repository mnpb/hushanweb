﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_WaterObserveManual : BaseDamChartPage, IDamMasterPage
{
    string[] selectColumns;
    protected void Page_Load(object sender, EventArgs e)
    {
        switch (DropDownListSource.SelectedValue)
        {
            case "W1_3":
                selectColumns = DamMonitorMeasureWaterObserveManual.W1_3;
                break;
            case "W4_9":
                selectColumns = DamMonitorMeasureWaterObserveManual.W4_9;
                break;
            case "W6_10":
                selectColumns = DamMonitorMeasureWaterObserveManual.W6_10;
                break;
            case "W11_14":
                selectColumns = DamMonitorMeasureWaterObserveManual.W11_14;
                break;
            case "W15_16":
                selectColumns = DamMonitorMeasureWaterObserveManual.W15_16;
                break;
            case "W17":
                selectColumns = new string[] { "W17" };
                break;
            case "W18_21":
                selectColumns = DamMonitorMeasureWaterObserveManual.W18_21;
                break;
            case "W22_24":
                selectColumns = DamMonitorMeasureWaterObserveManual.W22_24;
                break;
            case "W25":
                selectColumns = new string[] { "W25" };
                break;
            case "W26a":
                selectColumns = DamMonitorMeasureWaterObserveManual.W26a;
                break;
            case "W27_28":
                selectColumns = DamMonitorMeasureWaterObserveManual.W27_28;
                break;
            case "W29_30":
                selectColumns = DamMonitorMeasureWaterObserveManual.W29_30;
                break;
            case "W26b":
                selectColumns = DamMonitorMeasureWaterObserveManual.W26b;
                break;
            case "W31_33":
                selectColumns = DamMonitorMeasureWaterObserveManual.W31_33;
                break;
            default:
                break;
        }
        if (selectColumns != null)
        {
            checkBoxList.DataSource = DamMonitorMeasureWaterObserveManual.Half;
            checkBoxList.DataBind();
            for (int i = 0; i < checkBoxList.Items.Count; i++)
            {
                if (selectColumns.Contains(checkBoxList.Items[i].Text))
                    checkBoxList.Items[i].Selected = true;
            }
            GridResultMain.GridViewShowColumns = selectColumns;
            GridResultSub.GridViewShowColumns = selectColumns.Select(a => "Well_" + a).ToArray();
        }
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        selectColumns = checkBoxList.Items.Cast<ListItem>()
                                    .Where(li => li.Selected)
                                    .Select(li => li.Text)
                                    .ToArray();

        DamMonitorService<DamMonitorMeasureWaterObserveManual> service = new DamMonitorService<DamMonitorMeasureWaterObserveManual>();
        var dataMain = service.GetData(sdate, edate);
        if (dataMain.Count() > 0 && selectColumns != null && selectColumns.Length > 0)
        {
            GridResultMain.IsShowExcelButton = GridResultSub.IsShowExcelButton = true;

            GridResultMain.GridViewShowColumns = selectColumns;
            GridResultSub.GridViewShowColumns = selectColumns.Select(a => "Well_" + a).ToArray();

            GridResultMain.DataBind(dataMain);
            GridResultSub.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            List<DamChartData> listMain = service.GetObjectDatas(dataMain, selectColumns);
            ShowChart(WebChartMain, "水位觀測井水位深度圖", sdate, edate, listMain, rain, null,"水位深度(m)");
            List<DamChartData> listSub = service.GetObjectDatas(dataMain, selectColumns.Select(a => "Well_" + a).ToArray());
            ShowChart(WebChartSub, "水位觀測井水位高程高程圖", sdate, edate, listSub, rain, null, "水位高程(m)");
        }
        else
        {
            GridResultMain.IsShowExcelButton = GridResultSub.IsShowExcelButton = false;
            GridResultMain.DataBind();
            GridResultSub.DataBind();
        }
    }


    protected void DropDownListSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListSource.SelectedValue == "custom")
        {
            customColumns.Visible = true;
            for (int i = 0; i < checkBoxList.Items.Count; i++)
            {
                checkBoxList.Items[i].Selected = false;
            }
        }
        else
        {
            customColumns.Visible = false;
        }
        GridResultMain.DataBind();
        GridResultSub.DataBind();
    }
}
