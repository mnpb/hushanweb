﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridResultascx.ascx.cs" Inherits="Report_DamMonitor_uc_GridResultascx" %>

    <input id="ButtonExcel" class="btn btn-primary" value="下載excel" runat="server" visible="false" type="button" /><asp:Label ID="LabelUnit" class="unit" runat="server"></asp:Label>
&nbsp;<asp:GridView ID="GridViewResult" runat="server" CssClass="table table" 
        OnRowDataBound="GridView_RowDataBound"></asp:GridView>

    <script>
        $(document).ready(function () {
            var btnExcelID = '<%=ButtonExcel.ClientID%>';
            $('#' + btnExcelID).click(function () {
                export_table_to_excel('<%=GridViewResult.ClientID%>', "ExportExcel.xlsx");
            });
        });

    </script>