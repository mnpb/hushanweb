﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_uc_GridResultascx : System.Web.UI.UserControl, IName2NewName
{
    public bool IsShowExcelButton { set { ButtonExcel.Visible = value; } }
    Dictionary<string, string> name2NewName = new Dictionary<string, string>();
    public Dictionary<string, string> ChangeColumnName2NewName
    {
        get { return name2NewName; }
        set { name2NewName = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public string Unit { get; set; }
    private string[] gridViewShowColumns;
    /// <summary>
    /// 設定只顯示的欄位
    /// </summary>
    public string[] GridViewShowColumns
    {
        set
        {
            if (value != null)
            {
                List<string> al = value.ToList();
                //時間一定要顯示
                al.Add("InfoDate");
                gridViewShowColumns = al.ToArray();
                ViewState["gridViewShowColumns"] = gridViewShowColumns;
                GridViewResult.RowCreated += GridViewResult_RowCreated;
            }
        }
    }
    public void DataBind(object data)
    {
        GridViewResult.DataSource = data;
        GridViewResult.DataBind();

        if (!string.IsNullOrEmpty(Unit))
        {
            LabelUnit.Text = "單位：" + Unit;
        }
    }
    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (!ChangeColumnName2NewName.ContainsKey("InfoDate"))
            ChangeColumnName2NewName.Add("InfoDate", "日期");

        hiddenField = (List<int>)ViewState["hiddenField"];
        if (e.Row.RowType == DataControlRowType.Header)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace("Pressure_", "");//移除WaterPressure圖表的prefix
                e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace("Well_", "");//移除WaterObserveHalfAuto圖表的prefix
                //把db預設的欄位名稱改成指定的名稱
                if (ChangeColumnName2NewName.ContainsKey(e.Row.Cells[i].Text))
                    e.Row.Cells[i].Text = ChangeColumnName2NewName[e.Row.Cells[i].Text];
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = e.Row.Cells[0].Text.Replace(" 上午 12:00:00", "");
            //隱藏其他欄位CELL
            if (hiddenField != null && hiddenField.Count > 0)
            {
                for (int i = 0; i < hiddenField.Count; i++)
                {
                    e.Row.Cells[hiddenField[i]].Visible = false;
                }
            }

            //如果整列都是空值，直接隱藏
            bool hasData = false;
            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i].Visible && e.Row.Cells[i].Text != "&nbsp;")
                {
                    hasData = true;
                    break;
                }
            }
            if (!hasData)
            {
                e.Row.Visible = false;
            }
        }
    }

    List<int> hiddenField = new List<int>();
    protected void GridViewResult_RowCreated(object sender, GridViewRowEventArgs e)
    {
        gridViewShowColumns = (string[])ViewState["gridViewShowColumns"];
        //隱藏其他欄位HEADER
        if (e.Row.RowType == DataControlRowType.Header && gridViewShowColumns != null)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (!gridViewShowColumns.Contains(e.Row.Cells[i].Text))
                {
                    e.Row.Cells[i].Visible = false;
                    hiddenField.Add(i);
                }
            }
        }

        ViewState["hiddenField"] = hiddenField;

    }


}
