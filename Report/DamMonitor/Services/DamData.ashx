﻿<%@ WebHandler Language="C#" Class="DamData" %>

using System;
using System.Web;
using Models;
using Newtonsoft.Json;

public class DamData : IHttpHandler
{
    DamMonitorService<DamMonitorHorizontalChange_MainChange> dm_main_c = new DamMonitorService<DamMonitorHorizontalChange_MainChange>();
    DamMonitorService<DamMonitorHorizontalChange_MainSChange> dm_main_sc = new DamMonitorService<DamMonitorHorizontalChange_MainSChange>();
    DamMonitorService<DamMonitorHorizontalChange_SubChange> dm_sub_c = new DamMonitorService<DamMonitorHorizontalChange_SubChange>();
    DamMonitorService<DamMonitorHorizontalChange_SubSChange> dm_sub_sc = new DamMonitorService<DamMonitorHorizontalChange_SubSChange>();
    DamMonitorService<DamMonitorHorizontalChange_SouthChange> dm_south_c = new DamMonitorService<DamMonitorHorizontalChange_SouthChange>();
    DamMonitorService<DamMonitorHorizontalChange_SouthSChange> dm_south_sc = new DamMonitorService<DamMonitorHorizontalChange_SouthSChange>();

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var querystring = context.Request.QueryString[0];
            DateTime sdate = DateTime.MinValue, edate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(querystring))
            {
                dynamic para = JsonConvert.DeserializeObject(querystring);
                DateTime.TryParse(para.sDate.ToString(), out sdate);
                DateTime.TryParse(para.eDate.ToString(), out edate);
                edate.AddDays(1).AddSeconds(-1);
            }
            if (sdate > DateTime.MinValue && edate > DateTime.MinValue)
            {
                var data1 = dm_main_c.GetData(sdate, edate);
                var data2 = dm_sub_c.GetData(sdate, edate);

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(data1));
            }
            else
            {

                context.Response.ContentType = "application/json";
                context.Response.Write("日期格式不正確");
            }
        }
        catch (Exception ex)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write("日期格式不正確");
            throw;
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}