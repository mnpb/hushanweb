﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_Surface : BaseDamChartPage, IDamMasterPage
{
    string[] selectColumns;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            checkBoxList.DataSource = DamMonitorSurface.SerialColumns;
            checkBoxList.DataBind();
        }

        var selected = checkBoxList.Items.Cast<ListItem>()
                                    .Where(li => li.Selected)
                                    .Select(li => li.Text)
                                    .ToArray();
        if (selected.Length == 0)
        {
            for (int i = 0; i < checkBoxList.Items.Count; i++)
            {
                checkBoxList.Items[i].Selected = true;
            }
        }
        else
            selectColumns = selected;
    }

    public void ButtonQueryClick()
    {

        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorSurface> service = new DamMonitorService<DamMonitorSurface>();

        var dataMain = service.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultMain.IsShowExcelButton = true;

            List<DamChartData> listMain = service.GetObjectDatas(dataMain, selectColumns);
            GridResultMain.GridViewShowColumns = selectColumns;
            GridResultMain.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartMain, "表面侵蝕觀測點侵蝕或沉積量變化圖", sdate, edate, listMain, rain, reservoir, "侵蝕或沉積變化量(cm)");
        }
        else
        {
            GridResultMain.IsShowExcelButton = false;
        }
    }

}
