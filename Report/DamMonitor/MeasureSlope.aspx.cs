﻿using Steema.TeeChart;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Report_DamMonitor_MeasureSlope : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);

        IDam objModel = DamSlopeFactory.Create(DropDownListSource.SelectedValue);

        var dataMain = objModel.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultA.GridViewShowColumns = objModel.SerialColumnsA;
            GridResultB.GridViewShowColumns = objModel.SerialColumnsB;
            GridResultA.IsShowExcelButton = GridResultB.IsShowExcelButton = true;

            GridResultA.DataBind(dataMain);
            GridResultB.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            List<DamChartData> listWaterA = objModel.GetObjectDatas(dataMain, objModel.SerialColumnsA);
            List<DamChartData> listWaterB = objModel.GetObjectDatas(dataMain, objModel.SerialColumnsB);
            ShowChart(WebChartA, "A向位移量", sdate, edate, listWaterA, rain, reservoir);
            ShowChart(WebChartB, "B向位移量", sdate, edate, listWaterB, rain, reservoir);
        }
        else
        {
            GridResultA.IsShowExcelButton = GridResultB.IsShowExcelButton = false;
        }
    }

}
