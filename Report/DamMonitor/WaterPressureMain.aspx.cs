﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_WaterPressureMain : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorWaterMeasureMain> service = new DamMonitorService<DamMonitorWaterMeasureMain>();
        var dataMain = service.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultPressure.IsShowExcelButton = true;
            GridResultK.IsShowExcelButton = true;

            List<DamChartData> listBasic = service.GetObjectDatas(dataMain, DamMonitorWaterMeasureMain.SerialColumnsBasic);
            List<DamChartData> listHeart = service.GetObjectDatas(dataMain, DamMonitorWaterMeasureMain.SerialColumnsHeart);
            List<DamChartData> listFilter = service.GetObjectDatas(dataMain, DamMonitorWaterMeasureMain.SerialColumnsFilter);
            List<DamChartData> listBasicPressure = service.GetObjectDatas(dataMain, DamMonitorWaterMeasureMain.SerialColumnsBasicPressure);
            List<DamChartData> listHeartPressure = service.GetObjectDatas(dataMain, DamMonitorWaterMeasureMain.SerialColumnsHeartPressure);
            List<DamChartData> listFilterPressure = service.GetObjectDatas(dataMain, DamMonitorWaterMeasureMain.SerialColumnsFilterPressure);


            base.CustomChartColumnName(CustomColumns, listBasic, listHeart, listFilter, listBasicPressure, listHeartPressure, listFilterPressure);
            base.CustomGridColumnName(CustomColumns, new List<IName2NewName>() { (IName2NewName)GridResultPressure, GridResultK });

            GridResultPressure.GridViewShowColumns = DamMonitorWaterMeasureMain.SerialColumnsBasic.Concat(
                                                     DamMonitorWaterMeasureMain.SerialColumnsFilter).Concat(
                                                     DamMonitorWaterMeasureMain.SerialColumnsHeart).ToArray();
            GridResultK.GridViewShowColumns = DamMonitorWaterMeasureMain.SerialColumnsBasicPressure.Concat(
                                              DamMonitorWaterMeasureMain.SerialColumnsFilterPressure).Concat(
                                              DamMonitorWaterMeasureMain.SerialColumnsHeartPressure).ToArray();
            GridResultPressure.DataBind(dataMain);
            GridResultK.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartTotalBasic, "湖山主壩基礎水壓計總水頭變化曲線", sdate, edate, listBasic, rain, reservoir, "總水頭高程(m)");
            ShowChart(WebChartTotalHeart, "湖山主壩心層及殼層水壓計總水頭變化曲線", sdate, edate, listHeart, rain, reservoir, "水壓(kg/cm2)");
            ShowChart(WebChartTotalFilter, "湖山主壩濾層水壓計總水頭變化曲線", sdate, edate, listFilter, rain, reservoir, "總水頭高程(m)");
            ShowChart(WebChartChangeBasicPressure, "湖山主壩基礎水壓計壓力變化曲線", sdate, edate, listBasicPressure, rain, reservoir, "水壓(kg/cm2)");
            ShowChart(WebChartChangeHeartPressure, "湖山主壩心層及殼層水壓計壓力變化曲線", sdate, edate, listHeartPressure, rain, reservoir, "總水頭高程(m)");
            ShowChart(WebChartChangeFilterPressure, "湖山主壩濾層水壓計壓力變化曲線", sdate, edate, listFilterPressure, rain, reservoir, "水壓(kg/cm2)");
        }
        else
        {
            GridResultPressure.IsShowExcelButton = false;
            GridResultK.IsShowExcelButton = false;
        }
    }

    private Dictionary<string, string> CustomColumns
    {
        get
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Heart", "心層");
            dict.Add("Shell", "殼層");

            return dict;
        }
    }

    protected override void DrawDatas(Chart chart, List<DamChartData> datas, string axisTitle)
    {
        Axis axis = new Axis();
        axis.Title.Text = axisTitle;
        axis.Title.Angle = 90;
        axis.StartPosition = DamChartManager.UpPercent + 1;//y軸往下，51大概是中間的位置
        axis.EndPosition = 100;//最下面，圖框可視範選最大值為100
        axis.Automatic = true;
        chart.Axes.Custom.Add(axis);

        for (int i = 0; i < datas.Count(); i++)
        {
            var style = chartManager.GetLine(datas[i], datas[i].AxisTitle, colors[i % colors.Length]);
            style.CustomVertAxis = axis;
            chart.Series.Add(style);
        }
    }

}
