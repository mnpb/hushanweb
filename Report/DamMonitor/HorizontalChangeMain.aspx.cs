﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_HorizontalChangeMain : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorHorizontalChange_MainChange> serviceMainC = new DamMonitorService<DamMonitorHorizontalChange_MainChange>();
        DamMonitorService<DamMonitorHorizontalChange_MainSChange> serviceMainSC = new DamMonitorService<DamMonitorHorizontalChange_MainSChange>();

        var mainC = serviceMainC.GetData(sdate, edate);
        var mainSC = serviceMainSC.GetData(sdate, edate);
        if (mainC.Count() > 0)
        {
            GridResultChange.IsShowExcelButton = GridResultSChange.IsShowExcelButton = true;

            GridResultChange.DataBind(mainC);
            GridResultSChange.DataBind(mainSC);

            List<DamChartData> listL = serviceMainC.GetObjectDatas(mainC, DamMonitorHorizontalChange_MainChange.SerialColumnsL);
            List<DamChartData> listR = serviceMainSC.GetObjectDatas(mainSC, DamMonitorHorizontalChange_MainChange.SerialColumnsR);


            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartLChange, "湖山主壩左岸水平變位計竣工後變位量歷時曲線", sdate, edate, listL, rain, reservoir, "變化量(mm)");
            ShowChart(WebChartLSChange, "湖山主壩左岸水平變位計竣工後應變量歷時曲線", sdate, edate, listL, rain, reservoir, "應變量(&)");
            ShowChart(WebChartRChange, "湖山主壩右岸水平變位計竣工後變位量歷時曲線", sdate, edate, listR, rain, reservoir, "變化量(mm)");
            ShowChart(WebChartRSChange, "湖山主壩右岸水平變位計竣工後應變量歷時曲線", sdate, edate, listR, rain, reservoir, "應變量(&)");
        }
        else
        {
            GridResultChange.IsShowExcelButton = GridResultSChange.IsShowExcelButton = false;
        }

    }

    protected override void DrawDatas(Chart chart, List<DamChartData> datas, string axisTitle)
    {
        Axis axis = new Axis();
        axis.Title.Text = axisTitle;
        axis.Title.Angle = 90;
        axis.StartPosition = DamChartManager.UpPercent + 1;//y軸往下，51大概是中間的位置
        axis.EndPosition = 100;//最下面，圖框可視範選最大值為100
        axis.Automatic = true;
        chart.Axes.Custom.Add(axis);

        for (int i = 0; i < datas.Count(); i++)
        {
            var style = chartManager.GetLine(datas[i], datas[i].AxisTitle, colors[i % colors.Length], pointerStyles[i / colors.Length % pointerStyles.Length]);
            style.CustomVertAxis = axis;
            chart.Series.Add(style);
        }
    }
}
