﻿<%@ Page Title="水位觀測_人工" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" 
    CodeFile="WaterObserveManual.aspx.cs" Inherits="Report_DamMonitor_WaterObserveManual" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderQuery" runat="Server">
    <style>
        .checkBoxList td {
            width: 80px;
        }
    </style>
    <div>
        <div style="display: inline-block; margin: 0 0 0 20px;" class="title">查詢欄位：</div>
        <asp:DropDownList ID="DropDownListSource" runat="server" class="form-control" Width="240px"  AutoPostBack="True" OnSelectedIndexChanged="DropDownListSource_SelectedIndexChanged">
            <asp:ListItem Value="W1_3">副壩雜填W1-3</asp:ListItem>
            <asp:ListItem Value="W4_9">主副壩培厚區W4-9</asp:ListItem>
            <asp:ListItem Value="W6_10">湖山湖南連絡道邊坡W6-10</asp:ListItem>
            <asp:ListItem Value="W11_14">右山脊庫區外順向邊坡W11-14</asp:ListItem>
            <asp:ListItem Value="W15_16">湖南壩下游雜填W15-16</asp:ListItem>
            <asp:ListItem Value="W17">取出水工W17</asp:ListItem>
            <asp:ListItem Value="W18_21">雜填(二)區W18-21</asp:ListItem>
            <asp:ListItem Value="W22_24">溢洪道W22-24</asp:ListItem>
            <asp:ListItem Value="W25">主壩下游雜填W25無水</asp:ListItem>
            <asp:ListItem Value="W26a">庫區遷建道路W26</asp:ListItem>
            <asp:ListItem Value="W27_28">桶頭堰W27-28</asp:ListItem>
            <asp:ListItem Value="W29_30">湖南壩左山脊W29-30</asp:ListItem>
            <asp:ListItem Value="W26b">湖南壩左山脊W26.29.30</asp:ListItem>
            <asp:ListItem Value="W31_33">湖山主壩右山脊W31-33</asp:ListItem>
            <asp:ListItem Value="custom">自訂</asp:ListItem>
        </asp:DropDownList>
        <span id="customColumns" runat="server" visible="false">
        自訂欄位：<asp:CheckBoxList runat="server" CssClass="checkBoxList" Style="display: inline;text-align:left;" ID="checkBoxList" RepeatColumns="10">
        </asp:CheckBoxList></span>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-main">水位觀測井量測記錄表</a></li>
            <li><a href="#tabs-grid-sub">水位高程表</a></li>
            <li><a href="#tabs-chart-main">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-main">
            <uc1:GridResult runat="server" ID="GridResultMain" Unit="m" />
        </div>
        <div id="tabs-grid-sub">
            <uc1:GridResult runat="server" ID="GridResultSub" Unit="m" />
        </div>
        <div id="tabs-chart-main" class="center-block">
            <tchart:WebChart ID="WebChartMain" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" Style="margin: auto;" />
            <br />
            <tchart:WebChart ID="WebChartSub" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" Style="margin: auto;" />
            <br />
        </div>
    </div>

</asp:Content>
