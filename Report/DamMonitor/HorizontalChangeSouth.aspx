﻿<%@ Page Title="水平變位計_南" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="HorizontalChangeSouth.aspx.cs" Inherits="Report_DamMonitor_HorizontalChangeHorizontalChangeSouth" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-change">變化量</a></li>
            <li><a href="#tabs-grid-schange">應變量</a></li>
            <li><a href="#tabs-chart">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-change">
            <uc1:GridResult runat="server" id="GridResultChange" Unit="mm" />
        </div>
        <div id="tabs-grid-schange">
            <uc1:GridResult runat="server" id="GridResultSChange" Unit="%" />
        </div>
        <div id="tabs-chart">
            <tchart:WebChart ID="WebChartLChange" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" />
            <br />
            <tchart:WebChart ID="WebChartRChange" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" />
            <br />
            <tchart:WebChart ID="WebChartLSChange" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" />
            <br />
            <tchart:WebChart ID="WebChartRSChange" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG" />
        </div>
    </div>
</asp:Content>


