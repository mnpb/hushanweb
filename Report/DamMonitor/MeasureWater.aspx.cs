﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_MeasureWater : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        GridResultWater.ChangeColumnName2NewName.Add("water", "排水廊道");
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorMeasureWater> serviceMain = new DamMonitorService<DamMonitorMeasureWater>();

        var dataMain = serviceMain.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultMain.IsShowExcelButton = GridResultSub.IsShowExcelButton = GridResultSouth.IsShowExcelButton = GridResultWater.IsShowExcelButton
                = true;
            GridResultMain.GridViewShowColumns = DamMonitorMeasureWater.SerialColumnsMain;
            GridResultSub.GridViewShowColumns = DamMonitorMeasureWater.SerialColumnsSub;
            GridResultSouth.GridViewShowColumns = DamMonitorMeasureWater.SerialColumnsSouth;
            GridResultWater.GridViewShowColumns = DamMonitorMeasureWater.SerialColumnsWater;

            List<DamChartData> listMain = serviceMain.GetObjectDatas(dataMain, DamMonitorMeasureWater.SerialColumnsMain);
            List<DamChartData> listSub = serviceMain.GetObjectDatas(dataMain, DamMonitorMeasureWater.SerialColumnsSub);
            List<DamChartData> listSouth = serviceMain.GetObjectDatas(dataMain, DamMonitorMeasureWater.SerialColumnsSouth);
            List<DamChartData> listWater = serviceMain.GetObjectDatas(dataMain, DamMonitorMeasureWater.SerialColumnsWater);
            listWater[listWater.Count - 1].AxisTitle = "排水廊道";
            GridResultMain.DataBind(dataMain);
            GridResultSub.DataBind(dataMain);
            GridResultSouth.DataBind(dataMain);
            GridResultWater.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartMain, "湖山主壩量水堰滲流量變化圖", sdate, edate, listMain, rain, reservoir, "滲流量 (L/min)");
            ShowChart(WebChartSub, "湖山副壩量水堰滲流量變化圖", sdate, edate, listSub, rain, reservoir, "滲流量 (L/min)");
            ShowChart(WebChartSouth, "湖南壩量水堰滲流量變化圖", sdate, edate, listSouth, rain, reservoir, "滲流量 (L/min)");
            ShowChart(WebChartWater, "排水廊道量水堰滲流量變化圖", sdate, edate, listWater, rain, reservoir, "滲流量 (L/min)");
        }
        else
        {
            GridResultMain.IsShowExcelButton = GridResultSub.IsShowExcelButton = GridResultSouth.IsShowExcelButton = GridResultWater.IsShowExcelButton
                = false;
        }
    }

}
