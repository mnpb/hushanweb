﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_HorizontalChangeSub : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorHorizontalChange_SubChange> serviceSubC = new DamMonitorService<DamMonitorHorizontalChange_SubChange>();
        DamMonitorService<DamMonitorHorizontalChange_SubSChange> serviceSubSC = new DamMonitorService<DamMonitorHorizontalChange_SubSChange>();

        var mainC = serviceSubC.GetData(sdate, edate);
        var mainSC = serviceSubSC.GetData(sdate, edate);
        if (mainC.Count() > 0)
        {
            GridResultChange.IsShowExcelButton = GridResultSChange.IsShowExcelButton = true;

            GridResultChange.DataBind(mainC);
            GridResultSChange.DataBind(mainSC);

            List<DamChartData> listL = serviceSubC.GetObjectDatas(mainC, DamMonitorHorizontalChange_SubChange.SerialColumnsL);
            List<DamChartData> listR = serviceSubSC.GetObjectDatas(mainSC, DamMonitorHorizontalChange_SubSChange.SerialColumnsR);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartLChange, "湖山副壩左岸水平變位計竣工後變位量歷時曲線", sdate, edate, listL, rain, reservoir, "變化量(mm)");
            ShowChart(WebChartLSChange, "湖山副壩左岸水平變位計竣工後應變量歷時曲線", sdate, edate, listL, rain, reservoir, "應變量(&)");
            ShowChart(WebChartRChange, "湖山副壩右岸水平變位計竣工後變位量歷時曲線", sdate, edate, listR, rain, reservoir, "變化量(mm)");
            ShowChart(WebChartRSChange, "湖山副壩右岸水平變位計竣工後應變量歷時曲線", sdate, edate, listR, rain, reservoir, "應變量(&)");
        }
        else
        {
            GridResultChange.IsShowExcelButton = GridResultSChange.IsShowExcelButton = false;
        }
    }
}
