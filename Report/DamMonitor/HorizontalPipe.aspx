﻿<%@ Page Title="水平集水管" Language="C#" MasterPageFile="~/Report/DamMonitor/MasterPageDam.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="HorizontalPipe.aspx.cs" Inherits="Report_DamMonitor_HorizontalPipe" %>

<%@ Register Src="~/Report/DamMonitor/uc/GridResultascx.ascx" TagPrefix="uc1" TagName="GridResult" %>
<%@ Register Assembly="TeeChart" Namespace="Steema.TeeChart.Web" TagPrefix="tchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-grid-main">表面侵蝕觀測點</a></li>
            <li><a href="#tabs-chart-main">歷線圖</a></li>
        </ul>
        <div id="tabs-grid-main">
            <uc1:GridResult runat="server" ID="GridResultMain" Unit="L/min" />
        </div>
        <div id="tabs-chart-main">
            <tchart:WebChart ID="WebChartMain" runat="server" GetChartFile="GetChart.aspx" TempChart="Session"
                AutoPostback="False" Width="900px" Height="500px" PictureFormat="JPEG"  style="margin:auto;" />
            <br/>
        </div>
    </div>

</asp:Content>
