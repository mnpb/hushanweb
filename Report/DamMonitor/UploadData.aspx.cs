﻿using System.Reflection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_UploadData : System.Web.UI.Page
{
    DamUploadData data = new DamUploadData();
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            // FU1.FileName 只有 "檔案名稱.附檔名"，並沒有 Client 端的完整理路徑
            string filename = FileUpload1.FileName;

            string extension = Path.GetExtension(filename).ToLowerInvariant();
            // 判斷是否為允許上傳的檔案附檔名
            List<string> allowedExtextsion = new List<string> { ".mdb" };
            if (allowedExtextsion.IndexOf(extension) == -1)
            {
                lblMessage.Text = "不允許該檔案上傳";
                return;
            }

            // 限制檔案大小，限制為 2MB
            int filesize = FileUpload1.PostedFile.ContentLength;
            if (filesize > 21000000)
            {
                lblMessage.Text = "檔案大小上限為 20MB，該檔案無法上傳";
                return;
            }

            // 檢查 Server 上該資料夾是否存在，不存在就自動建立
            string serverDir = Server.MapPath(@"~\FileUploadDamMonitor");

            string serverFilePath = Path.Combine(serverDir, filename);
            string fileNameOnly = Path.GetFileNameWithoutExtension(filename);
            // 重覆檔案的命名規則為 檔名_1、檔名_2 以此類推
            filename = string.Concat(fileNameOnly, "_", DateTime.Now.ToString("yyyyMMdd_HHmmss"), extension);
            serverFilePath = Path.Combine(serverDir, filename);

            // 把檔案傳入指定的 Server 內路徑
            try
            {
                FileUpload1.SaveAs(serverFilePath);
                lblMessage.Text = "檔案上傳成功";
                HiddenFieldFileName.Value = filename;

                //只顯示白名單的table name
                List<string> ans = data.GetTableList(serverFilePath);
                CheckBoxList1.DataSource = ans;
                CheckBoxList1.DataBind();

                import.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
    }

    protected void ButtonImport_Click(object sender, EventArgs e)
    {
        List<string> importTables = new List<string>();
        foreach (ListItem item in CheckBoxList1.Items)
        {
            if (item.Selected)
            {
                importTables.Add(item.Text);
            }
        }

        string serverDir = Server.MapPath(@"~\FileUploadDamMonitor");

        if (importTables.Count > 0 && !string.IsNullOrEmpty(HiddenFieldFileName.Value))
        {
            List<DamUploadData.ImportResult> ans = data.ImportTables(serverDir + "/" + HiddenFieldFileName.Value, importTables, CheckBoxIsDelete.Checked);

            string message = "<h4>匯入結果(" + DateTime.Now.ToString("HH:mm") + ")：</h4>";
            foreach (var item in ans)
            {
                message += item.ToString() + "<br/>";
            }
            Label_result.Text = message;
            CheckBoxList1.DataSource = null;
            CheckBoxList1.DataBind();
        }

    }

}