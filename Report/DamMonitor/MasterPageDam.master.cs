﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_MasterPageDam : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //不能在頁面上用html5的type=number，server會掛掉，可能是.net framework版本的關係，改用cs碼去指定
        month.Attributes["type"] = "number";
        sDate.Attributes["type"] = "date";
        eDate.Attributes["type"] = "date";
        
        //if (!IsPostBack)
        //{
        //    if (string.IsNullOrEmpty(sDate.Value))
        //        sDate.Value = DateTime.Now.ToString("yyyy/MM/") + "01";

        //    if (string.IsNullOrEmpty(eDate.Value))
        //        eDate.Value = DateTime.Now.AddMonths(1).AddDays(-DateTime.Now.AddMonths(1).Day).ToString("yyyy/MM/dd");
        //}
    }

    protected void ButtonQuery_Click(object sender, EventArgs e)
    {
        IDamMasterPage page = Page as IDamMasterPage;
        if (page != null)
        {
            page.ButtonQueryClick();
        }
    }
}
