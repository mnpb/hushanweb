﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Styles;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Report_DamMonitor_HorizontalPipe : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorHorizontalPipe> service = new DamMonitorService<DamMonitorHorizontalPipe>();

        var dataMain = service.GetData(sdate, edate);
        if (dataMain.Count() > 0)
        {
            GridResultMain.IsShowExcelButton = true;

            List<DamChartData> listMain = service.GetObjectDatas(dataMain, DamMonitorHorizontalPipe.SerialColumns);

            GridResultMain.DataBind(dataMain);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);

            ShowChart(WebChartMain, "主壩右山脊水平排水管滲水量變化圖", sdate, edate, listMain, rain, reservoir, "滲流量 (L/min)");
        }
        else
        {
            GridResultMain.IsShowExcelButton = false;
        }
    }

}
