﻿using Models;
using Steema.TeeChart;
using Steema.TeeChart.Web;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Report_DamMonitor_HorizontalChangeHorizontalChangeSouth : BaseDamChartPage, IDamMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ButtonQueryClick()
    {
        DateTime sdate, edate;
        GetDateTime(out sdate, out edate);
        DamMonitorService<DamMonitorHorizontalChange_SouthChange> serviceC = new DamMonitorService<DamMonitorHorizontalChange_SouthChange>();
        DamMonitorService<DamMonitorHorizontalChange_SouthSChange> serviceSC = new DamMonitorService<DamMonitorHorizontalChange_SouthSChange>();

        var dataC = serviceC.GetData(sdate, edate);
        var dataSC = serviceSC.GetData(sdate, edate);

        if (dataC.Count() > 0)
        {
            GridResultChange.IsShowExcelButton = GridResultSChange.IsShowExcelButton = true;

            GridResultChange.DataBind(dataC);
            GridResultSChange.DataBind(dataSC);

            List<DamChartData> listL = serviceC.GetObjectDatas(dataC, DamMonitorHorizontalChange_SouthChange.SerialColumnsL);
            List<DamChartData> listR = serviceSC.GetObjectDatas(dataSC, DamMonitorHorizontalChange_SouthChange.SerialColumnsR);

            var rain = SerialRainDayData(sdate, edate);
            var reservoir = SerialReservoirDayData(sdate, edate);
            ShowChart(WebChartLChange, "湖山南壩左岸水平變位計竣工後變位量歷時曲線", sdate, edate, listL, rain, reservoir, "變化量(mm)");
            ShowChart(WebChartLSChange, "湖山南壩左岸水平變位計竣工後應變量歷時曲線", sdate, edate, listL, rain, reservoir, "應變量(&)");
            ShowChart(WebChartRChange, "湖山南壩右岸水平變位計竣工後變位量歷時曲線", sdate, edate, listR, rain, reservoir, "變化量(mm)");
            ShowChart(WebChartRSChange, "湖山南壩右岸水平變位計竣工後應變量歷時曲線", sdate, edate, listR, rain, reservoir, "應變量(&)");
        }
        else
        {
            GridResultChange.IsShowExcelButton = GridResultSChange.IsShowExcelButton = false;
        }
    }
}
