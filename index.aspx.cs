﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    private Account LoginAccount = null;


    protected void Page_PreInit(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //若處於登錄狀態直接導到首頁
        if (Session["AccountID"] != null)
        {
            using (Account Account = new Account())
                LoginAccount = Account.GetAccount((int)Session["AccountID"]);

            RedirectEditAccountPage();
        }
    }

    private void RedirectEditAccountPage()
    {
        string RedirectUrl = string.Empty;

        List<Menu> MenuList = new List<Menu>();

        using (Menu Menu = new Menu())
            MenuList = Menu.GetMenuList();

        RedirectUrl = MenuList.Find(delegate(Menu Mu)
        {
            return Mu.Name == "I1.個人資料編輯";
        }).NavigateUrl;

        Response.Redirect(RedirectUrl);
    }

    //protected void Page_PreRender(object sender, EventArgs e)
    //{
    //    //若處於登錄狀態直接導到首頁
    //    if (Session["AccountID"] != null && !IsChangePassword)
    //    {
    //        using (Account Account = new Account())
    //            LoginAccount = Account.GetAccount((int)Session["AccountID"]);

    //        //RedirectEditAccountPage();
    //    }
    //}

    //protected void LoginImageButton_Click(object sender, ImageClickEventArgs e)
    //{
    //    string ErrorMessage = string.Empty;

    //    LoginCheck(LoginNameTextBox.Value, PasswordTextBox.Value, out ErrorMessage);
    //    IsChangePassword = false;

    //    if (!string.IsNullOrEmpty(ErrorMessage))
    //        ScriptManager.RegisterClientScriptBlock(LoginImageButton, LoginImageButton.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
    //    else
    //        RedirectEditAccountPage();  //RedriectPage();
    //}

    //protected void EditAccountLinkButton_Click(object sender, EventArgs e)
    //{
    //    string ErrorMessage = string.Empty;

    //    LoginCheck(LoginNameTextBox.Value, PasswordTextBox.Value, out ErrorMessage);
    //    IsChangePassword = true;

    //    if (!string.IsNullOrEmpty(ErrorMessage))
    //        ScriptManager.RegisterClientScriptBlock(LoginImageButton, LoginImageButton.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
    //    else
    //    {           
    //        ChangePassword1.AccountID = (int)Session["AccountID"];
    //        Popup1.Title = "修改密碼";
    //        Popup1.Show();
    //    }
    //}

    protected void Button_ok_Click(object sender, EventArgs e)
    {
        string OutputErrorMessage = string.Empty;

        if (string.IsNullOrEmpty(OutputErrorMessage))
        {
            if (string.IsNullOrEmpty(LoginName.Text))
                OutputErrorMessage += "帳號不可空白<br/>";

            if (string.IsNullOrEmpty(Password.Text))
                OutputErrorMessage += "密碼不可空白<br/>";
        }

        try
        {
            if (string.IsNullOrEmpty(OutputErrorMessage))
            {
                using (Account Account = new Account())
                    LoginAccount = Account.GetAccountByLogin(LoginName.Text, Password.Text);

                if (LoginAccount == null)
                    OutputErrorMessage = "帳號或密碼輸入錯誤，請確認後重新輸入<br/>";
                else if (!LoginAccount.IsEnable)
                    OutputErrorMessage = "您的帳號已無法使用，請洽詢管理人員<br/>";
                else if (!LoginAccount.IsActive)
                    OutputErrorMessage = "您的帳號尚未啟用，請洽詢管理人員<br/>";
            }
        }
        catch (Exception ex)
        {
            OutputErrorMessage = "系統發生錯誤<br/>" + ex.Message;
        }

        if (string.IsNullOrEmpty(OutputErrorMessage))
        {
            string RedirectUrl = string.Empty;
            Session["AccountID"] = LoginAccount.AccountID;
            Session["AccountName"] = LoginAccount.Name;

            //寫入一筆登錄記錄
            using (AccountLogin AccountLogin = new AccountLogin())
                AccountLogin.AddAccountLogin(LoginAccount.AccountID
                                             , 1
                                             , string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? Request.ServerVariables["REMOTE_ADDR"] : Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
                                             , DateTime.Now);
            //RedriectPage();
        }

        ErrorMessage.Text = OutputErrorMessage;
    }

    private void RedriectPage()
    {
        string RedirectUrl = string.Empty;
        List<GroupToMenu> LoginGroupToMemuList = new List<GroupToMenu>();

        using (Group Group = new Group())
            LoginGroupToMemuList = Group.GetGroup(LoginAccount.AccountToGroupList[0].GroupID).GroupToMenuList;

        using (Menu Menu = new Menu())
        {
            foreach (GroupToMenu GroupToMemu in LoginGroupToMemuList)
            {
                Menu RedirectMenu = Menu.GetMenu(GroupToMemu.MenuID);
                if (RedirectMenu.NavigateUrl != "javascript:void(0);")
                {
                    RedirectUrl = RedirectMenu.NavigateUrl;
                    break;
                }
            }
        }
        RedirectUrl = "~/AccountManagement/AccountGroupManagement.aspx";
        Response.Redirect(RedirectUrl);
    }

    //private void RedirectEditAccountPage()
    //{
    //    string RedirectUrl = string.Empty;

    //    List<Menu> MenuList = new List<Menu>();

    //    using (Menu Menu = new Menu())
    //        MenuList = Menu.GetMenuList();

    //    RedirectUrl = MenuList.Find(delegate(Menu Mu) { return Mu.Name == "1.個人資料編輯"; }).NavigateUrl;

    //    Response.Redirect(RedirectUrl);
    //}
    //protected void LoginImageButton_ServerClick(object sender, EventArgs e)
    //{
    //    string ErrorMessage = string.Empty;

    //    //LoginCheck(LoginNameTextBox.Value, PasswordTextBox.Value, out ErrorMessage);
    //    //IsChangePassword = false;

    //    if (!string.IsNullOrEmpty(ErrorMessage))
    //    {//ScriptManager.RegisterClientScriptBlock(LoginImageButton, LoginImageButton.GetType(), "", "window.setTimeout(\"alert('" + ErrorMessage + "')\", 10);", true);
    //    }
    //    //else
    //    //    RedirectEditAccountPage();  //RedriectPage();
    //}
}