﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    private List<Menu> MenuList
    {
        get
        {
            if (ViewState["MenuList"] == null)
                using (Menu Menu = new Menu())
                    ViewState["MenuList"] = Menu.GetMenuList();

            return (List<Menu>)ViewState["MenuList"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Menu ThisMenu = MenuList.Find(delegate(Menu Mu) { return ResolveUrl(Mu.NavigateUrl) == Request.RawUrl; });

        Menu1.AccountID = (int)Session["AccountID"];

        if (!IsPostBack)
            GenerateTitle(ThisMenu);

        //if (!IsPostBack)
        //{
        //    Menu ParentMenu = MenuList.Find(delegate(Menu Mu) { return Mu.MenuID == ThisMenu.ParentID; });
        //    string ParentTitle = string.Empty;


        //    if (ParentMenu != null)
        //        GetParentTitieWithLink(ParentMenu, out ParentTitle);


        //    TitleLiteral.Text = ThisMenu.Name;
        //    ParentTitleLiteral.Text = ParentTitle;
        //}

	if (ThisMenu != null)
	{
        		Page.Title = ThisMenu.Name;
	}
    }

    private void GenerateTitle(Menu Menu)   
    {   
        string TitleString = string.Empty;
        GetParentTitie(Menu, out TitleString);
        TitleLiteral.Text = TitleString;        
    }

    private void GetParentTitie(Menu Menu, out string TitleString)
    {
        string ParentTitleString = string.Empty;
        string ReturnTitleString = Menu.Name;

        if (Menu.ParentID > 0)
        {
            GetParentTitie(MenuList.Find(delegate(Menu Mu) { return Mu.MenuID == Menu.ParentID; }), out ParentTitleString);

            ReturnTitleString = string.Format("{0} >> {1}", ParentTitleString, ReturnTitleString);
        }

        TitleString = ReturnTitleString;
    }

    private void GetParentTitieWithLink(Menu Menu, out string TitleString)
    {
        string ParentTitleString = string.Empty;
        string ReturnTitleString = string.Format("<a href=\"{0}\" title=\"{1}\" target=\"{2}\">{3}</a>", Menu.NavigateUrl, Menu.Name, Menu.NavigateUrl, Menu.Name); ;

        if (Menu.ParentID > 0)
        {
            GetParentTitie(MenuList.Find(delegate(Menu Mu) { return Mu.MenuID == Menu.ParentID; }), out ParentTitleString);

            ReturnTitleString = string.Format("{0} >> {1}", ParentTitleString, ReturnTitleString);
        }

        TitleString = ReturnTitleString;
    }
}
