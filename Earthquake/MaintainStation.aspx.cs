﻿using Models.Earthquake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Earthquake_MaintainStation : SuperPage
{
    #region Properties

    List<POINT> StationBaseInfos
    {
        get
        {
            return EarthquakeController.Instance.GetAllStations();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowStationBaseInfos(0);
        }
    }
    private void ShowStationBaseInfos(int pageIndex)
    {
        StationBaseGridView.DataSource = StationBaseInfos;
        StationBaseGridView.PageIndex = pageIndex;
        StationBaseGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增基本資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainStationUserControl.StationID = 0;
        Popup.Title = "地震儀測站基本資料";
        Popup.Show();
    }

    #region GridView Events

    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowStationBaseInfos(e.NewPageIndex);
    }

    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int stationID = Convert.ToInt32(StationBaseGridView.DataKeys[e.NewEditIndex].Values["POINT_ID"].ToString());
        MaintainStationUserControl.StationID = stationID;
        Popup.Title = "地震儀測站基本資料";
        Popup.Show();
    }

    protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int stationID = Convert.ToInt32(StationBaseGridView.DataKeys[e.RowIndex].Values["POINT_ID"].ToString());
            if (!EarthquakeController.Instance.DeleteStation(stationID))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowStationBaseInfos(StationBaseGridView.PageIndex);
        }
    }

    #endregion

    protected void MaintainStationUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowStationBaseInfos(StationBaseGridView.PageIndex);
    }
}