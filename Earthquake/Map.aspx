﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Map.aspx.cs" Inherits="Earthquake_Map" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        body {
            background-color: #D9ECF8;
        }

        .eqpanel {
            padding: 3px;
            position: absolute;
            height: 170px;
            width: 230px;
            font-size: medium;
            background-color: yellow;
            border-color: red;
            border-style: solid;
            line-height: 100%;
        }

        .auto-style1 {
            text-align: center;
            font-size: medium;
            color: #FF0000;
        }

        .auto-style2 {
            padding: 10px;
            font-size: medium;
            text-align: left;
            color: #3333CC;
        }

        .auto-style3 {
            color: #3333CC;
        }

        .auto-style4 {
            padding: 10px;
            text-align: left;
            font-size: medium;
            color: #FF0000;
        }
    </style>

    <script language="javascript">
    <!--
    //マーカーのロールオーバー時のイベントドライバ
    //飛んできたインデックスのマーカーオブジェクトの説明を表示・非表示切り替えする
    function mOver(index) {
        //cssrover.style.display = "block";
        eval("panel" + index + ".style.display = \"block\"");
    }

    function mOut(index) {
        //cssrover.style.display = "none";
        eval("panel" + index + ".style.display = \"none\"");
    }

    //-->
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <%--
    <center>
    <BR>
    <asp:Label ID="lblDesc" runat="server" Text="地震測站分佈圖" Font-Bold="true" Font-Size="18px" ForeColor="DarkBlue"></asp:Label>
    <BR>
    </center>
        --%>
        <%--        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>--%>
        <asp:Timer ID="Timer1" runat="server" Interval="10000" OnTick="Timer1_Tick">
        </asp:Timer>
        <center>
        <div align="center">
        <div style="position: relative; width:1000px; height:528px" align="center">

            <asp:Image ID="imgMap" runat="server" Width="1000px" ImageUrl="~/images/eqmap/mapLYT.jpg" />
<%--            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
            </asp:SqlDataSource>--%>

            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave1" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave2" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave3" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave4" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave5" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave6" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
            <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave7" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>
              <span style="position:absolute; left:526px; top:168px; height: 347px; width: 437px;">
                <asp:Image ID="imgWave8" runat="server" Height="347px" Width="437px" Visible="False" />
            </span>

            <%-- 地点1 --%>
             <span style="position:absolute; left:765px; top:160px;">
                <asp:ImageButton ID="imgMark1" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark1_Click" BorderStyle="None" 
                    onMouseover="mOver(1)" onMouseout="mOut(1)" />
            </span>
            <span ID="panel1" class="eqpanel" style="left:0px; top:0px; height: 350px; width: 400px; display:none">
                    <div style="text-align:left">
            <%--    <div class="auto-style1">--%>
                    <asp:Label ID="lblName1" runat="server" />
                    <asp:Label ID="lblFile1" runat="server" Visible="false" /><br />
                <%--</div>
                <div class="auto-style4">--%>
                    地震時間 : <asp:Label ID="lblDate1" runat="server" /><br />
                <%--</div>
                <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt1" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX1" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY1" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ1" runat="server" />
                     <img src="../images/eqmap/eqWave.png" width="390" height="275" />
                       </div>
            </span>

            <%-- 地点2 --%>
            <span style="position:absolute; left:745px; top:130px;">
                <asp:ImageButton ID="imgMark2" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark2_Click" BorderStyle="None" 
                    onMouseover="mOver(2)" onMouseout="mOut(2)" />
            </span>
            <span ID="panel2" class="eqpanel" style="left:0px; top:0px; height: 350px; width: 400px; display:none">
                    <div style="text-align:left">
                <%--<div class="auto-style1">--%>
                    <asp:Label ID="lblName2" runat="server" />
                    <asp:Label ID="lblFile2" runat="server" Visible="false" /><br />
             <%--   </div>
                <div class="auto-style4">--%>
                    地震時間 : <asp:Label ID="lblDate2" runat="server" /><br />
            <%--    </div>
                <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt2" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX2" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY2" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ2" runat="server" />
                       <img src="../images/eqmap/eqWave.png" width="390" height="275" />
                       </div>
            </span>

            <%-- 地点3 --%>
            <span style="position:absolute; left:870px; top:90px;">
                <asp:ImageButton ID="imgMark3" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark3_Click" BorderStyle="None" 
                    onMouseover="mOver(3)" onMouseout="mOut(3)" />
            </span>
            <span ID="panel3" class="eqpanel" style="left:0px; top:0px; height: 350px; width: 400px; display:none">
                    <div style="text-align:left">
             <%--   <div class="auto-style1">--%>
                    <asp:Label ID="lblName3" runat="server" />
                    <asp:Label ID="lblFile3" runat="server" Visible="false" /><br />
                <%--</div>
                <div class="auto-style4">--%>
                    地震時間 : <asp:Label ID="lblDate3" runat="server" /><br />
                <%--</div>
                <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt3" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX3" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY3" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ3" runat="server" />
                  <img src="../images/eqmap/eqWave.png" width="390" height="275" />
                       </div>
            </span>

            <%-- 地点4 --%>
            <span style="position:absolute; left:585px; top:30px;">
                <asp:ImageButton ID="imgMark4" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark4_Click" BorderStyle="None" 
                    onMouseover="mOver(4)" onMouseout="mOut(4)" />
            </span>
            <span ID="panel4" class="eqpanel"  style="left:0px; top:0px; height: 350px; width: 400px; display:none">
                    <div style="text-align:left">
                <%--<div class="auto-style1">--%>
                    <asp:Label ID="lblName4" runat="server" />
                    <asp:Label ID="lblFile4" runat="server" Visible="false" /><br />
                <%--</div>
                <div class="auto-style4">--%>
                    地震時間 : <asp:Label ID="lblDate4" runat="server" /><br />
           <%--     </div>--%>
            <%--    <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt4" runat="server" />級<br />
                    最大加速度値(gal) :
                    X軸 : <asp:Label ID="lblAccX4" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY4" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ4" runat="server" />
                          <img src="../images/eqmap/eqWave.png" width="390" height="275" />
                       </div>
          <%--      </div>--%>
            </span>

            <%-- 地点5 --%>
            <span style="position:absolute; left:560px; top:335px;">
                <asp:ImageButton ID="imgMark5" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark5_Click" BorderStyle="None" 
                    onMouseover="mOver(5)" onMouseout="mOut(5)" />
            </span>
            <span ID="panel5" class="eqpanel"  style="left:0px; top:0px; height: 350px; width: 400px; display:none">
                   <div style="text-align:left">
          <%--      <div class="auto-style1">--%>
                    <asp:Label ID="lblName5" runat="server" />
                    <asp:Label ID="lblFile5" runat="server" Visible="false" /><br />
               <%-- </div>
                <div class="auto-style4">--%>
                   地震時間 : <asp:Label ID="lblDate5" runat="server" /><br />
               <%-- </div>
                <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt5" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX5" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY5" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ5" runat="server" />
                         <img src="../images/eqmap/eqWave.png" width="390" height="275" />
                       </div>
               <%-- </div>--%>
            </span>

            <%-- 地点6 --%>
            <span style="position:absolute; left:323px; top:425px;">
                <asp:ImageButton ID="imgMark6" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark6_Click" BorderStyle="None" 
                    onMouseover="mOver(6)" onMouseout="mOut(6)" />
            </span>
            <span style="position:absolute; left:115px; top:44px; height: 150px; width: 280px; font-size: medium;">
            <span ID="panel6" class="eqpanel" style="left:-100px; top:-40px; height: 350px; width: 400px; display:none">
                <div style="text-align:left">
                <%--<div class="auto-style1">--%>
                    <asp:Label ID="lblName6" runat="server" />
                    <asp:Label ID="lblFile6" runat="server" Visible="false" /><br />
             <%--   </div>--%>
              <%--  <div class="auto-style4">--%>
                    地震時間 : <asp:Label ID="lblDate6" runat="server" /><br />
            <%--    </div>
                <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt6" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX6" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY6" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ6" runat="server" />
                    <img src="../images/eqmap/eqWave.png" width="390" height="275" />
               <%-- </div>--%>
                </div>
            </span>

            <%-- 地点7 --%>
            <span style="position:absolute; left:20px; top:410px;">
                <asp:ImageButton ID="imgMark7" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark7_Click" BorderStyle="None" 
                    onMouseover="mOver(7)" onMouseout="mOut(7)" />
            </span>
            <span ID="panel7" class="eqpanel" style="left:-100px; top:-40px; height: 350px; width: 400px; display:none">
                  <div style="text-align:left">
      <%--          <div class="auto-style1">--%>
                    <asp:Label ID="lblName7" runat="server" />
                    <asp:Label ID="lblFile7" runat="server" Visible="false" /><br />
              <%--  </div>
                <div class="auto-style4">--%>
                    地震時間 : <asp:Label ID="lblDate7" runat="server" /><br />
               <%-- </div>
                <div class="auto-style2">--%>
                    震度 : <asp:Label ID="lblInt7" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX7" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY7" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ7" runat="server" />
                  <img src="../images/eqmap/eqWave.png" width="390" height="275" />
           <%--     </div>--%>
                      </div>
            </span>

                 <%-- 桶頭堰 --%>
            <span style="position:absolute; left:830px; top:25px;">
                <asp:ImageButton ID="imgMark8" runat="server" Height="15px" Width="15px" ImageUrl="~/images/eqmap/mark_blue.gif" OnClick="imgMark8_Click" BorderStyle="None" 
                    onMouseover="mOver(8)" onMouseout="mOut(8)" />
            </span>
            <span ID="panel8" class="eqpanel" style="left:-100px; top:-40px; height: 350px; width: 400px; display:none">
                  <div style="text-align:left">
                       <asp:Label ID="lblName8" runat="server" />
                    <asp:Label ID="lblFile8" runat="server" Visible="false" /><br />
                    地震時間 : <asp:Label ID="lblDate8" runat="server" /><br />
                    震度 : <asp:Label ID="lblInt8" runat="server" />級<br />
                    最大加速度値(gal) : 
                    X軸 : <asp:Label ID="lblAccX8" runat="server" />&nbsp;&nbsp;
                    Y軸 : <asp:Label ID="lblAccY8" runat="server" />&nbsp;&nbsp;
                    Z軸 : <asp:Label ID="lblAccZ8" runat="server" />
                  <img src="../images/eqmap/eqWave.png" width="390" height="275" />
                      </div>
            </span>
        </div>
    </div>
    </center>

    </div>
    <asp:Label ID="LabelTest1" runat="server" Text=""></asp:Label>
    </span>
</asp:Content>

