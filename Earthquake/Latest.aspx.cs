﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Earthquake_Latest : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }

        //各列の書式を設定
        //DATETIME
        EQGridView.Columns[0].ItemStyle.Width = 150;
        EQGridView.Columns[0].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //STATE
        EQGridView.Columns[1].ItemStyle.Width = 100;
        EQGridView.Columns[1].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //POINT
        EQGridView.Columns[2].ItemStyle.Width = 150;
        EQGridView.Columns[2].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //震度
        EQGridView.Columns[3].ItemStyle.Width = 40;
        EQGridView.Columns[3].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //X
        EQGridView.Columns[4].ItemStyle.Width = 50;
        EQGridView.Columns[4].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //Y
        EQGridView.Columns[5].ItemStyle.Width = 50;
        EQGridView.Columns[5].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //Z
        EQGridView.Columns[6].ItemStyle.Width = 50;
        EQGridView.Columns[6].ItemStyle.HorizontalAlign = HorizontalAlign.Right;

        //※REC_EVENT降順に並べてTOP5を出す
        //UTC→台湾時間(+8)
        string sqlWhereEx = "";
        string sType = "AUTO";
        string sType2 = "CAL";
        if (sType != "") //AUTO and NULL
        {
            sqlWhereEx += "WHERE REC_EVENT.REC_ID=REC_POINT.REC_ID";
            //sqlWhereEx += string.Format("WHERE (STATE IS NULL) OR (STATE='') OR (STATE LIKE '{0}') OR (STATE LIKE '{1}')", sType, sType2);
        }
        string sqlSelect = "SELECT DATEADD(hour, 8, REC_EVENT.ORIGIN_DATE) AS ORIGIN_DATE, REC_EVENT.STATE, SITE.NAME AS SITE_NAME, POINT.NO AS POINT_NO, POINT.NAME AS POINT_NAME, REC_POINT.INTENSITY"
            + ", RC.X, RC.Y, RC.Z"
            + ", REC_POINT.DATA_FILENAME, REC_POINT.ANLZ_FILENAME"
            + " FROM"
                + " (SELECT DISTINCT TOP 5 REC_EVENT.ORIGIN_DATE"
                + " FROM REC_EVENT, REC_POINT, REC_CH"
                + " WHERE REC_EVENT.REC_ID=REC_POINT.REC_ID AND REC_POINT.REC_POINT_ID=REC_CH.REC_POINT_ID"
                + " ORDER BY REC_EVENT.ORIGIN_DATE DESC"
                + ") RE"
                + ", REC_EVENT, REC_POINT, SITE, POINT"
                + ", (SELECT REC_POINT_ID"
                + ", MAX(CASE WHEN CH_SET.DIR='X' THEN REC_CH.REC_CH_ID ELSE 0 END) as RCID"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='X' THEN REC_CH.MAX ELSE 0 END),1,1) as X"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='Y' THEN REC_CH.MAX ELSE 0 END),1,1) as Y"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='Z' THEN REC_CH.MAX ELSE 0 END),1,1) as Z"
                + " FROM REC_CH, CH_SET"
                + " WHERE REC_CH.CH_ID=CH_SET.CH_ID"
                + " GROUP BY REC_CH.REC_POINT_ID"
                + " ) RC"
            + " WHERE REC_POINT.REC_ID=REC_EVENT.REC_ID"
            + " AND RE.ORIGIN_DATE=REC_EVENT.ORIGIN_DATE"
            + " AND REC_EVENT.SITE_ID=SITE.SITE_ID"
            + " AND POINT.SITE_ID=SITE.SITE_ID"
            + " AND REC_POINT.POINT_ID=POINT.POINT_ID"
            + " AND RC.REC_POINT_ID=REC_POINT.REC_POINT_ID";
        string sqlWhere = " ";
        string sqlGroupby = " ";
        string sqlOrderby = " ORDER BY RE.ORIGIN_DATE DESC, POINT.POINT_ID";
        /*
        string sqlSelect = "SELECT DATEADD(hour, 8, RE.ORIGIN_DATE) AS ORIGIN_DATE, RE.STATE, SITE.NAME AS SITE_NAME, POINT.NO AS POINT_NO, POINT.NAME AS POINT_NAME, REC_POINT.INTENSITY"
            + ", RC.X, RC.Y, RC.Z"
            + ", REC_POINT.DATA_FILENAME, REC_POINT.ANLZ_FILENAME"
            + " FROM"
                + " (SELECT DISTINCT TOP 5 REC_EVENT.REC_ID, REC_EVENT.SITE_ID, REC_EVENT.ORIGIN_DATE, REC_EVENT.STATE"
                + " FROM REC_EVENT, REC_POINT"
                + " WHERE REC_EVENT.REC_ID=REC_POINT.REC_ID"
                + " ORDER BY REC_EVENT.ORIGIN_DATE DESC"
                + ") RE"
                + ", REC_POINT, REC_CH, CH_SET, SITE, POINT"
                + ", (SELECT DISTINCT REC_POINT_ID"
                + ", MAX(CASE WHEN CH_SET.DIR='X' THEN REC_CH.REC_CH_ID ELSE 0 END) as RCID"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='X' THEN REC_CH.MAX ELSE 0 END),1,1) as X"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='Y' THEN REC_CH.MAX ELSE 0 END),1,1) as Y"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='Z' THEN REC_CH.MAX ELSE 0 END),1,1) as Z"
                + " FROM REC_CH, CH_SET"
                + " WHERE REC_CH.CH_ID=CH_SET.CH_ID"
                + " GROUP BY REC_CH.REC_POINT_ID"
                + " ) RC"
            + " WHERE RE.REC_ID=REC_POINT.REC_ID"
            + " AND RE.SITE_ID=SITE.SITE_ID"
            + " AND POINT.SITE_ID=SITE.SITE_ID"
            + " AND REC_POINT.POINT_ID=POINT.POINT_ID"
            + " AND REC_POINT.REC_POINT_ID=REC_CH.REC_POINT_ID"
            + " AND REC_CH.CH_ID=CH_SET.CH_ID"
            + " AND RC.REC_POINT_ID=REC_POINT.REC_POINT_ID"
            + " AND RC.RCID=REC_CH.REC_CH_ID";
        string sqlWhere = " ";
        string sqlGroupby = " ";
        string sqlOrderby = " ORDER BY RE.ORIGIN_DATE DESC, POINT.POINT_ID, CH_SET.LOGGER_CHNO";
        */

        string sqlCommand = sqlSelect + sqlWhere + sqlGroupby + sqlOrderby;
        SqlDataSource1.SelectCommand = sqlCommand;

        //joinCells(EQGridView, 1);
    }

    //GridViewのヘッダを編集
    void makeHeader(GridView gv)
    {
        TableRow row1 = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
        TableRow row2 = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
        int i;
        string[] shead = { "發生時間", "記錄類別", "測站名稱", "震度", "最大加速度(gal)", "波形檔案", "文字檔案", "波形圖", "選取", "&nbsp" };
        string[] shead2 = { "X軸", "Y軸", "Z軸", "&nbsp", "&nbsp", "&nbsp", "&nbsp", "&nbsp", "&nbsp" };

        for (i = 0; i < 5; i++)
        {
            TableCell cell1 = new TableCell();
            if (i == 4)
            {
                cell1.ColumnSpan = 3;
                cell1.RowSpan = 1;
            }
            else
            {
                cell1.ColumnSpan = 1;
                cell1.RowSpan = 2;
            }
            cell1.Style.Add("color", "white");
            cell1.Style.Add("text-align", "center");
            cell1.Style.Add("font-weight", "bold");
            cell1.Style.Add("background-color", "darkblue");
            if (i == 4)
            {
                cell1.Style.Add("border-bottom", "1px #666666 solid");
            }
            else
            {
                cell1.Style.Add("border-bottom", "1px darkblue solid");
            }
            cell1.Style.Add("border-right", "1px #666666 solid");
            cell1.Text = shead[i];
            row1.Cells.Add(cell1);
        }
        gv.Controls[0].Controls.AddAt(0, row1);
        for (i = 0; i < 3; i++)
        {
            TableCell cell2 = new TableCell();
            cell2.Style.Add("color", "white");
            cell2.Style.Add("text-align", "center");
            cell2.Style.Add("font-weight", "bold");
            cell2.Style.Add("background-color", "darkblue");
            cell2.Style.Add("border-bottom", "1px #666666 solid");
            cell2.Style.Add("border-right", "1px #666666 solid");
            cell2.Text = shead2[i];
            row2.Cells.Add(cell2);
        }
        gv.Controls[0].Controls.AddAt(1, row2);

        gv.ShowHeader = false; //元のヘッダは非表示

    }

    //GridViewの重複セルを結合
    void joinCells(GridView gv, int column)
    {
        int numRow = gv.Rows.Count;
        int baseIndex = 0;

        TableCell baseCell;
        TableCell nextCell;
        while (baseIndex < numRow)
        {
            int nextIndex = baseIndex + 1;
            baseCell = gv.Rows[baseIndex].Cells[column];

            if (baseCell.RowSpan >= 2)
                return; //RowSpanを既に処理されていたらやめる

            while (nextIndex < numRow)
            {
                nextCell = gv.Rows[nextIndex].Cells[column];
                //内容が一致したら結合対象とする
                if (baseCell.Text == nextCell.Text)
                {
                    if (baseCell.RowSpan == 0)
                    {
                        baseCell.RowSpan = 2;
                    }
                    else
                    {
                        baseCell.RowSpan++;
                    }
                    gv.Rows[nextIndex].Cells.Remove(nextCell);
                    nextIndex++;
                }
                else
                {
                    break;
                }
            }
            baseIndex = nextIndex;
        }
    }

    //GridViewの重複セルを同じ背景色にする
    void SetCellsColor(GridView gv, int column)
    {
        int color_sw = 0;

        int numRow = gv.Rows.Count;
        if (numRow <= 0)
            return;

        TableCell baseCell;
        TableCell nextCell;

        int nIndex = 0;
        baseCell = gv.Rows[nIndex].Cells[column];
        while (nIndex < numRow)
        {
            nextCell = gv.Rows[nIndex].Cells[column];
            //内容が一致したら同じ色とする
            if (baseCell.Text == nextCell.Text)
            {
                ;
            }
            else
            {
                if (color_sw == 0)
                    color_sw = 1;
                else
                    color_sw = 0;
                baseCell = gv.Rows[nIndex].Cells[column];
            }
            //各行色を塗る
            if (color_sw == 0)
                gv.Rows[nIndex].BackColor = System.Drawing.Color.WhiteSmoke;
            else
                gv.Rows[nIndex].BackColor = System.Drawing.Color.White;

            nIndex++;
        }
    }

    protected void EQGridView_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    //GridViewのコマンドボタンがクリックされたイベント
    protected void EQGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int args = Int32.Parse((String)e.CommandArgument);

        switch (e.CommandName)
        {
            // 波形画像を開く
            case "GetPicFile":
                string s1 = EQGridView.Rows[args].Cells[10].Text;
                s1 = s1.Replace(".tpc", ".tpc.jpg");
                s1 = s1.Replace(".ana", ".tpc.jpg");
                s1 = s1.Replace(".dbl", ".tpc.jpg");
                string url1 = GetWaveFile(s1);
                if (url1.Length > 0)
                    Server.Transfer(url1);
                break;
            // データを取り込む
            case "GetDataFile":
                string s2 = EQGridView.Rows[args].Cells[10].Text;
                s1 = s2.Replace(".tpc", ".dbl");
                s1 = s2.Replace(".ana", ".dbl");
                string url2 = GetWaveFile(s2);
                if (url2.Length > 0)
                    Server.Transfer(url2);
                break;
            // テキストデータを取り込む
            case "GetTextFile":
                string s3 = EQGridView.Rows[args].Cells[10].Text;
                s1 = s3.Replace(".tpc", ".txt");
                s1 = s3.Replace(".ana", ".txt");
                s1 = s3.Replace(".dbl", ".txt");
                string url3 = GetWaveFile(s3);
                if (url3.Length > 0)
                    Server.Transfer(url3);
                break;

        }

    }

    //該当ファイルを転送してURLを返す
    private string GetWaveFile(string ref_file)
    {
        string ret = "";

        if (ref_file.Length > 10)
        {
            string sRootFrom = ConfigurationManager.AppSettings.Get("EarthquakeWaveRoot");
            string sRootTo = ConfigurationManager.AppSettings.Get("EarthquakeWebdataRoot");
            string fname = ref_file;
            string fyearmon = fname.Substring(0, 6); //年月を取得（フォルダ用）
            string fpath = "\\" + fyearmon + "\\" + fname;
            try
            {
                string sWaveFrom = sRootFrom + fpath;
                string sWaveTo = sRootTo + fpath;
                if (File.Exists(sWaveTo) != true)
                {
                    if (Directory.Exists(sRootTo + "\\" + fyearmon) != true)
                    {
                        Directory.CreateDirectory(sRootTo + "\\" + fyearmon);
                    }
                    File.Copy(sWaveFrom, sWaveTo, true); //webdataを書き込み許可にしておく事
                }
                ret = ConfigurationManager.AppSettings["EarthquakeWebdataRootURL"] + "/" + fyearmon + "/" + fname;
            }
            catch (IOException ei)
            {
                // FileNotFoundExceptions are handled here.
            }
        }
        return ret;
    }

    protected void EQGridView_PreRender(object sender, EventArgs e)
    {
        //SetCellsColor(EQGridView, 0);
        //joinCells(EQGridView, 0);

    }

    protected void EQGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            makeHeader(EQGridView);
        }
        if ((e.Row.RowType == DataControlRowType.Pager))
        {
            //Pagerまでできたらセル修正
            checkSet();
            SetCellsColor(EQGridView, 0); //COL=0:時刻
            joinCells(EQGridView, 0); //COL=0:時刻
        }

    }

    private string convStatestr(string state)
    {
        string ret = state;

        if (state == "AUTO")
        {
            ret = "地震觸發";
        }
        else if (state == "CAL")
        {
            ret = "系統測試";
        }

        return ret;
    }

    protected void checkSet()
    {
        for (int i = 0; i < EQGridView.Rows.Count; i++)
        {
            string state;
            state = EQGridView.Rows[i].Cells[1].Text;
            state = convStatestr(state);
            EQGridView.Rows[i].Cells[1].Text = state;
        }
    }
}