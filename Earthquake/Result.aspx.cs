﻿using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Earthquake_Result : System.Web.UI.Page
{
    string sFrom = "", sTo = "";
    string sPointNo = "";
    string sAcc = "";
    string sInt = "";
    string sType = "";

    Hashtable chkArray = new Hashtable();

    private static string dbconn = ConfigurationManager.ConnectionStrings["earthqEMAS"].ToString();
    string sqlCommand_s = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }

        if (Session["Account"] != null)
        {
            string sAccount = Session["Account"].ToString();
            if (sAccount == "guest")
            {
                btnDelete.Visible = false;
            }
        }
        else
        {
            btnDelete.Visible = false;
        }

        //各列の書式を設定
        //DATA_FILENAME
        EQGridView.Columns[0].ItemStyle.Width = 0;
        EQGridView.Columns[0].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //ANLZ_FILENAME
        EQGridView.Columns[1].ItemStyle.Width = 0;
        EQGridView.Columns[1].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //DATETIME
        EQGridView.Columns[2].ItemStyle.Width = 150;
        EQGridView.Columns[2].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //STATE
        EQGridView.Columns[3].ItemStyle.Width = 100;
        EQGridView.Columns[3].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //POINT
        EQGridView.Columns[4].ItemStyle.Width = 150;
        EQGridView.Columns[4].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //震度
        EQGridView.Columns[5].ItemStyle.Width = 40;
        EQGridView.Columns[5].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //X
        EQGridView.Columns[6].ItemStyle.Width = 50;
        EQGridView.Columns[6].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //Y
        EQGridView.Columns[7].ItemStyle.Width = 50;
        EQGridView.Columns[7].ItemStyle.HorizontalAlign = HorizontalAlign.Right;
        //Z
        EQGridView.Columns[8].ItemStyle.Width = 50;
        EQGridView.Columns[8].ItemStyle.HorizontalAlign = HorizontalAlign.Right;

        //UTC→台湾時間(+8)
        string sqlSelect = "SELECT DATEADD(hour, 8, REC_EVENT.ORIGIN_DATE) AS ORIGIN_DATE, REC_EVENT.STATE, SITE.NAME AS SITE_NAME, POINT.NO AS POINT_NO, POINT.NAME AS POINT_NAME, REC_POINT.INTENSITY"
            + ", RC.X, RC.Y, RC.Z"
            + ", REC_POINT.DATA_FILENAME, REC_POINT.ANLZ_FILENAME, REC_POINT.REC_POINT_ID"
            + " FROM REC_EVENT, REC_POINT, REC_CH, CH_SET, SITE, POINT"
                + ", (SELECT DISTINCT REC_POINT_ID"
                + ", MAX(CASE WHEN CH_SET.DIR='X' THEN REC_CH.REC_CH_ID ELSE 0 END) as RCID"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='X' THEN REC_CH.MAX ELSE 0 END),1,1) as X"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='Y' THEN REC_CH.MAX ELSE 0 END),1,1) as Y"
                + ", ROUND(MAX(CASE WHEN CH_SET.DIR='Z' THEN REC_CH.MAX ELSE 0 END),1,1) as Z"
                + " FROM REC_CH, CH_SET"
                + " WHERE REC_CH.CH_ID=CH_SET.CH_ID"
                + " GROUP BY REC_CH.REC_POINT_ID"
                + " ) RC"
            + " WHERE REC_EVENT.REC_ID=REC_POINT.REC_ID"
            + " AND REC_EVENT.SITE_ID=SITE.SITE_ID"
            + " AND POINT.SITE_ID=SITE.SITE_ID"
            + " AND REC_POINT.POINT_ID=POINT.POINT_ID"
            + " AND REC_POINT.REC_POINT_ID=REC_CH.REC_POINT_ID"
            + " AND REC_CH.CH_ID=CH_SET.CH_ID"
            + " AND RC.REC_POINT_ID=REC_POINT.REC_POINT_ID"
            + " AND RC.RCID=REC_CH.REC_CH_ID";
        string sqlWhereEx = "";
        string sqlGroupby = " ";
        string sqlOrderby = " ORDER BY REC_EVENT.ORIGIN_DATE DESC, POINT.POINT_ID, CH_SET.LOGGER_CHNO";

        //抽出条件
        // ページ間ポスティングでページが呼び出されたかをチェック
        if (Page.PreviousPage != null)
        {
            if (Page.PreviousPage.IsCrossPagePostBack)
            {
                DateTime dtFrom, dtTo;
                dtFrom = GetDateFrom();
                dtTo = GetDateTo();
                if (dtFrom > dtTo)
                {
                    sFrom = dtTo.ToString("yyyy-MM-dd 00:00:00");
                    sTo = dtFrom.ToString("yyyy-MM-dd 23:59:59");
                }
                else
                {
                    sFrom = dtFrom.ToString("yyyy-MM-dd 00:00:00");
                    sTo = dtTo.ToString("yyyy-MM-dd 23:59:59");
                }
                sPointNo = GetPointNo();
                sAcc = GetAcc();
                sInt = GetInt();
                sType = GetRecType();

                //パラメータをセッションに入れる
                Session["sFrom"] = sFrom;
                Session["sTo"] = sTo;
                Session["sPointNo"] = sPointNo;
                Session["sAcc"] = sAcc;
                Session["sInt"] = sInt;
                Session["sType"] = sType;
                Session["chkArrayREC"] = chkArray;

                //ViewStateにパラメータ値を保持
                ViewState.Add("sFrom", sFrom);
                ViewState.Add("sTo", sTo);
                ViewState.Add("sPointNo", sPointNo);
                ViewState.Add("sAcc", sAcc);
                ViewState.Add("sInt", sInt);
                ViewState.Add("sType", sType);
            }
        }

        if (Session["sFrom"] != null)
        {
            sFrom = Session["sFrom"].ToString();
        }
        if (Session["sTo"] != null)
        {
            sTo = Session["sTo"].ToString();
        }
        if (Session["sPointNo"] != null)
        {
            sPointNo = Session["sPointNo"].ToString();
        }
        if (Session["sAcc"] != null)
        {
            sAcc = Session["sAcc"].ToString();
        }
        if (Session["sInt"] != null)
        {
            sInt = Session["sInt"].ToString();
        }
        if (Session["sType"] != null)
        {
            sType = Session["sType"].ToString();
        }

        if (ViewState["sFrom"] != null)
        {
            sFrom = ViewState["sFrom"].ToString();
        }
        if (ViewState["sTo"] != null)
        {
            sTo = ViewState["sTo"].ToString();
        }
        if (ViewState["sPointNo"] != null)
        {
            sPointNo = ViewState["sPointNo"].ToString();
        }
        if (ViewState["sAcc"] != null)
        {
            sAcc = ViewState["sAcc"].ToString();
        }
        if (ViewState["sInt"] != null)
        {
            sInt = ViewState["sInt"].ToString();
        }
        if (ViewState["sType"] != null)
        {
            sType = ViewState["sType"].ToString();
        }

        if ((sFrom != "") && (sTo != ""))
        {
            sqlWhereEx = string.Format(" AND REC_EVENT.ORIGIN_DATE>=CONVERT(datetime,'{0}',120) AND REC_EVENT.ORIGIN_DATE<=CONVERT(datetime,'{1}',120)", sFrom, sTo);
        }
        if (sPointNo != "")
        {
            sqlWhereEx += string.Format(" AND POINT.POINT_ID='{0}'", sPointNo);
        }
        if (sAcc != "")
        {
            sqlWhereEx += string.Format(" AND REC_CH.MAX>='{0}'", sAcc);
        }
        if (sInt != "")
        {
            sqlWhereEx += string.Format(" AND REC_POINT.INTENSITY>='{0}'", sInt);
        }
        if (sType != "")
        {
            sqlWhereEx += string.Format(" AND ((REC_EVENT.STATE IS NULL) OR (REC_EVENT.STATE='') OR (REC_EVENT.STATE LIKE '{0}'))", sType);
        }

        string sqlCommand = sqlSelect + sqlWhereEx + sqlGroupby + sqlOrderby;
        sqlCommand_s = sqlCommand;
        SqlDataSource1.SelectCommand = sqlCommand;

        Hashtable hs = (Hashtable)Session["chkArrayREC"];
        if (hs != null)
        {
            chkArray = hs;
        }
        //joinCells(EQGridView, 1);
    }

    //前ページでリスト選択された地点番号を取得
    private string GetRecType()
    {
        CheckBox cBox = (CheckBox)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("CheckBox5");
        if (cBox.Checked == true)
        {
            DropDownList lPoint = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("lstType");
            return lPoint.SelectedValue;
        }
        else
        {
            return "";
        }
    }

    //前ページでリスト選択された震度を取得
    private string GetInt()
    {
        CheckBox cBox = (CheckBox)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("CheckBox4");
        if (cBox.Checked == true)
        {
            TextBox tInt = (TextBox)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("txtInt");
            float fInt = Convert.ToSingle(tInt.Text);
            if (fInt < 0)
                fInt = 0;
            return fInt.ToString();
        }
        else
        {
            return "";
        }
    }

    //前ページでリスト選択された加速度を取得
    private string GetAcc()
    {
        CheckBox cBox = (CheckBox)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("CheckBox3");
        if (cBox.Checked == true)
        {
            TextBox tAcc = (TextBox)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("txtAcc");
            float fAcc = Convert.ToSingle(tAcc.Text);
            if (fAcc < 0)
                fAcc = 0;
            return fAcc.ToString();
        }
        else
        {
            return "";
        }
    }

    //前ページでリスト選択された地点番号を取得
    private string GetPointNo()
    {
        CheckBox cBox = (CheckBox)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("CheckBox2");
        if (cBox.Checked == true)
        {
            DropDownList lPoint = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("lstPoint");
            return lPoint.SelectedValue;
        }
        else
        {
            return "";
        }
    }

    //前ページでリスト選択された開始日を文字列に変換
    private DateTime GetDateFrom()
    {
        DropDownList lFyear = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("Fyear");
        DropDownList lFmonth = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("Fmonth");
        DropDownList lFday = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("Fday");

        //DropDownList lFyear = (DropDownList)Page.PreviousPage.FindControl("Fyear");
        //DropDownList lFmonth = (DropDownList)Page.PreviousPage.FindControl("Fmonth");
        //DropDownList lFday = (DropDownList)Page.PreviousPage.FindControl("Fday");
        int ft_y = Convert.ToInt32(lFyear.Text);
        int ft_m = Convert.ToInt32(lFmonth.Text);
        int ft_d = Convert.ToInt32(lFday.Text);
        DateTime FromTime = new DateTime(ft_y, ft_m, ft_d, 0, 0, 0);  //開始時間

        return FromTime;
    }

    //前ページでリスト選択された終了日を文字列に変換
    private DateTime GetDateTo()
    {
        DropDownList lTyear = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("Tyear");
        DropDownList lTmonth = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("Tmonth");
        DropDownList lTday = (DropDownList)Page.PreviousPage.Master.FindControl("ContentPlaceHolder1").FindControl("Tday");
        int tt_y = Convert.ToInt32(lTyear.Text);
        int tt_m = Convert.ToInt32(lTmonth.Text);
        int tt_d = Convert.ToInt32(lTday.Text);
        DateTime ToTime = new DateTime(tt_y, tt_m, tt_d, 0, 0, 0);  //開始時間

        return ToTime;
    }

    //GridViewのヘッダを編集
    void makeHeader(GridView gv)
    {
        TableRow row1 = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
        TableRow row2 = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
        int i;
        string[] shead = { "發生時間", "記錄類別", "測站名稱", "震度", "最大加速度(gal)", "波形檔案<br>(DBL)", "文字檔案<br>(TXT)", "波形圖", "選取", "&nbsp" };
        string[] shead2 = { "X軸", "Y軸", "Z軸", "&nbsp", "&nbsp", "&nbsp", "&nbsp", "&nbsp", "&nbsp" };


        for (i = 0; i < 9; i++)
        {
            TableCell cell1 = new TableCell();
            if (i == 4)
            {
                cell1.ColumnSpan = 3;
                cell1.RowSpan = 1;
            }
            else
            {
                cell1.ColumnSpan = 1;
                cell1.RowSpan = 2;
            }
            cell1.Style.Add("color", "white");
            cell1.Style.Add("text-align", "center");
            cell1.Style.Add("font-weight", "bold");
            cell1.Style.Add("background-color", "darkblue");
            if (i == 4)
            {
                cell1.Style.Add("border-bottom", "1px #666666 solid");
            }
            else
            {
                cell1.Style.Add("border-bottom", "1px darkblue solid");
            }
            cell1.Style.Add("border-right", "1px #666666 solid");
            cell1.Text = shead[i];
            row1.Cells.Add(cell1);
        }
        gv.Controls[0].Controls.AddAt(0, row1);
        for (i = 0; i < 3; i++)
        {
            TableCell cell2 = new TableCell();
            cell2.Style.Add("color", "white");
            cell2.Style.Add("text-align", "center");
            cell2.Style.Add("font-weight", "bold");
            cell2.Style.Add("background-color", "darkblue");
            cell2.Style.Add("border-bottom", "1px #666666 solid");
            cell2.Style.Add("border-right", "1px #666666 solid");
            cell2.Text = shead2[i];
            row2.Cells.Add(cell2);
        }
        gv.Controls[0].Controls.AddAt(1, row2);

        gv.ShowHeader = false; //元のヘッダは非表示
    }

    //GridViewの重複セルを結合
    void joinCells(GridView gv, int column)
    {
        int numRow = gv.Rows.Count;
        int baseIndex = 0;

        TableCell baseCell;
        TableCell nextCell;
        while (baseIndex < numRow)
        {
            int nextIndex = baseIndex + 1;
            baseCell = gv.Rows[baseIndex].Cells[column];

            if (baseCell.RowSpan >= 2)
                return; //RowSpanを既に処理されていたらやめる

            while (nextIndex < numRow)
            {
                nextCell = gv.Rows[nextIndex].Cells[column];
                //内容が一致したら結合対象とする
                if (baseCell.Text == nextCell.Text)
                {
                    if (baseCell.RowSpan == 0)
                    {
                        baseCell.RowSpan = 2;
                    }
                    else
                    {
                        baseCell.RowSpan++;
                    }
                    gv.Rows[nextIndex].Cells.Remove(nextCell);
                    nextIndex++;
                }
                else
                {
                    break;
                }
            }
            baseIndex = nextIndex;
        }
    }

    //GridViewの重複セルを同じ背景色にする
    void SetCellsColor(GridView gv, int column)
    {
        int color_sw = 0;

        int numRow = gv.Rows.Count;
        if (numRow <= 0)
            return;

        TableCell baseCell;
        TableCell nextCell;

        int nIndex = 0;
        baseCell = gv.Rows[nIndex].Cells[column];
        while (nIndex < numRow)
        {
            nextCell = gv.Rows[nIndex].Cells[column];
            //内容が一致したら同じ色とする
            if (baseCell.Text == nextCell.Text)
            {
                ;
            }
            else
            {
                if (color_sw == 0)
                    color_sw = 1;
                else
                    color_sw = 0;
                baseCell = gv.Rows[nIndex].Cells[column];
            }
            //各行色を塗る
            if (color_sw == 0)
                gv.Rows[nIndex].BackColor = System.Drawing.Color.WhiteSmoke;
            else
                gv.Rows[nIndex].BackColor = System.Drawing.Color.White;

            nIndex++;
        }
    }


    private void DownloadText(string srcfile, string downfile)
    {
        //ファイルのダウンロード
        try
        {
            FileStream objFileStream = new FileStream(srcfile, FileMode.Open);
            long lngFileSize = objFileStream.Length;
            byte[] bytBuffer = new byte[(int)lngFileSize];
            objFileStream.Read(bytBuffer, 0, (int)lngFileSize);
            objFileStream.Close();
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(downfile));
            Response.ContentType = "text/plain";
            Response.BinaryWrite(bytBuffer);
        }
        catch (IOException ei)
        {
            // FileNotFoundExceptions are handled here.
            Response.Redirect("Result.aspx", true);
        }

    }

    private void DownloadBinary(string srcfile, string downfile)
    {
        //ファイルのダウンロード
        try
        {
            FileStream objFileStream = new FileStream(srcfile, FileMode.Open);
            long lngFileSize = objFileStream.Length;
            byte[] bytBuffer = new byte[(int)lngFileSize];
            objFileStream.Read(bytBuffer, 0, (int)lngFileSize);
            objFileStream.Close();
            Response.Clear();
            Response.AddHeader("Content-Disposition", "inline;filename=" + HttpUtility.UrlEncode(downfile));
            Response.ContentType = "application/x-binary";
            Response.BinaryWrite(bytBuffer);
        }
        catch (IOException ei)
        {
            // FileNotFoundExceptions are handled here.
            Response.Redirect("Result.aspx", true);
        }

    }

    //複数ファイルをZip圧縮してダウンロード
    //flag = 0:dbl, 1:txt
    private void DownloadZip(string downfile, int flag)
    {
        string srcfile, srcfile2;
        string reflp = "";

        try
        {
            DataTable dt = GetData();

            Response.Clear();
            Response.AddHeader("Content-Disposition", "filename=" + HttpUtility.UrlEncode(downfile));
            Response.ContentType = "application/zip";

            //(1)ZIPクラスをインスタンス化
            //using (ZipFile zip = new ZipFile(Encoding.GetEncoding("shift_jis")))
            using (ZipFile zip = new ZipFile())
            {
                ////(2)圧縮レベルを設定
                //zip.CompressionLevel = CompressionLevel.BestCompression;

                //(3)ファイルを追加
                //zip.AddFile(@"C:\work\20140729150630$.dbl");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    srcfile = dt.Rows[i]["DATA_FILENAME"].ToString();
                    string rpid = dt.Rows[i]["REC_POINT_ID"].ToString();
                    if ((chkArray != null) && (chkArray.ContainsKey(rpid) == true))
                    {
                        string s = chkArray[rpid].ToString();
                        if (Convert.ToInt32(s) == 1)
                        {
                            if (flag == 0)
                            {
                                srcfile = srcfile.Replace(".tpc", ".dbl");
                                srcfile2 = srcfile;
                                string url = GetWaveFile(srcfile, srcfile2, ref reflp, 0);
                                if (url.Length > 0)
                                {
                                    zip.AddFile(reflp, "");
                                }
                            }
                            else
                            {
                                srcfile = srcfile.Replace(".tpc", ".tpc");
                                srcfile2 = srcfile.Replace(".tpc", ".txt");
                                string url = GetWaveFile(srcfile, srcfile2, ref reflp, 1);
                                if (url.Length > 0)
                                {
                                    zip.AddFile(reflp, "");
                                }
                            }
                        }
                    }
                }

                //(4)ディレクトリを追加する場合
                //zip.AddDirectory(@"C:\追加したいフォルダ");

                //(5)ZIPファイルを保存
                //zip.Save(@"C:\work\dbl.zip");
                zip.Save(Response.OutputStream);

                Response.End();
            }
        }
        catch (Exception ex)
        {
            //MessageBox.Show(ex.Message, "ZIP作成失敗");
        }
    }


    //GridViewのコマンドボタンがクリックされたイベント
    protected void EQGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int args = Int32.Parse((String)e.CommandArgument);
        int refcell = 0;
        string reflp = "";

        if (args < EQGridView.Rows.Count)
        {
            switch (e.CommandName)
            {
                // 波形画像を開く
                case "GetPicFile":
                    string s1 = EQGridView.Rows[args].Cells[refcell].Text;
                    s1 = s1.Replace(".tpc", ".tpc.jpg");
                    s1 = s1.Replace(".ana", ".tpc.jpg");
                    s1 = s1.Replace(".dbl", ".tpc.jpg");
                    string url1 = GetWaveFile(s1, s1, ref reflp, 1);
                    if (url1.Length > 0)
                    {
                        //Server.Transfer(url1);
                        Response.Redirect(url1, true);
                    }
                    else
                    {
                        //見つからない場合自身をリダイレクトしてポストバックを発生（レイアウト崩れ防止用）
                        Response.Redirect("Result.aspx", true);
                    }
                    break;
                // データを取り込む
                case "GetDataFile":
                    string s2 = EQGridView.Rows[args].Cells[refcell].Text;
                    s2 = s2.Replace(".tpc", ".dbl");
                    s2 = s2.Replace(".ana", ".dbl");
                    string url2 = GetWaveFile(s2, s2, ref reflp, 1);
                    if (url2.Length > 0)
                    {
                        DownloadBinary(reflp, s2);
                        //Response.Redirect(url2, true);
                    }
                    else
                    {
                        //見つからない場合自身をリダイレクトしてポストバックを発生（レイアウト崩れ防止用）
                        Response.Redirect("Result.aspx", true);
                    }
                    break;
                // テキストデータを取り込む
                case "GetTextFile":
                    string s3 = EQGridView.Rows[args].Cells[refcell].Text;
                    s3 = s3.Replace(".tpc", ".tpc");
                    s3 = s3.Replace(".ana", ".tpc");
                    s3 = s3.Replace(".dbl", ".tpc");
                    string s4 = s3.Replace(".tpc", ".txt"); //拡張子をtpcからtxtに変えてコピーする
                    string url3 = GetWaveFile(s3, s4, ref reflp, 1);
                    if (url3.Length > 0)
                    {
                        DownloadText(reflp, s4);
                        //Response.Redirect(url3, true);
                    }
                    else
                    {
                        //見つからない場合自身をリダイレクトしてポストバックを発生（レイアウト崩れ防止用）
                        Response.Redirect("Result.aspx", true);
                    }
                    break;
            }
        }
    }

    //該当ファイルを転送してURLを返す
    //flag=0:コピーしないでローカルパスのみlocalpathに取得、1:コピーした先のローカルパスをlocalpathに取得
    private string GetWaveFile(string ref_file, string dst_file, ref string localpath, int flag)
    {
        string ret = "";
        localpath = "";

        if (ref_file.Length > 10)
        {
            string sRootFrom = ConfigurationManager.AppSettings["waveRoot"];
            string sRootTo = ConfigurationManager.AppSettings["webdataRoot"];
            string fyearmon = ref_file.Substring(0, 6); //年月を取得（フォルダ用）
            string fpath = "\\" + fyearmon + "\\";
            string sWaveFrom = sRootFrom + fpath + ref_file;
            string sWaveTo = sRootTo + fpath + dst_file;
            if (flag != 0)
            {
                try
                {
                    if (File.Exists(sWaveTo) != true)
                    {
                        if (Directory.Exists(sRootTo + "\\" + fyearmon) != true)
                        {
                            Directory.CreateDirectory(sRootTo + "\\" + fyearmon + "\\");
                        }
                        File.Copy(sWaveFrom, sWaveTo, true); //webdataを書き込み許可にしておく事
                    }
                    ret = ConfigurationManager.AppSettings["webdataRootURL"] + "/" + fyearmon + "/" + dst_file;
                    localpath = sWaveTo;
                }
                catch (IOException ei)
                {
                    // FileNotFoundExceptions are handled here.
                }
            }
            else
            {
                if (File.Exists(sWaveFrom) == true)
                {
                    ret = dst_file;
                    localpath = sWaveFrom;
                }
            }
        }
        return ret;
    }

    protected void EQGridView_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
    }

    protected void EQGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            makeHeader(EQGridView);
        }
        if ((e.Row.RowType == DataControlRowType.DataRow) || (e.Row.RowType == DataControlRowType.Header))
        {
            //ファイル名の列を動的に非表示にする（デザイン時に非表示設定するとデータバインドされなくなる）
            e.Row.Cells[0].Visible = false; //DATA_FILENAME
            e.Row.Cells[1].Visible = false; //REC_POINT_ID
        }
        if ((e.Row.RowType == DataControlRowType.Pager))
        {
            //Pagerまでできたらセル修正
            checkSet(); //チェックボックス
            SetCellsColor(EQGridView, 2); //COL=2:時刻
            joinCells(EQGridView, 2); //COL=2:時刻
        }
    }

    protected void btnDownDBL_Click(object sender, EventArgs e)
    {
        checkStateSave();
        DownloadZip("rec_event-DBL.zip", 0);
        Response.Redirect("Result.aspx", true);
    }

    protected void btnDownTXT_Click(object sender, EventArgs e)
    {
        checkStateSave();
        DownloadZip("rec_event-TXT.zip", 1);
        Response.Redirect("Result.aspx", true);
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        checkStateSave();
        //パラメータをセッションに入れる
        Session["sFrom"] = sFrom;
        Session["sTo"] = sTo;
        Session["sPointNo"] = sPointNo;
        Session["sAcc"] = sAcc;
        Session["sInt"] = sInt;
        Session["sType"] = sType;
        Session["chkArrayREC"] = chkArray;
        Response.Redirect("ResultPrint.aspx", true);

        Response.Redirect("Result.aspx", true);
    }

    //チェックされたデータを削除
    private void DeleteRecord()
    {
        int ret;
        string rpid;
        SqlDataSource dsrc = new SqlDataSource();

        dsrc.ConnectionString = dbconn;

        foreach (DictionaryEntry de in chkArray)
        {
            Console.WriteLine("{0} : {1}", de.Key, de.Value);
            if (Convert.ToInt32(de.Value.ToString()) == 1)
            {
                rpid = de.Key.ToString();
                string sqlCommand = "DELETE FROM REC_POINT WHERE REC_POINT_ID='" + rpid + "'";
                dsrc.DeleteCommand = sqlCommand;
                ret = dsrc.Delete();
            }
        }
    }

    private DataTable GetData()
    {
        DataTable dt = new DataTable();
        dt = new DataTable();

        DataSet ds = new DataSet();

        SqlDataSource dsrc = new SqlDataSource();

        dsrc.ConnectionString = dbconn;
        dsrc.SelectCommand = sqlCommand_s;
        dsrc.DataSourceMode = SqlDataSourceMode.DataSet;
        DataView dv = (DataView)dsrc.Select(DataSourceSelectArguments.Empty);
        ds.Tables.Add(dv.Table.Copy());

        dt = dv.Table.Copy();

        return dt;
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        try
        {
            checkStateSave();

            DataTable dt = GetData();

            string fileName = "rec_event.xls";

            Page.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Page.Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>");
            Page.Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            HtmlTable htmTable = new HtmlTable();
            htmTable.Border = 1;
            HtmlTableRow titleRow = new HtmlTableRow();
            HtmlTableCell titleCell = new HtmlTableCell();

            //標題
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "發生時間";
            titleRow.Cells.Add(titleCell);
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "記錄類別";
            titleRow.Cells.Add(titleCell);
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "測站名稱";
            titleRow.Cells.Add(titleCell);
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "震度";
            titleRow.Cells.Add(titleCell);
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "X軸";
            titleRow.Cells.Add(titleCell);
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "Y軸";
            titleRow.Cells.Add(titleCell);
            titleCell = new HtmlTableCell();
            titleCell.InnerText = "Z軸";
            titleRow.Cells.Add(titleCell);

            htmTable.Rows.Add(titleRow);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string rpid = dt.Rows[i]["REC_POINT_ID"].ToString();
                if ((chkArray != null) && (chkArray.ContainsKey(rpid) == true))
                {
                    string s = chkArray[rpid].ToString();
                    if (Convert.ToInt32(s) == 1)
                    {
                        titleRow = new HtmlTableRow();

                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = dt.Rows[i]["ORIGIN_DATE"].ToString();
                        titleRow.Cells.Add(titleCell);
                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = convStatestr(dt.Rows[i]["STATE"].ToString());
                        titleRow.Cells.Add(titleCell);
                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = dt.Rows[i]["POINT_NAME"].ToString();
                        titleRow.Cells.Add(titleCell);
                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = dt.Rows[i]["INTENSITY"].ToString();
                        titleRow.Cells.Add(titleCell);
                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = dt.Rows[i]["X"].ToString();
                        titleRow.Cells.Add(titleCell);
                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = dt.Rows[i]["Y"].ToString();
                        titleRow.Cells.Add(titleCell);
                        titleCell = new HtmlTableCell();
                        titleCell.InnerText = dt.Rows[i]["Z"].ToString();
                        titleRow.Cells.Add(titleCell);

                        htmTable.Rows.Add(titleRow);
                    }
                }
            }
            htmTable.RenderControl(htw);

            Page.Response.Write(sw.ToString());
            Page.Response.End();

        }
        catch (Exception ex)
        {

        }

        //Response.Redirect("eqresult.aspx", true);
    }

    private string convStatestr(string state)
    {
        string ret = state;

        if (state == "AUTO")
        {
            ret = "地震觸發";
        }
        else if (state == "CAL")
        {
            ret = "系統測試";
        }

        return ret;
    }

    //ページが切り替わる前にチェック状態を保持
    protected void EQGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        checkStateSave();
    }

    protected void checkStateSave()
    {
        for (int i = 0; i < EQGridView.Rows.Count; i++)
        {
            string rpid = EQGridView.Rows[i].Cells[1].Text.ToString();
            if (rpid != "")
            {
                CheckBox cb = (CheckBox)(EQGridView.Rows[i].FindControl("SelectCheckBox"));
                if (cb.Checked)
                {
                    chkArray[rpid] = "1";
                }
                else
                {
                    chkArray[rpid] = "0";
                }
            }
        }
        Session["chkArrayREC"] = chkArray;
    }

    protected void checkSet()
    {
        for (int i = 0; i < EQGridView.Rows.Count; i++)
        {
            string state;
            state = EQGridView.Rows[i].Cells[3].Text;
            state = convStatestr(state);
            EQGridView.Rows[i].Cells[3].Text = state;

            string rpid = EQGridView.Rows[i].Cells[1].Text;
            if (rpid != "")
            {
                CheckBox cb = (CheckBox)(EQGridView.Rows[i].FindControl("SelectCheckBox"));
                if ((chkArray != null) && (chkArray.ContainsKey(rpid) == true))
                {
                    string s = chkArray[rpid].ToString();
                    if (Convert.ToInt32(s) == 1)
                    {
                        cb.Checked = true;
                    }
                    else
                    {
                        cb.Checked = false;
                    }
                }
                else
                {
                    cb.Checked = false;
                }
            }
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        checkStateSave();
        DeleteRecord();
        Response.Redirect("Result.aspx", true);
    }

    protected void ChangeAllCheck(bool check)
    {
        DataTable dt = GetData();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string rpid = dt.Rows[i]["REC_POINT_ID"].ToString();
            if (check)
            {
                chkArray[rpid] = "1";
            }
            else
            {
                chkArray[rpid] = "0";
            }
        }
        Session["chkArrayREC"] = chkArray;
    }

    //グリッド上のチェックをすべてつける
    protected void btnCheckAll_Click(object sender, EventArgs e)
    {
        ChangeAllCheck(true);
        checkSet();
        Response.Redirect("Result.aspx", true);
    }

    //グリッド上のチェックをすべて消す
    protected void btnNoCheckAll_Click(object sender, EventArgs e)
    {
        ChangeAllCheck(false);
        checkSet();
        Response.Redirect("Result.aspx", true);
    }
}