﻿using Models.Earthquake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Earthquake_UserControls_MaintainStation : System.Web.UI.UserControl
{
    #region Properties

    private int stationID;
    public int StationID
    {
        get
        {
            return stationID;
        }
        set
        {
            stationID = value;
            ShowStationBaseInfo(StationBaseInfo);
        }
    }

    private dynamic StationBaseInfo
    {
        get
        {
            return (from info in EarthquakeController.Instance.GetAllStations()
                    where info.POINT_ID == StationID
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    private void EmptyAllControls()
    {
        ControlUtility.EmptyAllControls(this.Controls);
        StationIDTextBox.Enabled = true;
    }

    /// <summary>
    /// 顯示基本資料
    /// </summary>
    /// <param name="stationBaseInfo"></param>
    private void ShowStationBaseInfo(dynamic stationBaseInfo)
    {
        if (stationBaseInfo == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            StationIDTextBox.Text = stationBaseInfo.POINT_ID.ToString();
            StationIDTextBox.Enabled = false; // 不可修改
            StationNameTextBox.Text = stationBaseInfo.NAME;
            LongitudeTextBox.Text = stationBaseInfo.Longitude == null ? string.Empty : stationBaseInfo.Longitude.ToString();
            LatitudeTextBox.Text = stationBaseInfo.Latitude == null ? string.Empty : stationBaseInfo.Latitude.ToString();
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newStation = new POINT
            {
                POINT_ID = Convert.ToInt32(StationIDTextBox.Text),
                SITE_ID = 1,
                NAME = StationNameTextBox.Text,
                Longitude = string.IsNullOrEmpty(LongitudeTextBox.Text) ? (Decimal?)null : Convert.ToDecimal(LongitudeTextBox.Text),
                Latitude = string.IsNullOrEmpty(LatitudeTextBox.Text) ? (Decimal?)null : Convert.ToDecimal(LatitudeTextBox.Text),
            };
            if (string.IsNullOrEmpty(newStation.NAME) || !EarthquakeController.Instance.AddStation(newStation))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}