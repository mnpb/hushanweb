﻿<%@ Page Title="最近五筆地震資料" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Latest.aspx.cs" Inherits="Earthquake_Latest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        body {
            background-color: #D9ECF8;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
    <br />
    <asp:Label ID="lblDesc" runat="server" Text="最近五筆地震資料" Font-Bold="true" Font-Size="18px" ForeColor="DarkBlue"></asp:Label>
      <br />
    </center>
            &nbsp;
    <center>
    <div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
        </asp:SqlDataSource>

        <asp:GridView ID="EQGridView" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True" OnRowCommand="EQGridView_RowCommand" OnSelectedIndexChanged="EQGridView_SelectedIndexChanged" PageSize="20" CellPadding="3" HorizontalAlign="Center" OnPreRender="EQGridView_PreRender" OnRowCreated="EQGridView_RowCreated">
            <Columns>
                <asp:BoundField DataField="ORIGIN_DATE" HeaderText="發生時間" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"  />
                <asp:BoundField DataField="STATE" HeaderText="記錄類別" />
                <asp:BoundField DataField="POINT_NAME" HeaderText="測站名稱" />
                <asp:BoundField DataField="INTENSITY" HeaderText="震度" />
                <asp:BoundField DataField="X" HeaderText="X軸" />
                <asp:BoundField DataField="Y" HeaderText="Y軸" />
                <asp:BoundField DataField="Z" HeaderText="Z軸" />
                <asp:ButtonField ButtonType="Image" CommandName="GetDataFile" DataTextField="DATA_FILENAME" DataTextFormatString="{0}" HeaderText="波形檔案" ImageUrl="~/images/FileManager/txt.gif" Text="ボタン" Visible="False" />
                <asp:ButtonField ButtonType="Image" CommandName="GetTextFile" DataTextField="ANLZ_FILENAME" DataTextFormatString="{0}" HeaderText="文字檔案" ImageUrl="~/images/FileManager/txt.gif" Text="ボタン" Visible="False" />
                <asp:BoundField DataField="DATA_FILENAME" HeaderText="波形檔案" Visible="False" />
                <asp:BoundField DataField="ANLZ_FILENAME" HeaderText="文字檔案" Visible="False" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" 
                HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" ForeColor="White" Font-Bold="True" />
        </asp:GridView>

    </div>
    </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

