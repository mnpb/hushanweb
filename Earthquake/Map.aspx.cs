﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Earthquake_Map : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDataLabel();
        }
    }

    private string GetViewFileName(string strDate, int siteid, int pointno)
    {
        string fname = "";
        DateTime dt;

        if (strDate.IndexOf("--") < 0)
        {
            dt = Convert.ToDateTime(strDate);
            //時刻はローカルタイム+8になっているのでUTCに戻す
            dt = dt.AddHours(-8);
            pointno--; //ファイル名につけられるポイントNOは0から始まる
            fname = dt.ToString("yyyyMMddHHmmss") + "_" + siteid.ToString("00") + "-" + pointno.ToString("00") + ".tpc.jpg";
        }

        return fname;
    }

    //データベースから各種ラベルに値を入れる
    private void SetDataLabel()
    {
        //int siteid, pointno;
        //int lv = 0;
        int blink = 0;

        var intensity1 = EarthquakeController.Instance.GetStateIntensity(1);
        if (intensity1 != null && !intensity1.BLINK.Value.Equals(0))
        {
            blink = 1;
        }
        var intensity2 = EarthquakeController.Instance.GetStateIntensity(2);
        if (intensity2 != null && !intensity2.BLINK.Value.Equals(0))
        {
            blink = 1;
        }

        for (int pointID = 1; pointID <= 8; pointID++)
        {
            SetStatePoint(pointID, blink);
            SetPointInfo(pointID);
        }
    }

    private void SetStatePoint(int pointID, int blink)
    {
        string fieldInfo = "lblDate,NDATE;lblInt,INTENSITY;lblAccX,ACCX;lblAccY,ACCY;lblAccZ,ACCZ";
        var statePoint = EarthquakeController.Instance.GetStatePoint(pointID);
        if (statePoint != null)
        {
            foreach (var field in fieldInfo.Split(';').ToList())
            {
                try
                {
                    var control = ((ContentPlaceHolder)Page.Master
                        .FindControl("ContentPlaceHolder1") as ContentPlaceHolder)
                        .FindControl(field.Split(',')[0] + pointID) as Label;
                    if (control != null)
                    {
                        control.Text = statePoint.GetType().GetProperty(field.Split(',')[1]).GetValue(statePoint).ToString();
                    }
                }
                catch
                {
                }
            }

            try
            {
                ImageButton imgButton = ((ContentPlaceHolder)Page.Master
                            .FindControl("ContentPlaceHolder1") as ContentPlaceHolder)
                            .FindControl("imgMark" + pointID) as ImageButton;
                if (imgButton != null)
                {
                    imgButton.ImageUrl = GetMarkerUrl(statePoint.LV.Value, blink, statePoint.INTENSITY);
                }
            }
            catch
            {
            }
        }
    }
    private void SetPointInfo(int pointID)
    {
        string fieldInfo = "lblName,NAME";
        var pointInfo = EarthquakeController.Instance.GetPointInfo(pointID);
        if (pointInfo != null)
        {
            foreach (var field in fieldInfo.Split(';').ToList())
            {
                try
                {
                    var control = ((ContentPlaceHolder)Page.Master
                        .FindControl("ContentPlaceHolder1") as ContentPlaceHolder)
                        .FindControl(field.Split(',')[0] + pointID) as Label;
                    if (control != null)
                    {
                        control.Text = pointInfo.GetType().GetProperty(field.Split(',')[1]).GetValue(pointInfo).ToString();
                    }
                }
                catch
                {
                }
            }
        }

        try
        {
            var control = ((ContentPlaceHolder)Page.Master
                .FindControl("ContentPlaceHolder1") as ContentPlaceHolder)
                .FindControl("lblFile" + pointID) as Label;
            if (control != null)
            {
                var statePoint = EarthquakeController.Instance.GetStatePoint(pointID);
                control.Text = GetViewFileName(statePoint.NDATE, pointInfo.SITE_ID, pointInfo.NO.Value);
            }
        }
        catch
        {
        }
    }

    //震度に応じてマーカー色を取得
    private string GetMarkerUrl(int LV, int blink, string strInt)
    {
        string ret = "~/images/eqmap/mark_blue.gif";

        if (strInt.IndexOf("--") < 0)
        {
            if (LV == 0)
            {
                if (blink == 1)
                    ret = "~/images/eqmap/blink_green.gif";
                else
                    ret = "~/images/eqmap/mark_green.gif";
            }
            if (LV == 1)
            {
                if (blink == 1)
                    ret = "~/images/eqmap/blink_yellow.gif";
                else
                    ret = "~/images/eqmap/mark_yellow.gif";
            }
            if (LV == 2)
            {
                if (blink == 1)
                    ret = "~/images/eqmap/blink_red.gif";
                else
                    ret = "~/images/eqmap/mark_red.gif";
            }
        }

        return ret;
    }


    //該当ファイルを転送してURLを返す
    private string GetWaveImage(string ref_file)
    {
        //string ret = "";
        string ret = "~/images/eqmap/nodata.jpg";

        if (ref_file.Length > 10)
        {
            string sRootFrom = ConfigurationManager.AppSettings.Get("EarthquakeWaveRoot");
            string sRootTo = ConfigurationManager.AppSettings.Get("EarthquakeWebdataRoot");
            string fname = ref_file;
            string fyearmon = fname.Substring(0, 6); //年月を取得（フォルダ用）
            string fpath = "\\" + fyearmon + "\\" + fname;
            try
            {
                string sWaveFrom = sRootFrom + fpath;
                string sWaveTo = sRootTo + fpath;
                if (File.Exists(sWaveTo) != true)
                {
                    if (Directory.Exists(sRootTo + "\\" + fyearmon) != true)
                    {
                        Directory.CreateDirectory(sRootTo + "\\" + fyearmon);
                    }
                    File.Copy(sWaveFrom, sWaveTo, true); //webdataを書き込み許可にしておく事
                }
                ret = ConfigurationManager.AppSettings.Get("EarthquakeWebdataRootURL") + "/" + fyearmon + "/" + fname;
            }
            catch (IOException ei)
            {
                // FileNotFoundExceptions are handled here.
            }
        }
        return ret;
    }

    private void imgMark_AllHide()
    {
        imgWave1.Visible = false;
        imgWave2.Visible = false;
        imgWave3.Visible = false;
        imgWave4.Visible = false;
        imgWave5.Visible = false;
        imgWave6.Visible = false;
        imgWave7.Visible = false;
    }

    protected void imgMark1_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave1.Visible == true)
        {
            imgWave1.Visible = false;
        }
        else
        {
            imgWave1.ImageUrl = GetWaveImage(lblFile1.Text);
            imgMark_AllHide();
            imgWave1.Visible = true;
        }
    }

    protected void imgMark2_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave2.Visible == true)
        {
            imgWave2.Visible = false;
        }
        else
        {
            imgWave2.ImageUrl = GetWaveImage(lblFile2.Text);
            imgMark_AllHide();
            imgWave2.Visible = true;
        }
    }

    protected void imgMark3_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave3.Visible == true)
        {
            imgWave3.Visible = false;
        }
        else
        {
            imgWave3.ImageUrl = GetWaveImage(lblFile3.Text);
            imgMark_AllHide();
            imgWave3.Visible = true;
        }
    }

    protected void imgMark4_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave4.Visible == true)
        {
            imgWave4.Visible = false;
        }
        else
        {
            imgWave4.ImageUrl = GetWaveImage(lblFile4.Text);
            imgMark_AllHide();
            imgWave4.Visible = true;
        }
    }

    protected void imgMark5_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave5.Visible == true)
        {
            imgWave5.Visible = false;
        }
        else
        {
            imgWave5.ImageUrl = GetWaveImage(lblFile5.Text);
            imgMark_AllHide();
            imgWave5.Visible = true;
        }
    }

    protected void imgMark6_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave6.Visible == true)
        {
            imgWave6.Visible = false;
        }
        else
        {
            imgWave6.ImageUrl = GetWaveImage(lblFile6.Text);
            imgMark_AllHide();
            imgWave6.Visible = true;
        }
    }

    protected void imgMark7_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave7.Visible == true)
        {
            imgWave7.Visible = false;
        }
        else
        {
            imgWave7.ImageUrl = GetWaveImage(lblFile7.Text);
            imgMark_AllHide();
            imgWave7.Visible = true;
        }
    }

    protected void imgMark8_Click(object sender, ImageClickEventArgs e)
    {
        if (imgWave8.Visible == true)
        {
            imgWave8.Visible = false;
        }
        else
        {
            imgWave8.ImageUrl = GetWaveImage(lblFile8.Text);
            imgMark_AllHide();
            imgWave8.Visible = true;
        }
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        SetDataLabel();
    }
}