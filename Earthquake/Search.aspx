﻿<%@ Page Title="歷史地震資料查詢" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Earthquake_Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        body {
            background-color: #D9ECF8;
        }

        .auto-style1 {
            width: 330px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
    <br />
    <asp:Label ID="lblDesc" runat="server" Text="歷史地震資料查詢" Font-Bold="true" Font-Size="18px" ForeColor="DarkBlue"></asp:Label>
      <br />
    </center>

            &nbsp;<center>
    <div style="align-items:center; width: 979px; right: auto; left: auto;">
        <table border="0" height="15%" width="100%">
          <col span="1" width="20%" valign="middle" align="left">
          <col span="2" width="40%" valign="middle">
            <tr>
              <td align="left" class="auto-style1">
                  &nbsp;
                  <asp:CheckBox ID="CheckBox1" runat="server" Font-Size="Medium" Text="記錄時間" />
              </td>

              <td valign="middle" align="center">
              <asp:Label ID="Label1" runat="server" Text="Label" Font-Size="Medium">自&nbsp;</asp:Label>
                  <asp:DropDownList ID="Fyear" runat="server">
                  </asp:DropDownList>
                  <asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Medium">年&nbsp;</asp:Label>
                  <asp:DropDownList ID="Fmonth" runat="server">
                  </asp:DropDownList>
                  <asp:Label ID="Label3" runat="server" Text="Label" Font-Size="Medium">月&nbsp;</asp:Label>
                  <asp:DropDownList ID="Fday" runat="server">
                  </asp:DropDownList>
                  <asp:Label ID="Label4" runat="server" Text="Label" Font-Size="Medium">日&nbsp;</asp:Label>
              </td>

              <td valign="middle" align="center">
              <asp:Label ID="Label5" runat="server" Text="Label" Font-Size="Medium">到&nbsp;</asp:Label>
                  <asp:DropDownList ID="Tyear" runat="server">
                  </asp:DropDownList>
                  <asp:Label ID="Label6" runat="server" Text="Label" Font-Size="Medium">年&nbsp;</asp:Label>
                  <asp:DropDownList ID="Tmonth" runat="server">
                  </asp:DropDownList>
                  <asp:Label ID="Label7" runat="server" Text="Label" Font-Size="Medium">月&nbsp;</asp:Label>
                  <asp:DropDownList ID="Tday" runat="server">
                  </asp:DropDownList>
                  <asp:Label ID="Label8" runat="server" Text="Label" Font-Size="Medium">日&nbsp;</asp:Label>
              </td>
            </tr>

            <tr>
              <td align="left" class="auto-style1">
                  &nbsp;
                  <asp:CheckBox ID="CheckBox2" runat="server" Font-Size="Medium" Text="測站選擇" />
              </td>

              <td valign="middle" align="center" style="text-align: right">
                  <asp:DropDownList ID="lstPoint" runat="server" DataSourceID="SqlDataSource1" DataTextField="NAME" DataValueField="POINT_ID">
                  </asp:DropDownList>
              </td>

              <td valign="middle" align="center">
              </td>
            </tr>

            <tr>
              <td align="left" class="auto-style1">
                  &nbsp;
                  <asp:CheckBox ID="CheckBox3" runat="server" Font-Size="Medium" Text="最大加速度值" />
              </td>

              <td valign="middle" align="center" style="text-align: right">
                  <asp:TextBox ID="txtAcc" runat="server">0</asp:TextBox>
              </td>

              <td valign="middle" align="center" style="text-align: left">
                  <asp:Label ID="Label9" runat="server" Text="Label" Font-Size="Medium">最大加速度值以上範圍(GAL)</asp:Label>
              </td>
            </tr>

            <tr>
              <td align="left" class="auto-style1">
                  &nbsp;
                  <asp:CheckBox ID="CheckBox4" runat="server" Font-Size="Medium" Text="震度" />
              </td>

              <td valign="middle" align="center" style="text-align: right">
                  <asp:TextBox ID="txtInt" runat="server">0</asp:TextBox>
              </td>

              <td valign="middle" align="center" style="text-align: left">
                  <asp:Label ID="Label10" runat="server" Text="Label" Font-Size="Medium">震度以上範圍</asp:Label>
              </td>
            </tr>

            <tr>
              <td align="left" class="auto-style1">
                  &nbsp;
                  <asp:CheckBox ID="CheckBox5" runat="server" Font-Size="Medium" Text="記錄類別" />
              </td>

              <td valign="middle" align="center" style="text-align: right">
                  <asp:DropDownList ID="lstType" runat="server">
                  </asp:DropDownList>
              </td>

              <td valign="middle" align="center" style="text-align: left">
              </td>
            </tr>

        </table>
        <br />

        <center>
        &nbsp;<asp:Button ID="btnSearch" runat="server" Font-Bold="True" Font-Size="Large" PostBackUrl="~/Earthquake/Result.aspx" Text="查詢" />
        </center>

    </div>
    <a href="Software/pwave32.zip">DBL檔需要Pwave32軟體開啓、按此下載軟體</a><br>
    <a href="Software/wrar393.exe">ZIP檔需要WinRAR軟體開啓、按此下載軟體</a>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>" SelectCommand="SELECT POINT_ID, NAME FROM POINT ORDER BY POINT_ID"></asp:SqlDataSource>

    </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

