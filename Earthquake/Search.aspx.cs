﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Earthquake_Search : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitYear();
            InitMonth();
            InitDay(Convert.ToInt32(Fyear.SelectedItem.Text), Convert.ToInt32(Fmonth.SelectedItem.Text));
            select();
            InitType();
            txtAcc.Text = "0";
            txtInt.Text = "0";
        }
    }

    private void select()
    {
        string year = DateTime.Now.Year.ToString();
        string month = DateTime.Now.Month.ToString();
        string day = DateTime.Now.Day.ToString();

        //開始時間
        for (int i = 0; i < Fyear.Items.Count; i++)
        {

            if (Fyear.Items[i].Text.Equals(year))
            {
                Fyear.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < Fmonth.Items.Count; i++)
        {

            if (Fmonth.Items[i].Text.Equals(month))
            {
                Fmonth.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < Fday.Items.Count; i++)
        {

            if (Fday.Items[i].Text.Equals(day))
            {
                Fday.SelectedIndex = i;
                break;
            }
        }

        //結束時間
        for (int i = 0; i < Tyear.Items.Count; i++)
        {

            if (Tyear.Items[i].Text.Equals(year))
            {
                Tyear.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < Tmonth.Items.Count; i++)
        {

            if (Tmonth.Items[i].Text.Equals(month))
            {
                Tmonth.SelectedIndex = i;
                break;
            }
        }

        for (int i = 0; i < Tday.Items.Count; i++)
        {

            if (Tday.Items[i].Text.Equals(day))
            {
                Tday.SelectedIndex = i;
                break;
            }
        }

    }

    /// <summary>
    /// 初始年份
    /// </summary>
    private void InitYear()
    {
        Fyear.Items.Clear();
        Tyear.Items.Clear();
        int begYear = 1991;
        int endYear = DateTime.Today.Year;
        for (int year = begYear; year <= endYear; year++)
        {
            Fyear.Items.Add(year.ToString());
            Tyear.Items.Add(year.ToString());
        }
    }

    /// <summary>
    /// 初始月份
    /// </summary>
    private void InitMonth()
    {
        Fmonth.Items.Clear();
        Tmonth.Items.Clear();
        for (int month = 1; month <= 12; month++)
        {
            Fmonth.Items.Add(month.ToString());
            Tmonth.Items.Add(month.ToString());
        }
    }

    /// <summary>
    /// 初始日
    /// </summary>
    /// <param name="year"></param>
    /// <param name="month"></param>
    private void InitDay(int year, int month)
    {
        Fday.Items.Clear();
        Tday.Items.Clear();
        for (int day = 1; day <= DateTime.DaysInMonth(year, month); day++)
        {
            Fday.Items.Add(day.ToString());
            Tday.Items.Add(day.ToString());
        }
    }

    private void InitType()
    {
        lstType.Items.Clear();
        lstType.Items.Add(new ListItem("全部", ""));
        lstType.Items.Add(new ListItem("地震觸發", "AUTO"));
        //lstType.Items.Add(new ListItem("手動", "MANU"));
        lstType.Items.Add(new ListItem("系統測試", "CAL"));
    }
}