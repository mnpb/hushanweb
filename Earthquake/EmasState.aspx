﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmasState.aspx.cs" Inherits="Earthquake_EmasState" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        body {
            background-color: #D9ECF8;
        }

        .auto-styleint {
            background-color: black;
            height: 120px;
            width: 120px;
            position: relative;
        }

        .auto-styleint-val {
            position: absolute;
            top: 40px;
            left: 0px;
            color: green;
            font-size: 110px;
            width: 120px;
            text-align: center;
        }

        table {
            margin: 0 0 0 1px;
            padding: 0;
            border: 0;
            border-spacing: 0;
            border-collapse: collapse;
        }

        th {
            font: bold 12px;
            color: white;
            border: 1px solid black;
            letter-spacing: 2px;
            text-transform: uppercase;
            text-align: center;
            padding: 6px;
            background-color: darkblue;
            table-layout: fixed;
        }

        td {
            font: 12px;
            border: 1px solid black;
            background-color: #D9ECF8;
            padding: 6px;
            color: black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
            <asp:Label ID="lblDesc" runat="server" Text="地震監測事件資訊" Font-Bold="true" Font-Size="18px" ForeColor="DarkBlue"></asp:Label>
            <br />
            </center>
    &nbsp;
    <center>
    <asp:Timer ID="Timer1" runat="server" Interval="10000" OnTick="Timer1_Tick">
    </asp:Timer>
    <div style="position: relative; width:1000px; height:528px; margin-top: 0px;">
        <%--
        <asp:Image ID="imgEMAScap" runat="server" Height="576px" ImageUrl="<%$ appSettings:emasstateImageURL %>" Width="965px" />
        --%>
        <div style="position:absolute; left:5px; height: 193px; width: 199px; top: 4px;">
            <asp:Label ID="lblIntCap1" runat="server" Text="最大震度（自由場）" Font-Bold="true" Font-Size="16px" ForeColor="DarkBlue"></asp:Label><br />
            <div class="auto-styleint">
                <div class="auto-styleint-val">
                    <asp:Label ID="lblIntensity1" runat="server" Text="0"></asp:Label>
                </div>
            </div>
            <div style="text-align:left; padding-left:40px">
                X：南北向<br/>
                Y：東西向<br/>
                Z：垂直向<br/>
            </div>
        </div>

        <div style="position:absolute; left:5px; height: 192px; width: 199px; top: 205px;">
            <asp:Label ID="lblIntCap2" runat="server" Text="最大震度（結構物）" Font-Bold="true" Font-Size="16px" ForeColor="DarkBlue"></asp:Label><br />
            <div class="auto-styleint">
                <div class="auto-styleint-val">
                    <asp:Label ID="lblIntensity2" runat="server" Text="0"></asp:Label>
                </div>
            </div>
            <div style="text-align:left; padding-left:40px">
                X：平行壩軸<br/>
                Y：垂直壩軸<br/>
                Z：垂直向<br/>
            </div>
        </div>

        <div style="position:absolute; left:212px; height: 219px; width: 779px; top: 6px; text-align:left;">
        <table>
            <tr>
                <th width="100">測站名稱</th>
                <th width="140" colspan="3">主壩壩頂(SMS1D1)</th>
                <th width="140" colspan="3">主壩下游坡面(SMS2D1)</th>
                <th width="140" colspan="3">主壩壩頂自由場(SMS3D1)</th>
                <th width="140" colspan="3">主壩壩底自由場(SMS4D1)</th>
            </>
            <tr>
                <td>地震觸發時間</td>
                <td colspan="3">
                    <asp:Label ID="lblDate1" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblDate2" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblDate3" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblDate4" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>震度</td>
                <td colspan="3">
                    <asp:Label ID="lblInt1" runat="server" Text="---"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblInt2" runat="server" Text="---"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblInt3" runat="server" Text="---"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblInt4" runat="server" Text="---"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>通訊状態</td>
                <td colspan="12">
                    <asp:Label ID="lblUnit1" runat="server" Text="-"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>軸向</td>
                <td>X</td><td>Y</td><td>Z</td>
                <td>X</td><td>Y</td><td>Z</td>
                <td>X</td><td>Y</td><td>Z</td>
                <td>X</td><td>Y</td><td>Z</td>
            </tr>
            <tr>
                <td>最大加速度(gal)</td>
                <td>
                    <asp:Label ID="lblAccX1" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY1" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ1" runat="server" Text="---"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="lblAccX2" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY2" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ2" runat="server" Text="---"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="lblAccX3" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY3" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ3" runat="server" Text="---"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="lblAccX4" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY4" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ4" runat="server" Text="---"></asp:Label>
                </td>

            </tr>
        </table>
        </div>

        <div style="position:absolute; left:212px; height: 219px; width: 779px; top:233px; text-align:left;">
        <table>
            <tr>
                <th width="100">測站名稱</th>
                <th width="140" colspan="3">副壩壩頂(SMS1D2)</th>
                <th width="140" colspan="3">湖南壩壩頂(SMS1D3)</th>
                <th width="140" colspan="3">湖南壩頂自由場(SMS2D3)</th>
		<th width="140" colspan="3">桶頭堰(SMS5D1)</th>
            </>
            <tr>
                <td>地震觸發時間</td>
                <td colspan="3">
                    <asp:Label ID="lblDate5" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblDate6" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblDate7" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
		 <td colspan="3">
                    <asp:Label ID="lblDate8" runat="server" Text="----/--/-- --:--:--"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>震度</td>
                <td colspan="3">
                    <asp:Label ID="lblInt5" runat="server" Text="---"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblInt6" runat="server" Text="---"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblInt7" runat="server" Text="---"></asp:Label>
                </td>
 		<td colspan="3">
                    <asp:Label ID="lblInt8" runat="server" Text="---"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>通訊状態</td>
                <td colspan="3">
                    <asp:Label ID="lblUnit2" runat="server" Text="-"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblUnit3" runat="server" Text="-"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="lblUnit4" runat="server" Text="-"></asp:Label>
                </td>
   		<td colspan="3">
                    <asp:Label ID="lblUnit5" runat="server" Text="-"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>軸向</td>
                <td>X</td><td>Y</td><td>Z</td>
                <td>X</td><td>Y</td><td>Z</td>
                <td>X</td><td>Y</td><td>Z</td>
		<td>X</td><td>Y</td><td>Z</td>
            </tr>
            <tr>
                <td>最大加速度(gal)</td>
                <td>
                    <asp:Label ID="lblAccX5" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY5" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ5" runat="server" Text="---"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="lblAccX6" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY6" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ6" runat="server" Text="---"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="lblAccX7" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY7" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ7" runat="server" Text="---"></asp:Label>
                </td>

		        <td>
                    <asp:Label ID="lblAccX8" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccY8" runat="server" Text="---"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAccZ8" runat="server" Text="---"></asp:Label>
                </td>
            </tr>
        </table>
        </div>

        <div style="position:absolute; left:30px; height: 102px; width: 163px; top:412px; text-align:left;">
            3級：8～25gal<br />
            4級：25～80gal<br />
            5級：80～250gal<br />
            6級：250～400gal<br />
            7級：400gal以上<br />
        </div>
        <div style="position:absolute; left:950px; height: 155px; width: 196px; top:241px; text-align:left;">
            1. 通訊狀態”NG”表示<br />
            &nbsp&nbsp&nbsp   網路斷訊或記錄主機異常。<br />
            2. 觸發時間及最大加速度<br />
            &nbsp&nbsp&nbsp   顯示”---“表示記錄<br />
            &nbsp&nbsp&nbsp   主機未觸發。<br />
            3. 最大加速度顯示”-999”<br />
            &nbsp&nbsp&nbsp   表示感震計異常。<br />
        </div>
    </div>
    </center>
</asp:Content>

