﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResultPrint.aspx.cs" Inherits="Earthquake_ResultPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        body {
            background-color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <center>
    <BR>
    <asp:Label ID="lblDesc" runat="server" Text="查詢結果" Font-Bold="true" Font-Size="18px" ForeColor="DarkBlue"></asp:Label>
    <BR>
    </center>

    &nbsp;<center>
    <div class="hdisplay">

        <br />
&nbsp;&nbsp;
        &nbsp;&nbsp;
        <asp:Button ID="btnPrint" runat="server" Font-Size="Small" OnClick="btnPrint_Click" OnClientClick="window.print();" Text="列印" />
&nbsp;&nbsp;
        &nbsp;&nbsp;
        
    </div>
    <div style="font-size:18px">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:earthqEMAS %>">
        </asp:SqlDataSource>

        <asp:GridView ID="EQGridView" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True" CellPadding="3" HorizontalAlign="Center" OnRowCreated="EQGridView_RowCreated" OnPageIndexChanging="EQGridView_PageIndexChanging" PageSize="9999" ShowFooter="True">
            <Columns>
                <asp:BoundField DataField="DATA_FILENAME" HeaderText="ref波形檔案" />
                <%-- <asp:BoundField DataField="ANLZ_FILENAME" HeaderText="ref文字檔案" /> --%>
                <asp:BoundField DataField="REC_POINT_ID" HeaderText="recpointID" />
                <asp:BoundField DataField="ORIGIN_DATE" HeaderText="發生時間" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"  />
                <asp:BoundField DataField="STATE" HeaderText="記錄類別" />
                <asp:BoundField DataField="POINT_NAME" HeaderText="測站名稱" />
                <asp:BoundField DataField="INTENSITY" HeaderText="震度" />
                <asp:BoundField DataField="X" HeaderText="X軸" />
                <asp:BoundField DataField="Y" HeaderText="Y軸" />
                <asp:BoundField DataField="Z" HeaderText="Z軸" />
                <asp:ButtonField ButtonType="Image" CommandName="GetDataFile" HeaderText="波形檔案" ImageUrl="~/images/FileManager/txt.gif" Text="ボタン" Visible="False" />
                <asp:ButtonField ButtonType="Image" CommandName="GetTextFile" HeaderText="文字檔案" ImageUrl="~/images/FileManager/txt.gif" Text="ボタン" Visible="False" />
                <asp:ButtonField ButtonType="Image" CommandName="GetPicFile" HeaderText="波形圖" ImageUrl="~/images/FileManager/jpg.gif" Visible="False" />
                <asp:TemplateField HeaderText="選取" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="SelectCheckBox" CheckedChanged="chkChanged" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" 
                HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" ForeColor="White" Font-Bold="True" />
        </asp:GridView>

    </div>
    </center>

    &nbsp;<p>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </p>
    </div>
    </form>
</body>
</html>
