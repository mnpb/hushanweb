﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Earthquake_EmasState : SuperPage
{
    string blinkstate = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        try
        {
            string sImageFrom = ConfigurationManager.AppSettings["emasstateImageFrom"];
            string sImageTo = ConfigurationManager.AppSettings["emasstateImageTo"];
            File.Copy(sImageFrom, sImageTo, true); //webdataを書き込み許可にしておく事
        }
        catch (IOException ei)
        {
            // FileNotFoundExceptions are handled here.
        }
        */
        if (!IsPostBack)
        {
            SetDataLabel();
        }

        //ラベル点滅用フラグ
        if (ViewState["blinkstate"] != null)
        {
            blinkstate = ViewState["blinkstate"].ToString();
            if (blinkstate == "1")
            {
                blinkstate = "0";
            }
            else
            {
                blinkstate = "1";
            }
        }
        //ViewStateにパラメータ値を保持
        ViewState.Add("blinkstate", blinkstate);
    }

    //データベースから各種ラベルに値を入れる
    private void SetDataLabel()
    {
        SetStateUnit(1, lblUnit1);
        SetStateUnit(2, lblUnit2);
        SetStateUnit(3, lblUnit3);
        SetStateUnit(4, lblUnit4);

        for (int pointID = 1; pointID <= 8; pointID++)
        {
            SetStatePoint(pointID);
        }

        SetStateIntensity(1);
        SetStateIntensity(2);
    }

    private void SetStateUnit(int siteID, Label label)
    {
        int connection = 0;
        var unit = EarthquakeController.Instance.GetStateUnit(siteID);
        if (unit != null)
        {
            connection = unit.CONNECTION.Value;
        }

        if (connection.Equals(0))
        {
            label.Text = "[NG]";
            label.ForeColor = Color.Red;
        }
        else
        {
            label.Text = "[OK]";
            label.ForeColor = Color.Green;
        }
    }

    private void SetStatePoint(int pointID)
    {
        string fieldInfo = "lblDate,NDATE;lblInt,INTENSITY;lblAccX,ACCX;lblAccY,ACCY;lblAccZ,ACCZ";
        var statePoint = EarthquakeController.Instance.GetStatePoint(pointID);
        if (statePoint != null)
        {
            foreach (var field in fieldInfo.Split(';').ToList())
            {
                try
                {
                    var control = ((ContentPlaceHolder)Page.Master
                        .FindControl("ContentPlaceHolder1") as ContentPlaceHolder)
                        .FindControl(field.Split(',')[0] + pointID) as Label;
                    if (control != null)
                    {
                        control.Text = statePoint.GetType().GetProperty(field.Split(',')[1]).GetValue(statePoint).ToString();
                    }
                }
                catch
                {
                }
            }
        }
    }

    private void SetStateIntensity(int intensityID)
    {
        var intensity = EarthquakeController.Instance.GetStateIntensity(intensityID);
        var control = ((ContentPlaceHolder)Page.Master
            .FindControl("ContentPlaceHolder1") as ContentPlaceHolder)
            .FindControl("lblIntensity" + intensityID) as Label;
        if (control != null)
        {
            try
            {
                control.Text = intensity.INTENSITY.Value.ToString();
                var lv = intensity.LV.Value;
                if (lv.Equals(1))
                {
                    control.ForeColor = Color.Yellow;
                }
                else if (lv.Equals(2))
                {
                    control.ForeColor = Color.Red;
                }
                else
                {
                    control.ForeColor = Color.Green;
                }
                if (!intensity.BLINK.Value.Equals(0))
                {
                    if (blinkstate.Equals("1"))
                    {
                        control.ForeColor = Color.FromArgb(30, 30, 30);
                    }
                }
            }
            catch
            {
            }
        }
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        SetDataLabel();
    }
}