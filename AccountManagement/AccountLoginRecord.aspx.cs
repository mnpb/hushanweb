﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AccountManagement_AccountLoginRecord : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoginStartDateTimeTextBox.Text = string.Format("{0:yyyy/MM/dd}", DateTime.Now.AddDays(-7));
            LoginEndDateTimeTextBox.Text = string.Format("{0:yyyy/MM/dd}", DateTime.Now);
        }
    }

    protected void AccountLoginGridView_PreRender(object sender, EventArgs e)
    {
        GridView GridView = (GridView)sender;
        SaveToExcelButton.Visible = GridView.Rows.Count > 0;
    }

    protected void AccountLoginGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView GridView = (GridView)sender;

        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            byte Aaction = (byte)DataBinder.Eval(e.Row.DataItem, "Aaction");

            Literal AactionLiteral = (Literal)e.Row.FindControl("AactionLiteral");

            AactionLiteral.Text = Aaction == 1 ? "登入" : "登出";
        }
    }

    protected void QueryButton_Click(object sender, EventArgs e)
    {
        string ErrorMessage = string.Empty;
        DateTime LoginStartDateTime = DateTime.Now;
        DateTime LoginEndDateTime = DateTime.Now;

        SqlDataSource1.SelectParameters["LoginStartDateTime"].DefaultValue = string.Format("{0} 00:00:00", LoginStartDateTimeTextBox.Text);
        SqlDataSource1.SelectParameters["LoginEndDateTime"].DefaultValue = string.Format("{0} 23:59:59", LoginEndDateTimeTextBox.Text);

        if (string.IsNullOrEmpty(ActionDropDownList.SelectedValue))
            SqlDataSource1.SelectParameters["Action"].Type = TypeCode.DBNull;
        else
        {
            SqlDataSource1.SelectParameters["Action"].Type = TypeCode.Byte;
            SqlDataSource1.SelectParameters["Action"].DefaultValue = ActionDropDownList.SelectedValue;
        }

        if (string.IsNullOrEmpty(LoginStartDateTimeTextBox.Text))
            ErrorMessage += "請輸入起始日期\\n";
        else if (!DateTime.TryParse(LoginStartDateTimeTextBox.Text, out LoginStartDateTime))
            ErrorMessage += "起始日期格式不正確\\n";        

        if (string.IsNullOrEmpty(LoginEndDateTimeTextBox.Text))
            ErrorMessage += "請輸入結束日期\\n";
        else if (!DateTime.TryParse(LoginEndDateTimeTextBox.Text, out LoginEndDateTime))
            ErrorMessage += "結束日期格式不正確\\n";

        if (string.IsNullOrEmpty(ErrorMessage))
        {
            AccountLoginGridView.Visible = true;
            SaveToExcelButton.Visible = true;
            AccountLoginGridView.DataSourceID = "SqlDataSource1";
        }
        else
            ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('" + ErrorMessage + "')</SCRIPT>");        
    }

    protected void SaveToExcelButton_Click(object sender, EventArgs e)
    {
        Button Button = (Button)sender;

        Response.ClearContent();
        Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>");
        string ExcelFileName = "登入記錄查詢.xls";
        Response.AddHeader("content-disposition", "attachment;filename=" + Server.UrlEncode(ExcelFileName));
        Response.ContentType = "application/excel";
        System.IO.StringWriter StringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter HtmlWrite = new HtmlTextWriter(StringWrite);

        AccountLoginGridView.AllowPaging = false;
        AccountLoginGridView.DataBind();

        AccountLoginGridView.RenderControl(HtmlWrite);
        Response.Write(StringWrite.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // '處理'GridView' 的控制項 'GridView' 必須置於有 runat=server 的表單標記之中   
    }
}