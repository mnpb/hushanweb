﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccountLoginRecord.aspx.cs" Inherits="AccountManagement_AccountLoginRecord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding: 5px; background: #e0dbdb; margin-bottom: 5px; text-align: left">
        <span style="margin-right: 15px; margin-left: 15px">請選擇登入系統的日期範圍：</span>
        <span style="margin-right: 15px">起始日期：         
            <asp:TextBox ID="LoginStartDateTimeTextBox" runat="server" Width="80px"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="LoginStartDateTimeTextBox_CalendarExtender" runat="server" Enabled="True" TargetControlID="LoginStartDateTimeTextBox" DaysModeTitleFormat="yyyy年MMMM" Format="yyyy/MM/dd" TodaysDateFormat="yyyy年MMMMd日">
            </ajaxToolkit:CalendarExtender>
        </span>
        <span style="margin-right: 15px">結束日期：         
            <asp:TextBox ID="LoginEndDateTimeTextBox" runat="server" Width="80px"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="LoginEndDateTimeTextBox_CalendarExtender" runat="server" Enabled="True" TargetControlID="LoginEndDateTimeTextBox" DaysModeTitleFormat="yyyy年MMMM" Format="yyyy/MM/dd" TodaysDateFormat="yyyy年MMMMd日">
            </ajaxToolkit:CalendarExtender>
        </span>
        <span style="margin-right: 15px">動作：
            <asp:DropDownList ID="ActionDropDownList" runat="server">
                <asp:ListItem Value="">全部</asp:ListItem>
                <asp:ListItem Value="1">登入</asp:ListItem>
                <asp:ListItem Value="2">登出</asp:ListItem>
            </asp:DropDownList>
        </span>
        <span style="margin-right: 15px">
            <asp:Button ID="QueryButton" runat="server" Text="查詢" OnClick="QueryButton_Click" />
        </span>
        <span style="float: right">
            <asp:Button ID="SaveToExcelButton" runat="server" Text="另存Excel" OnClick="SaveToExcelButton_Click" Visible="false" />
        </span>
    </div>
    <center>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <span>
                <asp:GridView ID="AccountLoginGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="fancytable" DataKeyNames="LoginID" OnPreRender="AccountLoginGridView_PreRender" OnRowDataBound="AccountLoginGridView_RowDataBound" PageSize="15" Visible="false" Width="800px">
                    <Columns>
                        <asp:BoundField DataField="LoginName" HeaderText="登入帳號" SortExpression="LoginName">
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="LoginIP" HeaderText="IP位置" SortExpression="LoginIP">
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ActionDateTime" DataFormatString="{0: yyyy/MM/dd HH:mm:ss}" HeaderText="時間" HtmlEncode="False" HtmlEncodeFormatString="False" SortExpression="ActionDateTime">
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="動作" SortExpression="Aaction">
                            <ItemTemplate>
                                <asp:Literal ID="AactionLiteral" runat="server" Text='<%# Bind("Aaction") %>'></asp:Literal>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="Gainsboro" />
                    <EmptyDataTemplate>
                        查無任何資料！
                    </EmptyDataTemplate>
                    <PagerStyle CssClass="cleartable" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
                </span>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HushanConnectionString %>" SelectCommand="SELECT B.LoginName, A.* FROM [AccountLogin] A INNER JOIN
                 [Account] B ON A.[AccountID] = B.[AccountID]
WHERE A.[ActionDateTime] BETWEEN @LoginStartDateTime AND @LoginEndDateTime
   AND ( A.[Aaction] = @Action OR @Action IS NULL)
ORDER BY A.[ActionDateTime] DESC">
        <SelectParameters>
            <asp:Parameter Name="LoginStartDateTime" />
            <asp:Parameter Name="LoginEndDateTime" />
            <asp:Parameter Name="Action" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

