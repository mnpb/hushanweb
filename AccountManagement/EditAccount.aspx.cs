﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AccountManagement_EditAccount : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void AccountFormView_DataBound(object sender, EventArgs e)
    {
        FormView FormView = (FormView)sender;

        if (FormView.CurrentMode == FormViewMode.Edit)
        {
            string Password = (string)DataBinder.Eval(FormView.DataItem, "Password");
            bool IsActive = (bool)DataBinder.Eval(FormView.DataItem, "IsActive");
            List<AccountToGroup> AccountToGroupList = (List<AccountToGroup>)DataBinder.Eval(FormView.DataItem, "AccountToGroupList");

            TextBox PasswordTextBox = (TextBox)FormView.FindControl("PasswordTextBox");
            Label IsActiveLabel1 = (Label)FormView.FindControl("IsActiveLabel1");
            Label GroupLabel = (Label)FormView.FindControl("GroupLabel");

            PasswordTextBox.Attributes["value"] = Password;

            IsActiveLabel1.Text = IsActive ? "是" : "否";

            using (Group Group = new Group())
            {
                List<Group> GroupList = Group.GetGroupList(AccountToGroupList);

                foreach (Group ThisGroup in GroupList)
                    GroupLabel.Text += string.Format("{0}、", ThisGroup.Name);

                if (GroupLabel.Text.Length > 0)
                    GroupLabel.Text = GroupLabel.Text.Remove(GroupLabel.Text.LastIndexOf("、"), 1);
            }
        }
    }

    protected void AccountFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        string ErrorMessage = string.Empty;

        FormView FormView = (FormView)sender;

        TextBox NameTextBox = (TextBox)FormView.FindControl("NameTextBox");     

        if (string.IsNullOrEmpty(NameTextBox.Text))
            ErrorMessage += "請填入使用者姓名\\n";

        e.Cancel = !string.IsNullOrEmpty(ErrorMessage);

        if (e.Cancel)
            ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<SCRIPT LANGUAGE=\"JavaScript\">alert('" + ErrorMessage + "');</SCRIPT>");

        e.NewValues["ModifiedDateTime"] = DateTime.Now;
    }

    protected void AccountFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        FormView FormView = (FormView)sender;

        ScriptManager.RegisterClientScriptBlock(FormView, FormView.GetType(), "", "alert('更新成功');", true);
    }
}