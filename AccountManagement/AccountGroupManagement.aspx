﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccountGroupManagement.aspx.cs" Inherits="AccountManagement_AccountGroupManagement" %>

<%@ Register Src="../WebUserControlers/AccountManagement/GroupManagement.ascx" TagName="GroupManagement" TagPrefix="uc1" %>
<%@ Register Src="../WebUserControlers/AccountManagement/GroupEdit.ascx" TagName="GroupEdit" TagPrefix="uc2" %>
<%@ Register Src="../WebUserControlers/AccountManagement/AccountList.ascx" TagName="AccountList" TagPrefix="uc3" %>
<%@ Register Src="../WebUserControlers/AccountManagement/AccountEdit.ascx" TagName="AccountEdit" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/AjaxTab/ajaxtab.css" rel="stylesheet" />

    <link href="../css/jquery.treeview/jquery.treeview.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.treeview/lib/jquery.cookie.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.treeview/jquery.treeview.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <ajaxToolkit:TabContainer ID="AccountGroupTabContainer" runat="server" ActiveTabIndex="0" Width="100%" Height="430px"
            CssClass="ajax__tab_technorati-theme" ScrollBars="Auto" AutoPostBack="True" 
            OnActiveTabChanged="AccountGroupTabContainer_ActiveTabChanged">
            <ajaxToolkit:TabPanel ID="GroupTabPanel" runat="server" HeaderText="瀏覽所有群組" OnDemandMode="Always">
                <ContentTemplate>
                    <uc1:GroupManagement ID="GroupManagement1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="AccountTabPanel" runat="server" HeaderText="瀏覽所有人員" OnDemandMode="Always">
                <ContentTemplate>
                    <uc3:AccountList ID="AccountList1" runat="server" Visible="false" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="AddGroupTabPanel" runat="server" HeaderText="新增群組" OnDemandMode="Always">
                <ContentTemplate>
                    <uc2:GroupEdit ID="GroupEdit1" runat="server" Visible="False" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="AddAccountTabPanel" runat="server" HeaderText="新增人員" OnDemandMode="Always">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <uc4:AccountEdit ID="AccountEdit1" runat="server" Visible="False" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
    </center>
</asp:Content>

