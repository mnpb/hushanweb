﻿using AjaxControlToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class AccountManagement_AccountGroupManagement : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void AccountGroupTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        TabContainer TabContainer = (TabContainer)sender;

        switch (TabContainer.ActiveTabIndex)
        {
            case 0:
                GroupManagement1.Visible = true;
                AccountList1.Visible = false;
                GroupEdit1.Visible = false;
                AccountEdit1.Visible = false;
                break;

            case 1:
                GroupManagement1.Visible = false;
                AccountList1.Visible = true;
                GroupEdit1.Visible = false;
                AccountEdit1.Visible = false;
                break;

            case 2:
                GroupManagement1.Visible = false;
                AccountList1.Visible = false;                
                GroupEdit1.Visible = true;
                AccountEdit1.Visible = false;
                break;

            case 3:
                GroupManagement1.Visible = false;
                AccountList1.Visible = false;                
                GroupEdit1.Visible = false;
                AccountEdit1.Visible = true;
                break;
        }

    }
}