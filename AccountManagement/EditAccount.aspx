﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditAccount.aspx.cs" Inherits="AccountManagement_EditAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:FormView ID="AccountFormView" runat="server" DataSourceID="ObjectDataSource1" DefaultMode="Edit" DataKeyNames="AccountID" RenderOuterTable="False" OnDataBound="AccountFormView_DataBound" OnItemUpdated="AccountFormView_ItemUpdated" OnItemUpdating="AccountFormView_ItemUpdating">
                    <EditItemTemplate>
                        <table class="fancytable" style="width: 450px">
                            <tr>
                                <th style="text-align: right; width: 30%">帳號名稱</th>
                                <td style="text-align: left">
                                    <asp:Label ID="LoginNameLabel" runat="server" Text='<%# Bind("LoginName") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">使用者姓名</th>
                                <td style="text-align: left">
                                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' Width="150px" MaxLength="15" />
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">E-mail</th>
                                <td style="text-align: left">
                                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' Width="300px" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">是否啟用</th>
                                <td style="text-align: left">
                                    <asp:Label ID="IsActiveLabel1" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">指定所屬群組</th>
                                <td style="text-align: left">
                                    <asp:Label ID="GroupLabel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">單位名稱</th>
                                <td style="text-align: left">
                                    <asp:TextBox ID="UnitTextBox" runat="server" Text='<%# Bind("Unit") %>' Width="200px" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">聯絡電話</th>
                                <td style="text-align: left">
                                    <asp:TextBox ID="PhoneNoTextBox" runat="server" Text='<%# Bind("PhoneNo") %>' Width="150px" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: right">地址</th>
                                <td style="text-align: left">
                                    <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' Width="300px" MaxLength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="確認修改" />
                                </td>
                            </tr>
                        </table>
                        <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' Visible="false" />
                        <asp:TextBox ID="LoginNameTextBox" runat="server" Text='<%# Bind("LoginName") %>' Visible="false" />
                        <asp:CheckBox ID="IsActiveCheckBox" runat="server" Checked='<%# Bind("IsActive") %>' Visible="false" />
                        <asp:CheckBox ID="IsEnableCheckBox" runat="server" Checked='<%# Bind("IsEnable") %>' Visible="false" />
                        <asp:TextBox ID="CreatedDateTimeTextBox" runat="server" Text='<%# Bind("CreatedDateTime") %>' Visible="false" />
                    </EditItemTemplate>
                </asp:FormView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAccount" TypeName="Account" UpdateMethod="ModifyAccount">
        <SelectParameters>
            <asp:SessionParameter Name="AccountID" SessionField="AccountID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="AccountID" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="LoginName" Type="String" />
            <asp:Parameter Name="Password" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="IsActive" Type="Boolean" />
            <asp:Parameter Name="IsEnable" Type="Boolean" />
            <asp:Parameter Name="Unit" Type="String" />
            <asp:Parameter Name="PhoneNo" Type="String" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="CreatedDateTime" Type="DateTime" />
            <asp:Parameter Name="ModifiedDateTime" Type="DateTime" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</asp:Content>

