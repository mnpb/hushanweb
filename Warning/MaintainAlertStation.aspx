﻿<%@ Page Title="警報站警戒設定" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainAlertStation.aspx.cs" Inherits="Warning_MaintainAlertStation" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Warning/UserControls/MaintainAlertStation.ascx" TagPrefix="uc1" TagName="MaintainAlertStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainAlertStation runat="server" ID="MaintainAlertStationUserControl" OnCompleted="MaintainAlertStationUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增警戒站" CssClass="buttonStyle" />
            <br />
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="StationID" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing"
                OnRowDeleting="StationBaseGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此警戒站?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站碼" Visible="false">
                        <ItemTemplate><%# Eval("StationID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站名">
                        <ItemTemplate><%# Eval("StationName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AC異常">
                        <ItemTemplate><%# GetActiveDescription(Eval("ACError").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AC正常">
                        <ItemTemplate><%# GetActiveDescription(Eval("ACNormal").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DC異常">
                        <ItemTemplate><%# GetActiveDescription(Eval("DCError").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DC正常">
                        <ItemTemplate><%# GetActiveDescription(Eval("DCNormal").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="門禁開啟">
                        <ItemTemplate><%# GetActiveDescription(Eval("DoorOpen").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="門禁關閉">
                        <ItemTemplate><%# GetActiveDescription(Eval("DoorClose").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="擴大機異常">
                        <ItemTemplate><%# GetActiveDescription(Eval("AmplifierError").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="擴大機正常">
                        <ItemTemplate><%# GetActiveDescription(Eval("AmplifierNormal").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="喇叭異常">
                        <ItemTemplate><%# GetActiveDescription(Eval("TrumpetError").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="喇叭正常">
                        <ItemTemplate><%# GetActiveDescription(Eval("TrumpetNormal").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="啟動與否?">
                        <ItemTemplate><%# GetActiveDescription(Eval("Active").ToString()) %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

