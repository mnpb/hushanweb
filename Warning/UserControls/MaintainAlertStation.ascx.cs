﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warning_UserControls_MaintainAlertStation : System.Web.UI.UserControl
{
    #region Properties

    private string stationID;
    public string StationID
    {
        get
        {
            return stationID;
        }
        set
        {
            stationID = value;
            ShowInfo(AlertStation);
        }
    }

    private dynamic AlertStation
    {
        get
        {
            return (from info in WarningController.Instance.GetAllAlertStations()
                    where info.StationID == StationID
                    select info).FirstOrDefault();
        }
    }

    List<string> MainControlNames
    {
        get
        {
            return new List<string> { "ACError", "ACNormal", "DCError", "DCNormal", "DoorOpen", "DoorClose", "AmplifierError", "AmplifierNormal", "TrumpetError", "TrumpetNormal" };
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillStations(WarningController.Instance.GetAllStations());
        }
    }

    private void EmptyAllControls()
    {
        ControlUtility.EmptyAllControls(this.Controls);

        MessageLabel.Text = string.Empty;
        MessageLabel.Visible = false;
    }

    private void FillStations(dynamic stations)
    {
        ControlUtility.FillData(StationDropDownList, stations, "StationName", "StationID");
    }

    /// <summary>
    /// 顯示資料
    /// </summary>
    /// <param name="alertStations"></param>
    private void ShowInfo(dynamic alertStation)
    {
        if (alertStation == null)
        { // 新增
            EmptyAllControls();
            FillNotYetSetStations();
            return;
        }

        FillStations(WarningController.Instance.GetAllStations());
        try
        {
            StationDropDownList.SelectedIndex = StationDropDownList.Items.IndexOf(StationDropDownList.Items.FindByValue(StationID));
            StationDropDownList.Enabled = false;

            foreach (var propertyName in MainControlNames)
            {
                var control = this.FindControl(propertyName + "CheckBox");
                (control as CheckBox).Checked = 
                    Convert.ToBoolean(alertStation.GetType().GetProperty(propertyName).GetValue(alertStation).ToString());
            }
            ActiveCheckBox.Checked = alertStation.Active;

            MessageLabel.Text = string.Empty;
            MessageLabel.Visible = false;
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        AlertWarningStation newStation = new AlertWarningStation
        {
            StationID = StationDropDownList.SelectedItem.Value,
            Active = ActiveCheckBox.Checked,
        };
        foreach (var propertyName in MainControlNames)
        {
            var control = this.FindControl(propertyName + "CheckBox");
            newStation.GetType().GetProperty(propertyName).SetValue(newStation, (control as CheckBox).Checked);
        }

        if (!WarningController.Instance.AddAlertStation(newStation))
        {
            MessageLabel.Text = "新增/更新失敗!";
            MessageLabel.Visible = true;
        }
        else
        { // 成功
            MessageLabel.Text = string.Empty;
            MessageLabel.Visible = false;
            if (Completed != null)
            {
                Completed(this, null);
            }
        }
    }

    /// <summary>
    /// 取得尚未設定的警戒站
    /// </summary>
    private void FillNotYetSetStations()
    {
        try
        {
            var allStations = WarningController.Instance.GetAllStations();
            var alreadySetStations = WarningController.Instance.GetAllAlertStations().Select(x => x.StationID).Distinct().ToList();
            allStations.RemoveAll(x => alreadySetStations.Contains(x.StationID));
            FillStations(allStations);
        }
        catch
        {
        }
    }

    #endregion
}