﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintainAlertStation.ascx.cs" Inherits="Warning_UserControls_MaintainAlertStation" %>

<link href="<%=ResolveUrl("~/css/fancytable.css") %>" rel="stylesheet" />
<%--<link href="<%=ResolveUrl("~/css/TextBoxStyle.css") %>" rel="stylesheet" />--%>
<link href="<%=ResolveUrl("~/css/SelectStyle.css") %>" rel="stylesheet" />
<link href="<%=ResolveUrl("~/css/ButtonStyle.css") %>" rel="stylesheet" />

<table class="fancytable">
    <tr>
        <th style="text-align: right">測站</th>
        <td style="text-align: left" colspan="5">
            <div class="selectStyle" style="width:200px">
                <asp:DropDownList ID="StationDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <th style="text-align: right; width:100px">AC異常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="ACErrorCheckBox" />
        </td>
        <th style="text-align: right; width:100px">AC正常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="ACNormalCheckBox" />
        </td>
    </tr>
    <tr>
        <th style="text-align: right">DC異常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="DCErrorCheckBox" />
        </td>
        <th style="text-align: right">DC正常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="DCNormalCheckBox" />
        </td>
    </tr>
    <tr>
        <th style="text-align: right">門禁開啟</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="DoorOpenCheckBox" />
        </td>
        <th style="text-align: right">門禁關閉</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="DoorCloseCheckBox" />
        </td>
    </tr>
    <tr>
        <th style="text-align: right">擴大機異常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="AmplifierErrorCheckBox" />
        </td>
        <th style="text-align: right">擴大機正常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="AmplifierNormalCheckBox" />
        </td>
    </tr>
    <tr>
        <th style="text-align: right">喇叭異常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="TrumpetErrorCheckBox" />
        </td>
        <th style="text-align: right">喇叭正常</th>
        <td style="text-align: left">
            <asp:CheckBox runat="server" ID="TrumpetNormalCheckBox" />
        </td>
    </tr>
    <tr>
        <th style="text-align: right">啟動與否?</th>
        <td style="text-align: left" colspan="3">
            <asp:CheckBox runat="server" ID="ActiveCheckBox" />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Label ID="MessageLabel" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="text-align: center" colspan="4">
            <asp:Button ID="UpdateButton" runat="server" Text="確定" CssClass="buttonStyle" OnClick="UpdateButton_Click" />
        </td>
    </tr>
</table>