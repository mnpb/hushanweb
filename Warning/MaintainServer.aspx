﻿<%@ Page Title="伺服器主機設備資料維護設定" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainServer.aspx.cs" Inherits="Warning_MaintainAlertStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input[type='number'] {
            border: 1px solid #cccccc;
            border-radius: 3px;
            height: 24px;
            line-height: 30px;
            padding-left: 14px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            margin-bottom: 10px;
            width: 60px;
        }

        .div-table {
            display: table;
            padding-left:50px;
        }

        .div-tr {
            display: table-row;
        }

        .div-td {
            display: table-cell;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3>伺服器主機警戒值設定</h3>
    <div class="div-table">
        <div class="div-tr">
            <div class="div-td">CPU溫度：</div>
            <div class="div-td">
                <input max="90" min="20" id="input_cputemp" value="20" runat="server" />  ℃
            </div>
        </div>
        <div class="div-tr">
            <div class="div-td">記憶體使用率：</div>
            <div class="div-td">
                <input max="90" min="20" id="input_memusage" runat="server" />  %
            </div>
        </div>
        <div class="div-tr">
            <div class="div-td">硬碟使用率：</div>
            <div class="div-td">
                <input max="90" min="20" id="input_hdusage" runat="server" />  %
            </div>
        </div>
    </div>
    <asp:Button ID="ButtonUpdate" runat="server" Text="更新" OnClick="ButtonUpdate_Click"  />
</asp:Content>

