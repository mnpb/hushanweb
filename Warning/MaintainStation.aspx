﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainStation.aspx.cs" Inherits="Warning_MaintainStation" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/Warning/UserControls/MaintainStation.ascx" TagPrefix="uc1" TagName="MaintainStation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainStation runat="server" ID="MaintainStationUserControl" OnCompleted="MaintainStationUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="新增測站" CssClass="buttonStyle" /><br />
            <br />
            <asp:GridView ID="StationBaseGridView" runat="server" AutoGenerateColumns="False" Width="100%"
                DataKeyNames="StationID" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="StationBaseGridView_PageIndexChanging" OnRowEditing="StationBaseGridView_RowEditing" OnRowDeleting="StationBaseGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站碼">
                        <ItemTemplate><%# Eval("StationID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站名">
                        <ItemTemplate><%# Eval("StationName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="經度">
                        <ItemTemplate><%# Eval("Longitude") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="緯度">
                        <ItemTemplate><%# Eval("Latitude") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
