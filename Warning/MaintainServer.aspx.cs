﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Warning_MaintainAlertStation : SuperPage
{

    AlertServerDAL alertServer = new AlertServerDAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        input_cputemp.Attributes["type"] = "number";
        input_hdusage.Attributes["type"] = "number";
        input_memusage.Attributes["type"] = "number";
        if (!IsPostBack)
        {
            AlertServerModel model = new AlertServerModel();
            model = alertServer.Get();
            input_cputemp.Value = model.CPUTemp.ToString();
            input_hdusage.Value = model.HDUsage.ToString();
            input_memusage.Value = model.MemUsage.ToString();
        }
    }

    protected void ButtonUpdate_Click(object sender, EventArgs e)
    {
        short cputemp = short.Parse(input_cputemp.Value);
        short hdusage = short.Parse(input_hdusage.Value);
        short memusage = short.Parse(input_memusage.Value);
        bool ans = alertServer.Update(cputemp, memusage, hdusage);
        if (ans)
        {
            ScriptManager.RegisterClientScriptBlock(ButtonUpdate, ButtonUpdate.GetType(), "", "alert('更新成功');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(ButtonUpdate, ButtonUpdate.GetType(), "", "alert('更新失敗');", true);
        }
    }
}