﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WaterPipe_MaintainTenMinute : SuperPage
{
    #region Properties

    string StationID
    {
        get
        {
            try
            {
                return StationDropDownList.SelectedItem.Value;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
    string StationName
    {
        get
        {
            try
            {
                return StationDropDownList.SelectedItem.Text;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
    DateTime BeginTime
    {
        get
        {
            return TwoDatesSelectorUserControl.BeginTime;
        }
    }
    DateTime EndTime
    {
        get
        {
            return TwoDatesSelectorUserControl.EndTime;
        }
    }

    dynamic TenMinuteInfos
    {
        get
        {
            string stationName = StationName;
            return (from info in WaterPipeController.Instance.GetTenMinuteInfos(StationID, BeginTime, EndTime)
                    select new
                    {
                        StationID = info.StationID,
                        StationName = stationName,
                        InfoTime = info.InfoTime,
                        Flow = info.Flow,
                    }).ToList();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillStations();
            ShowTenMinuteInfos(0);
        }
    }
    private void FillStations()
    {
        try
        {
            StationDropDownList.DataSource = WaterPipeController.Instance.GetAllStations();
            StationDropDownList.DataTextField = "StationName";
            StationDropDownList.DataValueField = "StationID";
            StationDropDownList.DataBind();
        }
        catch
        {
        }
    }
    private void ShowTenMinuteInfos(int pageIndex)
    {
        InfoGridView.DataSource = TenMinuteInfos;
        InfoGridView.PageIndex = pageIndex;
        InfoGridView.DataBind();
    }

    ///// <summary>
    ///// 顯示「新增基本資料介面」
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void AddButton_Click(object sender, EventArgs e)
    //{
    //    MaintainTenMinuteUserControl.StationID = string.Empty;
    //    Popup.Title = "水位流量資料";
    //    Popup.Show();
    //}

    #region GridView Events
    protected void InfoGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowTenMinuteInfos(e.NewPageIndex);
    }

    protected void InfoGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string stationID = InfoGridView.DataKeys[e.NewEditIndex].Values["StationID"].ToString();
        DateTime infoTime = Convert.ToDateTime(InfoGridView.DataKeys[e.NewEditIndex].Values["InfoTime"].ToString());
        MaintainTenMinuteUserControl.StationID = stationID;
        MaintainTenMinuteUserControl.StationName = StationName;
        MaintainTenMinuteUserControl.InfoTime = infoTime;
        Popup.Title = "流量資料";
        Popup.Show();
    }

    protected void InfoGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string stationID = InfoGridView.DataKeys[e.RowIndex].Values["StationID"].ToString();
            DateTime infoTime = Convert.ToDateTime(InfoGridView.DataKeys[e.RowIndex].Values["InfoTime"].ToString());
            if (!WaterPipeController.Instance.DeleteTenMinuteInfo(stationID, infoTime))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            InfoGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowTenMinuteInfos(InfoGridView.PageIndex);
        }
    }

    #endregion

    protected void MaintainTenMinuteUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowTenMinuteInfos(InfoGridView.PageIndex);
    }

    protected void QueryButton_Click(object sender, EventArgs e)
    {
        ShowTenMinuteInfos(0);
    }
}