﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WaterPipe_UserControls_MaintainTenMinute : System.Web.UI.UserControl
{
    #region Properties

    private string stationID;
    public string StationID
    {
        get
        {
            return stationID;
        }
        set
        {
            stationID = value;
        }
    }

    private string stationName;
    public string StationName
    {
        get
        {
            return stationName;
        }
        set
        {
            stationName = value;
        }
    }

    private DateTime infoTime;
    public DateTime InfoTime
    {
        get
        {
            return infoTime;
        }
        set
        {
            infoTime = value;
            ShowTenMinuteInfo(TenMinuteInfo);
        }
    }

    private dynamic TenMinuteInfo
    {
        get
        {
            return (from info in WaterPipeController.Instance.GetTenMinuteInfos(StationID, InfoTime, InfoTime)
                    select info).FirstOrDefault();
        }
    }

    #endregion

    #region Methods

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    private void EmptyAllControls()
    {
        foreach (Control c in this.Controls)
        {
            if (c is TextBox)
            {
                TextBox t = c as TextBox;
                t.Text = string.Empty;
            }
            else if (c is DropDownList)
            {
                DropDownList drp = c as DropDownList;
                drp.SelectedIndex = 0;
            }
        }
        StationIDTextBox.Enabled = true;
    }

    /// <summary>
    /// 顯示資料
    /// </summary>
    /// <param name="tenMinuteInfo"></param>
    private void ShowTenMinuteInfo(dynamic tenMinuteInfo)
    {
        if (tenMinuteInfo == null)
        { // 新增
            EmptyAllControls();
            return;
        }

        try
        {
            StationIDTextBox.Text = tenMinuteInfo.StationID;
            StationIDTextBox.Enabled = false; // 不可修改
            StationNameTextBox.Text = StationName;
            StationNameTextBox.Enabled = false; // 不可修改
            InfoTimeTextBox.Text = Convert.ToDateTime(tenMinuteInfo.InfoTime).ToString("yyyy年MM月dd日HH時mm分");
            InfoTimeTextBox.Enabled = false; // 不可修改
            FlowTextBox.Text = tenMinuteInfo.Flow.ToString();
        }
        catch
        {
        }
    }

    public event EventHandler Completed;
    /// <summary>
    /// 新增/更新測站基本資料
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        try
        {
            var newTenMinuteInfo = new WaterPipeTenMinute
            {
                StationID = StationIDTextBox.Text,
                InfoTime = Convert.ToDateTime(InfoTimeTextBox.Text + "00秒"),
                Flow = NumericUtility.ToDecimal(FlowTextBox.Text),
            };
            if (!WaterPipeController.Instance.AddTenMinuteInfo(newTenMinuteInfo))
            {
                MessageLabel.Text = "新增/更新失敗!";
                MessageLabel.Visible = true;
            }
            else
            { // 成功
                MessageLabel.Text = string.Empty;
                MessageLabel.Visible = false;
                if (Completed != null)
                {
                    Completed(this, null);
                }
            }
        }
        catch (Exception ex)
        {
            MessageLabel.Text = "新增/更新失敗 : " + ex.Message;
            MessageLabel.Visible = true;
        }
    }

    #endregion
}