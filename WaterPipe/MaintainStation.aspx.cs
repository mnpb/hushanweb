﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WaterPipe_MaintainStation : System.Web.UI.Page
{
    #region Properties

    string FilterStationName
    {
        get
        {
            return SearchStationUserControl.FilterStationName;
        }
    }

    List<WaterPipeStationBase> RainStationBaseInfos
    {
        get
        {
            var allInfos = WaterPipeController.Instance.GetAllStations();
            if (string.IsNullOrEmpty(FilterStationName))
            {
                return allInfos;
            }
            else
            {
                return (from info in allInfos
                        where info.StationName.Contains(FilterStationName)
                        select info).ToList();
            }
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowStationBaseInfos(0);
        }
    }
    private void ShowStationBaseInfos(int pageIndex)
    {
        StationBaseGridView.DataSource = RainStationBaseInfos;
        StationBaseGridView.PageIndex = pageIndex;
        StationBaseGridView.DataBind();
    }

    /// <summary>
    /// 顯示「新增基本資料介面」
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        MaintainStationUserControl.StationID = string.Empty;
        Popup.Title = "原水管站基本資料";
        Popup.Show();
    }

    #region GridView Events
    protected void StationBaseGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ShowStationBaseInfos(e.NewPageIndex);
    }

    protected void StationBaseGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string stationID = StationBaseGridView.DataKeys[e.NewEditIndex].Values["StationID"].ToString();
        MaintainStationUserControl.StationID = stationID;
        Popup.Title = "原水管站基本資料";
        Popup.Show();
    }

    protected void StationBaseGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string stationID = StationBaseGridView.DataKeys[e.RowIndex].Values["StationID"].ToString();
            if (!WaterPipeController.Instance.DeleteStation(stationID))
            {
                IISI.Util.WebClientMessage.ShowMsg(this.UpdatePanel1, "刪除失敗!");
            }
        }
        finally
        {
            StationBaseGridView.EditIndex = -1; // 設定-1讓GridView離開編輯狀態
            ShowStationBaseInfos(StationBaseGridView.PageIndex);
        }
    }

    #endregion

    protected void SearchStationUserControl_FilterStation(object sender, EventArgs e)
    {
        ShowStationBaseInfos(0);
    }
    protected void SearchStationUserControl_ClearFilter(object sender, EventArgs e)
    {
        ShowStationBaseInfos(0);
    }
    protected void MaintainStationUserControl_Completed(object sender, EventArgs e)
    {
        Popup.Hide();
        ShowStationBaseInfos(StationBaseGridView.PageIndex);
    }

    protected string GetBasinName(object basinID)
    {
        return BasinController.Instance.GetBasinName(basinID.ToString());
    }
    protected string GetOrganizationName(object organizationID)
    {
        return OrganizationController.Instance.GetOrganizationName(organizationID.ToString());
    }
}