﻿<%@ Page Title="流量資料維護" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainTenMinute.aspx.cs" Inherits="WaterPipe_MaintainTenMinute" %>

<%@ Register Src="~/WebUserControlers/Popup.ascx" TagPrefix="uc1" TagName="Popup" %>
<%@ Register Src="~/WaterPipe/UserControls/MaintainTenMinute.ascx" TagPrefix="uc1" TagName="MaintainTenMinute" %>
<%@ Register Src="~/WebUserControlers/DateTimes/TwoDatesSelector.ascx" TagPrefix="uc1" TagName="TwoDatesSelector" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <link href="../css/fancytable.css" rel="stylesheet" />
    <link href="../css/ButtonStyle.css" rel="stylesheet" />
    <link href="../css/TextBoxStyle.css" rel="stylesheet" />
    <link href="../css/SelectStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
            <uc1:Popup runat="server" ID="Popup">
                <ContentTemplate>
                    <uc1:MaintainTenMinute runat="server" ID="MaintainTenMinuteUserControl" OnCompleted="MaintainTenMinuteUserControl_Completed" />
                </ContentTemplate>
            </uc1:Popup>
            <uc1:TwoDatesSelector runat="server" id="TwoDatesSelectorUserControl" />&nbsp;
             <div class="selectStyle">   <asp:DropDownList ID="StationDropDownList" runat="server" CssClass="selectStyle-select"></asp:DropDownList></div>
                  &nbsp;<asp:Button ID="QueryButton" runat="server" Text="查詢" CssClass="buttonStyle" OnClick="QueryButton_Click" />
            <asp:GridView ID="InfoGridView" runat="server" AutoGenerateColumns="False" Width="600px"
                DataKeyNames="StationID,InfoTime" EmptyDataText="沒有任何資料" AllowPaging="true" PageSize="10"
                CssClass="fancytable" OnPageIndexChanging="InfoGridView_PageIndexChanging" OnRowEditing="InfoGridView_RowEditing" OnRowDeleting="InfoGridView_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="編輯">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="刪除" OnClientClick="javascript:return confirm('確定要刪除此筆紀錄?');">  
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站碼">
                        <ItemTemplate><%# Eval("StationID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="站名">
                        <ItemTemplate><%# Eval("StationName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="資料時間">
                        <ItemTemplate><%# Eval("InfoTime", "{0:yyyy年MM月dd日HH時mm分}") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="流量">
                        <ItemTemplate><%# Eval("Flow") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>      
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

