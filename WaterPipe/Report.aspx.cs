﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WaterPipe_DayReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        object type = Request.QueryString["Type"];
        if (type != null)
        {
            iframeContainer.Src = string.Format("{0}Report.html", type.ToString());
        }
    }
}